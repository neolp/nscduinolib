
#ifndef __DMA__H__
#define __DMA__H__

#include <stdbool.h>
#include <stdint.h>

/* DMA->CCR_PL */
#define DMA_CCR_PL_LOW		0
#define DMA_CCR_PL_MED		1
#define DMA_CCR_PL_HIGH		2
#define DMA_CCR_PL_VHIGH	3

/* DMA->CCR_MSIZE, DMA->CCR_PSIZE */
#define DMA_CCR_SIZE_8BIT	0
#define DMA_CCR_SIZE_16BIT	1
#define DMA_CCR_SIZE_32BIT	2



/* priority limits */
#define IPR_HIGHEST			0
#define IPR_LOWEST			255

/* define IPR_COMMON, if common priority value is needed */
#if !defined(IPR_COMMON)
#define __IPR_VAL(pr)			(pr)
#else
#define __IPR_VAL(pr)			IPR_COMMON
#endif

#define IPR_MS_TIMER			__IPR_VAL(2)
#define IPR_ADC_DMA			__IPR_VAL(4)

extern void dma_init(void);

extern __weak void dma_irq_callback(void);

extern "C" { void DMA1_Channel1_IRQHandler(void);}; 

#endif /* __DMA__H__ */