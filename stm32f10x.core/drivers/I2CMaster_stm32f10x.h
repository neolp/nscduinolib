#ifndef _I2CMasterStm32F10x_H_
#define _I2CMasterStm32F10x_H_

#include <I2CMaster.h>
#include <gpio-alloc.h>
#include "stm32f10x.h"

#define I2C_DATA_DIRECTION_BIT ((uint16_t)0x0001)

class I2CMasterSTM32F10x:public I2CMaster
{
  private:
    I2C_TypeDef * I2C;
    unsigned int TransactionTimeout;
    gpio_pin SCL_pin;
    gpio_pin SDA_pin;
    bool I2CReset(void);
    unsigned int ResetCounter;
  public:
    I2CMasterSTM32F10x(I2C_TypeDef * _I2C, unsigned int msec)
    {
      TransactionTimeout = msec;
      I2C=_I2C;
      ResetCounter=0;
    }
    bool Init(int sclpin = PORTB | PIN6,int sdapin = PORTB | PIN7);
    virtual int Read(uint16_t dev_addr, uint16_t reg_addr, uint8_t *data, int data_len);
    virtual int Write(uint16_t dev_addr, uint16_t reg_addr, const uint8_t *data, int data_len);
    
};






#endif /* _I2CMasterStm32F10x_H_ */

