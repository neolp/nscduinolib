
//#include "core.h"
#include "stm32f10x.h"
#include "adc.h"

extern uint8_t const adc_ch[ADC_CH_MAX];
/*
uint8_t const adc_ch[ADC_CH_MAX] = // moved to adc_def.cpp
{
	[ADC_CH_E1] = 0,
	[ADC_CH_E2] = 1,
	[ADC_CH_1] = 2,
	[ADC_CH_2] = 3,
	[ADC_CH_3] = 4,
	[ADC_CH_4] = 5,
	[ADC_CH_5] = 6,
	[ADC_CH_6] = 7,
	[ADC_CH_7] = 8,
	[ADC_CH_8] = 9,
};
*/
uint16_t volatile adc_val[ADC_CH_MAX];


/* ADC->SMPR */
#define ADC_SMPR_1C5			0x00
#define ADC_SMPR_7C5			0x01
#define ADC_SMPR_13C5			0x02
#define ADC_SMPR_28C5			0x03
#define ADC_SMPR_41C5			0x04
#define ADC_SMPR_55C5			0x05
#define ADC_SMPR_71C5			0x06
#define ADC_SMPR_239C5			0x07

#define ADC_SMPR_VAL			ADC_SMPR_239C5
#define adc_smpr_pos(n)			((n) * 3)
#define adc_sqr_pos(n)			((n) * 5)

void adc_init(void)
{
	ADC1->CR2 = ADC_CR2_ADON | ADC_CR2_CAL | ADC_CR2_CONT | ADC_CR2_DMA;
	while (ADC1->CR2 & ADC_CR2_CAL);
	ADC1->CR1 = ADC_CR1_SCAN;
	for (uint8_t i = 0; i < ADC_CH_MAX; i++)
	{
		/* sample time */
		if (i < 10)
			ADC1->SMPR2 |= ADC_SMPR_VAL << adc_smpr_pos(i);
		else if (i < 17)
			ADC1->SMPR1 |= ADC_SMPR_VAL << adc_smpr_pos(10 - i);

		/* channel */
		if (i < 6)
			ADC1->SQR3 |= adc_ch[i] << adc_sqr_pos(i);
		else if (i < 11)
			ADC1->SQR2 |= adc_ch[i] << adc_sqr_pos(6 - i);
		else if (i < 15)
			ADC1->SQR1 |= adc_ch[i] << adc_sqr_pos(11 - i);
	}
	ADC1->SQR1 |= ADC_SQR1_L & ((ADC_CH_MAX - 1) << 20);
	ADC1->CR2 |= ADC_CR2_ADON;
}
