
#ifndef __SYS_H__
#define __SYS_H__

//#include <stdint.h>
//#include <stdbool.h>

#include "stm32f10x_extend.h"
//#include "core_cm3.h"

#define IWDG_MAGIC_UNLOCK		0x5555
#define IWDG_MAGIC_FOOD			0xAAAA
#define IWDG_MAGIC_START		0xCCCC

void sys_init(void);
uint32_t sys_get_osc(void);
//void wdg_init(void);
inline void wdg_reset(void)
  {
  IWDG->KR = IWDG_MAGIC_FOOD;
  }
//void wdg_reset(void);

//class sys
//  {
//public:
#define IWDG_MAGIC_UNLOCK		0x5555
#define IWDG_MAGIC_FOOD			0xAAAA
#define IWDG_MAGIC_START		0xCCCC

/*
static inline void wdg_waitfor_reload(void)
{
	uint32_t poll_cnt = 0x7FFFF;

	while (IWDG->SR & IWDG_SR_RVU && --poll_cnt);
}

static inline void wdg_waitfor_prescaler(void)
{
	uint32_t poll_cnt = 0x7FFFF;

	while (IWDG->SR & IWDG_SR_PVU && --poll_cnt);
}
*/
    #define IWDG_PRES_UP_TO_400ms   0x00 // minimum and step 0.1 ms
    #define IWDG_PRES_UP_TO_800ms   0x01 // minimum and step 0.2 ms
    #define IWDG_PRES_UP_TO_1600ms  0x02 // minimum and step 0.4 ms
    #define IWDG_PRES_UP_TO_3200ms  0x03 // minimum and step 0.8 ms
    #define IWDG_PRES_UP_TO_6400ms  0x04 // minimum and step 1.6 ms
    #define IWDG_PRES_UP_TO_13000ms 0x05 // minimum and step 3.2 ms
    #define IWDG_PRES_UP_TO_26000ms 0x06 // minimum and step 6.4 ms

//static inline void wdg_init_ms(int max_wathdog_ms)
#define wdg_init_ms(max_wathdog_ms) \
    { \
    uint32_t poll_cnt; \
      RCC->CSR |= RCC_CSR_LSION; \
      poll_cnt = 0x7FFFF; \
      while (IWDG->SR & IWDG_SR_PVU && --poll_cnt); \
      IWDG->KR = IWDG_MAGIC_UNLOCK; \
      if ((max_wathdog_ms)<40) { IWDG->PR = IWDG_PRES_UP_TO_400ms; poll_cnt = 0x7FFFF; while (IWDG->SR & IWDG_SR_RVU && --poll_cnt); IWDG->RLR=(max_wathdog_ms)*10; }  \
      else if ((max_wathdog_ms)<80) { IWDG->PR = IWDG_PRES_UP_TO_800ms; poll_cnt = 0x7FFFF; while (IWDG->SR & IWDG_SR_RVU && --poll_cnt); IWDG->RLR=(max_wathdog_ms)*5; } \
      else if ((max_wathdog_ms)<160) { IWDG->PR = IWDG_PRES_UP_TO_1600ms; poll_cnt = 0x7FFFF; while (IWDG->SR & IWDG_SR_RVU && --poll_cnt); IWDG->RLR=(max_wathdog_ms)*10/4; } \
      else if ((max_wathdog_ms)<320) { IWDG->PR = IWDG_PRES_UP_TO_3200ms; poll_cnt = 0x7FFFF; while (IWDG->SR & IWDG_SR_RVU && --poll_cnt); IWDG->RLR=(max_wathdog_ms)*10/8; } \
      else if ((max_wathdog_ms)<640) { IWDG->PR = IWDG_PRES_UP_TO_6400ms; poll_cnt = 0x7FFFF; while (IWDG->SR & IWDG_SR_RVU && --poll_cnt); IWDG->RLR=(max_wathdog_ms)*10/16; } \
      else if ((max_wathdog_ms)<1300) { IWDG->PR = IWDG_PRES_UP_TO_13000ms; poll_cnt = 0x7FFFF; while (IWDG->SR & IWDG_SR_RVU && --poll_cnt); IWDG->RLR=(max_wathdog_ms)*10/32; } \
      else if ((max_wathdog_ms)<26000) { IWDG->PR = IWDG_PRES_UP_TO_26000ms; poll_cnt = 0x7FFFF; while (IWDG->SR & IWDG_SR_RVU && --poll_cnt); IWDG->RLR=(max_wathdog_ms)*10/64; } \
      IWDG->KR = IWDG_MAGIC_FOOD; \
      IWDG->KR = IWDG_MAGIC_START; \
    }

//static inline void wdg_init_ms(int max_wathdog_ms)
#define wdg_init_us(max_wathdog_us) \
    { \
    uint32_t poll_cnt; \
      RCC->CSR |= RCC_CSR_LSION; \
      poll_cnt = 0x7FFFF; \
      while (IWDG->SR & IWDG_SR_PVU && --poll_cnt); \
      IWDG->KR = IWDG_MAGIC_UNLOCK; \
      if ((max_wathdog_ms)<400000) { IWDG->PR = IWDG_PRES_UP_TO_400ms; poll_cnt = 0x7FFFF; while (IWDG->SR & IWDG_SR_RVU && --poll_cnt); IWDG->RLR=(max_wathdog_ms)*10/1000; }  \
      else if ((max_wathdog_ms)<800000) { IWDG->PR = IWDG_PRES_UP_TO_800ms; poll_cnt = 0x7FFFF; while (IWDG->SR & IWDG_SR_RVU && --poll_cnt); IWDG->RLR=(max_wathdog_ms)*5/1000; } \
      else if ((max_wathdog_ms)<160000) { IWDG->PR = IWDG_PRES_UP_TO_1600ms; poll_cnt = 0x7FFFF; while (IWDG->SR & IWDG_SR_RVU && --poll_cnt); IWDG->RLR=(max_wathdog_ms)*10/4000; } \
      else if ((max_wathdog_ms)<320000) { IWDG->PR = IWDG_PRES_UP_TO_3200ms; poll_cnt = 0x7FFFF; while (IWDG->SR & IWDG_SR_RVU && --poll_cnt); IWDG->RLR=(max_wathdog_ms)*10/8000; } \
      else if ((max_wathdog_ms)<640000) { IWDG->PR = IWDG_PRES_UP_TO_6400ms; poll_cnt = 0x7FFFF; while (IWDG->SR & IWDG_SR_RVU && --poll_cnt); IWDG->RLR=(max_wathdog_ms)*10/16000; } \
      else if ((max_wathdog_ms)<1300000) { IWDG->PR = IWDG_PRES_UP_TO_13000ms; poll_cnt = 0x7FFFF; while (IWDG->SR & IWDG_SR_RVU && --poll_cnt); IWDG->RLR=(max_wathdog_ms)*10/32000; } \
      else if ((max_wathdog_ms)<26000000) { IWDG->PR = IWDG_PRES_UP_TO_26000ms; poll_cnt = 0x7FFFF; while (IWDG->SR & IWDG_SR_RVU && --poll_cnt); IWDG->RLR=(max_wathdog_ms)*10/64000; } \
      IWDG->KR = IWDG_MAGIC_FOOD; \
      IWDG->KR = IWDG_MAGIC_START; \
    }

//  };

#endif /* __SYS_H__ */