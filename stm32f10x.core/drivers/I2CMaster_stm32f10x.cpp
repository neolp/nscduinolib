
#include <I2CMaster_stm32f10x.h>
#include "sys.h"
#include <tim.h>

bool I2CMasterSTM32F10x::I2CReset(void)
{
  ResetCounter++;
  unsigned int StartOfTransaction = get_us();
  // I2C analog filter may provide wrong value, locking BUSY flag and preventing master mode entry (see: Errata sheet p. 2.10.7)
  if ((I2C->SR2&I2C_SR2_BUSY) !=0)                                          
    {// Do "Workaround" 
      //1. Disable the I2C peripheral by clearing the PE bit in I2Cx_CR1 register
      I2C->CR1 &= ~I2C_CR1_PE;
      //2. Configure the SCL and SDA I/Os as General Purpose Output Open-Drain, High level (Write 1 to GPIOx_ODR).
      SCL_pin.cfg(GPIO_OUT_OPENDRAIN_50MHz);
      SDA_pin.cfg(GPIO_OUT_OPENDRAIN_50MHz);
      SCL_pin.set();
      SDA_pin.set();  
      //3. Check SCL and SDA High level in GPIOx_IDR.
      while ((SCL_pin.read()==0)||(SDA_pin.read()==0)) 
      {
        if((get_us()-StartOfTransaction) >= TransactionTimeout) 
          break;
//          return false;
      }
      //4. Configure the SDA I/O as General Purpose Output Open-Drain, Low level (Write 0 to GPIOx_ODR).
      SDA_pin.reset();
      //5. Check SDA Low level in GPIOx_IDR.
      while (SDA_pin.read()!=0) 
      {
        if((get_us()-StartOfTransaction) >= TransactionTimeout) 
          break;
//          return false;
      }
      //6. Configure the SCL I/O as General Purpose Output Open-Drain, Low level (Write 0 to GPIOx_ODR).
      SCL_pin.reset();
      //7. Check SCL Low level in GPIOx_IDR.
      while (SCL_pin.read()!=0)
      {
        if((get_us()-StartOfTransaction) >= TransactionTimeout) 
          break;
//          return false;
      }
      //8. Configure the SCL I/O as General Purpose Output Open-Drain, High level (Write 1 to GPIOx_ODR).
      SCL_pin.set();
      //9. Check SCL High level in GPIOx_IDR.
      while (SCL_pin.read()==0)
      {
        if((get_us()-StartOfTransaction) >= TransactionTimeout) 
          break;
//          return false;
      }
      //10. Configure the SDA I/O as General Purpose Output Open-Drain , High level (Write 1 to GPIOx_ODR).
      SDA_pin.set();
      //11. Check SDA High level in GPIOx_IDR.
      while (SDA_pin.read()==0)
      {
        if((get_us()-StartOfTransaction) >= TransactionTimeout) 
          break;
//          return false;
      }
      //12. Configure the SCL and SDA I/Os as Alternate function Open-Drain.
      SCL_pin.cfg(GPIO_OUT_ALT_OPENDRAIN_50MHz);
      SDA_pin.cfg(GPIO_OUT_ALT_OPENDRAIN_50MHz);
      //13. Set SWRST bit in I2Cx_CR1 register.
      //14. Clear SWRST bit in I2Cx_CR1 register
      I2C->CR1 |= I2C_CR1_SWRST;
      for (int d=0; d<2; ++d){}
      I2C->CR1 &= ~I2C_CR1_SWRST;
      //15. Enable the I2C peripheral by setting the PE bit in I2Cx_CR1 register.
      I2C->CR1 |= (I2C_CR1_ACK | I2C_CR1_PE);
    }
//reconfigure I2C
  I2C->CR1     &= ~I2C_CR1_PE;  //!< ��� ������������ ���������� ��������� ������ �� ��������� ����� PE (������ ����) (���� ����� ������ �� � ��� ��������) 
  I2C->CR1 |= I2C_CR1_SWRST; // ��������� ����� I2C
  for (int d=0; d<2; ++d){}
  I2C->CR1 &= ~I2C_CR1_SWRST;
  I2C->CR2     &= ~I2C_CR2_FREQ; //!< �������� ������� ���� ���������� 
// TODO freq detection
  I2C->CR2     |=  I2C_CR2_FREQ_3 | I2C_CR2_FREQ_4; //!< ������� �������� ������� PCLK1 ((I2C_CR2_FREQ_3 | I2C_CR2_FREQ_4)=24 ���)) ���������� ��������� � ������� (I2C_CR2) 
//  I2C->CCR      =  I2C_CCR_CCR & 119;//100kHz
  I2C->CCR      =  I2C_CCR_CCR & 0x0003;  
  I2C->CCR     |= (I2C_CCR_FS | I2C_CCR_DUTY);  //!< fast �����, 400 KHz duty cycle (1<<15)|(1<<14) 
  I2C->TRISE   =  0x0A/*I2C_TRISE_TRISE*/;      //!< ������������ ����� �������� � �������/����������� ������ (����� Master) 
  I2C->CR1     &= ~I2C_CR1_SMBUS;               //!< �������� �������� I2C(SMBUS ��������)                               
  I2C->CR1     |= (I2C_CR1_ACK | I2C_CR1_PE);   //!< �������� I2C ��������� (�.�. ������) 
  while (!(I2C->CR1&I2C_CR1_PE))//!< ������� ���������
  {
    if((get_us()-StartOfTransaction) >= TransactionTimeout) 
      return false;
  }
  
  return true;

}

bool I2CMasterSTM32F10x::Init(int sclpin,int sdapin) {
      
  if (I2C==I2C1) {
    __disable_irq();
    RCC->APB1ENR  |=  RCC_APB1ENR_I2C1EN;                                    /*!< �������� ������������ i2c1 */ 
    RCC->APB2ENR  |=  RCC_APB2ENR_IOPBEN | RCC_APB2ENR_AFIOEN;               /*!< �������� ������������ ����� B � ������ ��� �������������� ������� */  
    __enable_irq();

    if ( sclpin == (PORTB | PIN6) && sdapin == (PORTB | PIN7)) {
      SCL_pin.acquire(PORTB | PIN6,GPIO_OUT_ALT_OPENDRAIN_50MHz); // ��������� ������ SCL
      SDA_pin.acquire(PORTB | PIN7,GPIO_OUT_ALT_OPENDRAIN_50MHz); // ��������� ������ SDA
      AFIO->MAPR &= ~AFIO_MAPR_I2C1_REMAP;
    } else if ( sclpin == (PORTB | PIN8) && sdapin == (PORTB | PIN9)) {
      SCL_pin.acquire(PORTB | PIN8,GPIO_OUT_ALT_OPENDRAIN_50MHz); // ��������� ������ SCL
      SDA_pin.acquire(PORTB | PIN9,GPIO_OUT_ALT_OPENDRAIN_50MHz); // ��������� ������ SDA
      AFIO->MAPR |= AFIO_MAPR_I2C1_REMAP;
    }

    RCC->APB1RSTR |=  RCC_APB1RSTR_I2C1RST;                                  /*!< ����� I2C ��������� */
    RCC->APB1RSTR &= ~RCC_APB1RSTR_I2C1RST;                                  /*!< ������� I2C1 �� ��������� ������ */
  } else if (I2C==I2C2) {
#ifdef RCC_APB1ENR_I2C2EN
    __disable_irq();
    RCC->APB1ENR  |=  RCC_APB1ENR_I2C2EN;                                    /*!< �������� ������������ i2c2 */ 
    RCC->APB2ENR  |=  RCC_APB2ENR_IOPBEN | RCC_APB2ENR_AFIOEN;               /*!< �������� ������������ ����� B � ������ ��� �������������� ������� */  
    __enable_irq();
#endif 
    SCL_pin.acquire(PORTB | PIN10,GPIO_OUT_ALT_OPENDRAIN_50MHz); // ��������� ������ SCL
    SDA_pin.acquire(PORTB | PIN11,GPIO_OUT_ALT_OPENDRAIN_50MHz); // ��������� ������ SDA
#ifdef RCC_APB1RSTR_I2C2RST
    RCC->APB1RSTR |=  RCC_APB1RSTR_I2C2RST;                                  /*!< ����� I2C ��������� */
    RCC->APB1RSTR &= ~RCC_APB1RSTR_I2C2RST;                                  /*!< ������� I2C2 �� ��������� ������ */
#endif
  };  
  return I2CReset();
}

inline int I2CTransmitRegAddress(I2C_TypeDef *I2C, uint8_t dev_addr, uint8_t reg_addr, unsigned int StartOfTransaction, unsigned int TransactionTimeout)
{
  while((I2C->CR2 & I2C_SR2_BUSY) == I2C_SR2_BUSY)// ���� ������������ ����������
  {
    if((get_us()-StartOfTransaction) >= TransactionTimeout) 
      return false;
  }               
  I2C->CR1 |= I2C_CR1_START;// ���������� START �� ���� 
  while (!(I2C1->SR1 & I2C_SR1_SB))//while(!((I2C->SR2 == (uint32_t)0x0003) && (I2C->SR1 == (uint32_t)0x0001 )));// ���� I2C1 EV5 --> ������������� ������� ���� /* BUSY, MSL and SB flag */
  {
    if(((I2C1->SR1 & I2C_SR1_BERR)) != 0) //NEED check BERR (See: Errata P.2.10.3)
      return false;
    if((get_us()-StartOfTransaction) >= TransactionTimeout) 
      return false;
  }
  I2C->DR = ((dev_addr << 1) & (~I2C_DATA_DIRECTION_BIT));// ���������� ����� �������� � ��� ����������� 
//  while(!((I2C->SR2 == (uint32_t)0x0007) && (I2C->SR1 == (uint32_t)0x0082 )));// ���� I2C1 EV6, ask �� �������� /* BUSY, MSL, ADDR, TXE and TRA flags */
  while(!((I2C->SR2 == (uint32_t)0x0007) && (I2C->SR1 == (uint32_t)0x0080 )))//EV8_1
  {
    if((get_us()-StartOfTransaction) >= TransactionTimeout) 
      return false;
  }
  I2C->DR = reg_addr;//write byte to i2c
  while(!((I2C->SR2 == (uint32_t)0x0007) && (I2C->SR1 == (uint32_t)0x0084 )))// ���� I2C EV8_2 --> ask �� �������� /* TRA, BUSY, MSL, TXE and BTF flags */
  {
    if((get_us()-StartOfTransaction) >= TransactionTimeout) 
      return false;
  }
  return true;
}

inline int I2CTransmitData(I2C_TypeDef *I2C, const uint8_t *data, int data_len, unsigned int StartOfTransaction, unsigned int TransactionTimeout)
{
  for (int i = 0; i < data_len; i++) 
  {
    I2C->DR = data[i];//write byte to i2c
    while(!((I2C->SR2 == (uint32_t)0x0007) && (I2C->SR1 == (uint32_t)0x0084 )))// ���� I2C EV8_2 --> ask �� �������� /* TRA, BUSY, MSL, TXE and BTF flags */
    {
      if((get_us()-StartOfTransaction) >= TransactionTimeout) 
        return false;
    }
  };
  return true;
}

inline bool I2CReceiveData(I2C_TypeDef *I2C, uint8_t dev_addr, uint8_t *data, int data_len, unsigned int StartOfTransaction, unsigned int TransactionTimeout)
{
  while((I2C->CR2 & I2C_SR2_BUSY) == I2C_SR2_BUSY)// ���� ������������ ����������
  {
    if((get_us()-StartOfTransaction) >= TransactionTimeout) 
      return false;
  }
  I2C->CR1 |= I2C_CR1_START;// ���������� START �� ���� 
  while (!(I2C1->SR1 & I2C_SR1_SB))//  while(!((I2C->SR2 == (uint32_t)0x0003) && (I2C->SR1 == (uint32_t)0x0001 )));// ���� I2C1 EV5 --> ������������� ������� ���� /* BUSY, MSL and SB flag */
  {
    if(((I2C1->SR1 & I2C_SR1_BERR)) != 0) //NEED check BERR (See: Errata P.2.10.3)
      return false;
    if((get_us()-StartOfTransaction) >= TransactionTimeout) 
      return false;
  }  
  I2C->DR = ((dev_addr << 1) | (I2C_DATA_DIRECTION_BIT));// ���������� ����� �������� � ��� ����������� 
  while(!((I2C->SR2 == (uint32_t)0x0003) && (I2C->SR1 == (uint32_t)0x0002 ))) // ���� I2C1 EV6, ask �� �������� /* BUSY, MSL and ADDR flags */
    {
    if((get_us()-StartOfTransaction) >= TransactionTimeout) 
      return false;
  }
  I2C->CR1 |= I2C_CR1_ACK;// ��������� ask ����� ������
  for (int i = 0; i < data_len-1; i++) 
  {
    while(!((I2C->SR2 == (uint32_t)0x0003) && (I2C->SR1 == (uint32_t)0x0040 )))// ���� ���� ������� �������� ���� /* BUSY, MSL and RXNE flags */
    {
      if((get_us()-StartOfTransaction) >= TransactionTimeout) 
        return false;
    }
    data[i] = (uint8_t)(I2C->DR);//read byte from I2C with ACK generation 
  };
  if (data_len > 0) 
  {//reading last byte
    I2C->CR1 &= ~I2C_CR1_ACK; //��������� ask ����� ������
    while(!((I2C->SR2 == (uint32_t)0x0003) && (I2C->SR1 == (uint32_t)0x0040 )))// ���� ���� ������� �������� ���� /* BUSY, MSL and RXNE flags */
    {
      if((get_us()-StartOfTransaction) >= TransactionTimeout) 
        return false;
    }
    data[data_len-1] = (uint8_t)(I2C->DR);//read byte from I2C without ACK generation
  } else {
    return false;//something go wrong
  }
  return true;
}

inline bool I2CStop(I2C_TypeDef *I2C)
{
  I2C->CR1 |= I2C_CR1_STOP;//generate stop conditions when is last operation
  return true;
}

int I2CMasterSTM32F10x::Write(uint16_t dev_addr, uint16_t reg_addr, const uint8_t *data, int data_len)
{
  unsigned int StartOfTransaction = get_us();
  bool ret = I2CTransmitRegAddress(I2C, dev_addr, reg_addr, StartOfTransaction, TransactionTimeout);
  if (!ret) {
    I2CReset(); 
    return -1;
  }
  ret = I2CTransmitData(I2C, data, data_len, StartOfTransaction, TransactionTimeout);
  if (!ret) {
    I2CReset(); 
    return -1;
  }
  ret = I2CStop(I2C);
  return data_len;
}
int I2CMasterSTM32F10x::Read(uint16_t dev_addr, uint16_t reg_addr, uint8_t *data, int data_len)
{
  unsigned int StartOfTransaction = get_us();
  bool ret = I2CTransmitRegAddress(I2C, dev_addr, reg_addr, StartOfTransaction, TransactionTimeout);
  if (!ret) {
    I2CReset(); 
    return -1;
  }
  ret = I2CReceiveData(I2C, dev_addr, data, data_len, StartOfTransaction, TransactionTimeout);
  if (!ret) {
    I2CReset(); 
    return -1;
  }  
  ret = I2CStop(I2C);
  return data_len;
}
//int I2CMasterSTM32F10x::Write(uint16_t dev_addr, uint16_t reg_addr, const uint8_t *data, int data_len)
//{
//  I2CTransmitRegAddress(I2C, dev_addr, reg_addr);
//  I2CTransmitData(I2C, data, data_len);
//  I2CStop(I2C);
//  return 0;
//}
//int I2CMasterSTM32F10x::Read(uint16_t dev_addr, uint16_t reg_addr, uint8_t *data, int data_len)
//{
//  I2CTransmitRegAddress(I2C, dev_addr, reg_addr);
//  I2CReceiveData(I2C, dev_addr, data, data_len);  
//  I2CStop(I2C);  
//  return 0;
//}