
#ifndef __DAC_H__
#define __DAC_H__

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

extern void dac_init(void);
extern void dac_set(int val);
extern int dac_get(void);

#endif /* __DAC_H__ */