
#ifndef __TIM_H__
#define __TIM_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "gpio-alloc.h"

//#include "include/limb_driver.h"

extern unsigned int get_ms();
extern unsigned long long getlong_ms();
extern unsigned int get_us();
extern unsigned long long getlong_us();
extern unsigned int get_ms_di();//old version vith irq disabling
extern unsigned long long getlong_ms_di();
extern unsigned int get_us_di();
extern unsigned long long getlong_us_di();


inline unsigned int msToTicks(unsigned int ms)
  {
  return ms*24000;
  }
inline unsigned int usToTicks(unsigned int us)
  {
  return us*24;
  }

enum pwm_ch
{
	PWM_CH1 = 0,
	PWM_CH2,
};

extern void tim_init(void);
extern void pwm_init(void);

extern void pwm_set(enum pwm_ch ch, uint16_t val);

//class TIM

#define CFG_AS_ENCODER 0
#define CFG_AS_TACHOMETER 1
class T2Encoder
  {
  gpio_pin A;
  gpio_pin B;
//  unsigned short lastenc;
//  int lastpos;
  public:
  void init(int pinA = PORTA | PIN0, int pinB =  PORTA | PIN1);

  unsigned short get()
    {
    return TIM2->CNT;
    }
  void reverse(){
    TIM2->CR1 |= TIM_CR1_DIR;
  }
  void forward(){
    TIM2->CR1 &= ~TIM_CR1_DIR;
  }
  
  void config(int v){
    if (v==CFG_AS_TACHOMETER){
      TIM2->SMCR = TIM_SMCR_ECE; // count A
      TIM2->SMCR = TIM_SMCR_TS_2 | TIM_SMCR_TS_0 |   TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1 | TIM_SMCR_SMS_2; // count T1
      A.recfg(GPIO_IN_PULLUP);
      B.recfg(GPIO_IN_PULLUP);
    } else {
      TIM2->SMCR = TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1; // count both
      A.recfg(GPIO_IN_FLOATING);
      B.recfg(GPIO_IN_FLOATING);
    }
  }
  
  
/*  int pos()
    {
    unsigned short enc=TIM2->CNT;
    lastpos+=(signed short)(lastenc-enc);
    lastenc=enc;
    return lastpos;
    }*/
  };
class T3Encoder
  {
  gpio_pin A;
  gpio_pin B;
//  unsigned short lastenc;
//  int lastpos;
  public:
  void init(int pinA = PORTB | PIN4, int pinB = PORTB | PIN5);
  unsigned short get()
    {
    return TIM3->CNT;
    }
  void reverse(){
    TIM3->CR1 |= TIM_CR1_DIR;
  }
  void forward(){
    TIM3->CR1 &= ~TIM_CR1_DIR;
  }
  void config(int v){
    if (v==CFG_AS_TACHOMETER){
      TIM3->SMCR = TIM_SMCR_TS_2 | TIM_SMCR_TS_1 | TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1 | TIM_SMCR_SMS_2; // count T2
      A.recfg(GPIO_IN_PULLUP);
      B.recfg(GPIO_IN_PULLUP);
    } else {
      TIM3->SMCR = TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1; // count both
      A.recfg(GPIO_IN_FLOATING);
      B.recfg(GPIO_IN_FLOATING);
    }
  }
/*  int pos()
    {
    unsigned short enc=TIM3->CNT;
    lastpos+=(signed short)(lastenc-enc);
    lastenc=enc;
    return lastpos;
    }*/
  };

typedef void (*IRQHandler)(void * param);

int SetIRQHandler(IRQn irq, IRQHandler handler, void* param);
int ResetIRQHandler(IRQn irq, IRQHandler handler);
IRQHandler GetIRQHandler(IRQn irq);
void* GetIRQHandlerParam(IRQn irq);


#endif /* __TIM_H__ */