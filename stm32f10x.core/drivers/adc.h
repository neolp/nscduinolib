
#ifndef __ADC_H__
#define __ADC_H__

//#include <stddef.h>
//#include <stdbool.h>
#include <stdint.h>
#include "stm32f10x.h"


//#include "adc_def.h"
/*
enum adc_chnl // moved to adc_def.h
{
	ADC_CH_E1,
	ADC_CH_E2,
	ADC_CH_1,
	ADC_CH_2,
	ADC_CH_3,
	ADC_CH_4,
	ADC_CH_5,
	ADC_CH_6,
	ADC_CH_7,
	ADC_CH_8,
	ADC_CH_MAX,
};
*/
//extern uint16_t volatile adc_val[ADC_CH_MAX];

//extern void adc_init(void);

/* ADC->SMPR */
#define ADC_SMPR_1C5			0x00
#define ADC_SMPR_7C5			0x01
#define ADC_SMPR_13C5			0x02
#define ADC_SMPR_28C5			0x03
#define ADC_SMPR_41C5			0x04
#define ADC_SMPR_55C5			0x05
#define ADC_SMPR_71C5			0x06
#define ADC_SMPR_239C5			0x07

//#define ADC_SMPR_VAL			ADC_SMPR_13C5 //ADC_SMPR_239C5
#define ADC_SMPR_VAL			ADC_SMPR_239C5
#define adc_smpr_pos(n)			((n) * 3)
#define adc_sqr_pos(n)			((n) * 5)

template<int channels> 
class ADC {
  uint8_t ADC_CH_INTTEMP;
  uint8_t ADC_CH_INTREF;
  //uint8_t ADC_CH_SUPPLY;
  public:
  uint16_t volatile adc_val[channels];
  int volatile temperature;
//  int volatile supplyvoltage;
  int volatile internalvoltage;
  int volatile externalvoltage;

  void begin( const uint8_t adc_ch[channels], uint8_t ext_volt = channels  ) {
    ADC1->CR2 = ADC_CR2_ADON | ADC_CR2_CAL | ADC_CR2_CONT | ADC_CR2_DMA | ADC_CR2_TSVREFE;
    while (ADC1->CR2 & ADC_CR2_CAL);
    ADC1->CR1 = ADC_CR1_SCAN;
    
    RCC->CFGR = (RCC->CFGR & ~RCC_CFGR_ADCPRE) | (3<<14); // 0 :PLCK2 divided by 2, 1: PLCK2 divided by 4, 2: PLCK2 divided by 6, 3: PLCK2 divided by 8
   
    
//    ADC_CH_SUPPLY = ext_volt;
    ADC_CH_INTTEMP = channels;
    ADC_CH_INTREF = channels;
    for (uint8_t i = 0; i < channels; i++)
    {
            /* sample time */
            if (i < 10)
                    ADC1->SMPR2 |= ADC_SMPR_VAL << adc_smpr_pos(i);
            else if (i < 17)
                    ADC1->SMPR1 |= ADC_SMPR_VAL << adc_smpr_pos(i - 10);

            /* channel */
            if (i < 6)
                    ADC1->SQR3 |= ((unsigned int)adc_ch[i]) << adc_sqr_pos(i);
            else if (i < 12)
                    ADC1->SQR2 |= ((unsigned int)adc_ch[i]) << adc_sqr_pos(i - 6 );
            else if (i < 16)
                    ADC1->SQR1 |= ((unsigned int)adc_ch[i]) << adc_sqr_pos(i - 12);
            
      if (adc_ch[i] == 16 ) 
        ADC_CH_INTTEMP = i;
      if (adc_ch[i] == 17 ) 
        ADC_CH_INTREF = i;
    }
    ADC1->SQR1 |= ADC_SQR1_L & ((channels - 1) << 20);
    ADC1->CR2 |= ADC_CR2_ADON;
    
    dma_init();
}
/* DMA->CCR_PL */
#define DMA_CCR_PL_LOW		0
#define DMA_CCR_PL_MED		1
#define DMA_CCR_PL_HIGH		2
#define DMA_CCR_PL_VHIGH	3

/* DMA->CCR_MSIZE, DMA->CCR_PSIZE */
#define DMA_CCR_SIZE_8BIT	0
#define DMA_CCR_SIZE_16BIT	1
#define DMA_CCR_SIZE_32BIT	2

void dma_init(void)
{
	DMA1_Channel1->CNDTR = channels;
	DMA1_Channel1->CPAR = (uint32_t)&ADC1->DR;
	DMA1_Channel1->CMAR = (uint32_t)&adc_val;
	DMA1_Channel1->CCR = (DMA_CCR1_PL & (DMA_CCR_PL_MED << 12))
			| (DMA_CCR1_MSIZE & (DMA_CCR_SIZE_16BIT << 10))
			| (DMA_CCR1_PSIZE & (DMA_CCR_SIZE_16BIT << 8))
			| DMA_CCR1_MINC | DMA_CCR1_CIRC | DMA_CCR1_TEIE
			| DMA_CCR1_TCIE | DMA_CCR1_EN;
	NVIC_EnableIRQ(DMA1_Channel1_IRQn);
}
int dma_ch1_err;
int dma_compl_cnt;
void IRQHandler(void){

	if (DMA1->ISR & DMA_ISR_TEIF1)
		dma_ch1_err++;
	DMA1->IFCR |= DMA_IFCR_CTEIF1 | DMA_IFCR_CHTIF1 | DMA_IFCR_CTCIF1 | DMA_IFCR_CGIF1;
	dma_compl_cnt++;

#define ADCv25 (1750)
  //Temperature (in �C) = {(V25 - VSENSE) / Avg_Slope} + 25.
  if (ADC_CH_INTTEMP < channels)  {
    if (adc_val[ADC_CH_INTTEMP] < ADCv25)
      temperature=((ADCv25 - adc_val[ADC_CH_INTTEMP])*3300/4096)*10/43 + 25;
    else
      {
      temperature=(adc_val[ADC_CH_INTTEMP]-ADCv25)*3300/4096;
      temperature=temperature*10/43;
      temperature=25-temperature;
      //temperature=25 - ((adcval[ADC_CH_INTTEMP]-1410)*3300/4096)*10/43;
      }
  }

  //ADC_CH_TEMP

  if (ADC_CH_INTREF < channels) {
    internalvoltage = adc_val[ADC_CH_INTREF] * 3300  / 4096; // should be 1200
    externalvoltage = 4096 * 1200 / adc_val[ADC_CH_INTREF]; // should be 3300
  }
}

};

extern "C" { void DMA1_Channel1_IRQHandler(void); }

#endif /* __ADC_H__ */