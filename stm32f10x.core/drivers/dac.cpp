#include "stm32f10x_extend.h"
#include "core_cm3.h"
#include "dac.h"

void dac_init(void)
{
	DAC->CR = DAC_CR_EN1;
}

void dac_set(int val)
{
	DAC->DHR12R1 = val;
}

int dac_get(void)
{
	return DAC->DHR12R1;
}