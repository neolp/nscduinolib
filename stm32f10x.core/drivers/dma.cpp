
//#include "include/core.h"
#include "stm32f10x.h"
#include "dma.h"
#include "adc.h"
//#include "include/dac.h"
//#include "include/fir.h"
//#include "include/limb_driver.h"

uint32_t volatile dma_ch1_err = 0;

void dma_init(void)
{
	DMA1_Channel1->CNDTR = ADC_CH_MAX;
	DMA1_Channel1->CPAR = (uint32_t)&ADC1->DR;
	DMA1_Channel1->CMAR = (uint32_t)&adc_val;
	DMA1_Channel1->CCR = (DMA_CCR1_PL & (DMA_CCR_PL_MED << 12))
			| (DMA_CCR1_MSIZE & (DMA_CCR_SIZE_16BIT << 10))
			| (DMA_CCR1_PSIZE & (DMA_CCR_SIZE_16BIT << 8))
			| DMA_CCR1_MINC | DMA_CCR1_CIRC | DMA_CCR1_TEIE
			| DMA_CCR1_TCIE | DMA_CCR1_EN;
	NVIC_SetPriority(DMA1_Channel1_IRQn, IPR_ADC_DMA);
	NVIC_EnableIRQ(DMA1_Channel1_IRQn);
}
/*
static uint32_t volatile dma_compl_cnt = 0;
bool dma_completed(void)
{
	static uint32_t cnt = 0;
	uint32_t complet = dma_compl_cnt;
	bool ret = complet != cnt;
	
	cnt = complet;
	return ret;
}
void DMA1_Channel1_IRQHandler(void)
{
	if (DMA1->ISR & DMA_ISR_TEIF1)
		dma_ch1_err++;
	DMA1->IFCR |= DMA_IFCR_CTEIF1 | DMA_IFCR_CHTIF1 | DMA_IFCR_CTCIF1
			| DMA_IFCR_CGIF1;
	dma_compl_cnt++;
	
	NVIC_ClearPendingIRQ(DMA1_Channel1_IRQn);
}
*/
uint32_t volatile dma_err_cnt = 0;
uint32_t volatile dma_conv_cnt = 0;

void DMA1_Channel1_IRQHandler(void)
{
	if (DMA1->ISR & DMA_ISR_TEIF1)
		dma_err_cnt++;
	else
		dma_conv_cnt++;
	DMA1->IFCR = DMA_IFCR_CTEIF1 | DMA_IFCR_CHTIF1 | DMA_IFCR_CTCIF1
			| DMA_IFCR_CGIF1;
	
	if (dma_irq_callback != NULL) dma_irq_callback();
}


