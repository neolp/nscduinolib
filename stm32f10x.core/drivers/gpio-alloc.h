
#ifndef __GPIO_ALLOCATOR_H__
#define __GPIO_ALLOCATOR_H__

#include <stdbool.h>
#include <stdint.h>
#include <stm32f10x.h>

/* simple dynamic gpio manager for drivers internal using */
class gpio_pin
  {
  GPIO_TypeDef* GPIOREG;
  unsigned char port_id;
  unsigned char pin_id;
public:
  gpio_pin():GPIOREG(0) {}
  gpio_pin(int port,int cfg):GPIOREG(0) { acquire(port, cfg); }
  ~gpio_pin() { release(); }
  int acquire(int port,int cfg);
  static void configure(GPIO_TypeDef* GPIOREG, int pin, int cfg);
  void recfg(int cfg);
  int release();
  static void deconfigure(GPIO_TypeDef* GPIOREG, int pin);
  static void set(GPIO_TypeDef* GPIOREG, int pin_id, int val);
  static int read(GPIO_TypeDef* GPIOREG, int pin_id);
  static bool isFree(int pin);
  void cfg(int cfg)
    {
    if (pin_id>7) GPIOREG->CRH=(GPIOREG->CRH & ~(0x0F << ((pin_id-8)<<2))) | ((cfg&0x0F) << ((pin_id-8)<<2));
    else          GPIOREG->CRL=(GPIOREG->CRL & ~(0x0F << ((pin_id-0)<<2))) | ((cfg&0x0F) << ((pin_id-0)<<2));
    }

  void set()
    {
    GPIOREG->BSRR=1<<pin_id; // set
    }
  bool acquired() { return GPIOREG!=0; }

  void pull_up() // enable pull up mode for pull up/down input pin
    {
    GPIOREG->BSRR=1<<pin_id; // set
    }

  void reset()
    {
    GPIOREG->BRR=1<<pin_id; // reset
    }

  void pull_down() // enable pull down mode for pull up/down input pin
    {
    GPIOREG->BRR=1<<pin_id; // reset
    }

  int read()
    {
    return (GPIOREG->IDR>>pin_id)&0x01;
    }

  bool toggle(void)
    {
    int ret=(GPIOREG->ODR>>pin_id)&0x01;
    if (ret)
      GPIOREG->BRR=1<<pin_id; // reset
    else
      GPIOREG->BSRR=1<<pin_id; //set
    return ret;
    }

  void set(int val)
    {
    if (val)
      GPIOREG->BSRR=1<<pin_id; //set
    else
      GPIOREG->BRR=1<<pin_id; // reset
    }

  };

#define GPIO_IN_ANALOG                  (0x00)
#define GPIO_IN_FLOATING                (0x04)
#define GPIO_IN_PULLDOWN                (0x08|0x00)
#define GPIO_IN_PULLUP                  (0x08|0x10)
#define GPIO_IN_reserv                  (0x0C)

#define GPIO_OUT_PUSHPULL_10MHz         (0x00|0x01)
#define GPIO_OUT_OPENDRAIN_10MHz        (0x04|0x01)
#define GPIO_OUT_ALT_PUSHPULL_10MHz     (0x08|0x01)
#define GPIO_OUT_ALT_OPENDRAIN_10MHz    (0x0C|0x01)

#define GPIO_OUT_PUSHPULL_2MHz          (0x00|0x02)
#define GPIO_OUT_OPENDRAIN_2MHz         (0x04|0x02)
#define GPIO_OUT_ALT_PUSHPULL_2MHz      (0x08|0x02)
#define GPIO_OUT_ALT_OPENDRAIN_2MHz     (0x0C|0x02)

#define GPIO_OUT_PUSHPULL_50MHz         (0x00|0x03)
#define GPIO_OUT_OPENDRAIN_50MHz        (0x04|0x03)
#define GPIO_OUT_ALT_PUSHPULL_50MHz     (0x08|0x03)
#define GPIO_OUT_ALT_OPENDRAIN_50MHz    (0x0C|0x03)

#define PORTA   0x0000
#define PORTB   0x0010
#define PORTC   0x0020
#define PORTD   0x0030
#define PORTE   0x0040
#define PORTF   0x0050
#define PORTG   0x0060


#define PIN0  0
#define PIN1  1
#define PIN2  2
#define PIN3  3
#define PIN4  4
#define PIN5  5
#define PIN6  6
#define PIN7  7
#define PIN8  8
#define PIN9  9
#define PIN10  10
#define PIN11  11
#define PIN12  12
#define PIN13  13
#define PIN14  14
#define PIN15  15

#endif /* __GPIO_ALLOCATOR_H__ */
