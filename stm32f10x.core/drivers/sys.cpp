
#include <assert.h>
#include "stm32f10x.h"
#include "stm32f10x_extend.h"
#include "sys.h"

void abort(void)
{
	__ASM volatile ("bkpt #1");
}

void sys_init(void)
{
	uint32_t const poll_times = 0xFFFF;
	uint32_t poll = poll_times;
	uint32_t sck_sel;

	RCC->APB1ENR |= RCC_APB1ENR_USART2EN | RCC_APB1ENR_DACEN
			| RCC_APB1ENR_TIM6EN;// | RCC_APB1ENR_TIM2EN | RCC_APB1ENR_TIM3EN;
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPDEN
			| RCC_APB2ENR_IOPCEN | RCC_APB2ENR_ADC1EN
			| RCC_APB2ENR_AFIOEN | RCC_APB2ENR_TIM16EN | RCC_APB2ENR_TIM15EN
			| RCC_APB2ENR_TIM17EN;
	RCC->AHBENR |= RCC_AHBENR_DMA1EN;

	RCC->CR |= RCC_CR_HSEON;
	while (--poll && !(RCC->CR & RCC_CR_HSERDY));

	if (poll)
	{
		RCC->CFGR |= RCC_CFGR_PLLSRC;
		RCC->CFGR &= ~RCC_CFGR_PLLMULL;
		RCC->CFGR2 = 0;
		RCC->CR |= RCC_CR_PLLON;
		poll = poll_times;
		while (--poll && !(RCC->CR & RCC_CR_PLLRDY));

		sck_sel = poll ? RCC_CFGR_SW_PLL : RCC_CFGR_SW_HSE;
	}
	else {

//		sck_sel = RCC_CFGR_SW_HSI;
		RCC->CFGR &= ~RCC_CFGR_PLLSRC;
		RCC->CFGR &= ~RCC_CFGR_PLLMULL;
		RCC->CFGR |= RCC_CFGR_PLLMULL6;
		RCC->CFGR2 = 0;
		RCC->CR |= RCC_CR_PLLON;
		poll = poll_times;
		while (--poll && !(RCC->CR & RCC_CR_PLLRDY));

		sck_sel = poll ? RCC_CFGR_SW_PLL : RCC_CFGR_SW_HSI;

  }
	RCC->CFGR = (RCC->CFGR & ~RCC_CFGR_SW) | sck_sel;
}

#define IWDG_MAGIC_UNLOCK		0x5555
#define IWDG_MAGIC_FOOD			0xAAAA
#define IWDG_MAGIC_START		0xCCCC

/*
static inline void wdg_waitfor_reload(void)
{
	uint32_t poll_cnt = 0x7FFFF;

	while (IWDG->SR & IWDG_SR_RVU && --poll_cnt);
}

static inline void wdg_waitfor_prescaler(void)
{
	uint32_t poll_cnt = 0x7FFFF;

	while (IWDG->SR & IWDG_SR_PVU && --poll_cnt);
}
*/
/*
void wdg_init(void)
{
	RCC->CSR |= RCC_CSR_LSION;
	// from 1,1 to 2,2 sec
	wdg_waitfor_prescaler();
	IWDG->KR = IWDG_MAGIC_UNLOCK;
#define IWDG_PRES_UP_TO_400ms   0x00 // minimum and step 0.1 ms
#define IWDG_PRES_UP_TO_800ms   0x01 // minimum and step 0.2 ms
#define IWDG_PRES_UP_TO_1600ms  0x02 // minimum and step 0.4 ms
#define IWDG_PRES_UP_TO_3200ms  0x03 // minimum and step 0.8 ms
#define IWDG_PRES_UP_TO_6400ms  0x04 // minimum and step 1.6 ms
#define IWDG_PRES_UP_TO_13000ms 0x05 // minimum and step 3.2 ms
#define IWDG_PRES_UP_TO_26000ms 0x06 // minimum and step 6.4 ms
	IWDG->PR = IWDG_PRES_UP_TO_1600ms;
	wdg_waitfor_reload();
	IWDG->KR = IWDG_MAGIC_UNLOCK;
	IWDG->RLR = 100;// 40 ms
	IWDG->KR = IWDG_MAGIC_FOOD;
	IWDG->KR = IWDG_MAGIC_START;
}
*/
uint32_t sys_get_osc(void)
{
	uint32_t sck_sel = RCC->CFGR & RCC_CFGR_SW;

	if (sck_sel == RCC_CFGR_SW_HSI)
		return 8*1000000;
	if (sck_sel == RCC_CFGR_SW_HSE)
		return 12*1000000;
	return 24*1000000;
}
