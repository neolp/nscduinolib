#include "stm32f10x.h"
//#include "core/stm32f10x_extend.h"

#include "spi.h"
//#include "include/gpio.h"
//#include "cycl_buf_skel.h"


void spi::begin(int baud, int bits)
    {
//    bool ext_osc = (RCC->CFGR & RCC_CFGR_SW) == RCC_CFGR_SW_HSE;
    int osc = sys_get_osc() >> 1;
    int cr1_br=0;
    while (osc > baud && cr1_br < 7) {
      cr1_br++;
      osc >>= 1;
    }

    //__disable_irq();
    RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
    //__enable_irq();

    SPI->CR2 = 0;
//    SPI->CR1 = SPI_CR1_MSTR | (cr1_br<<3) | SPI_CR1_SPE | SPI_CR1_SSM | SPI_CR1_SSI;
    SPI->CR1 = SPI_CR1_MSTR | (cr1_br<<3) | SPI_CR1_SPE | SPI_CR1_SSM | SPI_CR1_SSI | SPI_CR1_CPHA | (bits == 16 ? SPI_CR1_DFF : 0 );

    if (SPI==SPI1 && (AFIO->MAPR & AFIO_MAPR_SPI1_REMAP)==0)
      {
//      gpio_pin::configure(GPIOA,4,GPIO_OUT_ALT_PUSHPULL_50MHz);// SPI1 SS
      gpio_pin::configure(GPIOA,5,GPIO_OUT_ALT_PUSHPULL_50MHz);// SPI1 SCK
      gpio_pin::configure(GPIOA,6,GPIO_IN_FLOATING);// SPI1 MISO
      gpio_pin::configure(GPIOA,7,GPIO_OUT_ALT_PUSHPULL_50MHz);// SPI1 MOSI
      }
    if (SPI==SPI1 && (AFIO->MAPR & AFIO_MAPR_SPI1_REMAP)!=0)
      {
//      gpio_pin::configure(GPIOA,15,GPIO_OUT_ALT_PUSHPULL_50MHz);// SPI1 SS remap
      gpio_pin::configure(GPIOB,3,GPIO_OUT_ALT_PUSHPULL_50MHz);// SPI1 SCK remap
      gpio_pin::configure(GPIOB,4,GPIO_IN_FLOATING);// SPI1 MISO remap
      gpio_pin::configure(GPIOB,5,GPIO_OUT_ALT_PUSHPULL_50MHz);// SPI1 MOSI remap
      }

/*    if (SPI==SPI2)
      {
//      gpio_pin::configure(GPIOB,12,GPIO_OUT_ALT_PUSHPULL_50MHz);// SPI1 SS
      gpio_pin::configure(GPIOB,13,GPIO_OUT_ALT_PUSHPULL_50MHz);// SPI1 SCK
      gpio_pin::configure(GPIOB,14,GPIO_IN_FLOATING);// SPI1 MISO
      gpio_pin::configure(GPIOB,15,GPIO_OUT_ALT_PUSHPULL_50MHz);// SPI1 MOSI
      }*/


//    if (SPI==SPI1) NVIC_EnableIRQ(SPI1_IRQn);
    //else if (SPI==SPI2) NVIC_EnableIRQ(SPI2_IRQn);
    }

void spi::setBitOrder(int order)// ������������� ������� ������� ����� ������ (order):
  {
  SPI->CR1 = (SPI->CR1 & ~SPI_CR1_LSBFIRST) | (order & SPI_CR1_LSBFIRST);
  }

void spi::setClockDivider(int divider) // ������������� �������� ������ ��� SPI ������������ �������� �������. �������� �������� 2, 4, 8, 16, 32, 64 � 128. ��������������� ��������� ����� ����� ���� SPI_CLOCK_DIVn, ��� n � ��������, ��������, SPI_CLOCK_DIV32. �� ��������� �������� ����� 4 � ��� ������� �������� ������� �� �� Arduino � 16 ��� SPI ����� �������� �� ������� 4 ���.
  {
  SPI->CR1 = (SPI->CR1 & ~SPI_CR1_BR) | (divider & SPI_CR1_BR);
  }

void spi::setDataMode(int mode) // ����� ����� ������ SPI, ��������� ��������� SPI_MODE0 (�� ���������), SPI_MODE1, SPI_MODE2 � SPI_MODE3. ��� �� ����� ������ c ����������� CPOL � CPHA.
  {
  SPI->CR1 = (SPI->CR1 & ~(SPI_CR1_CPHA|SPI_CR1_CPOL)) | (mode & (SPI_CR1_CPHA|SPI_CR1_CPOL));
  }

int spi::transfer(int value,int mode)
  {
  if (cs) cs->set(); //	GPIOC->ODR |= GPIOC_RS485DE;

  while ( (SPI->SR & SPI_SR_TXE) == 0)
    ; // wait for empty transmitt buffer

  SPI->DR = value;

  while ( (SPI->SR & SPI_SR_RXNE) == 0)
    ; // wait for input buffer buffer

  int ret=SPI->DR;

  if (cs && (mode & SPI_LAST)) cs->reset();

  return ret;
	}

int spi::fast_transfer(int value)
  {
  while ( (SPI->SR & SPI_SR_TXE) == 0)
    ; // wait for empty transmitt buffer

  SPI->DR = value;

  while ( (SPI->SR & SPI_SR_RXNE) == 0)
    ; // wait for input buffer buffer

  int ret=SPI->DR;
  return ret;
	}



/*
void spi::IRQHandler(void)
  {
  }

extern "C" { void SPI1_IRQHandler(void); }
extern "C" { void SPI2_IRQHandler(void); }

*/
spi spi1(SPI1);
//spi spi2(SPI2);
/*
void SPI1_IRQHandler(void)
  {
  spi1.IRQHandler();
	NVIC_ClearPendingIRQ(SPI1_IRQn);
  }

void SPI2_IRQHandler(void)
  {
  spi2.IRQHandler();
	NVIC_ClearPendingIRQ(SPI2_IRQn);
  }
*/