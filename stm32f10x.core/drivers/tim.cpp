
//#include "include/core.h"
#include "tim.h"
//#include "adc.h"
//#include "dac.h"
#include "stm32f10x.h"
#include "stm32f10x_extend.h"
/* timer6 for 1 ms ticks */
volatile unsigned long long ticks=0;
uint32_t volatile ms = 0;



#include "gpio-alloc.h"
#include "sys.h"

void tim_init(void)
{
  RCC->APB1ENR |=RCC_APB1ENR_TIM6EN;
	bool ext_osc = (RCC->CFGR & RCC_CFGR_SW) == RCC_CFGR_SW_HSE;
	TIM6->CR1 = TIM_CR1_ARPE | TIM_CR1_CEN;
	TIM6->PSC = sys_get_osc() / 1000000 - 1;
	TIM6->ARR = 65535;
  TIM6->CNT=0;
	TIM6->EGR |= TIM_EGR_UG;
	TIM6->DIER |= TIM_DIER_UIE;
  TIM6->SR &= ~TIM_SR_UIF;
  NVIC_ClearPendingIRQ(TIM6_DAC_IRQn);
	NVIC_EnableIRQ(TIM6_DAC_IRQn);
  ticks=0;
}



extern "C" { void TIM6_DAC_IRQHandler(void); }


unsigned int get_ms()
{
  unsigned long long now;
    unsigned int t1;
  unsigned int ov1;
  unsigned int t2;
  unsigned int ov2;

  do
  {
  now = ticks;
  t1=TIM6->CNT;
  ov1=TIM6->SR;
  t2=TIM6->CNT;
  ov2=TIM6->SR;
  } while (now != ticks);
  if (t1<=t2) now+=t1+ov1*65536;
  else now+=t2+ov2*65536;
  return (now)/1000;
}

unsigned long long getlong_ms()
{
  unsigned long long now;
  unsigned int t1;
  unsigned int ov1;
  unsigned int t2;
  unsigned int ov2;
  do
  {
	  now = ticks;
	  t1=TIM6->CNT;
	  ov1=TIM6->SR;
	  t2=TIM6->CNT;
	  ov2=TIM6->SR;
  } while (now != ticks);
  if (t1<=t2) now+=t1+ov1*65536;
  else now+=t2+ov2*65536;
  return (now)/1000;
}

unsigned int get_us()
{
  unsigned long long now;
  unsigned int t1;
  unsigned int ov1;
  unsigned int t2;
  unsigned int ov2;

  do
  {
	  now = ticks;
	  t1=TIM6->CNT;
	  ov1=TIM6->SR;
	  t2=TIM6->CNT;
	  ov2=TIM6->SR;
  } while (now != ticks);
  if (t1 <= t2) now+=t1+ov1*65536;
  else now+=t2+ov2*65536;
  return (now);
}

unsigned long long getlong_us()
{
  unsigned long long now;
    unsigned int t1;
  unsigned int ov1;
  unsigned int t2;
  unsigned int ov2;

  do
  {
	  now = ticks;
	  t1=TIM6->CNT;
	  ov1=TIM6->SR;
	  t2=TIM6->CNT;
	  ov2=TIM6->SR;
  } while (now != ticks);
  if (t1<=t2) now+=t1+ov1*65536;
  else now+=t2+ov2*65536;
  return (now);
}

unsigned int get_ms_di()
  {
  __disable_irq();
  unsigned long long now=ticks;
  unsigned int t1=TIM6->CNT;
  unsigned int ov1=TIM6->SR;
  unsigned int t2=TIM6->CNT;
  unsigned int ov2=TIM6->SR;
  __enable_irq();
  if (t1<=t2) now+=t1+ov1*65536;
  else now+=t2+ov2*65536;
  return (now)/1000;
  }

unsigned long long getlong_ms_di()
  {
  __disable_irq();
  unsigned long long now=ticks;
  unsigned int t1=TIM6->CNT;
  unsigned int ov1=TIM6->SR;
  unsigned int t2=TIM6->CNT;
  unsigned int ov2=TIM6->SR;
  __enable_irq();
  if (t1<=t2) now+=t1+ov1*65536;
  else now+=t2+ov2*65536;
  return (now)/1000;
  }

unsigned int get_us_di()
  {
  __disable_irq();
  unsigned long long now=ticks;
  unsigned int t1=TIM6->CNT;
  unsigned int ov1=TIM6->SR;
  unsigned int t2=TIM6->CNT;
  unsigned int ov2=TIM6->SR;
  __enable_irq();
  if (t1<=t2) now+=t1+ov1*65536;
  else now+=t2+ov2*65536;
  return (now);
  }

unsigned long long getlong_us_di()
  {
  __disable_irq();
  unsigned long long now=ticks;
  unsigned int t1=TIM6->CNT;
  unsigned int ov1=TIM6->SR;
  unsigned int t2=TIM6->CNT;
  unsigned int ov2=TIM6->SR;
  __enable_irq();
  if (t1<=t2) now+=t1+ov1*65536;
  else now+=t2+ov2*65536;
  return (now);
  }


void TIM6_DAC_IRQHandler(void)
{
  __istate_t cpsr=__get_interrupt_state();
  __disable_interrupt();
  
  
  ticks+=65536;
	ms++;
/*	static uint16_t cnt = 0;

	if (++cnt == 1000)
	{
		cnt = 0;
		sec++;
//		GPIOC->ODR ^= GPIOC_LEDB;
	}*/

	TIM6->SR &= ~TIM_SR_UIF;

	NVIC_ClearPendingIRQ(TIM6_DAC_IRQn);
  __set_interrupt_state(cpsr);
}

/* timer16 for pwm channel 1 and timer17 for channel 2 (~12 kHz when external
   12 MHz quartz used) */
void pwm_init(void)
{
	TIM16->CR1 = TIM_CR1_ARPE | TIM_CR1_CEN;
	TIM16->CCMR1 = TIM_CCMR1_OC1M & (TIM_CCMR_OCM_PWM_LOW_MATCH << 4)
			| TIM_CCMR1_OC1PE;
	TIM16->ARR = 0x500;
	TIM16->CCER = TIM_CCER_CC1E;
	TIM16->BDTR = TIM_BDTR_AOE | TIM_BDTR_MOE;

	TIM17->CR1 = TIM_CR1_ARPE | TIM_CR1_CEN;
	TIM17->CCMR1 = TIM_CCMR1_OC1M & (TIM_CCMR_OCM_PWM_LOW_MATCH << 4)
			| TIM_CCMR1_OC1PE;
	TIM17->ARR = 0x500;
	TIM17->CCER = TIM_CCER_CC1E;
	TIM17->BDTR = TIM_BDTR_AOE | TIM_BDTR_MOE;
}

void pwm_set(enum pwm_ch ch, uint16_t val)
{
//	__disable_irq();
	if (ch == PWM_CH1)
		TIM16->CCR1 = val;
	else if (ch == PWM_CH2)
		TIM17->CCR1 = val;
//	__enable_irq();
}

void T2Encoder::init(int pinA, int pinB)
  {
  if ( pinA == (PORTA | PIN0) && pinB == (PORTA | PIN1) ) { // no remap
    AFIO->MAPR = AFIO->MAPR & ~(AFIO_MAPR_TIM2_REMAP_0 | AFIO_MAPR_TIM2_REMAP_1);
  } else if ( pinA == (PORTA | PIN15) && pinB == (PORTB | PIN3) ) { // remap
    AFIO->MAPR=(AFIO->MAPR & ~AFIO_MAPR_TIM2_REMAP_1) | AFIO_MAPR_TIM2_REMAP_0;
  } else {
    return;
  }

  RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
  A.acquire(pinA, GPIO_IN_FLOATING);
  B.acquire(pinB, GPIO_IN_FLOATING);
	TIM2->CR1 = TIM_CR1_ARPE | TIM_CR1_CEN;
	TIM2->SMCR = TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1; // count both
	TIM2->CCER = 0;//TIM_CCER_CC1E | TIM_CCER_CC2E;

	TIM2->CCMR1 = 0x0101;//(TIM_CCMR1_OC1M & (TIM_CCMR_OCM_PWM_LOW_MATCH << 4)) | TIM_CCMR1_OC1PE | (TIM_CCMR1_OC2M & (TIM_CCMR_OCM_PWM_LOW_MATCH << (4+8))) | TIM_CCMR1_OC2PE;
	TIM2->ARR = 0xFFFF;
	//TIM2->BDTR = TIM_BDTR_AOE | TIM_BDTR_MOE;
  }

void T3Encoder::init(int pinA, int pinB)
  {
  if ( pinA == (PORTB | PIN4) && pinB == (PORTB | PIN5) ) { // partial remap
    AFIO->MAPR=(AFIO->MAPR&~AFIO_MAPR_TIM3_REMAP_0) | AFIO_MAPR_TIM3_REMAP_1;
  } else if ( pinA == (PORTA | PIN6) && pinB == (PORTA | PIN7) ) { // no remap
    AFIO->MAPR=AFIO->MAPR & ~(AFIO_MAPR_TIM3_REMAP_0 | AFIO_MAPR_TIM3_REMAP_1);
  } else if ( pinA == (PORTC | PIN6) && pinB == (PORTC | PIN7) ) { // full remap
    AFIO->MAPR=AFIO->MAPR | (AFIO_MAPR_TIM3_REMAP_0 | AFIO_MAPR_TIM3_REMAP_1);
  } else {
    return;
  }
  RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
  A.acquire(pinA, GPIO_IN_FLOATING);
  B.acquire(pinB, GPIO_IN_FLOATING);
	TIM3->CR1 = TIM_CR1_ARPE | TIM_CR1_CEN;
	TIM3->SMCR = TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1; // count both
	TIM3->CCER = 0;//TIM_CCER_CC1E | TIM_CCER_CC2E;

	TIM3->CCMR1 = 0x0101;//(TIM_CCMR1_OC1M & (TIM_CCMR_OCM_PWM_LOW_MATCH << 4)) | TIM_CCMR1_OC1PE | (TIM_CCMR1_OC2M & (TIM_CCMR_OCM_PWM_LOW_MATCH << (4+8))) | TIM_CCMR1_OC2PE;
	TIM3->ARR = 0xFFFF;
  }

class SonicCtrl
  {
  TIM_TypeDef * TIM;
  unsigned short starttm,lastmeasured;
  gpio_pin start_pin,measure_pin;
  static void IRQ(void* param);
public:
  void begin(int portpin_start, int portpin_measure);
  void start();
  int get();
  };

void SonicCtrl::begin(int portpin_start, int portpin_measure)
  {
  if (portpin_measure==(PORTB|PIN8) || portpin_measure==(PORTA|PIN6) )
    {// tim16
    RCC->APB2ENR |= RCC_APB2ENR_TIM16EN;
    if (portpin_measure==(PORTA|PIN6)) // if remap need
      AFIO->MAPR2|=AFIO_MAPR2_TIM16_REMAP;
    else
      AFIO->MAPR2&=~AFIO_MAPR2_TIM16_REMAP;

    TIM=TIM16;
    }
  else if (portpin_measure==(PORTB|PIN9) || portpin_measure==(PORTA|PIN7) )
    {// tim17
    RCC->APB2ENR |= RCC_APB2ENR_TIM17EN;
    if (portpin_measure==(PORTA|PIN7)) // if remap need
      AFIO->MAPR2|=AFIO_MAPR2_TIM17_REMAP;
    else
      AFIO->MAPR2&=~AFIO_MAPR2_TIM17_REMAP;

    TIM=TIM17;
    }
  else
    return ;
  measure_pin.acquire(portpin_measure,GPIO_IN_FLOATING);
  start_pin.acquire(portpin_start,GPIO_OUT_PUSHPULL_2MHz);

  TIM->CR1 = TIM_CR1_ARPE | TIM_CR1_CEN;
  TIM->CR2 = 0;
  TIM->DIER = TIM_DIER_CC1IE;
  TIM->EGR = 0;
  TIM->CCMR1 = TIM_CCMR1_CC1S_0; // set mode to input capture mode for channel 1
  TIM->CCER = TIM_CCER_CC1E | TIM_CCER_CC1P | TIM_CCER_CC1NP; // enable input capture for both edges
  TIM->CNT = 0;
  // bit_freq = Ftimer / (PSC+1) ===>  PSC = (Ftimer / bit_freq) - 1
  // bitfreq = freq * max
  // PSC = (Ftimer / freq * max) - 1
  TIM->PSC = sys_get_osc() / (1000000) - 1;  // us per tick
  TIM->ARR = 65535;
  TIM->RCR = 0;
  TIM->CCR1 = 4;
  TIM->BDTR = TIM_BDTR_AOE | TIM_BDTR_MOE;

  if (portpin_measure==(PORTB|PIN8) || portpin_measure==(PORTA|PIN6) )
    {// tim16
    SetIRQHandler(TIM1_UP_TIM16_IRQn, IRQ, this);
    }
  else if (portpin_measure==(PORTB|PIN9) || portpin_measure==(PORTA|PIN7) )
    {
    SetIRQHandler(TIM1_TRG_COM_TIM17_IRQn, IRQ, this);
    }

  }
void SonicCtrl::start()
  {
  __istate_t cpsr=__get_interrupt_state();
  __disable_interrupt();
  start_pin.set();
  for (volatile int i=0;i<25;i++);
  start_pin.reset();
  __set_interrupt_state(cpsr);
  }

void SonicCtrl::IRQ(void* param)
  {
  SonicCtrl* sonar=(SonicCtrl*)param;
  unsigned int st=TIM17->SR;
  if ((st&TIM_SR_CC1OF)==0) // if it is not overcapture
    {
    if (sonar->measure_pin.read()) // if now high level -> it was rising edge
      sonar->starttm=sonar->TIM->CCR1;
    else
      sonar->lastmeasured=sonar->TIM->CCR1-sonar->starttm;
    }
  TIM17->SR=0;
  }

int SonicCtrl::get()
  {
  return lastmeasured;
  }

typedef void (*IRQHandler)(void * param);

IRQHandler tim16irq=0; void* tim16irqparam;
IRQHandler tim17irq=0; void* tim17irqparam;

int SetIRQHandler(IRQn irq, IRQHandler handler, void* param)
  {
  if (irq==TIM1_UP_TIM16_IRQn)
    {
    if (tim16irq!=0) return -1;
    tim16irqparam=param; tim16irq=handler;
    NVIC_ClearPendingIRQ(TIM1_UP_TIM16_IRQn);
    NVIC_EnableIRQ(TIM1_UP_TIM16_IRQn);
    }
  else if (irq==TIM1_TRG_COM_TIM17_IRQn)
    {
    if (tim17irq!=0) return -1;
    tim17irqparam=param; tim17irq=handler;
    NVIC_ClearPendingIRQ(TIM1_TRG_COM_TIM17_IRQn);
    NVIC_EnableIRQ(TIM1_TRG_COM_TIM17_IRQn);
    }
  else
    return -2;
  return 0;
  }

int ResetIRQHandler(IRQn irq, IRQHandler handler)
  {
  if (irq==TIM1_UP_TIM16_IRQn)
    {
    NVIC_DisableIRQ(TIM1_UP_TIM16_IRQn);
    tim16irq=0;
    }
  else if (irq==TIM1_TRG_COM_TIM17_IRQn)
    {
    NVIC_DisableIRQ(TIM1_TRG_COM_TIM17_IRQn);
    tim17irq=0;
    }
  else
    return -2;
  return 0;
  }

IRQHandler GetIRQHandler(IRQn irq)
  {
  if (irq==TIM1_UP_TIM16_IRQn)
    return tim16irq;
  else if (irq==TIM1_TRG_COM_TIM17_IRQn)
    return tim17irq;
  return 0;
  }

void* GetIRQHandlerParam(IRQn irq)
  {
  if (irq==TIM1_UP_TIM16_IRQn)
    return tim16irqparam;
  else if (irq==TIM1_TRG_COM_TIM17_IRQn)
    return tim17irqparam;
  return 0;
  }

void TIM1_UP_TIM16_IRQHandler(void)
  {
  if (tim16irq)
    tim16irq(tim16irqparam);
  NVIC_ClearPendingIRQ(TIM1_UP_TIM16_IRQn);
  }

void TIM1_TRG_COM_TIM17_IRQHandler(void)
  {
  if (tim17irq)
    tim17irq(tim17irqparam);
  NVIC_ClearPendingIRQ(TIM1_TRG_COM_TIM17_IRQn);
  }
