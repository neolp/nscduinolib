#ifndef _SPI_H_
#define _SPI_H_

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <gpio-alloc.h>
#include "sys.h"

#define MSBFIRST 0x00 // � ������ ��� ������� ��� ������� (�� ���������)
#define LSBFIRST SPI_CR1_LSBFIRST // � ������ ��� ������� ���

#define SPI_CLOCK_DIV2    (0x00 << 3)
#define SPI_CLOCK_DIV4    (0x01 << 3)
#define SPI_CLOCK_DIV8    (0x02 << 3)
#define SPI_CLOCK_DIV16   (0x03 << 3)
#define SPI_CLOCK_DIV32   (0x04 << 3)
#define SPI_CLOCK_DIV64   (0x05 << 3)
#define SPI_CLOCK_DIV128  (0x06 << 3)
#define SPI_CLOCK_DIV256  (0x07 << 3)

#define SPI_MODE0 0
#define SPI_MODE1 1
#define SPI_MODE2 2
#define SPI_MODE3 3
//����� ����� ������ SPI, ��������� ��������� SPI_MODE0 (�� ���������), SPI_MODE1, SPI_MODE2 � SPI_MODE3. ��� �� ����� ������ c ����������� CPOL � CPHA.

#define SPI_CONTINUE 0x00 // keeps the SS pin low, allowing a subsequent byte transfer.
#define SPI_LAST 0x01 // default if not specified the SS pin returns to high after one byte has been transferred.

class spi
  {
  SPI_TypeDef * SPI;
  public:
  gpio_pin* cs;
  spi(SPI_TypeDef * _SPI)
    {
    SPI=_SPI;
    cs=0;
    }

  /* 115200, parity none, 1 stop bit */
  void begin(int baud, int bits = 8);
  void end();

  void setBitOrder(int order);// ������������� ������� ������� ����� ������ (order):
  void setClockDivider(int divider); // ������������� �������� ������ ��� SPI ������������ �������� �������. �������� �������� 2, 4, 8, 16, 32, 64 � 128. ��������������� ��������� ����� ����� ���� SPI_CLOCK_DIVn, ��� n � ��������, ��������, SPI_CLOCK_DIV32. �� ��������� �������� ����� 4 � ��� ������� �������� ������� �� �� Arduino � 16 ��� SPI ����� �������� �� ������� 4 ���.
  int getOutClock() {
    return sys_get_osc() >> (((SPI->CR1 >> 3) & 0x07)+1);
  }

  void setDataMode(int mode); // ����� ����� ������ SPI, ��������� ��������� SPI_MODE0 (�� ���������), SPI_MODE1, SPI_MODE2 � SPI_MODE3. ��� �� ����� ������ c ����������� CPOL � CPHA.
  int transfer(int value,int mode=SPI_LAST);
  int fast_transfer(int value); // ����� ��� ���������� ����� CS

  void IRQHandler(void);
  };

extern spi spi1;
extern spi spi2;

#endif /* _USART_H_ */