#ifndef __NORDIC_NRF24L01P_H__
#define __NORDIC_NRF24L01P_H__

#include <stddef.h>
#include <stdint.h>
#ifdef LPOS10
#include "spi-adapter.h"
#include <string.h>
#else
#include <gpio-alloc.h>
#include <spi.h>
#define spi_t spi
#endif

/* Memory Map */
#define CONFIG      0x00
#define EN_AA       0x01
#define EN_RXADDR   0x02
#define SETUP_AW    0x03
#define SETUP_RETR  0x04
#define RF_CH       0x05
#define RF_SETUP    0x06
#define STATUS      0x07
#define OBSERVE_TX  0x08
#define RPD         0x09
#define RX_ADDR_P0  0x0A
#define RX_ADDR_P1  0x0B
#define RX_ADDR_P2  0x0C
#define RX_ADDR_P3  0x0D
#define RX_ADDR_P4  0x0E
#define RX_ADDR_P5  0x0F
#define TX_ADDR     0x10
#define RX_PW_P0    0x11
#define RX_PW_P1    0x12
#define RX_PW_P2    0x13
#define RX_PW_P3    0x14
#define RX_PW_P4    0x15
#define RX_PW_P5    0x16
#define FIFO_STATUS 0x17
#define DYNPD       0x1C
#define FEATURE     0x1D

/* Bit Mnemonics */

#define RF_SETUP_1Mbit    0x00
#define RF_SETUP_2Mbit    0x08
#define RF_SETUP_250Kbit  0x20
#define RF_SETUP_Reserv   0x28

#define RF_SETUP_PWRm18dBm  0x00
#define RF_SETUP_PWRm12dBm  0x02
#define RF_SETUP_PWRm6dBm   0x04
#define RF_SETUP_PWR0dBm    0x06

#define DPL_P0      0x01
#define DPL_P1      0x02
#define DPL_P2      0x04
#define DPL_P3      0x08
#define DPL_P4      0x10
#define DPL_P5      0x20

#define EN_DPL      0x04 // Enables Dynamic Payload Length
#define EN_ACK_PAYd 0x02 // Enables Payload with ACK
#define EN_DYN_ACK  0x01 // Enables the W_TX_PAYLOAD_NOACK command

#define MASK_RX_DR  6
#define MASK_TX_DS  5
#define MASK_MAX_RT 4
#define EN_CRC      3
#define CRCO        2
#define PWR_UP      1
#define PRIM_RX     0
#define ENAA_P5     5
#define ENAA_P4     4
#define ENAA_P3     3
#define ENAA_P2     2
#define ENAA_P1     1
#define ENAA_P0     0
#define ERX_P5      5
#define ERX_P4      4
#define ERX_P3      3
#define ERX_P2      2
#define ERX_P1      1
#define ERX_P0      0
#define AW          0
#define ARD         4
#define ARC         0
#define PLL_LOCK    4
#define RF_DR       3
#define RF_PWR      1
#define LNA_HCURR   0
#define RX_DR       6
#define TX_DS       5
#define MAX_RT      4
#define RX_P_NO     1
#define TX_FULL     0
#define PLOS_CNT    4
#define ARC_CNT     0
#define TX_REUSE    6
#define FIFO_FULL   5
#define TX_EMPTY    4
#define RX_FULL     1
#define RX_EMPTY    0

/* Instruction Mnemonics */
#define R_REGISTER    0x00
#define W_REGISTER    0x20
#define REGISTER_MASK 0x1F
#define R_RX_PAYLOAD  0x61
#define W_TX_PAYLOAD  0xA0
#define FLUSH_TX      0xE1
#define FLUSH_RX      0xE2
#define REUSE_TX_PL   0xE3

#define R_RX_PL_WID   0x60
#define W_ACK_PAYLOAD 0xA0 // +channel
#define W_TX_PAYLOAD_NOACK  0xB0
#define NOP           0xFF

// nrf24l01p settings

#define nrf_ADDR_LEN	5
#define nrf_CONFIG ((1<<EN_CRC) | (0<<CRCO) )  // enable crc with length 1

class nrf24l01p {
  uint8_t pipe0addr[nrf_ADDR_LEN];
  uint8_t pipe1addr[nrf_ADDR_LEN];

#define NRF_STATE_POWER_DOWN  0
#define NRF_STATE_STANDBY_RX  1
#define NRF_STATE_RX          2
#define NRF_STATE_STANDBY_TX  3 // not good state (no way to start rx ONLY AFTER POWER DOWN)
#define NRF_STATE_TX          4
		uint8_t state;
		uint8_t enabled_pipes; //
    uint8_t moredata;

		gpio_pin* cePin; // CE Pin controls RX / TX, default 8.
		gpio_pin* csnPin; // CSN Pin Chip Select Not, default 7.
		gpio_pin* irqPin;

		/*
		 * Channel 0 - 127 or 0 - 84 in the US.
		 */
		//uint8_t channel;

		/*
		 * Payload width in bytes default 16 max 32.
		 */

//		uint8_t payload;

		/*
		 * Spi interface (must extend spi).
		 */

		spi_t *_spi;



  void transmitSync(const uint8_t *dataout,uint8_t len);
  void transferSync(uint8_t *dataout,uint8_t *datain,uint8_t len);
  void configRegister(uint8_t reg, uint8_t value);
  uint8_t readRegister(uint8_t reg);
  uint8_t readRegister(uint8_t reg, uint8_t * value, uint8_t len);
  uint8_t writeRegister(uint8_t reg, uint8_t value);
  uint8_t writeRegister(uint8_t reg, uint8_t const *value, uint8_t len);

  void csnHi();
  void csnLow();

  void ceHi();
  void ceLow();
  void flushRx();
  void flushTx();
public:
  nrf24l01p();

  void enable_pipe(int pipe_num);
  void disable_pipe(int pipe_num);

  void startListening();
  void stopListening();
//    void powerUp();
//    void powerUpRx();
		void powerUpTx();
		void powerDown();
  bool sendPkt(unsigned int pipe, const void* buf, uint8_t len );
//    uint8_t getDynamicPayloadSize();
//    bool available(void);
  bool available(uint8_t* pipe_num);
  int getData( void* buf);

  void init(spi_t *__spi, gpio_pin* _cePin, gpio_pin* _csnPin, gpio_pin* _irqPin);
  void config(int channel);
//		void send(unsigned int pipe, const uint8_t *value,int len);
  void setRADDR(unsigned int pipe, uint8_t const *addr_array);
  void setTADDR(uint8_t const *addr_array);
//		int dataReady();
//		bool isSending();
//		bool txFifoEmpty();
//		int getData(uint8_t * data);
//		unsigned int getStatus();


    void readCFG();

};

#endif /* __NORDIC_NRF24L01P_HPP__ */
