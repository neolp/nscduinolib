
#ifndef __SI446X_HPP__
#define __SI446X_HPP__

#include <assert.h>
#include <drivers/si446x_defs.hpp>

#ifndef LPOS10
#include <gpio-alloc.h>
#include <spi.h>
#include <tim.h>
#else
#include <LPOS_threads.h>
#include <kernel/LPOS_kernel_VFS.h>
#include <LPOS_errors.h>
#include <kernel/LPOS_timers.h>
#include <lplib/lpscoped.hpp>
#endif

namespace si446x {

/* ---- basic types --------------------------------------------------------- */
#ifndef LPOS10

typedef ::ssize_t ssize_t;
typedef ::uint    uint;

#else /* LPOS10 */

typedef ::lp::ssize_t ssize_t;
typedef ::uint        uint;

#endif

/* ---- time wrappers ------------------------------------------------------- */
#ifndef LPOS10

typedef unsigned int       msec_t;
typedef unsigned long long tick_t;

static inline void sleep_usec(unsigned long long usec)
{
	/* minimize using this sh*t */
	unsigned long long started_at = getlong_us();

	for (;;)
	{
		unsigned long long diff = getlong_us() - started_at;
		if (diff >= usec)
			return;
	}
}


static inline void sleep_msec(msec_t msec)
{
	sleep_usec(msec * 1000);
}

static inline tick_t get_ticks()
{
	return getlong_us();
}

static inline tick_t msec_to_ticks(msec_t msec)
{
	return static_cast<tick_t>(msec) * 1000;
}

#else /* LPOS10 */

typedef ::lp::msec_t msec_t;

static inline void sleep_usec(unsigned long long usec)
{
	::sleep(usec / 1000);
}

static inline void sleep_msec(msec_t msec)
{
	::sleep(msec);
}

typedef unsigned long long tick_t;

static inline tick_t get_ticks()
{
	return GetLongTicks();
}

static inline tick_t msec_to_ticks(msec_t msec)
{
	return MsToTicks(msec);
}

#endif

/* ---- spi wrappers -------------------------------------------------------- */
#ifndef LPOS10

class __spi_t
{
public:
	__spi_t(::spi &io): io_(io)
		{ }

	int write_octet(uint8_t octet, msec_t timeout)
	{
		io_.fast_transfer(octet);
		return 0;
	}

	ssize_t force_write(void const *buf, size_t size, msec_t timeout)
	{
		for (size_t i = 0; i < size; ++i)
		{
			uint8_t const *ptr = static_cast<uint8_t const *>(buf);
			io_.fast_transfer(ptr[i]);
		}
		return size;
	}

	int read_octet(uint8_t *octet, msec_t timeout)
	{
		int value = io_.fast_transfer(0xff);
		if (octet != NULL)
			*octet = value;
		return value < 0 ? value : 0;
	}

	ssize_t force_read(void *buf, size_t size, msec_t timeout)
	{
		for (size_t i = 0; i < size; ++i)
		{
			int value = io_.fast_transfer(0xff);
			if (value < 0)
				return value;

			uint8_t *ptr = static_cast<uint8_t *>(buf);
			ptr[i] = value;
		}
		return size;
	}

	void *get_dev() const
	{
		return NULL;
	}

private:
	::spi &io_;
};

class scoped_inode_lock
{
public:
	scoped_inode_lock(void *, msec_t)
		{ }

	int get_errcode() const
	{
		return 0;
	}
};

#else /* LPOS10 */

class __spi_t
{
public:
	__spi_t(i_node &io): io_(io)
		{ }

	int write_octet(uint8_t octet, msec_t timeout)
	{
		lp::ssize_t ret = io_.write(0, &octet, sizeof(octet), timeout);
		return ret == sizeof(octet) ? 0 : ERR_IO_ERROR;
	}

	ssize_t force_write(void const *buf, size_t size, msec_t timeout)
	{
		uint8_t const  *ptr = static_cast<uint8_t const *>(buf);
		size_t  const rsize = size;

		while (size > 0)
		{
			lp::ssize_t ret = io_.write(0, ptr, size, timeout);
			if (ret < 0)
				return ret;

			assert(ret <= size);
			ptr  += ret;
			size -= ret;
		}
		return rsize;
	}

	int read_octet(uint8_t *octet, msec_t timeout)
	{
		lp::ssize_t ret = io_.read(0, octet, sizeof(*octet), timeout);
		return ret == sizeof(*octet) ? 0 : ERR_IO_ERROR;
	}

	ssize_t force_read(void *buf, size_t size, msec_t timeout)
	{
		uint8_t *ptr = static_cast<uint8_t *>(buf);
		size_t const rsize = size;

		while (size > 0)
		{
			lp::ssize_t ret = io_.read(0, ptr, size, timeout);
			if (ret < 0)
				return ret;

			assert(ret <= size);
			ptr  += ret;
			size -= ret;
		}
		return rsize;
	}

	void *get_dev() const
	{
		return &io_;
	}

private:
	i_node &io_;
};

class scoped_inode_lock
{
public:
	scoped_inode_lock(void *inode, msec_t timeout = WAIT_INFINITY):
			lk_(static_cast<i_node *>(inode), timeout)
		{ }

	int get_errcode(void) const
	{
		return lk_.get_errcode();
	}

private:
	::lp::scoped_inode_lock lk_;
};

#endif

/* ---- gpio wrappers ------------------------------------------------------- */
#ifndef LPOS10

typedef ::gpio_pin __gpio_t;

#else /* LPOS10 */

class __gpio_t
{
public:
	__gpio_t(i_node &pin): pin_(pin)
		{ }

	void set(bool value = true)
	{
		io(SET_GPIO_OUT_BOOL, &value);
	}

	void clr()
	{
		set(false);
	}

	bool read()
	{
		bool value = false;
		io(GET_GPIO_IN_BOOL, &value);
		return value;
	}

private:
	void io(int cmd, bool *iovalue)
	{
		lp::ssize_t ret = pin_.ioctl(cmd, iovalue, sizeof(*iovalue),
				WAIT_INFINITY);
		if (ret != sizeof(*iovalue))
			assert(false);
	}

	i_node &pin_;
};

#endif

class gpio_scoped_level
{
public:
	gpio_scoped_level(__gpio_t &gpio, bool value = true):
			gpio_(gpio), value_(value)
	{
		gpio.set(value);
	}

	~gpio_scoped_level(void)
	{
		gpio_.set(!value_);
	}

private:
	__gpio_t &gpio_;
	bool      value_;
};

/* ---- rf_module, rf_channel ----------------------------------------------- */
#ifndef LPOS10

static int const EIO       = 1;
static int const EINVAL    = 2;
static int const E2BIG     = 3;
static int const EBUSY     = 4;
static int const ETIMEDOUT = 5;

#else /* LPOS10 */

static int const EIO       = -ERR_IO_ERROR;
static int const EINVAL    = -ERR_WRONG_VALUE;
static int const E2BIG     = -ERR_TOO_LONG;
static int const EBUSY     = -ERR_BUSY;
static int const ETIMEDOUT = -ERR_TIME_OUT;

#endif

class rf_module
{
public:
	rf_module(__spi_t &io, __gpio_t &nsel);

	/* initialization */
	static msec_t const default_timeout = 2500 * 2;
	virtual int init(msec_t timeout = default_timeout);

	/* general commands */
	int power_on(uint32_t xtal_freq, bool txco, msec_t timeout = default_timeout);
	int nop(msec_t timeout = default_timeout);
	int get_part_info(struct part_info *info, msec_t timeout = default_timeout);
	int get_func_info(struct func_info *info, msec_t timeout = default_timeout);

	/* basic configuration */
	int gpio_cfg(struct gpio_cfg const &wcfg, struct gpio_cfg *rcfg,
			msec_t timeout = default_timeout);

	int read_adc(uint8_t adc_cfg, struct adc_info *info,
			msec_t timeout = default_timeout);

	int fifo_ctl(struct fifo_info *info, uint8_t flags = 0,
			msec_t timeout = default_timeout);
	ssize_t get_packet_size(msec_t timeout = default_timeout);

	int ircal(struct ircal_cfg const &cfg, msec_t timeout = default_timeout);
	int set_proto(bool ieee802_15_4g, msec_t timeout = default_timeout);

	/* interrupt control */
	int get_irq_stat(struct irq_stat_req const &req,
			struct irq_stat_resp *resp,
			msec_t timeout = default_timeout);
	int get_ph_stat(struct irq_stat_ph_resp *resp,
			msec_t timeout = default_timeout);
	int get_modem_stat(struct irq_stat_modem_resp *resp,
			msec_t timeout = default_timeout);
	int get_chip_stat(struct irq_stat_chip_resp *resp,
			msec_t timeout = default_timeout);

	/* state control */
	int get_device_state(struct device_state *state,
			msec_t timeout = default_timeout);

	/* send / recv control */
	int start_tx(struct start_tx_req const &req, msec_t timeout = default_timeout);
	int start_rx(struct start_rx_req const &req, msec_t timeout = default_timeout);

	int write_tx(void const *ptr, size_t size, msec_t timeout = default_timeout);
	int read_rx(void *ptr, size_t size, msec_t timeout = default_timeout);

	/* set / get properties */
	int set_property(uint16_t prop_addr, void const *ptr, size_t size,
			msec_t timeout = default_timeout);
	int set_verify_property(uint16_t prop_addr, void const *ptr, size_t size,
			msec_t timeout = default_timeout);
	int get_property(uint16_t prop_addr, void *ptr, size_t size,
			msec_t timeout = default_timeout);

	/* apply generated configuration by silabs wireless development suite */
	int apply_cfg_array(void const *cfg_array, msec_t timeout = default_timeout);

protected:
	int rw_once(uint8_t cmd, void const *warg, size_t wsize,
			void *rarg, size_t rsize, msec_t timeout);
	int rw(uint8_t cmd, void const *warg, size_t wsize,
			void *rarg, size_t rsize, msec_t timeout);

	int set_property_raw(uint16_t prop_addr, void const *ptr, size_t size,
			msec_t timeout);
	int get_property_raw(uint16_t prop_addr, void *ptr, size_t size,
			msec_t timeout);

private:
	void nsel_minimal_pause(void);

	int wait_cts_cycle(msec_t timeout);
	virtual int wait_cts(bool stay_selected, msec_t timeout);

	int write(uint8_t cmd, void const *ptr, size_t size, msec_t timeout);
	int read(uint8_t const *opt_cmd, void *ptr, size_t size, msec_t timeout);

	__spi_t  &io_;
	__gpio_t nsel_;
};

#if 0
class rf_module_cts: public rf_module
{
public:
	rf_module_cts(spi_t &io, gpio_t &nsel, gpio_t &cts);

	virtual int init(msec_t timeout = default_timeout);

private:
	static void cts_pin_callback(uint gpioid, uint flags, void *arg);
	virtual int wait_cts(bool stay_selected, msec_t timeout);

	SYS_EVENT isready_;
	gpio_t   &cts_;
};
#endif
#if 0
/* rf_channel: raw bytes io */
class rf_channel
{
public:
	rf_channel(__spi_t &io, __gpio_t &nsel, __gpio_t &sdn, __gpio_t &nirq);

	/* init & configure */
	void    power_down();
	int     power_up(void const *cfg_array, msec_t timeout = rf_module::default_timeout);

	/* io workflow */
	static uint const event_rx     = 0x0001; /* packet has been received (after start_rx()) */
	static uint const event_rx_err = 0x0002; /* error has been occurred (after start_rx()) */
	static uint const event_tx     = 0x8000; /* packet has been sent (after start_tx()) */

  int get_part_info(part_info* info, msec_t timeout){
    return rf_.get_part_info(info, timeout);
  }

	uint    get_events_flags();

	int     start_rx();
	ssize_t read_rx(void *ptr, size_t size);
	ssize_t clear_rx(void);

	ssize_t write_tx(void const *ptr, size_t size);
	int     start_tx();

	bool    nirq_is_active() const;

//private:
	rf_module rf_;
	__gpio_t  &sdn_;
	__gpio_t  &nirq_;
};

/* rf_packet_handler: smart packet io */
class rf_packet_handler
{
public:
	rf_packet_handler(rf_channel &rf);

	struct endpoint
	{
		uint8_t lowpan;
		uint8_t dst_addr;
		uint8_t src_addr;
	};

	/* use this instead read_rx() and write_tx() */
	ssize_t read_pkt(struct endpoint *ep, void *ptr, size_t size);
	ssize_t write_pkt(struct endpoint const &ep, void const *ptr, size_t size);

	rf_channel &channel() const;

private:
	static size_t const payload_fixed_size = 33;

	rf_channel &rf_;
};

/* rf_pipe: smart packet io with irq waiting or polling */
class rf_pipe
{
public:
	rf_pipe(rf_packet_handler &rf);

	/* useful io interface */
	ssize_t recv(struct rf_packet_handler::endpoint *ep,
			void *inbuf, size_t insize, msec_t timeout);
	ssize_t send(struct rf_packet_handler::endpoint const &ep,
			void const *buf, size_t size, msec_t timeout);

	/* master helpers */
	static bool is_answer_endpoint(struct rf_packet_handler::endpoint const &ep,
			struct rf_packet_handler::endpoint const &rep,
			uint8_t broadcast);

	/* slave helpers */
	static bool is_proper_endpoint(struct rf_packet_handler::endpoint const &ep,
			uint8_t lowpan, uint8_t self_addr, uint8_t broadcast);
	static void make_answer_endpoint(struct rf_packet_handler::endpoint *ep,
			struct rf_packet_handler::endpoint const &rep,
			uint8_t self_addr);
	static void make_answer_endpoint(struct rf_packet_handler::endpoint *ep,
			uint8_t self_addr);

#ifdef LPOS10
	/* please call into gpio interrupt */
	void nirq_falling_edge();
#endif

private:
	void wait_nirq_falling_edge(msec_t timeout);

	rf_packet_handler &rf_;
#ifdef LPOS10
	SYS_EVENT          event_;
#endif
};

#endif

} /* namespace si446x */

#endif /* __SI446X_HPP__ */
