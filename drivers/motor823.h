#ifndef _MOTOR_H_
#define _MOTOR_H_

#include "tim.h"
#include "lpalg/MovingAverage.hpp"

#define Pdef 4.4f
#define Ddef 15.0f
#define Idef 0.01f

#define SPmdef 0.0625f

#define MODE_PWM 0  
#define MODE_SPEED 1  
#define MODE_POSITION 2  
#define MODE_SPEED_TAHOMETER 3

#define POLL_STEP_mS 10

struct wheel_t {
  float acc; // positive parameter steps per second per second
  float speed; // positive or negative steps per second
  float target_speed; // positive or negative steps per second

  long long actual_pos; // actual position in steps absolute value always actual
  long long last_actual_pos; //for speed measurement
  float measuredSpeed;

  long long start_pos; // which position is global zero position
  float calculated_pos; // which position was should be in calculation time      pos = start_pos + calculated_pos

  long long target_pos; // target position in absolute values

  bool brake;

  int mode;
  void pidreset();
  void to_mode(int new_mode){
    if ( mode == MODE_PWM && (new_mode == MODE_SPEED || new_mode == MODE_POSITION)) {
      speed = 0; // current speed
      brake = false; // brake flag
      last_err = 0;
      itegral_err = 0;
      calculated_pos = 0;
      start_pos= actual_pos;
    }
    mode = new_mode;
  }
  void go_to(int target, float speed, float acc);
  void set_sp(int sp, float _acc);
  void sppoll(int dt);
  void trpoll(int dt);

  void poll(){
    if (mode == MODE_SPEED) {
      sppoll(POLL_STEP_mS);
    } else if (mode == MODE_POSITION) {
      trpoll(POLL_STEP_mS);
    }
  }

  float last_err;
  float itegral_err;

  float P;
  float D;
  float I;

  float SPm;

  void pidpoll(float err, int dt);

  float pid_out; // pid regulator out
};

class motor823_driver
  {
  gpio_pin EN1;
  gpio_pin INA1;
  gpio_pin TIM15_CH2;
  gpio_pin INB1;

  gpio_pin EN2;
  gpio_pin INA2;
  gpio_pin TIM15_CH1;
  gpio_pin INB2;

  gpio_pin En5V;
  gpio_pin Fault5V;


  void trajectoryreset();
  void pidreset(wheel_t* w, int speed, int acc);

  private:
  wheel_t left;
  wheel_t right;

  unsigned int overpower;

  public:
    
    long long get_actual_leftpos() {return left.actual_pos; }
    long long get_actual_rightpos() {return right.actual_pos; }
    int get_actual_leftspeed() {return (int)left.speed; }// positive or negative steps per second
    int get_actual_rightspeed() {return (int)right.speed; }// positive or negative steps per second
    int get_actual_leftmeasuredspeed() {return (int)left.measuredSpeed; }
    int get_actual_rightmeasuredspeed() {return (int)right.measuredSpeed; }
    
    float global_target_speed; // parameter
    float global_acc; // parameter
  
#define MODE_ENCODER 0
#define MODE_TACHOMETER 1
    signed short alpha_pwm;
    signed short beta_pwm;
    int encoder_mode;
    unsigned int lastmeasuretime;
    unsigned short last_leftEnc;
    unsigned short last_rightEnc;
    T2Encoder leftEnc;
    T3Encoder rightEnc;
  
  motor823_driver(){
    left.mode = 0;
    right.mode = 0;
    encoder_mode = 0;
    overpower = 0;
  }

  int begin();//int pwm_freq,int
  int end();//int pwm_freq,int
  void set_pwm(int alpha,int beta);
  void set_left_pwm(int alpha) {
    left.mode = MODE_PWM;
    set_alpha(alpha);
  }
  void set_right_pwm(int beta) {
    right.mode = MODE_PWM;
    set_beta(beta);
  }

  int set_alpha(int alpha); // return 1 of overpower 0 otherwise
  int set_beta(int beta);// return 1 of overpower 0 otherwise
  void setspeed(int ls);
  void set_acc(int la);
  void set_sp(int lsp, int rsp);
  void go_to(int l, int r);
  void left_go_to(int l);
  void right_go_to(int r);
  void poll();
  void set_pid_SPm(float spm) {
    right.SPm = left.SPm = spm;
  }      
  void set_pid_P(float p) {
    right.P = left.P = p;
  }
  void set_pid_I(float i) {
    right.I = left.I= i;
  }
  void set_pid_D(float d) {
    right.D = left.D = d;
  }
  float get_pid_SPm() {
    return right.SPm;
  }      
  float get_pid_P() {
    return right.P;
  }
  float get_pid_I() {
    return right.I;
  }
  float get_pid_D() {
    return right.D;
  }

public:
  signed short get_alpha_pwm() {return alpha_pwm; }
  signed short get_beta_pwm() {return beta_pwm; }

  };

#endif