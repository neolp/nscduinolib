#include <si4463.h>

#define SI446X_PROPERTY_PKT_FIELD_2_LENGTH_7_0 0x1212

#define SI446X_PROPERTY_GLOBAL_WUT_CONFIG           0x0004
#define SI446X_CONDITION_RX_START_IMMEDIATE     0x00
#define SI446X_CONDITION_RETRANSMIT_NO 0x00
#define SI446X_CONDITION_TX_START_IMMEDIATE 0x00
#define SI446X_CHANNEL     0x00

  //RSSI (in dBm) = (RSSI_value /2) � RSSIcal
  //Without external LNA, the value of RSSIcal is around 130 �30.
#define RSSIcal 130
#define RSSIval(RSSI) (((RSSI) + RSSIcal)*2)
#define RSSI(RSSIval) (((RSSIval)/2) - RSSIcal)


si4463::si4463 (spi &_io, gpio_pin &_nsel, gpio_pin &_sdn, gpio_pin &_nirq):
io(_io),
nsel(_nsel),
sdn(_sdn),
nirq(_nirq),
chip_state(0),
_lastRssi(-127)
{
}


int si4463::readPkt(unsigned int pipe, void* buf, uint8_t len ) {
  if ( chip_state != SI_STATE_RX) { // start listening
    // Tell the receiver the max data length we will accept (a TX may have changed it)
    uint8_t l[] = { 64 }; // max pkt length
    set_property(SI446X_PROPERTY_PKT_FIELD_2_LENGTH_7_0,l,sizeof(l));
    // Start Rx
    uint8_t rx_cfg[] = { SI446X_CHANNEL, SI446X_CONDITION_RX_START_IMMEDIATE, 0x00, 0x00, SI_STATE_NO_CHENGE, SI_STATE_READY, SI_STATE_READY }; // packet by packet mode
    rw(0x32, rx_cfg, sizeof(rx_cfg), NULL, 0, 100);

    chip_state=SI_STATE_RX;
  }

// 	uint64_t const recv_started_at = GetLongTicks();
  uint32_t flags = get_events_flags();
  if (flags & event_rx_err) {
    clearRxFifo();
    return 0;
  } else if (flags & event_rx) {
    uint8_t const req[] = { 0, 0, 0, 0, 0 };
    uint16_t size=0;
    int ret = rw(0x16, req, sizeof(req), &size, sizeof(size), 100);
    if (ret<0)
      return ret;
    size = (size >> 8) | ((size&0xff)<<8);

    if (size==0 || size>63) return 0;

    uint8_t modem_status[6];

    rw(0x22, NULL, 0, modem_status, sizeof(modem_status),1000); //#define RH_RF24_CMD_GET_MODEM_STATUS           0x22
    //_curRssi = modem_status[2]; //CURR_RSSI[7:0]
    _lastRssi = RSSI(modem_status[3]);// (modem_status[3]>>1) - 130; //LATCH_RSSI[7:0]
    //_lastPreambleTime = GetLongTicks();

    nsel.clr();
    int readed=0;

    io.fast_transfer(0x77);
    uint8_t frame_len = io.fast_transfer(0x00);

    if (frame_len!=size){
      ret=-1;
      nsel.set();
      clearRxFifo();
      return -1;
//      goto ret;
    }

    while (size > 0) {
      *((char*)buf)= io.fast_transfer(0x00);
      buf  = ((char*)buf) + 1;
      size--;
      readed++;
    }
ret:
    nsel.set();
    return ret>=0?readed:ret;
  }
  return 0;
}

/*
int si4463::clearrx() {
    uint8_t const req[] = { 0, 0, 0, 0, 0 };
    uint16_t      size=0;

    int ret = rw(0x16, req, sizeof(req), &size, sizeof(size), 1000);
    if (ret<0)
      return ret;
    size = (size >> 8) | ((size&0xff)<<8);

    if (size==0 || size>63) return 0;

    char buf[1];
    for (size_t i = 0; i < size; ++i) {

    uint8_t const cmd = 0x77;
    return read_cmd(&cmd, buf, 1);
  }

	return size;
}
*/

/*
int rf_module::power_on(uint32_t xtal_freq, bool txco, msec_t timeout)
{
	uint8_t const boot_opt = 0x01; // boot up as transceiver
	uint8_t const xtal_opt = txco ? 0x01 : 0x00;

	uint8_t wbuf[sizeof(boot_opt) + sizeof(xtal_opt) + sizeof(xtal_freq)];
	void *wptr = wbuf;
	wptr = put_be(wptr, boot_opt);
	wptr = put_be(wptr, xtal_opt);
	wptr = put_be(wptr, xtal_freq);

	return rw(0x02, wbuf, sizeof(wbuf), NULL, 0, timeout);
}
*/
int si4463::config(void const *cfg_array, uint32_t timeout) {
  sdn.set(true);
	sleep_msec(25);
	sdn.set(false);

  nsel.set();

	sleep_msec(20);

  uint8_t write_buf[] = { 0x00, 0x00, 0x00 };
  write_cmd(0x20, write_buf, sizeof(write_buf),0); // GET_INT_STATUS

	uint8_t const *ptr = static_cast<uint8_t const *>(cfg_array);

	while (*ptr != 0x00)
	{
		uint8_t const  op_size = *ptr++ - 1;
		uint8_t const  op_cmd  = *ptr++;
		uint8_t const *op_data = ptr;
		ptr += op_size;

		int ret = rw(op_cmd, op_data, op_size, NULL, 0, timeout);
		if (ret)
			return ret;
/*
		static struct irq_stat_req const irq_req =
		{
			// clear all pending interrupts
			.ph_clr_pend    = 0x00,
			.modem_clr_pend = 0x00,
			.chip_clr_pend  = 0x00,
		};

		struct irq_stat_resp irq_resp;
		ret = get_irq_stat(irq_req, &irq_resp);
		if (ret)
			return ret;
    */
	}

  uint8_t l[] = { 0 };
  set_property(SI446X_PROPERTY_GLOBAL_WUT_CONFIG,l,sizeof(l));

	return 0;
}


int si4463::rw(uint8_t cmd, void const *warg, size_t wsize,void *rarg, size_t rsize, uint32_t timeout){
	int ret = wait_cts(false, timeout);
	if (ret)
		return ret;

	ret = write_cmd(cmd, warg, wsize, timeout);
	if (ret)
		return ret;

	ret = wait_cts(true, timeout);
	if (ret)
	{
		nsel.set();
		return ret;
	}

	return read_cmd(NULL, rarg, rsize);
}

int si4463::wait_cts(bool stay_selected, uint32_t timeout)
{
  timeout=1000;
	uint32_t const started_at = get_ticks();

	for (;;)
	{
		nsel.clr();
    io.fast_transfer(0x44);
    uint8_t cts=io.fast_transfer(0x00);
    if (cts == 0xff) {
			if (!stay_selected)
        nsel.set();
      return 0;
    }
    nsel.set();
		uint32_t const now  = get_ticks();
		uint32_t const diff = now - started_at;
		uint32_t const tout = msec_to_ticks(timeout);
		if (diff > tout)
			return -1;

		sleep_usec(20);
	}
}

int si4463::write_cmd(uint8_t cmd, void const *ptr, size_t size, uint32_t timeout)
{
  nsel.clr();

	int ret = io.fast_transfer(cmd);

	while (size > 0)
	{
		io.fast_transfer(*(char*)ptr);
    ptr=((char*)ptr)+1;
    size--;
	}
/*	else
	{
		// dummy: rf_module a1 revision bug workaround
		ret = io_.write_octet(cmd, timeout);
		if (ret < 0)
			return -EIO;
	}*/
  nsel.set();

	return 0;
}

int si4463::read_cmd(uint8_t const *opt_cmd, void *ptr, size_t size)
{
//  int ret;
	nsel.clr();

	if (opt_cmd != NULL)
	{
		io.fast_transfer(*opt_cmd);
	}

  while (size>0) {
    *((char*)ptr) = io.fast_transfer(0x00);
    ptr= ((char*)ptr)+1;
    size--;
  }
  nsel.set();
	return 0;
}

bool si4463::sendPkt(unsigned int pipe, const void* buf, uint8_t len ) {
  /*
  int wt=0;
again:
  if ( chip_state != SI_STATE_RX ) { // start listening
    // Tell the receiver the max data length we will accept (a TX may have changed it)
    uint8_t l[] = { 64 }; // max pkt length
    set_property(SI446X_PROPERTY_PKT_FIELD_2_LENGTH_7_0,l,sizeof(l));
    // Start Rx
    uint8_t rx_cfg[] = { SI446X_CHANNEL, SI446X_CONDITION_RX_START_IMMEDIATE, 0x00, 0x00, SI_STATE_NO_CHENGE, SI_STATE_READY, SI_STATE_READY }; // packet by packet mode
    rw(0x32, rx_cfg, sizeof(rx_cfg), NULL, 0, 100);

//    chip_state=SI_STATE_RX;
  }
  sleep_usec(200);
  int rssi = get_modem_rssi();
  */
//  get_events_flags();
//    uint8_t state[] = { SI_STATE_READY };
//    rw(0x33, state, sizeof(state),0,0,1000); //#define RH_RF24_CMD_REQUEST_DEVICE_STATE       0x33

//  if (rssi > RSSIval(-90)) {
//    wt++;
//    if (wt > 3) return false;
//    sleep_usec(5000);
//      goto again;
//  }

  uint8_t l[] = { (uint8_t)(len)};
  set_property(SI446X_PROPERTY_PKT_FIELD_2_LENGTH_7_0,l,sizeof(l));

	int ret = write_cmd(0x66, &len, 1, 0);//  write_tx(&hdr, sizeof(hdr));
	ret = write_cmd(0x66, buf, len, 0);// write_tx(ptr, size);

	uint8_t wbuf[] = {SI446X_CHANNEL, (SI_STATE_READY << 4) | SI446X_CONDITION_RETRANSMIT_NO | SI446X_CONDITION_TX_START_IMMEDIATE };
  chip_state = SI_STATE_TX;

	rw(0x31, wbuf, sizeof(wbuf), NULL, 0, 0);

	int const send_started_at = get_ticks();
	for (;;)
	{
		if (get_events_flags() & event_tx)
			return len;

		int diff = get_ticks() - send_started_at;
		if (diff >= msec_to_ticks(100))
			return -1;

	}
	//return len;
}

#pragma pack(push,1)
struct irq_stat_resp
{
	uint8_t global_pend;
	uint8_t global_stat;
	uint8_t ph_pend;
	uint8_t ph_stat;
	uint8_t modem_pend;
	uint8_t modem_stat;
	uint8_t chip_pend;
	uint8_t chip_stat;
};
#pragma pack(pop)

#pragma pack(push,1)
struct modem_stat_resp
{
	uint8_t MODEM_PEND;
	uint8_t MODEM_STATUS;
	uint8_t CURR_RSSI;
	uint8_t LATCH_RSSI;
	uint8_t ANT1_RSSI;
	uint8_t ANT2_RSSI;
	uint16_t AFC_FREQ_OFFSET;
};
#pragma pack(pop)

int si4463::get_events_flags()
{
//	if (nirq.read())
//		return 0;

	irq_stat_resp irq_resp;

	uint8_t wbuf[]={0,0,0};
	int ret = rw(0x20, wbuf, sizeof(wbuf), &irq_resp, sizeof(irq_resp), 0);  // GET_INT_STATUS

	if (ret)
	{
//		assert(false);
		return 0;
	}

	int flags = 0;
	if (irq_resp.ph_pend & (1 << 3))
		flags |= event_rx_err;
	if (irq_resp.ph_pend & (1 << 4))
		flags |= event_rx;
	if (irq_resp.ph_pend & (1 << 5))
		flags |= event_tx;
	return flags;
}

int si4463::get_modem_rssi()
{
  modem_stat_resp modem_resp;
	int ret = rw(0x20, 0, 0, &modem_resp, sizeof(modem_resp), 0);  // GET_INT_STATUS

	if (ret)
	{
		return -127;
	}

  return modem_resp.LATCH_RSSI;
}

bool si4463::clearRxFifo() {
  uint8_t fifo_clear[] = { 0x02 };
  return rw(0x15, fifo_clear, sizeof(fifo_clear),0,0,1000); //#define RH_RF24_CMD_FIFO_INFO                  0x15
}

int si4463::setModeIdle() {
  if (chip_state != SI_STATE_READY) {
    // Set the antenna switch pins using the GPIO, assuming we have an RFM module with antenna switch
    // uint8_t config[] = { RH_RF24_GPIO_HIGH, RH_RF24_GPIO_HIGH };
    // command(RH_RF24_CMD_GPIO_PIN_CFG, config, sizeof(config));

    uint8_t state[] = { SI_STATE_READY };
    rw(0x33, state, sizeof(state),0,0,1000); //#define RH_RF24_CMD_REQUEST_DEVICE_STATE       0x33
    chip_state = SI_STATE_READY;
  }
  return 0;
}

static size_t const set_prop_max_size    = 12;
static size_t const get_prop_max_size    = 16;

int si4463::set_property(uint16_t prop_addr, void const *ptr, size_t size, uint32_t timeout)
{
	uint8_t const group     = prop_addr >> 8;
	uint8_t const ofs_props = prop_addr;
	uint8_t const num_props = size;

	if (size > set_prop_max_size)
		return -1;//-EINVAL;

	uint8_t wbuf[3 + set_prop_max_size];
  wbuf[0] = group;
  wbuf[1] = num_props;
  wbuf[2] = ofs_props;
  memcpy(wbuf+3,ptr,size);
	return rw(0x11, wbuf, sizeof(group) + sizeof(num_props) + sizeof(ofs_props)
			+ size, NULL, 0, timeout);
}

int si4463::get_property(uint16_t prop_addr, void *ptr, size_t size,
		uint32_t timeout)
{
	uint8_t const group     = prop_addr >> 8;
	uint8_t const ofs_props = prop_addr;
	uint8_t const num_props = size;

	if (size > get_prop_max_size)
		return -1;

	uint8_t wbuf[3];
  wbuf[0] = group;
  wbuf[1] = num_props;
  wbuf[2] = ofs_props;

	return rw(0x12, wbuf, sizeof(wbuf), ptr, size, timeout);
}

void si4463::powerDown(){

  uint8_t write_buf[] = { 0x00, 0x00, 0x00 };
  write_cmd(0x20, write_buf, sizeof(write_buf),0); // GET_INT_STATUS
  sleep_usec(20);
	uint8_t wbuf[] = {1};
	write_cmd(0x34, wbuf, sizeof(wbuf), 0);//  write_tx(&hdr, sizeof(hdr));
}

void si4463::startListening(){
	uint8_t wbuf[7];
	wbuf[0]=0; // channel
	wbuf[1]=0; // start immidiately
	wbuf[2]=0; // use configured length
	wbuf[3]=0; // use configured length
  wbuf[4]=0; // timeout state
  wbuf[5]=0; // rx_valid state
  wbuf[6]=0; // rx_invalid state

  rw(0x32, wbuf, sizeof(wbuf), NULL, 0, 0);
}

int si4463::get_part_info(struct part_info *info, uint32_t timeout)
{
	int ret = rw(0x01, NULL, 0, info, sizeof(part_info), timeout);
	if (ret)
		return ret;
	return 0;
}
