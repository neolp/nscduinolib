#ifndef _ITMP_2_SERIAL_H_
#define _ITMP_2_SERIAL_H_

#include "itmp.h"
#include "usart.h"


//#define address_mode SLAVE_ADDRESS //this can be defined between "itmp.h" and "itmp_serial.h" including in main
#if !defined(ITMP_SERIAL_NO_ADDRESS) &&  !defined(ITMP_SERIAL_SLAVE_ADDRESS) && !defined(ITMP_SERIAL_MASTER_ADDRESS) && !defined(ITMP_SERIAL_TWO_ADDRESSES)
#error set address_mode in project options
#endif
class itmp_link_serial:public itmp_link
  {
  byte* __inbuf;
  int __inbuflen;
  usart* serial;
  int received;
  byte curcrc;
  byte lastchar;
  void write_byte(byte d,byte* crc);
public:
  itmp_link_serial(usart* _serial, byte* buf, int buflen):itmp_link(0,0)
    {
    serial=_serial;
    __inbuf=buf;
    __inbuflen=buflen;
    curcrc=0xFF;
    received=0;
    lastchar=0;
    //state = LINK_STATE_CONNECTED;
    }
  int read();

  virtual int readAndProcess(unsigned int timeout);
  virtual void send_frame(uint32_t tp, uint32_t da, const byte* header, int headerlen, const byte* body, int body_len);
  virtual void set_addr(byte addr);
  };

#endif