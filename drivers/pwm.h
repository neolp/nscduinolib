#ifndef _PWM_H_
#define _PWM_H_

class pwm
  {
  TIM_TypeDef * TIM;
  gpio_pin pwm_pin;
public:
  pwm():TIM(0){}
  bool isStarted() { return TIM != 0; }
  void begin(int port, int freq,int bits, int initvalue);
  void set(int value);
  int get();
  };

extern "C" { void TIM1_UP_TIM16_IRQHandler(void); }
extern "C" { void TIM1_TRG_COM_TIM17_IRQHandler(void); }

class softpwm_t
  {
friend void TIM1_UP_TIM16_IRQHandler(void);
friend void TIM1_TRG_COM_TIM17_IRQHandler(void);
  TIM_TypeDef * TIM;
  unsigned char pins[10];
  unsigned short values[10];

  unsigned short values_num;
  unsigned short index;

  static void IRQ_Handler(void* param);
  void set(unsigned int index);
  void reset(unsigned int index);
public:
  softpwm_t() { values_num=index=0; }
  void begin(TIM_TypeDef* SELTIM,int freq, int tickspersec);
  bool isconfigured();
  void addPin(int pin,unsigned short value);
  void removePin(int port);
  bool set_pwm(unsigned int pin,unsigned int value);
  };


#endif
