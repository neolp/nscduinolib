#include "stm32f10x.h"
#include "stm32f10x_extend.h"

#include "gpio-alloc.h"
#include "singlemotor.h"

int singlemotor_driver::begin()
  {
  RCC->APB2ENR |= RCC_APB2ENR_TIM16EN;
  TIM16->CR1 = TIM_CR1_ARPE | TIM_CR1_CEN;
  TIM16->CCMR1 = (TIM_CCMR1_OC1M & (TIM_CCMR_OCM_PWM_LOW_MATCH << 4)) | TIM_CCMR1_OC1PE;
  TIM16->ARR = 0x500;
  TIM16->CCER = TIM_CCER_CC1E;
  TIM16->BDTR = TIM_BDTR_AOE | TIM_BDTR_MOE;

  EN1.acquire(PORTB | PIN9,GPIO_IN_PULLUP);
  EN1.acquire(PORTB | PIN9,GPIO_IN_PULLUP);
  INA1.acquire(PORTB | PIN7,GPIO_OUT_PUSHPULL_50MHz);
  INB1.acquire(PORTB | PIN6, GPIO_OUT_PUSHPULL_50MHz);
  TIM16_CH1.acquire(PORTB | PIN8,GPIO_OUT_ALT_PUSHPULL_50MHz);
  EndSw.acquire(PORTB | PIN5, GPIO_OUT_PUSHPULL_50MHz); // end switch

  Enc.init();

  lastmeasuretime=get_us();
  trajectoryreset();

  return 0;
  }

int singlemotor_driver::end()
  {
  return -1;
  }
void singlemotor_driver::set_alpha(int alpha)
  {
  if (alpha==0)
    {
    INB1.reset();
    INA1.reset();
		//GPIO_INB1_REG->ODR &= ~GPIO_INB1_BIT;
		//GPIO_INA1_REG->ODR &= ~GPIO_INA1_BIT;
    }
  else if (alpha>0)
    {
    INB1.set();
    INA1.reset();
		//GPIO_INB1_REG->ODR |= GPIO_INB1_BIT;
		//GPIO_INA1_REG->ODR &= ~GPIO_INA1_BIT;
		TIM16->CCR1 = alpha;
    }
  else
    {
    INB1.reset();
    INA1.set();
		//GPIO_INB1_REG->ODR &= ~GPIO_INB1_BIT;
		//GPIO_INA1_REG->ODR |= GPIO_INA1_BIT;
		TIM16->CCR1 = -alpha;
    }
  }

void singlemotor_driver::set_pwm(int alpha)
  {
  mode=0;
  set_alpha(alpha);
  }


// max speed 10000 ticks per second (10 ticks per 1ms)

#define PIDmode 2
#define Pm (200)
#define Pd 20
#define Dm 200
#define Dd 20
#define Im 0
#define Id 16000

#define SPm 1
#define SPd 8
#define PID_Kd 1


void singlemotor_driver::setspeed(int s)
  {
  max_speed=s;
  }

void singlemotor_driver::set_acc(int a)
  {
  acc=a;
  }


void singlemotor_driver::go_to(int p)
  {
  if (mode==2)
    {
    }
  else
    {
    wheel.current_speed=0;
    wheel.calculated_pos_mm=wheel.actual_pos_mm;
    wheel.last_err=0;
    wheel.itegral_err=0;
    }
  int delta=abs((int)(p*1000-wheel.calculated_pos_mm));
  wheel.target_pos_mm=p*1000;
  wheel.acc=acc;
  wheel.max_speed=max_speed;
  wheel.brake=false;
  mode=2;
  }

void singlemotor_driver::set_sp(int sp)
  {
  wheel.max_speed=sp;
  if (mode==0)
    {
    wheel.current_speed=0;
    wheel.calculated_pos_mm=wheel.actual_pos_mm;
    wheel.last_err=0;
    wheel.itegral_err=0;
    }
  int delta=abs(sp);
  wheel.acc=acc;
  mode=1;
  }

void singlemotor_driver::trajectoryreset()
  {
  mode=0;

  max_speed=5000; // parameter
  acc=10000; // parameter

  pidreset(&wheel, max_speed, acc);
  }

void singlemotor_driver::pidreset(wheel_t* w, int speed, int acc)
  {
  w->target_pos_mm=0;
  w->calculated_pos_mm=0; // which position was should be in calculation time
//  w->calculated_pos_m=0; // which position was should be in calculation time
  w->current_speed=0;
  w->brake=false;

  w->max_speed=speed;//g_max_speed;
  w->acc=acc;//g_acc;


  w->last_err=0;
  w->itegral_err=0;
  w->last_actual_pos_mm=w->actual_pos_mm;
  w->measuredSpeed=0;
  w->pid_out=0;
  }

void singlemotor_driver::poll()
  {
  unsigned short enc=Enc.get();
  wheel.actual_pos_mm+=(int)1000 * (signed short)(last_Enc-enc);
  last_Enc=enc;
  unsigned int now=get_us();
  if ( now-lastmeasuretime > 10000 ) // it is 100 measurement per second (dt is about 10ms and 10000us)
    {
    wheel.measuredSpeed = ((signed int)( wheel.actual_pos_mm - wheel.last_actual_pos_mm )) / 10;
    wheel.last_actual_pos_mm = wheel.actual_pos_mm;

    if (mode==1)
      {
      sppoll(10, &wheel);
      }
    else if (mode==2)
      {
      trpoll(10, &wheel);
      }
    if (mode!=0)
      {
      set_alpha(wheel.current_speed * SPm / SPd + wheel.pid_out / PID_Kd );
      }
    lastmeasuretime+=10000;
    }
  }

void singlemotor_driver::trpoll(int dt, wheel_t* w)
	{
  int stopDistance_mm = (int)(1000UL*(long long)w->current_speed*w->current_speed/(w->acc*2)) + dt * abs(w->current_speed)*(1000/1000);
    // calculate distance to stop with maximum decceleration and one more time step with current speed
  int delta_mm = w->target_pos_mm - w->calculated_pos_mm; // calculate distance to target position

  if (delta_mm > 0) // if target is in forward direction
    {
    if ((delta_mm <= stopDistance_mm && w->current_speed >= 0) || w->current_speed > w->max_speed) // max_speed here is allways positive value
      {
      w->brake = w->current_speed <= w->max_speed;
      w->current_speed -= w->acc*dt/1000;
      if (w->current_speed <= 0)
        {
        if (delta_mm / dt <= w->max_speed)
          w->current_speed = delta_mm/dt;
        w->brake = false;
        }
      }
    else if (w->brake && w->current_speed>=0 )
      {
      w->current_speed -= w->acc * dt / (3*1000);
      if (w->current_speed <= 0)
        {
        if (delta_mm / dt <= w->max_speed)
          w->current_speed = delta_mm / dt;
        w->brake = false;
        }
      else
        {
        w->brake = true;
        }
      }
    else
      {
      w->brake = false;
      w->current_speed += w->acc*dt/1000; // if we can accelerate
      if (w->current_speed > w->max_speed) w->current_speed = w->max_speed; //limit speed to the max
      }
    }
  else if (delta_mm<0)
    {
    if ((-delta_mm <= stopDistance_mm && w->current_speed <= 0) || w->current_speed < -w->max_speed)
      {
      w->brake = w->current_speed >= -w->max_speed;
      w->current_speed += w->acc * dt / 1000;
      if (w->current_speed >= 0)
        {
        if (-delta_mm / dt <= w->max_speed)
          w->current_speed = delta_mm / dt;
        w->brake = false;
        }
      }
    else if (w->brake && w->current_speed <= 0)
      {
      w->current_speed += w->acc * dt / (3 * 1000);
      if (w->current_speed >= 0)
        {
        if (-delta_mm / dt <= w->max_speed)
          w->current_speed = delta_mm / dt;
        w->brake = false;
        }
      else
        {
        w->brake = true;
        }
      }
    else
      {
      w->brake = false;
      w->current_speed -= w->acc*dt/1000; // if we can accelerate
      if (w->current_speed < -w->max_speed) w->current_speed = -w->max_speed; //limit speed to the max
      }
    }
  else
    {
    if (stopDistance_mm < 10*1000) w->current_speed = 0;
    }

  // trajectory new speed are calculated
  pidpoll(dt,w);
  w->calculated_pos_mm += dt * w->current_speed;
  }

void singlemotor_driver::sppoll(int dt, wheel_t* w)
	{
  if (w->max_speed>=0)
    {
    if (w->current_speed > w->max_speed)
      {
      w->current_speed -= w->acc * dt / 1000;
      if (w->current_speed < w->max_speed) w->current_speed = w->max_speed; //limit speed to the max
      }
    else
      {
      w->current_speed += w->acc*dt/1000; // if we can accelerate
      if (w->current_speed > w->max_speed) w->current_speed = w->max_speed; //limit speed to the max
      }
    }
  else
    {
    if (w->current_speed < w->max_speed)
      {
      w->current_speed += w->acc * dt / 1000;
      if (w->current_speed > w->max_speed) w->current_speed = w->max_speed; //limit speed to the max
      }
    else
      {
      w->current_speed -= w->acc*dt/1000; // if we can accelerate
      if (w->current_speed < w->max_speed) w->current_speed = w->max_speed; //limit speed to the max
      }
    }

  // trajectory new speed are calculated
  pidpoll(dt,w);
  w->calculated_pos_mm += dt * w->current_speed;
  }

void singlemotor_driver::pidpoll(int dt, wheel_t* w)
	{
  int err = w->calculated_pos_mm - w->actual_pos_mm;
  if (err>0) err/=1000;
  else if (err<0) err=-((-err)/1000);
#if iM>0
  w->itegral_err += err;
#define MAX_I_ERR   ((972/4)*PID_Kd*Id/Im)
  if (w->itegral_err > MAX_I_ERR) w->itegral_err = MAX_I_ERR;
  if (w->itegral_err < -MAX_I_ERR) w->itegral_err = MAX_I_ERR;
#endif
  w->pid_out = Pm * err / Pd  -  Dm * (w->last_err-err) / Dd  +  Im * w->itegral_err / Id;
  if (w->pid_out>972*PID_Kd) w->pid_out=972*PID_Kd;
  if (w->pid_out<-972*PID_Kd) w->pid_out=-972*PID_Kd;
  w->last_err=err;
	}
