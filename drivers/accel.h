#ifndef _ACCELEROMETER_H_
#define _ACCELEROMETER_H_

#include "sys.h"

typedef struct
  {
    int x;
    int y;
    int z;
  } Accelerations_t;

class Accelerometer
{
  public:
    virtual bool GetValues(Accelerations_t &data) = 0;
};

#endif /* _ACCELEROMETER_H_ */
