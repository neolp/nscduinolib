#ifndef _MOTOR_H_
#define _MOTOR_H_

#include "tim.h"
/*
struct pid_t
  {
//  int acc;

  int last_err;
  int itegral_err;

  int measuredSpeed;
  int last_actual_pos; //for speed measurement

  int start_pos;
//  int calculated_pos;
//  int calculated_speed;
//  long long calculated_pos_m; // which position was should be in calculation time

//  int current_speed;
//  int target_speed;
//  int target_pos;

  int pid_out; // pid regulator out
  };
*/
struct wheel_t
  {
  unsigned int actual_pos_mm;
  unsigned int target_pos_mm;

  unsigned int calculated_pos_mm; // which position was should be in calculation time
  int current_speed;
  bool brake;

  int max_speed; // parameter
  int acc; // parameter

  //long long calculated_pos_m; // which position was should be in calculation time

  int last_err;
  long long itegral_err;

  int pid_out; // pid regulator out

  int measuredSpeed;
  unsigned int last_actual_pos_mm; //for speed measurement
  };

class motor823BLDC_driver
  {
  gpio_pin EN1;
  gpio_pin INA1;
  gpio_pin TIM15_CH2;
  gpio_pin INB1;

  gpio_pin EN2;
  gpio_pin INA2;
  gpio_pin TIM15_CH1;
  gpio_pin INB2;

  int pos;
  int H1;
  int H2;
  int H3;

  void setphases(int sp);

//  void pidpoll(int dt, pid_t* w, int pos, pid_t* sw, int spos);
  void trajectoryreset();
  void pidreset(wheel_t* w, int speed);
  void trpoll(int dt, wheel_t* w);
  void sppoll(int dt, wheel_t* w);
  void pidpoll(int dt, wheel_t* w);

  public:
  wheel_t left;
  wheel_t right;
  int g_max_speed; // parameter
  int g_acc; // parameter
  //trajectory tr;
  int mode;
  unsigned int lastmeasuretime;
  unsigned short last_leftEnc;
  unsigned short last_rightEnc;
  T2Encoder leftEnc;
  T3Encoder rightEnc;

  int begin();//int pwm_freq,int
  int end();//int pwm_freq,int
  void set_pwm(int alpha,int beta);
  void set_alpha(int alpha);
  void set_beta(int beta);
  void setspeed(int s);
  void set_acc(int a);
  void set_sp(int lsp, int rsp);
  void go_to(int l, int r);
  void poll();
  };

#endif