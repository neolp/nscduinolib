#ifndef _ITMP_H_
#define _ITMP_H_

#include <tim.h>
#include <string.h>
#include "md5.h"


#include <intrinsics.h>

#define htons(n) (__REV16(n))
#define ntohs(n) htons(n)

#define htonl(n) (__REV(n))
#define ntohl(n) htonl(n)

#define htonll(n) (htonl(n>>32) | (unsigned long long)(htonl(n & 0xFFFFFFFF))<<32)
#define ntohll(n) htonll(n)

#define ALLOW_SUBSCRIPTIONS

#ifdef ALLOW_SUBSCRIPTIONS
#define MAX_SUBS_NUM 3
struct subscription{
  char wildcard[8];
  char addr;
};
#endif

/*

<field description>::=<name><description><signature>
<name>::=string
<signature>::=<interface>|<func>|<event>|<container>
<interface>::=�:�[<interface name>[,<interface name>]]
<container>::=�*�[<interface name>[,<interface name>]]
<func>::=�&�<ret type list>[�(�<argument type list>�)�]
<event>::=�!�<type list>
<interface name>::=string
<ret type list>::=<type list>
<argument type list>::=<type list>
<type list>::=[<type>[,<type>]]
<type>::=<simple type>|a<simple type>
<simple type>::=<type char>[<description>]
<description>::='{'[<name>][<version>][<UniqueId>] [<units>][<variants>][<manufacturer>]'}'
<name>::=string
<version>::=�%�string
<UniqueId>::=�#�string
<units>::='$'string
<variants>::='['<value>[,<value>]']'
<manufacturer>::='@'string
<type char>::=b|n|q|i|u|x|t|N|Q|I|U|X|T|f|d|s


primitive types
b  8-bit unsigned integer (byte)
n  16-bit signed integer
q  16-bit unsigned integer
i  32-bit signed integer
u  32-bit unsigned integer
x  64-bit signed integer
t  64-bit unsigned integer
N  16-bit packed signed integer
Q  16-bit packed unsigned integer
I  32-bit packed signed integer
U  32-bit packed unsigned integer
X  64-bit packed signed integer
T  64-bit packed unsigned integer
f  single-precision floating point (IEEE 754)
d  double-precision floating point (IEEE 754)
s  UTF-8 string with length prefix


+------+------------------------------+---------+
| Code | Description                  | encoded |
+------+------------------------------+---------+
| 1.02 | Processing                   |    102   |
| 4.00 | Bad Request                  |    400   |
| 4.01 | Unauthorized                 |    401   |
| 4.02 | Bad Option                   |    402   |
| 4.03 | Forbidden                    |    403   |
| 4.04 | Not Found                    |    404   |
| 4.05 | Method Not Allowed           |    405   |
| 4.06 | Not Acceptable               |    406   |
| 4.12 | Precondition Failed          |    412   |
| 4.13 | Request Entity Too Large     |    413   |
| 4.15 | Unsupported Content-Format   |    415   |
| 5.00 | Internal Server Error        |    500   |
| 5.01 | Not Implemented              |    501   |
| 5.02 | Bad Gateway                  |    502   |
| 5.03 | Service Unavailable          |    503   |
| 5.04 | Gateway Timeout              |    504   |
| 5.05 | Proxying Not Supported       |    505   |
| 5.06 | call exception               |    506   |
+------+------------------------------+---------+

*/

typedef unsigned char byte;

#define BROADCAST_ADDRESS 0xFFFFFFFF
struct itmpframe
  {
  unsigned int DA;
  unsigned int SA;
  byte* _body;
  byte body_len;
  };

#define CONTINUE_PROCESSING 1
#define STOP_PROCESSING 0

class CBOR_encoder;
class CBOR_decoder;

typedef int (*itmp3handler) (const char* url, CBOR_decoder& dec, CBOR_encoder& enc, void* data);
//typedef int (*call_func)(const char* url, CBOR_decoder& dec, CBOR_encoder& enc, void* data);
typedef int (*descr_func)(const char* url, byte* output);

/*
struct iface {
  const char* name;
  int namelen;
  const char* desc;
  itmp3handler handler;
};
*/

struct topic_descr
  {
  unsigned short type;
  unsigned short namelen;
  const char* name;
  union {
    const char* descr;
    const descr_func dfunc;
  };
//  union {
    const itmp3handler handler;
//    const call_func call_handler;
//  };
  void* data;
  };

typedef const topic_descr* (*gettopic_func)(const byte* uri);

#define ITMP_TOPIC_TYPE_EXACT 0
#define ITMP_TOPIC_TYPE_MASK 1

#define ITMP_TABLE_START(_descr) \
  static const topic_descr topics[]={ {.type=ITMP_TOPIC_TYPE_EXACT,  .name="",      .namelen=0, .descr=_descr, .handler=0},

#define ITMP_EXPORTN(exportname,_descr,fname,_data) \
  {.type=ITMP_TOPIC_TYPE_EXACT, .name=exportname, .namelen=sizeof(exportname)-1, .descr=_descr, .handler=fname, .data=_data},

#define ITMP_EXPORTMASK(exportname,_descr,fname,_data) \
  {.type=ITMP_TOPIC_TYPE_MASK, .name=exportname, .namelen=sizeof(exportname)-1, .descr=_descr, .handler=fname, .data=_data},

#define ITMP_TABLE_END() \
  };

#define ITMP_TOPICS topics
#define ITMP_TOPICS_NUM (sizeof(topics)/sizeof(topic_descr))


/*
#define ITMP_DEVICE(_descr) \
  const char itmp_dev_get_mem[1]={0}; \
  int itmp_dev_get (const char* url, CBOR_decoder& dec, CBOR_encoder& enc); \
  __root const topic_descr fname##ext @ "externs" ={.type=2, .namelen =0, .name=itmp_dev_get_mem,.descr=_descr,.handler=itmp_dev_get}; \
  int itmp_dev_get (const char* url, CBOR_decoder& dec, CBOR_encoder& enc)

#define ITMP_EXPORT(fname,_descr) \
  int fname (CBOR_decoder& dec, char* result, int maxresultlen); \
  __root const topic_descr fname##ext @ "externs" ={.type=0, .name=(const byte*)&fname##mem,.descr=(const byte*)&fname##desc,.func=(rpc_func)fname}; \
  int fname (CBOR_decoder& dec, char* result, int maxresultlen)
#define ITMP_EXPORTN(exportname,_descr,fname) \
  int fname (const char* url, CBOR_decoder& dec, CBOR_encoder& enc); \
  __root const topic_descr fname##ext @ "externs" ={.type=0, .namelen=sizeof(exportname)-1, .name=exportname,.descr=_descr,.handler=fname}; \
  int fname (const char* url, CBOR_decoder& dec, CBOR_encoder& enc)

#define ITMP_EXPORT_DIR(exportname,_descr,fname) \
  struct fname##enc { byte len; const char nm[sizeof(exportname)]; }; \
  const fname##enc fname##mem={sizeof(exportname)-1, exportname}; \
  struct fname##descenc { byte len; const char desc[sizeof(_descr)]; }; \
  const fname##descenc fname##desc={sizeof(_descr)-1, _descr}; \
  int write_topic_list (const char* url, CBOR_decoder& dec, CBOR_encoder& enc); \
  __root const topic_descr fname##ext @ "externs" ={.type=0, .namelen =sizeof(_descr)-1, .name=(const byte*)&fname##mem,.descr=_descr, .func=write_topic_list}; \

*/

//int write_topic_list(const char* url, CBOR_decoder& dec, CBOR_encoder& enc);

//#define EXPORT_IMPL_START(name) const topic_descr name[]={
//#define EXPORT_IMPL(nm) {.type=0, .name=(const byte*)&nm##mem,.descr=(const byte*)&nm##desc,.func=(rpc_func)nm},
//#define EXPORT_IMPL_END() };


//ITMP_EXPORT(function_name,string_description)
//ITMP_EXPORT_N(export_name,function_name,string_description)
//ITMP_EXPORT_D(get_fg_dev,func_descr)
//ITMP_EXPORT_ND("export_name",get_fg_dev,func_descr)



#define LINK_STATE_DOWN           0
#define LINK_STATE_ADDRESSED      1
#define LINK_STATE_GOT_RANDOM     0x08
#define LINK_STATE_SECURED        2
#define LINK_STATE_ACCEPTED       4
#define LINK_STATE_CONNECTED      7

#define ITMP_DEVICE_DEFAULT_STATE     0
#define ITMP_DEVICE_COORDINATOR       1
#define ITMP_DEVICE_NODE              0
#define ITMP_DEVICE_REPEATER          2
#define ITMP_DEVICE_BRIDGE            4
#define ITMP_DEVICE_ADDRESSED         8

#define LOPAN_HEADER_TYPE_ITMP        0x20


//#define MAX_LINK_NUMBER 16


#define FEATURE_CONFIGURABLE_ADDRESS      0x01
#define FEATURE_READ_ABILITY              0x02
#define FEATURE_SECURITY_ABILITY          0x04
#define FEATURE_LONG_TERM_SECRET_PRESENT  0x08

#define PACKET_NOT_FOR_US (-1)
#define PACKET_ACCEPTED 1
#define PACKET_ACCEPTED_ANSWER_SENT 3
#define PACKET_UNKNOWN_TYPE (-2)
#define PACKET_SIGNATURE_ERROR (-3)
#define PACKET_DUPLICATE (-4)
#define TIME_OUT (-5)
#define MESSAGE_ENCODE_ERROR (-6)

class itmp_core;

typedef unsigned char byte;

class itmp_peer {
public:
  uint64_t ID;
  union {
    uint64_t dword;
    uint32_t words[2];
  } nonce;
  uint32_t snd_seq; // The sequence number that was last sent by us.
  uint32_t rcv_seq;  // The sequence number that we expect to receive next.

  int32_t rssi;

  uint32_t err_reconnect;
  uint32_t err_dup;
  uint32_t err_lost;
  uint32_t err_sign;

  uint32_t stk[4]; // short term key
  uint32_t send_time;
  uint32_t receive_time;
  uint32_t roundtrip_time;
  byte state;
  byte peer_addr;
  byte peer_opt;
};

class itmp_link {
friend class itmp_core;
protected:
  itmp_peer master;
  byte joined;
  byte connected_state;
  byte _my_addr;
  byte coordinator_addr;

	gpio_pin* linkPin;
	gpio_pin* actPin;

#ifdef ALLOW_SUBSCRIPTIONS
  int subs_num;
  subscription subs[MAX_SUBS_NUM];
#endif

  union {
    uint8_t bytes[8];
    uint32_t words[2];
    uint64_t dword;
  } my_random;
  uint32_t LTK[4];

  itmp_core* core;

  virtual void set_addr(byte addr);

  itmp_link(gpio_pin* _linkPin, gpio_pin* _actPin) {
    //itmp_peer master;
    joined=0;
    connected_state=0;
    //my_state=0;
    _my_addr=0;
    coordinator_addr=0;
    linkPin = _linkPin;
    actPin = _actPin;
#ifdef ITMP_COUNTERS
    err_crc = 0;
    err_underflow = 0;
    err_overflow = 0;
    ok_frame = 0;
#endif
  }

#ifdef ITMP_COUNTERS
  uint32_t err_crc;
  uint32_t err_underflow;
  uint32_t err_overflow;
  uint32_t ok_frame;
#endif


  bool isConnected();
  void disconnect();
  virtual int readAndProcess(unsigned int timeout)=0;
  void send_frame(uint32_t tp, uint32_t da, const byte* header, int headerlen) {
    send_frame(tp, da, header, headerlen, 0,0);
  }
  virtual void send_frame(uint32_t tp, uint32_t da, const byte* header, int headerlen, const byte* body, int body_len)=0;
  int process_frame_LCP(uint8_t sa, byte* buf, int buflen, int rssi);

  void process_frame(uint32_t type, byte* buf, int buflen);
  public:
  bool sendConnect(int rssi=-127);
  void setRandom(unsigned long long rnd){
    my_random.dword = rnd;
  }
  int getMasterRssi() {
    return master.rssi;
  }
public:
  void set_LTK(byte* id);
#ifdef ITMP_COUNTERS
// Define "ITMP_COUNTERS" in project options for use this 
  unsigned int ItmpCounterErrors(void){return err_crc + err_underflow + err_overflow;}
  unsigned int ItmpCounterOkFrames(void){return ok_frame;}
#endif  
};


/*
class itmp_link
  {
friend class itmp_core;
protected:
  byte features;
  byte my_addr; // device address
  byte coordinator_addr; // coordinator of micronet (coordinator distribute addresses and security parameters)

  byte state;
public:
  byte get_addr() { return my_addr; }
//  unsigned int lasttm;
public:
  itmp_link()
    {
//    lasttm=get_ms();
    state=0;
    my_addr=0;
    coordinator_addr=0;
    }
  void set_addr(int addr, int from_coordinator)
    {
    my_addr=addr;
    coordinator_addr=from_coordinator;
    state|=LINK_STATE_ADDRESSED;
    }

  virtual int read(itmpframe* inbuf)=0; // return nonzero if it has got a frame
  virtual void write_command(int DA, int SA, const byte* header, int headerlen, const byte* body, int body_len)=0;
  };
*/

/*
struct route
  {
  int base;
  int mask;
  itmp_link* to;
  };

class itmp_router
  {
  route routes[2];
  int routes_num;
public:
  itmp_router() { routes_num=0; }
//  void add_default_route(itmp_link* to);
  void add_route(byte base, byte mask, itmp_link* to);
//  if ((tp&0xE0) == 0x20) { process_LCP_frame(from, DA, SA, buf+1, buflen-1); return; } // it is LCP message
  int process_frame(itmp_link* from, itmpframe* buf);

  };
*/
/*
class itmp_core
  {
  friend class itmp_link;
public:
  itmp_core()
    {
    links_num=0;
    itmp_state=0;
    }
  int process_frame(itmp_link* from, itmpframe* buf);
  int process_frame3(itmp_link* from, itmpframe* inbuf);
  byte DeviceID[8];  // LCP_OPT_RAND
  byte DeviceID_len;

private:
  byte itmp_state;

//  const char* devdescr;
//  int devdescr_len;
//  const topic_descr* topics;
//  int topics_num;
public:
//  byte rand[8];  // LCP_OPT_RAND

protected:
  const topic_descr* gettopic(const byte* uri);
public:
  itmp_link* links[2];
  int links_num;
  void add_link(itmp_link* lnk)
    {
    if ( links_num >= sizeof(links)/sizeof(itmp_link*) ) return;
    links[links_num]=lnk;
    links_num++;
    }
  void begin(int features);
  void process();
  };
*/

/*
class itmp_node:public itmp_core
  {
  friend class itmp_link;

  void process_LCP_frame(itmp_link* from, int DA, int SA, byte* buf, int buflen);

public:
  void sendconnect();
  itmp_link* main_link;
  itmp_node()
    {
    node_state=0;
    node_addr=0;
    coordinator_addr=0;
    }
  void process_frame(itmp_link* from, int index, byte* buf, int buflen); // !!!


//  itmp_peer known_peers[MAX_LINK_NUMBER];
//  byte known_peers_hash[MAX_LINK_NUMBER*2];
//  int known_peers_num;

private:
  byte node_state;
  byte node_addr;
  byte coordinator_addr;


//  int process_LCP_CONNECT_option(itmp_link* link, itmp_peer* peer, byte* buf);
//  int process_LCP_CONNECT_PARAMETERS_option(itmp_link* link, int SA, byte* buf);

//  void write_command(int DA, int itmp_tp,const byte* header, int headerlen, const byte* body, int body_len);
//  void process_itmp_frame(int DA, int SA, int tp, byte* uri, int urilen, int payloadlen); // !!!
//  void process_LCP_frame(itmp_link* from, int index, byte* buf, int buflen);


//  byte my_addr;
  void begin(const topic_descr* _topics, int _topic_num);
  void poll(); // !!!
  };

class itmp_coord:public itmp_core
  {
  friend class itmp_link;

public:
  void sendconnect();
  itmp_link* main_link;
  itmp_link* radio;
  itmp_coord()
    {
    node_state=0;
    node_addr=0;
    coordinator_addr=0;
    }
//  void process_frame(itmp_link* from, int index, byte* buf, int buflen); // !!!


//  itmp_peer known_peers[MAX_LINK_NUMBER];
//  byte known_peers_hash[MAX_LINK_NUMBER*2];
//  int known_peers_num;

private:
  byte node_state;
  byte node_addr;
  byte coordinator_addr;


//  int process_LCP_CONNECT_option(itmp_link* link, itmp_peer* peer, byte* buf);
//  int process_LCP_CONNECT_PARAMETERS_option(itmp_link* link, int SA, byte* buf);

//  void write_command(int DA, int itmp_tp,const byte* header, int headerlen, const byte* body, int body_len);
//  void process_itmp_frame(int DA, int SA, int tp, byte* uri, int urilen, int payloadlen); // !!!
//  void process_LCP_frame(itmp_link* from, int index, byte* buf, int buflen);

  int process_frame(itmp_link* from, itmpframe* buf);

//  byte my_addr;
  void begin(const topic_descr* _topics, int _topic_num);
  void poll(); // !!!
  };
*/

#define ITMP_HEADER_METHOD_MASK   0xC0
#define ITMP_HEADER_METHOD_LCP    0x00
#define ITMP_HEADER_METHOD_DESCR  0x40
#define ITMP_HEADER_METHOD_GET    0x80
#define ITMP_HEADER_METHOD_PUB    0xC0

#define ITMP_HEADER_T_MASK        0x30
#define ITMP_HEADER_NON           0x00
#define ITMP_HEADER_CON           0x10
#define ITMP_HEADER_ACK           0x20
#define ITMP_HEADER_ERR           0x30

#define ITMP_HEADER_MID_LEN       0x03
#define ITMP_HEADER_MID_LEN_1     0x01
#define ITMP_HEADER_MID_LEN_2     0x02
#define ITMP_HEADER_MID_LEN_4     0x03


























#ifndef __ITMP3_H__
#define __ITMP3_H__

#include <vector>
// Connection
#define ITMP_TP_CONNECT 0             // [CONNECT, Realm|uri, Details|dict] 	open connection
#define ITMP_TP_CONNECTED 1           // [CONNECTED, Session|id, Details|dict] 	confirm connection
#define ITMP_TP_ABORT 2               // [ABORT, Code|integer, Reason|string, Details|dict] 	terminate connection
#define ITMP_TP_DISCONNECT 3          // [DISCONNECT, Code|integer, Reason|string, Details|dict] 	clear finish connection
//	Information
#define ITMP_TP_KEEP_ALIVE 4          // [KEEP_ALIVE] keep alive
#define ITMP_TP_ERROR 5               // [ERROR, Request|id, Code|integer, Reason|string, Details|dict] 	error notificarion
//	Description
#define ITMP_TP_DESCRIBE 6            // [DESCRIBE, Request|id, Topic|uri, Options|dict] 	get description
#define ITMP_TP_DESCRIPTION 7         // [DESCRIPTION, DESCRIBE.Request|id, description|list, Options|dict] 	description response
//	RPC
#define ITMP_TP_CALL 8                // [CALL, Request|id, Procedure|uri, Arguments, Options|dict] 	call
#define ITMP_TP_RESULT 9              // [RESULT, CALL.Request|id, Result, Details|dict] 	call response
//	RPC Extended
#define ITMP_TP_ARGUMENTS 10          // [ARGUMENTS, CALL.Request|id, Arguments, Options|dict] 	additional arguments for call
#define ITMP_TP_PROGRESS 11           // [PROGRESS, CALL.Request|id, Result, Details|dict] 	call in progress
#define ITMP_TP_CANCEL 12             // [CANCEL, CALL.Request|id, Details|dict] 	call cancel
//	publish
#define ITMP_TP_EVENT 13              // [EVENT, Request|id, Topic|uri, Arguments, Options|dict] 	event
#define ITMP_TP_PUBLISH 14            // [PUBLISH, Request|id, Topic|uri, Arguments, Options|dict] 	event with acknowledge awaiting
#define ITMP_TP_PUBLISHED 15          // [PUBLISHED, Request|id, Publication|id, Options|dict] 	event acknowledged
//	subscribe
#define ITMP_TP_SUBSCRIBE 16          // [SUBSCRIBE, SUBSCRIBE.Request|id, Topic|uri, Options|dict] 	subscribe
#define ITMP_TP_SUBSCRIBED 17         // [SUBSCRIBED, Request|id, SubscriptionId|id, Options|dict] 	subscription confirmed
#define ITMP_TP_UNSUBSCRIBE 18        // [UNSUBSCRIBE, Request|id, Topic|uri, Options|dict]
#define ITMP_TP_UNSUBSCRIBE_BY_ID 19  // [UNSUBSCRIBE_BY_ID, Request|id, SUBSCRIBED.SubscriptionId|id, Options|dict] 	unsibscribe
#define ITMP_TP_UNSUBSCRIBED 20       // [UNSUBSCRIBED, UNSUBSCRIBE.Request|id, Options|dict]
//	anounce
#define ITMP_TP_ANOUNCE 21 // [ANOUNCE, Request|id, Topic|uri, description|list, Options|dict] 	announce interface or event
#define ITMP_TP_ACCEPTED 22 // [ACCEPTED, ANOUNCE.Request|id, Options|dict] 	accept announcement


#define CBOR_TAG_MASK 0xE0

#define CBOR_PINT_TAG 0x00
#define CBOR_NINT_TAG 0x20
#define CBOR_BSTR_TAG 0x40
#define CBOR_STR_TAG  0x60
#define CBOR_ARR_TAG  0x80
#define CBOR_MAP_TAG  0xA0
#define CBOR_TAG_TAG  0xC0
#define CBOR_SMPL_TAG 0xE0

#define CBOR_PINT_MT 0
#define CBOR_NINT_MT 1
#define CBOR_BSTR_MT 2
#define CBOR_STR_MT  3
#define CBOR_ARR_MT  4
#define CBOR_MAP_MT  5
#define CBOR_TAG_MT  6
#define CBOR_SMPL_MT 7

#define CBOR_FALSE_TAG 0xF4
#define CBOR_TRUE_TAG 0xF5
#define CBOR_NULL_TAG 0xF6
#define CBOR_UNDEF_TAG 0xF7

#define CBOR_FLOAT16_TAG 0xF9
#define CBOR_FLOAT32_TAG 0xFA
#define CBOR_FLOAT64_TAG 0xFB
#define CBOR_BREAK_TAG 0xFF

#define CBOR_LEN_TAG_1  0x18
#define CBOR_LEN_TAG_2  0x19
#define CBOR_LEN_TAG_4  0x1A
#define CBOR_LEN_TAG_BREAK  0x1F


//400 Bad Request (�������, �������� ������)
//401 Unauthorized (���������������)
//403 Forbidden (����������)[2][3].
//404 Not Found (��� �������)[2][3].
//405 Method Not Allowed (������ �� ���������������)[2][3].
//406 Not Acceptable (������������)[2][3].
//413 Request Entity Too Large (������� ������� ������� �����)[2][3].
//414 Request-URI Too Large (�������������� URI ������� �������)[2][3].
//418 I'm a teapot (�� - ������)[8].
//429 Too Many Requests (�������� ����� ��������)[10].
//419 Format error
//420 Type error
//421

#define FORMAT_ERROR_CODE 419
#define TYPE_ERROR_CODE 420

//500 Internal Server Error (����������� ������ �������)[2][3].
//501 Not Implemented (��� �����������)[2][3].
#define INSUFFICIENT_STORAGE_CODE 507 //Insufficient Storage (������������� ���������).




typedef int (*itmpsendevent) (const char* msg, int msglen, void* data);


/// itmp connection for event emission
struct itmpcon {
  itmpsendevent send;
  void* data;
};

class itmp_core
  {
  friend class itmp_link;
public:
  itmp_core() {
    links_num=0;
//    itmp_state=0;
  }
  uint64_t DeviceID;

private:
  const topic_descr* topics;
  int topics_num;
//  byte itmp_state;

//  const char* devdescr;
//  int devdescr_len;
//  const topic_descr* topics;
//  int topics_num;
public:

protected:
  const topic_descr* gettopic(const byte* uri);
public:
  itmp_link* links[2];
  int links_num;
  int add_link(itmp_link* lnk)
    {
    if ( links_num >= sizeof(links)/sizeof(itmp_link*) ) return -1;
    lnk->core=this;
    int ret=links_num;
    links[links_num]=lnk;
    links_num++;
    return ret;
    }

  bool isLinkConnected(int linkindex=-1);
  bool isConnected();
  bool sendConnect();
  void disconnect(int linkindex=-1);
  int readAndProcess(unsigned int usec, int linkindex=-1);


  void begin(int features, uint64_t id , const topic_descr* _topics, int _topic_num/*, const char* _devdescr*/);

  int process_describe(itmp_link* from, unsigned int addr, CBOR_decoder& dec, char* resp, int resplen);
  int process_call(itmp_link* from, unsigned int addr, CBOR_decoder& dec, char* resp, int resplen);
  int process_event(itmp_link* from, unsigned int addr, CBOR_decoder& dec, char* resp, int resplen);
  int process_publish(itmp_link* from, unsigned int addr, CBOR_decoder& dec, char* resp, int resplen);


#ifdef ALLOW_SUBSCRIPTIONS

  int process_subscribe(itmp_link* from, unsigned int addr, CBOR_decoder& dec, char* resp, int resplen);
  int process_unsubscribe(itmp_link* from, unsigned int addr, CBOR_decoder& dec, char* resp, int resplen);

  int is_someone_subscribed(const char* uri);
  int testwildcard(const char* uri, int urilen);
  bool test_uri_for_subscription(const char* uri, subscription* sub);
  int emit_event(const char* uri, const char* body, int bodylen);
  int emit_event_1(const char* uri, const char* name, int val);
  int emit_event_1(const char* uri, const char* name, const char* val);

#endif
//  int process_responce(itmp_link* from, unsigned int addr, int tp, CBOR_decoder& dec, char* resp, int resplen);

  int write_itmp_err(char* buf, int buflen, int tp, int id, int code, const char* str);
  int write_itmp_event(char* buf, int buflen, const char* topic, const char* body, int bodylen);
  int write_itmp_root_descr(char* buf, int buflen, uint64_t id);
  int write_itmp_shortresp(char* buf, int buflen, int tp, int id);
  int process_itmp_frame(itmp_link* from, unsigned int addr, const char* msg, int msglen);//, char* resp, int resplen);

//  int process_frame(itmp_link* from, uint32_t type, byte* buf, int buflen);

  int32_t send_event(itmp_link* to, const char* topic, void* data, uint32_t data_len);

  int32_t send_event_int(const char* topic, int val);
  int32_t send_event_str(const char* topic, const char* val);
  int32_t send_event_bstr(const char* topic, const char* val, int vallen);
  };

/*
class itmp3 {
  uint32_t itmp_sequence_nb;
//  vector<iface> ifaces;
  vector<itmpcon> cons;
  char* event_buf;
  int event_buf_len;
public:
  itmp3() { itmp_sequence_nb=0; // vent_buf=0; event_buf_len=0;
 }
  int adduri(const char* uri, const char* desc, itmp3handler handler);
  int write_itmp_event(char* buf, int buflen, const char* topic, const char* body, int bodylen);
  int write_itmp_root_descr(char* buf, int buflen, uint64_t id);
  int process_itmp3(const char* msg, int msglen, char* resp, int resplen);
  int add_conection(itmpsendevent func, void* data);
  int del_conection(itmpsendevent func, void* data);
  int emit_event(const char* uri, const char* body, int bodylen);
};
*/
#endif



#endif
