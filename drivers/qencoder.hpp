#ifndef __QENCODER_HPP__
#define __QENCODER_HPP__

#include "gpio-alloc.h"

class QEncoder
  {
  gpio_pin A;
  gpio_pin B;
  unsigned int last;
  bool lastB;
  unsigned int encoderPos;
  unsigned int errorsCnt;
  public:
    QEncoder():encoderPos(0),errorsCnt(0){}
    void init(int pinA, int pinB) {
      A.acquire(pinA, GPIO_IN_PULLUP);
      B.acquire(pinB, GPIO_IN_PULLUP);
    }
    unsigned int get() {
      return encoderPos;
    }
    unsigned int getErrors() {
      return errorsCnt;
    }
  
  int poll() {
    int ret = 0;
    unsigned int current = A.read() | (B.read()<<1);
    unsigned int transition = last | current;
    switch (transition) {
      case 0x00: // A_ stay low B stay low   NOTHING CHANGES
      case 0x05: // A^ stay high B stay low  NOTHING CHANGES
      case 0x0a: // A_ stay low B stay high  NOTHING CHANGES
      case 0x0f: // A^ stay high B stay high NOTHING CHANGES
        break;
      case 0x01: // A+ goes high B stay low  +
      case 0x07: // A^ stay high B goes high +
      case 0x08: // A_ stay low B goes low   +
      case 0x0e: // A- goes low B stay high  +
        encoderPos++;
        ret = 1;
        break;
      case 0x02: // A_ stay low B goes high  -
      case 0x04: // A- goes low B stay low   -
      case 0x0b: // A+ goes high B stay high -
      case 0x0d: // A^ stay high B goes low  -
        encoderPos--;
        ret = -1;
        break;
      case 0x03: // A+ goes high B goes high DOUBLE CHANGES
      case 0x06: // A- goes low B goes high  DOUBLE CHANGES
      case 0x09: // A+ goes high B goes low  DOUBLE CHANGES
      case 0x0c: // A- goes low B goes low   DOUBLE CHANGES
        errorsCnt++;
        break;
    }
    last = current << 2;
    return ret;
  }
};

#endif

