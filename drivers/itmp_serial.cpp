#include "itmp_serial.h"
#include "crc.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////// itmp_link_serial ///////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define FRAME_DELIMETER			0x7E
#define FRAME_REPLACEMENT		0x7D

#define SERIAL_ADDR_BROADCAST  255
#define SERIAL_ADDR_UNKNOWN    0



int itmp_link_serial::read()
  {
  while (serial->available()>0)
    {
    byte c=serial->read();
    if (c==FRAME_DELIMETER)
      {
      int ret=0;
      if (received>=2) {
        if (curcrc==__inbuf[received-1]) {
          ret=received-1;
#ifdef ITMP_COUNTERS
          ok_frame++;
#endif
        } else {
#ifdef ITMP_COUNTERS
          err_crc++;
#endif
        }
      } else {
#ifdef ITMP_COUNTERS
        err_underflow++;
#endif
      }
      received=0; curcrc=0xFF; lastchar=0;
      return ret;
/*      int ret=0;
      if (received>2 && curcrc==__inbuf[received-1])
        {
        if (address_mode==TWO_ADDRESSES)
          {
          inbuf->_body=__inbuf+2;
          inbuf->body_len=received-2-1;

          if (__inbuf[0]==SERIAL_ADDR_BROADCAST)  inbuf->DA=BROADCAST_ADDRESS;
          else inbuf->DA=__inbuf[0];
          if (__inbuf[1]==SERIAL_ADDR_BROADCAST)  inbuf->SA=BROADCAST_ADDRESS;
          else inbuf->SA=__inbuf[1];
          }
        else if (address_mode==SLAVE_ADDRESS)
          {
          inbuf->_body=__inbuf+1;
          inbuf->body_len=received-1-1;
          if (__inbuf[0]==SERIAL_ADDR_BROADCAST)  inbuf->DA=BROADCAST_ADDRESS;
          else inbuf->DA=__inbuf[0];
          inbuf->SA=0;//coordinator_addr;
          }
        else if (address_mode==MASTER_ADDRESS)
          {
          inbuf->_body=__inbuf+1;
          inbuf->body_len=received-1-1;
          inbuf->DA=0;//coordinator_addr; // I am coordinator
          if (__inbuf[0]==SERIAL_ADDR_BROADCAST)  inbuf->SA=BROADCAST_ADDRESS;
          else inbuf->SA=__inbuf[0];
          }
        else if (address_mode==NO_ADDRESS)
          {
          inbuf->_body=__inbuf;
          inbuf->body_len=received-1;
          inbuf->DA=0;//my_addr;
          inbuf->SA=0;//coordinator_addr;
          }
        ret=received;
        }
      else
        {
        volatile int x=0;
        }
      received=0; curcrc=0xFF; lastchar=0;
      return ret;*/
      }
    else if (c==FRAME_REPLACEMENT)
      {
      lastchar=FRAME_REPLACEMENT;
      }
    else
      {
      if (lastchar==FRAME_REPLACEMENT)
        { c^=0x20; lastchar=0;}

      if (received!=0)
        curcrc=docrc8(curcrc, __inbuf[received-1]);
      __inbuf[received++]=c;
      if (received>=__inbuflen) {
        received=0; curcrc=0xFF; 
#ifdef ITMP_COUNTERS
        err_overflow++;
#endif
      }
      }
    }
  return 0;
  }


void itmp_link_serial::write_byte(byte d,byte* crc)
  {
  *crc=docrc8(*crc,d);
  if (d==0x7D || d==0x7E )
    {
      serial->write(0x7D);
      serial->write(d^0x20);
    }
  else
    serial->write(d);
  }

//int itmp_link_serial::write_msg(const char* msg, int msglen, void* data);
void itmp_link_serial::set_addr(byte addr){
  _my_addr=addr;
}


void itmp_link_serial::send_frame(uint32_t tp, uint32_t da, const byte* header, int headerlen, const byte* body, int body_len)
//void itmp_link_serial::write_command(int DA, int SA, const byte* header, int headerlen, const byte* body, int body_len)
  {
	byte crc=0xFF;
#if defined(ITMP_SERIAL_TWO_ADDRESSES)
  write_byte(da,&crc);
  write_byte(_my_addr,&crc);
#elif defined(ITMP_SERIAL_SLAVE_ADDRESS) //             0x04 // in pkt address of slave device looked from slave device (I am slave)
  write_byte(_my_addr,&crc);
#elif define(ITMP_SERIAL_MASTER_ADDRESS) //            0x08 // in pkt address of slave device looked from master device (we are master)
  write_byte(da,&crc);
#elif defined(ITMP_SERIAL_NO_ADDRESS)
#else
#error something wrong
#endif
  while (headerlen)
    {
    write_byte(*header++,&crc);
    headerlen--;
    }
	while (body_len)
		{
    write_byte(*body++,&crc);
    body_len--;
		}
  write_byte(crc,&crc);
 	serial->write(0x7E);
	}


int itmp_link_serial::readAndProcess(unsigned int timeout) {
  unsigned int pstart=get_us();
  do {

    int len = read();
    if (len<=0) continue; // if return 0 - no return packet
#if defined(ITMP_SERIAL_TWO_ADDRESSES)
    if (__inbuf[0]==_my_addr || __inbuf[0]==0xff)
      return core->process_itmp_frame(this, __inbuf[1]/*sa*/, (char const*)__inbuf+2, len-2);
#elif defined(ITMP_SERIAL_SLAVE_ADDRESS) //             0x04 // in pkt address of slave device looked from slave device (I am slave)
    if (__inbuf[0]==_my_addr || __inbuf[0]==0xff)
      return core->process_itmp_frame(this, 0/*sa always from master*/, (char const*)__inbuf+1, len-1);
#elif define(ITMP_SERIAL_MASTER_ADDRESS) //            0x08 // in pkt address of slave device looked from master device (we are master)
    return core->process_itmp_frame(this, __inbuf[0]/*sa*/, (char const*)__inbuf+1, len-1);
#elif defined(ITMP_SERIAL_NO_ADDRESS)
    return core->process_itmp_frame(this, 0/*sa*/, (char const*)__inbuf, len);
#else
#error something wrong
#endif
  } while (get_us()-pstart < timeout);
  return TIME_OUT;
}
