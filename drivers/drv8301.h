#define FAULT    0x0400
#define GVDD_UV  0x0200
#define PVDD_UV  0x0100
#define OTSD     0x0080
#define OTW      0x0040
#define FETHA_OC 0x0020
#define FETLA_OC 0x0010
#define FETHB_OC 0x0008
#define FETLB_OC 0x0004
#define FETHC_OC 0x0002
#define FETLC_OC 0x0001

#define GVDD_OV  0x0080

#define GATE_CURRENT        0x0003
#define GATE_CURRENT_1_7    0x0000
#define GATE_CURRENT_0_7    0x0001
#define GATE_CURRENT_0_25   0x0002

#define GATE_RESET          0x0004

#define PWM_MODE            0x0008
#define PWM_MODE_6PWM       0x0000
#define PWM_MODE_3PWM       0x0008

#define OCP_MODE            0x0030
#define OCP_MODE_CURRENT_LIMIT  0x0000
#define OCP_MODE_OC_LACH_SHUTDOWN  0x0010
#define OCP_MODE_REPORT_ONLY 0x0020
#define OCP_MODE_OC_DESABLED 0x0030

#define OC_ADJ              0x07C0
#define OC_ADJV(adj)        (((adj)&0x1F)<<6)

#define OC_ADJ_SET_0_060V   0x0
#define OC_ADJ_SET_0_068V   0x1
#define OC_ADJ_SET_0_076V   0x2
#define OC_ADJ_SET_0_086V   0x3
#define OC_ADJ_SET_0_097V   0x4
#define OC_ADJ_SET_0_109V   0x5
#define OC_ADJ_SET_0_123V   0x6
#define OC_ADJ_SET_0_138V   0x7
#define OC_ADJ_SET_0_155V   0x8
#define OC_ADJ_SET_0_175V   0x9
#define OC_ADJ_SET_0_197V   0xA
#define OC_ADJ_SET_0_222V   0xB
#define OC_ADJ_SET_0_250V   0xC
#define OC_ADJ_SET_0_282V   0xD
#define OC_ADJ_SET_0_317V   0xE
#define OC_ADJ_SET_0_358V   0xF
#define OC_ADJ_SET_0_403V   0x10
#define OC_ADJ_SET_0_454V   0x11
#define OC_ADJ_SET_0_511V   0x12
#define OC_ADJ_SET_0_576V   0x13
#define OC_ADJ_SET_0_648V   0x14
#define OC_ADJ_SET_0_730V   0x15
#define OC_ADJ_SET_0_822V   0x16
#define OC_ADJ_SET_0_926V   0x17
#define OC_ADJ_SET_1_043V   0x18
#define OC_ADJ_SET_1_175V   0x19
#define OC_ADJ_SET_1_324V   0x1A
#define OC_ADJ_SET_1_491V   0x1B
#define OC_ADJ_SET_1_679V   0x1C //Do not use if PVDD is 6V - 8V
#define OC_ADJ_SET_1_892V   0x1D //Do not use if PVDD is 6V - 8V
#define OC_ADJ_SET_2_131V   0x1E //Do not use if PVDD is 6V - 8V
#define OC_ADJ_SET_2_400V   0x1F //Do not use if PVDD is 6V - 8V

#define OCTW_MODE           0x0003
#define OCTW_MODE_OT_OC     0x0000
#define OCTW_MODE_OT        0x0001
#define OCTW_MODE_OC        0x0002
#define OCTW_MODE_OC_reserv 0x0003

#define GAIN                0x000C
#define GAIN_10VV           0x0000
#define GAIN_20VV           0x0004
#define GAIN_40VV           0x0008
#define GAIN_80VV           0x000C

#define DC_CAL_CH1          0x0010
#define DC_CAL_CH1_CONNECT  0x0000
#define DC_CAL_CH1_DISCONNECT 0x0010

#define DC_CAL_CH2          0x0020
#define DC_CAL_CH2_CONNECT  0x0000
#define DC_CAL_CH2_DISCONNECT 0x0020

#define OC_TOFF             0x0040
#define OC_TOFF_CYCLE_BY_CYCLE 0x0000
#define OC_TOFF_OFF_TIME_CONTROL 0x0040
