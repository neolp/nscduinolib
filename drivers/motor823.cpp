#include "stm32f10x.h"
#include "stm32f10x_extend.h"

#include "gpio-alloc.h"
#include "motor823.h"

int motor823_driver::begin()
  {
  // if 0: No remap (CH1/PA2, CH2/PA3)
  // AFIO->MAPR2 &= ~AFIO_MAPR2_TIM15_REMAP;
  //else 1: Remap (CH1/PB14, CH2/PB15)
  AFIO->MAPR2 |= AFIO_MAPR2_TIM15_REMAP;
	RCC->APB2ENR |= RCC_APB2ENR_TIM15EN;
	TIM15->CR1 = TIM_CR1_ARPE | TIM_CR1_CEN;
	TIM15->CCMR1 = (TIM_CCMR1_OC1M & (TIM_CCMR_OCM_PWM_LOW_MATCH << 4)) | TIM_CCMR1_OC1PE |
                 (TIM_CCMR1_OC2M & (TIM_CCMR_OCM_PWM_LOW_MATCH << (4+8))) | TIM_CCMR1_OC2PE;
	TIM15->ARR = 0x500;
	TIM15->CCER = TIM_CCER_CC1E | TIM_CCER_CC2E;
	TIM15->BDTR = TIM_BDTR_AOE | TIM_BDTR_MOE;

  
#if defined MOT823111


#elif defined MOT823112

  EN1.acquire(PORTB | PIN11,GPIO_IN_PULLUP);
  INA1.acquire(PORTA | PIN15,GPIO_OUT_PUSHPULL_50MHz);
  INB1.acquire(PORTA | PIN8, GPIO_OUT_PUSHPULL_50MHz);
  TIM15_CH2.acquire(PORTB | PIN14,GPIO_OUT_ALT_PUSHPULL_50MHz);

  EN2.acquire(PORTB | PIN10,GPIO_IN_PULLUP);
  INA2.acquire(PORTB | PIN13,GPIO_OUT_PUSHPULL_50MHz);
  INB2.acquire(PORTB | PIN12,GPIO_OUT_PUSHPULL_50MHz);
  TIM15_CH1.acquire(PORTB | PIN15,GPIO_OUT_ALT_PUSHPULL_50MHz);

//  leftEnc.init(PORTA | PIN0, PORTA | PIN1);
//  rightEnc.init(PORTB | PIN4, PORTB | PIN5);

#elif defined MOT823200

  EN1.acquire(PORTA | PIN8, GPIO_IN_PULLUP);
  INA1.acquire(PORTA | PIN12, GPIO_OUT_PUSHPULL_50MHz);
  INB1.acquire(PORTB | PIN13, GPIO_OUT_PUSHPULL_50MHz);
  TIM15_CH2.acquire(PORTB | PIN14,GPIO_OUT_ALT_PUSHPULL_50MHz);

  EN2.acquire(PORTB | PIN10,GPIO_IN_PULLUP);
  INA2.acquire(PORTB | PIN11,GPIO_OUT_PUSHPULL_50MHz);
  INB2.acquire(PORTB | PIN12,GPIO_OUT_PUSHPULL_50MHz);
  TIM15_CH1.acquire(PORTB | PIN15,GPIO_OUT_ALT_PUSHPULL_50MHz);

  leftEnc.init(PORTA | PIN0, PORTA | PIN1);
  rightEnc.init(PORTA | PIN6, PORTA | PIN7);
  
#if 0
  leftEnc.config(CFG_AS_TACHOMETER);
  rightEnc.config(CFG_AS_TACHOMETER);
  encoder_mode = MODE_TACHOMETER;
#else
  leftEnc.config(CFG_AS_ENCODER);
  rightEnc.config(CFG_AS_ENCODER);
  encoder_mode = MODE_ENCODER;
#endif
  En5V.acquire(PORTB | PIN6,GPIO_OUT_PUSHPULL_50MHz);
  En5V.set();
  Fault5V.acquire(PORTB | PIN7, GPIO_IN_PULLUP);
  
#elif defined MOT823201

  EN1.acquire(PORTA | PIN8, GPIO_IN_PULLUP);
  INA1.acquire(PORTA | PIN12, GPIO_OUT_PUSHPULL_50MHz);
  INB1.acquire(PORTB | PIN13, GPIO_OUT_PUSHPULL_50MHz);
  TIM15_CH2.acquire(PORTB | PIN14,GPIO_OUT_ALT_PUSHPULL_50MHz);

  EN2.acquire(PORTB | PIN10,GPIO_IN_PULLUP);
  INA2.acquire(PORTB | PIN11,GPIO_OUT_PUSHPULL_50MHz);
  INB2.acquire(PORTB | PIN12,GPIO_OUT_PUSHPULL_50MHz);
  TIM15_CH1.acquire(PORTB | PIN15,GPIO_OUT_ALT_PUSHPULL_50MHz);

  leftEnc.init(PORTA | PIN15, PORTB | PIN3);
  rightEnc.init(PORTA | PIN6, PORTA | PIN7);
  
#if 0
  leftEnc.config(CFG_AS_TACHOMETER);
  rightEnc.config(CFG_AS_TACHOMETER);
  encoder_mode = MODE_TACHOMETER;
#else
  leftEnc.config(CFG_AS_ENCODER);
  rightEnc.config(CFG_AS_ENCODER);
  encoder_mode = MODE_ENCODER;
#endif
  En5V.acquire(PORTA | PIN9,GPIO_OUT_PUSHPULL_50MHz);
  En5V.set();
  Fault5V.acquire(PORTA | PIN10, GPIO_IN_PULLUP);
  
#else
#error set MOT823111 or MOT823112 or MOT823200 or MOT823201
#endif
  

  lastmeasuretime=get_us();
  trajectoryreset();
  right.P = left.P = 24.4f/10;
  right.D = left.D = 25.0f/10;
  right.I = left.I = 0;//0.01f;

  right.SPm = left.SPm = 0.0625f;

  return 0;
  }

int motor823_driver::end()
  {
  return -1;
  }
int motor823_driver::set_alpha(int alpha) {
  int ret=0;
  if (alpha >= 0x500) { alpha = 0x500-1; ret=1; }
  if (alpha <=-0x500) { alpha = -0x500+1; ret=1; }
  alpha_pwm = alpha;
  if (alpha==0) {
    INB1.reset();
    INA1.reset();
  } else if (alpha>0) {
    INB1.set();
    INA1.reset();
    TIM15->CCR2 = alpha;
    
    if (encoder_mode == MODE_TACHOMETER) {
      leftEnc.reverse();
    }
  } else {
    INB1.reset();
    INA1.set();
    TIM15->CCR2 = -alpha;
    if (encoder_mode == MODE_TACHOMETER) {
      leftEnc.forward();
    }
  }
  return ret;
}

int motor823_driver::set_beta(int beta) {
  int ret=0;
  if (beta >= 0x500) { beta = 0x500-1; ret=1; }
  if (beta <=-0x500) { beta = -0x500+1; ret=1; }
  beta_pwm = beta;
  if (beta==0) {
    INB2.reset();
    INA2.reset();
  } else if (beta>0) {
    INB2.set();
    INA2.reset();
    TIM15->CCR1 = beta;
    if (encoder_mode == MODE_TACHOMETER) {
      rightEnc.forward();
    }
  } else {
    INB2.reset();
    INA2.set();
    TIM15->CCR1 = -beta;
    if (encoder_mode == MODE_TACHOMETER) {
      rightEnc.reverse();
    }
  }
  return ret;
}

void motor823_driver::set_pwm(int alpha, int beta)
  {
  left.mode = MODE_PWM;
  right.mode = MODE_PWM;
  set_alpha(alpha);
  set_beta(beta);
  }


void motor823_driver::setspeed(int s)
  {
  global_target_speed=s;
  }

void motor823_driver::set_acc(int a)
  {
  global_acc=a;
  }

void wheel_t::go_to(int target, float _target_speed, float target_acc)
  {
  to_mode(MODE_POSITION);
  
  start_pos += (int)calculated_pos;
  calculated_pos -= (int)calculated_pos;

  
  acc = target_acc;
  target_speed = _target_speed;
  
  target_pos = target;

  mode = MODE_POSITION;
  }

void motor823_driver::left_go_to(int l) {
  left.go_to(l,global_target_speed,global_acc);
}

void motor823_driver::right_go_to(int r) {
  right.go_to(r,global_target_speed,global_acc);
}

void motor823_driver::go_to(int l, int r) {
  left.to_mode(MODE_POSITION);
  right.to_mode(MODE_POSITION);

  int left_delta = abs((int)(l  - (left.start_pos + left.calculated_pos)));
  int right_delta = abs((int)(r  - (right.start_pos + right.calculated_pos)));
  
  left.start_pos += (int)left.calculated_pos;
  left.calculated_pos -= (int)left.calculated_pos;
  right.start_pos += (int)right.calculated_pos;
  right.calculated_pos -= (int)right.calculated_pos;

//////////////////
//  left.target_speed = _lsp;// / 100.0f;
//  right.target_speed = _rsp;// / 100.0f;
  
  if ( left_delta >= right_delta ) {
    left.acc = global_acc;
    left.target_speed = global_target_speed;
    if ( left_delta == 0) {
      right.acc = 0;
      right.target_speed = 0;
    } else {
      right.acc =  global_acc * right_delta / left_delta;
      right.target_speed = global_target_speed * right_delta / left_delta;
    }
  } else {
    right.acc =  global_acc;
    right.target_speed = global_target_speed;
    if (right_delta == 0) {
      left.acc = 0;
      left.target_speed = 0;
    } else {
      left.acc = global_acc * left_delta / right_delta;
      left.target_speed = global_target_speed * left_delta / right_delta;
    }
  }
  
  left.target_pos = l;
  right.target_pos = r;

//  if (abs(left_delta) >= abs(right_delta)) {// if left path longer than right
  }

void wheel_t::set_sp(int sp, float _acc) { // set new speeds
  to_mode(MODE_SPEED);
  start_pos += (int)calculated_pos;
  calculated_pos -= (int)calculated_pos;
  target_speed = sp;
  acc = _acc;
}

void motor823_driver::set_sp(int _lsp, int _rsp) { // set new speeds
  left.to_mode(MODE_SPEED);
  right.to_mode(MODE_SPEED);

  left.start_pos += (int)left.calculated_pos;
  left.calculated_pos -= (int)left.calculated_pos;
  right.start_pos += (int)right.calculated_pos;
  right.calculated_pos -= (int)right.calculated_pos;

  if (left.calculated_pos > 100000 || right.calculated_pos >100000) {
    volatile int xxx =0;
  }
  
  left.target_speed = _lsp;// / 100.0f;
  right.target_speed = _rsp;// / 100.0f;
  
  float left_delta = left.speed - left.target_speed;
  if (left_delta < 0 ) left_delta = -left_delta;
  float right_delta = right.speed - right.target_speed;
  if (right_delta < 0 ) right_delta = -right_delta;
  
  if ( left_delta >= right_delta ) {
    left.acc = global_acc;
    if ( left_delta == 0)
      right.acc = 0;
    else
      right.acc =  global_acc * right_delta / left_delta;
    //if (right.acc<0) right.acc = -right.acc;
  } else {
    right.acc =  global_acc;
    if (right_delta == 0)
      left.acc = 0;
    else
      left.acc = global_acc * left_delta / right_delta;
    //if (left.acc<0) left.acc = -left.acc;
  }
}

/*
void motor823_driver::set_spRPHI(int r, int phi)
  {
  if (mode == MODE_PWM)
    {
    current_speed = 0; // current speed
    brake = false; // brake flag
    left_pid.last_err=0;
    left_pid.itegral_err=0;
    right_pid.last_err=0;
    right_pid.itegral_err=0;
    left_driven_wheel = true;
    }
  R.target_speed = r;
  PHI.target_speed = phi;
  //acc_R;
  //acc_PHI;
  mode=MODE_PWM5;
  }
*/
void motor823_driver::trajectoryreset()
  {
  global_target_speed=5000; // parameter
  global_acc=20000; // parameter

  left.pidreset();
  right.pidreset();
  }

void wheel_t::pidreset()
  {
  mode = MODE_PWM;
  calculated_pos=0;
  speed=0;

  last_err=0;
  itegral_err=0;
  last_actual_pos = actual_pos; // no speed
  measuredSpeed=0;
  pid_out=0;
  }
int globalmeasurestep;

void motor823_driver::poll() {
  unsigned short enc=leftEnc.get();
  left.actual_pos += ((int)((signed short)(last_leftEnc-enc)))*5;
  last_leftEnc = enc;
  enc=rightEnc.get();
  right.actual_pos -= ((int)((signed short)(last_rightEnc-enc)))*5;
  last_rightEnc = enc;
  unsigned int now = get_us();
  if (now-lastmeasuretime > POLL_STEP_mS * 1000 *2){
    volatile int xxx = 0;
  }
  if ( now-lastmeasuretime > POLL_STEP_mS * 1000 ) { // it is 100 measurement per second (dt is about 10ms and 10000us)
    if (globalmeasurestep < now-lastmeasuretime)
      globalmeasurestep = now-lastmeasuretime;
    left.measuredSpeed = ((signed int)( left.actual_pos - left.last_actual_pos )) / ((float)POLL_STEP_mS/1000);
    left.last_actual_pos = left.actual_pos;
    right.measuredSpeed = ((signed int)( right.actual_pos - right.last_actual_pos )) / ((float)POLL_STEP_mS/1000);
    right.last_actual_pos = right.actual_pos;

    left.poll();
    right.poll();

    int over=0;
    if (left.mode != MODE_PWM) {
      left.pidpoll(left.start_pos + left.calculated_pos - left.actual_pos, POLL_STEP_mS);
      over+=set_alpha((int)(left.speed * left.SPm + left.pid_out) );
    }
    if (right.mode != MODE_PWM) {
      right.pidpoll(right.start_pos+ right.calculated_pos - right.actual_pos, POLL_STEP_mS);
      over += set_beta((int)(right.speed * right.SPm + right.pid_out) );
    }
    if (over) overpower++;
    else overpower=0;
      
    if (overpower > 500/POLL_STEP_mS) {
      // stop !!!
      set_pwm(0,0);
    }
    lastmeasuretime += POLL_STEP_mS * 1000;
  }
}
/*
void motor823_driver::RPHItrpoll(int dt, speedComponent_t* s) //, wheel_t* w)
	{
  int stopDistance_mm = (int)(1000UL*(long long)s->current_speed*s->current_speed/(acc*2)) + dt * abs(s->current_speed)*(1000/1000);
    // calculate distance to stop with maximum decceleration and one more time step with current speed
  int delta_mm = s->target_pos - s->calculated_pos; // calculate distance to target position

  if (delta_mm > 0) // if target is in forward direction
    {
    if ((delta_mm <= stopDistance_mm && s->current_speed >= 0) || s->current_speed > s->target_speed) // if we need to brake (target_speed here is allways positive value)
      {
      s->brake = s->current_speed <= s->target_speed; // we start to brake (or continue to brake) if current speed less than max
      s->current_speed -= acc * dt / 1000;
      if (s->current_speed <= 0)
        {
        if (delta_mm / dt <= s->target_speed)
          s->current_speed = delta_mm / dt;
        s->brake = false;
        }
      }
    else if (s->brake && s->current_speed>=0 )
      {
      s->current_speed -= s->acc * dt / (3*1000);
      if (s->current_speed <= 0)
        {
        if (delta_mm / dt <= s->target_speed)
          s->current_speed = delta_mm / dt;
        s->brake = false;
        }
      else
        {
        s->brake = true;
        }
      }
    else
      {
      s->brake = false;
      s->current_speed += s->acc*dt/1000; // if we can accelerate
      if (s->current_speed > s->target_speed) s->current_speed = s->target_speed; //limit speed to the max
      }
    }
  else if (delta_mm<0)
    {
    if ((-delta_mm <= stopDistance_mm && s->current_speed <= 0) || s->current_speed < -s->target_speed)
      {
      s->brake = s->current_speed >= -s->target_speed;
      s->current_speed += s->acc * dt / 1000;
      if (s->current_speed >= 0)
        {
        if (-delta_mm / dt <= s->target_speed)
          s->current_speed = delta_mm / dt;
        s->brake = false;
        }
      }
    else if (s->brake && s->current_speed <= 0)
      {
      s->current_speed += s->acc * dt / (3 * 1000);
      if (s->current_speed >= 0)
        {
        if (-delta_mm / dt <= s->target_speed)
          s->current_speed = delta_mm / dt;
        s->brake = false;
        }
      else
        {
        s->brake = true;
        }
      }
    else
      {
      s->brake = false;
      s->current_speed -= s->acc*dt/1000; // if we can accelerate
      if (s->current_speed < -s->target_speed) s->current_speed = -s->target_speed; //limit speed to the max
      }
    }
  else
    {
    if (stopDistance_mm < 10*POSITION_MULTIPLEXER) s->current_speed = 0;
    }

  // trajectory new speed are calculated
//  pidpoll(&left_pid, left._calculated_pos_mm - left._actual_pos_mm, dt);
//  pidpoll(&right_pid, right._calculated_pos_mm - right._actual_pos_mm, dt);
  
  s->calculated_pos += dt * s->current_speed;
  //left._calculated_pos_mm = left._start_mm + (long long)calculated_pos_mm * left.multiplier / left.divider;
  //right._calculated_pos_mm = right._start_mm + (long long)calculated_pos_mm * right.multiplier / right.divider;
  }
*/

void wheel_t::trpoll(int dt)
	{
  calculated_pos += dt * speed / 1000.0f;
  //calculated_pos += dt * speed;
  float stopDistance = speed*speed / (acc*2);// + dt * abs(speed);
    // calculate distance to stop with maximum decceleration and one more time step with current speed
  //float delta = calculated_pos - (actual_pos - start_pos); // calculate distance to target position
  float delta = (target_pos - start_pos) - calculated_pos;

  if (delta > 0) {// if target is in forward direction
    if ((delta <= stopDistance && speed >= 0) || speed > target_speed) { // if we need to brake (target_speed here is allways positive value)
      brake = speed <= target_speed; // we start to brake (or continue to brake) if current speed less than target
      speed -= acc * dt / 1000;
      if (speed <= 0) {
        if (delta / dt <= target_speed)
          speed = delta / dt;
        brake = false;
      }
    } else if (brake && speed >= 0 ) {
      speed -= acc * dt / (3*1000);
      if (speed <= 0) {
        if (delta / dt <= target_speed)
          speed = delta / dt;
        brake = false;
      } else {
        brake = true;
      }
    } else {
      brake = false;
      speed += acc * dt / 1000; // if we can accelerate
      if (speed > target_speed) speed = target_speed; //limit speed to the max
    }
  } else if (delta<0) {
    if ((-delta <= stopDistance && speed <= 0) || speed < -target_speed) {
      brake = speed >= -target_speed;
      speed += acc * dt / 1000;
      if (speed >= 0) {
        if (-delta / dt <= target_speed)
          speed = delta / dt;
        brake = false;
      }
    } else if (brake && speed <= 0) {
      speed += acc * dt / (3 * 1000);
      if (speed >= 0) {
        if (-delta / dt <= target_speed)
          speed = delta / dt;
        brake = false;
      } else {
        brake = true;
      }
    } else {
      brake = false;
      speed -= acc*dt/1000; // if we can accelerate
      if (speed < -target_speed) speed = -target_speed; //limit speed to the max
    }
  } else {
    if (stopDistance < 10) speed = 0;
  }
}

/*
void motor823_driver::RPHIsppoll(int dt, speedComponent_t* s) //, wheel_t* w)
	{
    s->calculated_pos += dt * s->current_speed;
  if (s->target_speed >= 0) // move forward
    {
    if (s->current_speed > s->target_speed) // brake
      {
      s->current_speed -= s->acc * dt / 1000;
      if (s->current_speed < s->target_speed) s->current_speed = s->target_speed; //limit speed to the target
      }
    else // accelerate
      {
      s->current_speed += s->acc*dt/1000; // if we can accelerate
      if (s->current_speed > s->target_speed) s->current_speed = s->target_speed; //limit speed to the target
      }
    }
  else
    {
    if (s->current_speed < s->target_speed) // brake
      {
      s->current_speed += s->acc * dt / 1000;
      if (s->current_speed > s->target_speed) s->current_speed = s->target_speed; //limit speed to the max
      }
    else // accelerate
      {
      s->current_speed -= s->acc*dt/1000; // if we can accelerate
      if (s->current_speed < s->target_speed) s->current_speed = s->target_speed; //limit speed to the max
      }
    }

  // trajectory new speed are calculated
  //pidpoll(&left_pid, left._calculated_pos_mm - left._actual_pos_mm, dt);
  //pidpoll(&right_pid, right._calculated_pos_mm - right._actual_pos_mm, dt);
  
//  left._calculated_pos_mm = left._start_mm + (long long)calculated_pos_mm * left.multiplier / left.divider;
//  right._calculated_pos_mm = right._start_mm + (long long)calculated_pos_mm * right.multiplier / right.divider;
  }
*/

void wheel_t::sppoll(int dt)
	{
  calculated_pos += dt * speed / 1000.0f;
  if (target_speed >= 0) // move forward
    {
    if (speed > target_speed)
      {
      speed -= acc * dt / 1000;
      if (speed < target_speed) speed = target_speed; //limit speed to the max
      }
    else
      {
      speed += acc * dt / 1000; // if we can accelerate
      if (speed > target_speed) speed = target_speed; //limit speed to the max
      }
    }
  else
    {
    if (speed < target_speed)
      {
      speed += acc * dt / 1000;
      if (speed > target_speed) speed = target_speed; //limit speed to the max
      }
    else
      {
      speed -= acc * dt / 1000; // if we can accelerate
      if (speed < target_speed) speed = target_speed; //limit speed to the max
      }
    }
  }

/*
	const int filter_data[] =
	{
		3,
		11,
		31,
		66,
		117,
		173,
		223,
		251,
		251,
		223,
		173,
		117,
		66,
		31,
		11,
		3,
	};
int pidfifo[16];
LPAlg::WindowFilter<int, int> pidout(filter_data, pidfifo, sizeof(filter_data) / sizeof(filter_data[0]), 1750);
int madata[4];
LPAlg::MovingAverage<int,long long> ma_in(madata,sizeof(madata)/sizeof(madata[0]));
*/

void wheel_t::pidpoll(float err, int dt)
	{
//    ma_in.Push(err);
//    err=ma_in.GetAverage();
itegral_err += err;
#define MAX_I_ERR   ((972/4)/I)
  if (itegral_err > MAX_I_ERR) itegral_err = MAX_I_ERR;
  if (itegral_err < -MAX_I_ERR) itegral_err = -MAX_I_ERR;

  pid_out = P * err  -  D * (last_err-err)  +  I * itegral_err;
//  if (pid_out>972*PID_Kd) pid_out=972*PID_Kd;
//  if (pid_out<-972*PID_Kd) pid_out=-972*PID_Kd;
  last_err=err;
	}
