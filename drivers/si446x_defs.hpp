
#ifndef __SI446X_DEFS_HPP__
#define __SI446X_DEFS_HPP__

#ifndef LPOS10
#include <defs.h>
#else
#include <lplib/lpdefs.h>
#endif

namespace si446x {

/* ---- si446x hw & fw info ------------------------------------------------- */
struct part_info
{
	uint8_t  chip_rev;
	uint8_t  part_build;
	uint16_t part;
	uint16_t id;
	uint8_t  customer_id;
	uint8_t  rom_id;
};

struct func_info
{
	uint8_t  rev_ext;
	uint8_t  rev_branch;
	uint8_t  rev_int;
	uint8_t  func;
	uint16_t patch;
};

/* ---- properties size limits ---------------------------------------------- */
static size_t const set_prop_max_size    = 12;
static size_t const get_prop_max_size    = 16;

/* ---- gpio control -------------------------------------------------------- */
static uint8_t const gpio_cfg_pullup     = 1 << 6;
static uint8_t const gpio_cfg_mode_mask  = 0x3f;
static uint8_t const gpio_cfg_mode_shift = 0;

enum gpio_mode
{
	GPIO_DONT_CHANGE,
	GPIO_IO_DISABLED,
	GPIO_OUT_LOW,
	GPIO_OUT_HIGH,
	GPIO_INPUT,
	GPIO_32KHZ_CLOCK,
	GPIO_30MHZ_CLOCK,
	GPIO_DIV_MCU_CLOCK,
	GPIO_CTS,
	GPIO_NCTS,
	GPIO_CMD_OVERLAP,
	GPIO_SDO,
	GPIO_PULSE_RESET,
	GPIO_PULSE_CALIB,
	GPIO_PULSE_WAKEUP,
	GPIO_UNUSED0,
	GPIO_TX_CLOCK,
	GPIO_RX_CLOCK,
	GPIO_UNUSED1,
	GPIO_TX_DATA,
	GPIO_RX_DATA,
	GPIO_RX_RAW_DATA,
	GPIO_ANTENNA_1SWITCH,
	GPIO_ANTENNA_2SWITCH,
	GPIO_VALID_PREAMBLE,
	GPIO_INVALID_PREAMBLE,
	GPIO_SYNC_WORD,
	GPIO_RSSI_ABOVE_THRESHOLD,
	GPIO_TX_STATE = 32,
	GPIO_RX_STATE,
	GPIO_RXFIFO_FULL,
	GPIO_TXFIFO_EMPTY,
	GPIO_BATT_VOLT_LOW,
	GPIO_RSSI_ABOVE_THRESHOLD_PACKET,
	GPIO_TOGGLE_HOP,
	GPIO_TOGGLE_HOP_TABLE_WRAP_OR_NIRQ_ACTIVE_LOW,
};

enum gpio_strength
{
	GPIO_STRENGTH_HIGH,
	GPIO_STRENGTH_MEDIUM,
	GPIO_STRENGTH_MEDUIM2,
	GPIO_STRENGTH_LOW,
};

static uint8_t const gpio_general_strength_mask  = 0x3;
static uint8_t const gpio_general_strength_shift = 5;

struct gpio_cfg
{
	uint8_t gpio[4]; /* gpio cfg */
	uint8_t nirq;    /* gpio cfg */
	uint8_t sdo;     /* gpio cfg */
	uint8_t general; /* gpio general cfg */
};

/* ---- adc reading --------------------------------------------------------- */
static uint8_t const adc_temp_en    = 1 << 4;
static uint8_t const adc_batt_en    = 1 << 3;
static uint8_t const adc_gpio_en    = 1 << 2;
static uint8_t const adc_gpio_mask  = 0x03;
static uint8_t const adc_gpio_shift = 0;

struct adc_info
{
	uint16_t gpio;
	uint16_t batt;
	uint16_t temp;
	uint8_t  temp_slope;
	uint8_t  temp_intercept;
};

/* ---- fifo info ----------------------------------------------------------- */
static uint8_t const fifo_rx_reset     = 1 << 2;
static uint8_t const fifo_tx_reset     = 1 << 0;

struct fifo_info
{
	uint8_t rx_count;
	uint8_t tx_space;
};

/* ---- image rejection calibration ----------------------------------------- */
static uint8_t const ircal_step_init_ph_amp_prev       = 1 << 6;
static uint8_t const ircal_step_fine_step_size_mask    = 0x03;
static uint8_t const ircal_step_fine_step_size_shift   = 4;
static uint8_t const ircal_step_course_step_size_mask  = 0x0f;
static uint8_t const ircal_step_course_step_size_shift = 0;

static uint8_t const ircal_rssi_fine_avg_mask          = 0x03;
static uint8_t const ircal_rssi_fine_avg_shift         = 4;
static uint8_t const ircal_rssi_course_avg_mask        = 0x03;
static uint8_t const ircal_rsii_course_avg_shift       = 0;

enum ircal_rssi_avg
{
	IRCAL_RSSI_1MSR,
	IRCAL_RSSI_2MSR,
	IRCAL_RSSI_4MSR,
	IRCAL_RSII_8MSR,
};

static uint8_t const ircal_rxchs1_en_hrmnic_gen        = 1 << 7;
static uint8_t const ircal_rxchs1_irclkdiv             = 1 << 6;
static uint8_t const ircal_rxchs1_rf_src_pwr_mask      = 0x03;
static uint8_t const ircal_rxchs1_rf_src_pwr_shift     = 4;
static uint8_t const ircal_rxchs1_close_shunt_swt      = 1 << 3;
static uint8_t const ircal_rxchs1_pga_gain_mask        = 0x07;
static uint8_t const ircal_rxchs1_pga_gain_shift       = 0;

static uint8_t const ircal_rxchs2_adc_high_gain        = 1 << 0;

enum ircal_pga_gain
{
	PGA_GAIN_6DB,
	PGA_GAIN_9DB,
	PGA_GAIN_12DB,
	PGA_GAIN_0DB = 6,
	PGA_GAIN_3DB,
};

struct ircal_cfg
{
	uint8_t src_step_size;
	uint8_t src_rssi_avg;
	uint8_t rx_chain_setting1;
	uint8_t rx_chain_setting2;
};

/* ---- interrupt status ---------------------------------------------------- */
static uint8_t const irq_ph_filter_match         = 1 << 7;
static uint8_t const irq_ph_filter_miss          = 1 << 6;
static uint8_t const irq_ph_packet_sent          = 1 << 5;
static uint8_t const irq_ph_packet_recv          = 1 << 4;
static uint8_t const irq_ph_crc32_error          = 1 << 3;
static uint8_t const irq_ph_txfifo_almost_empty  = 1 << 1;
static uint8_t const irq_ph_rxfifo_almost_full   = 1 << 0;

static uint8_t const irq_modem_invalid_sync      = 1 << 5;
static uint8_t const irq_modem_rssi_jump         = 1 << 4;
static uint8_t const irq_modem_rssi              = 1 << 3;
static uint8_t const irq_modem_invalid_preamble  = 1 << 2;
static uint8_t const irq_modem_preamble_detect   = 1 << 1;
static uint8_t const irq_modem_sync_detect       = 1 << 0;

static uint8_t const irq_chip_fifo_underoverflow = 1 << 5;
static uint8_t const irq_chip_state_change       = 1 << 4;
static uint8_t const irq_chip_cmd_error          = 1 << 3;
static uint8_t const irq_chip_ready              = 1 << 2;
static uint8_t const irq_chip_low_batt           = 1 << 1;
static uint8_t const irq_chip_wakeup_timer       = 1 << 0;

struct irq_stat_req
{
	uint8_t ph_clr_pend;
	uint8_t modem_clr_pend;
	uint8_t chip_clr_pend;
};

static uint8_t const irq_global_chip_status      = 1 << 2;
static uint8_t const irq_global_modem_status     = 1 << 1;
static uint8_t const irq_global_ph_status        = 1 << 0;

struct irq_stat_resp
{
	uint8_t global_pend;
	uint8_t global_stat;
	uint8_t ph_pend;
	uint8_t ph_stat;
	uint8_t modem_pend;
	uint8_t modem_stat;
	uint8_t chip_pend;
	uint8_t chip_stat;
};

struct irq_stat_ph_resp
{
	uint8_t pend;
	uint8_t stat;
};

struct irq_stat_modem_resp
{
	uint8_t pend;
	uint8_t stat;
	uint8_t rssi_curr;
	uint8_t rssi_latch;
	uint8_t rssi_ant1;
	uint8_t rssi_ant2;
};

enum irq_stat_cmd_error
{
	NO_ERROR,
	BAD_COMMAND,
	INVALID_ARG,
	PREV_CMD_PEND,
	BAD_PROP_ID,
};

struct irq_stat_chip_resp
{
	uint8_t pend;
	uint8_t stat;
	uint8_t error;
};

/* ---- start rx & tx ------------------------------------------------------- */
static uint8_t const start_tx_condition_compl_state_mask   = 0x0f;
static uint8_t const start_tx_condition_compl_state_shift  = 4;
static uint8_t const start_tx_condition_retransmit         = 1 << 2;
static uint8_t const start_tx_condition_start_wakeup_timer = 1 << 0;

enum io_state
{
	STATE_NO_CHANGE,
	STATE_SLEEP,
	STATE_SPI_ACTIVE,
	STATE_READY,
	STATE_ANOTHER_READY,
	STATE_TUNE_TX,
	STATE_TUNE_RX,
	STATE_TX,
	STATE_RX,
};

struct start_tx_req
{
	uint8_t  channel;
	uint8_t  condition;
	uint16_t size;
};

static uint8_t const start_rx_condition_start_wakeup_timer = 1 << 0;

static uint8_t const start_rx_next_state_mask  = 0x0f;
static uint8_t const start_rx_next_state_shift = 0;

struct start_rx_req
{
	uint8_t  channel;
	uint8_t  condition;
	uint16_t size;
	uint8_t  timeout_state;
	uint8_t  vaild_state;
	uint8_t  invalid_state;
};

/* ---- device state -------------------------------------------------------- */
struct device_state
{
	uint8_t main_state;
	uint8_t channel;
};

/* ---- rx hop -------------------------------------------------------------- */
struct rx_hop_req
{
	uint8_t  integer;
	uint32_t fract;
	uint16_t vco_cnt;
};

/* ==== propperites ========================================================= */
enum prop_addr
{
	PROP_GLOBAL        = 0x0000,
	PROP_INTERRUPTS    = 0x0100,
	PROP_FAST_RESP_REG = 0x0200,
	PROP_PREAMBLE      = 0x1000,
	PROP_SYNC          = 0x1100,
	PROP_PACKET        = 0x1200,
	PROP_MODEM         = 0x2000,
	PROP_MODEM_FILT    = 0x2100,
	PROP_MATCH         = 0x3000,
	PROP_FREQ          = 0x4000,
	PROP_RX_HOP        = 0x5000,
};

/* ---- global configuration property --------------------------------------- */
static uint8_t const global_clock_div_en          = 1 << 6;
static uint8_t const global_clock_div_sel_mask    = 0x07;
static uint8_t const global_clock_div_sel_shift   = 3;
static uint8_t const global_clock_32khz_sel_mask  = 0x07;
static uint8_t const global_clock_32khz_sel_shift = 0;

enum global_clock_div_sel
{
	DIV_SEL_1,
	DIV_SEL_2,
	DIV_SEL_3,
	DIV_SEL_7_5,
	DIV_SEL_10,
	DIV_SEL_15,
	DIV_SEL_30,
};

enum global_clock_32khz_sel
{
	CLK_32KHZ_SEL_DISBLED,
	CLK_32KHZ_SEL_INTERNAL_RC,
	CLK_32KHZ_SEL_EXTERNAL_XTAL,
};

static uint8_t const global_low_batt_thresh_mask     = 0x1f;
static uint8_t const global_low_batt_thresh_shift    = 0;

static uint    const global_low_batt_thresh_min_mv  = 1520;
static uint    const global_low_batt_thresh_step_mv =  500;

static uint8_t const global_cfg_enter_tx_fast        = 1 << 5;
static uint8_t const global_cfg_fifo_sharing         = 1 << 4;
static uint8_t const global_cfg_proto_mask           = 0x07;
static uint8_t const global_cfg_proto_shift          = 1;
static uint8_t const global_cfg_low_power_mode       = 1 << 0;

static uint8_t const global_wakeup_ldc_en_mask       = 0x03;
static uint8_t const global_wakeup_ldc_en_shift      = 6;
static uint8_t const global_wakeup_cal_period_mask   = 0x07;
static uint8_t const global_wakeup_cal_period_shift  = 3;
static uint8_t const global_wakeup_lbd_en            = 1 << 2;
static uint8_t const global_wakeup_en                = 1 << 1;
static uint8_t const global_wakeup_cal_en            = 1 << 0;

__packed struct global_prop
{
	uint8_t xo_tune;
	uint8_t clock;
	uint8_t low_batt_thresh;
	uint8_t cfg;
	uint8_t wakeup;
	uint8_t wakeup_m_be[2];
	uint8_t wakeup_r;
	uint8_t wakeup_ldc;
};

/* ---- interrupt control property ------------------------------------------ */
__packed struct irq_ctl_prop
{
	uint8_t global_en;
	uint8_t ph_en;
	uint8_t modem_en;
	uint8_t chip_en;
};

/* ---- fast response register property ------------------------------------- */
enum frr_mode
{
	FRR_DISABLED,
	FRR_GLOBAL_STAT,
	FRR_GLOBAL_PEND,
	FRR_PH_STAT,
	FRR_PH_PEND,
	FRR_MODEM_STAT,
	FRR_MODEM_PEND,
	FRR_CHIP_STAT,
	FRR_CHIP_PEND,
	FRR_CURR_STATE,
	FRR_LATCHED_RSSI,
};

__packed struct frr_all_mode
{
	uint8_t a_mode;
	uint8_t b_mode;
	uint8_t c_mode;
	uint8_t d_mode;
};

/* ---- preamble configuration property ------------------------------------- */
static uint8_t const preamble_std1_skip_sync_timeout       = 1 << 7;
static uint8_t const preamble_std1_rx_thresh_mask          = 0x7f;
static uint8_t const preamble_std1_rx_thresh_shift         = 0;

static uint8_t const preamble_nstd_rx_errors_mask          = 0x07;
static uint8_t const preamble_nstd_rx_errors_shift         = 5;
static uint8_t const preamble_nstd_pattern_length_mask     = 0x1f;
static uint8_t const preamble_nstd_pattern_length_shift    = 0;

static uint8_t const preamble_std2_rx_timeout_extend_mask  = 0x0f;
static uint8_t const preamble_std2_rx_timeout_extend_shift = 4;
static uint8_t const preamble_std2_rx_timeout_mask         = 0x0f;
static uint8_t const preamble_std2_rx_timeout_shift        = 0;

static uint8_t const preamble_cfg_dsa_only                 = 1 << 7;
static uint8_t const preamble_cfg_first_bit_one            = 1 << 5;
static uint8_t const preamble_cfg_length_in_bytes          = 1 << 4; /* else nibbles */
static uint8_t const preamble_cfg_manchester_const         = 1 << 3;
static uint8_t const preamble_cfg_manchester_en            = 1 << 2;
static uint8_t const preamble_cfg_standard_mask            = 0x03;
static uint8_t const preamble_cfg_standard_shift           = 0;

enum preamble_standard
{
	PREAMBLE_NON_STANARD,
	PREAMBLE_1010,
	PREAMBLE_0101,
};

static uint8_t const preamble_postamble_en                 = 1 << 7;
static uint8_t const preamble_postamble_packet_vaild       = 1 << 6;
static uint8_t const preamble_postamble_size_mask          = 0x03;
static uint8_t const preamble_postamble_size_shift         = 0;

__packed struct preamble_cfg
{
	uint8_t tx_length;
	uint8_t std1;
	uint8_t nstd;
	uint8_t std2;
	uint8_t cfg;
	uint8_t nstd_pattern_be[4];
	uint8_t postamble_cfg;
	uint8_t postamble_pattern_be[4];
};

/* ---- sync control property ----------------------------------------------- */
static uint8_t const sync_cfg_skip_tx            = 1 << 7;
static uint8_t const sync_cfg_rx_errors_mask     = 0x07;
static uint8_t const sync_cfg_rx_errors_shift    = 4;
static uint8_t const sync_cfg_4fsk               = 1 << 3;
static uint8_t const sync_cfg_manshester_en      = 1 << 2;
static uint8_t const sync_cfg_size_mask          = 0x03;
static uint8_t const sync_cfg_size_shift         = 0;

static uint8_t const sync_cfg2_errors_begin_only = 1 << 7;
static uint8_t const sync_cfg2_sub_size_mask     = 0x03;
static uint8_t const sync_cfg2_sub_size_shift    = 0;

enum sync_sub_size
{
	SYNC_SUB0,
	SYNC_SUB2,
	SYNC_SUB4,
	SYNC_SUB6,
};

__packed struct sync_cfg
{
	uint8_t cfg;
	uint8_t word_be[4];
	uint8_t cfg2;
};

/* ---- packet control property --------------------------------------------- */
static uint8_t const packet_crc_seed_0xff      = 1 << 7; /* else 0x00 */
static uint8_t const packet_crc_alt_poly_mask  = 0x07;
static uint8_t const packet_crc_alt_poly_shift = 4;
static uint8_t const packet_crc_poly_mask      = 0x0f;
static uint8_t const packet_crc_poly_shift     = 0;

enum packet_crc_poly
{
	CRC_NONE,
	CRC_ITU_T_8,
	CRC_IEC_16,
	CRC_BAICHEVA_16,
	CRC_IBM_16,
	CRC_CCITT_16,
	CRC_ALT_DNP_16_OR_KOOPMAN_32, /* for alt crc DNP_16, for main crc KOOPMAN_32 */
	CRC_IEEE_802_3_32,            /* main crc only */
	CRC_CASTAGNOLI_32,            /* main crc only */
	CRC_DNP_16,                   /* main crc only */
};

static uint8_t const packet_white_en               = 1 << 7;
static uint8_t const packet_white_crc_en           = 1 << 6;
static uint8_t const packet_white_dir_0_to_7       = 1 << 5; /* else 7_to_0 */
static uint8_t const packet_white_bits_mask        = 0x0f;
static uint8_t const packet_white_bits_shift       = 0;

static uint8_t const packet_cfg1_ph_field_split    = 1 << 7; /* rx and tx ph fileds are different */
static uint8_t const packet_cfg1_ph_rx_off         = 1 << 6;
static uint8_t const packet_cfg1_4fsk_en           = 1 << 5;
static uint8_t const packet_cfg1_manchester_01     = 1 << 3; /* else manchester_10 */
static uint8_t const packet_cfg1_invert_crc        = 1 << 2;
static uint8_t const packet_cfg1_crc_bytes_be      = 1 << 1; /* msbyte first */
static uint8_t const packet_cfg1_data_bits_le      = 1 << 0; /* lsbit first */

static uint8_t const packet_cfg2_crc_bits_le       = 1 << 7; /* lsbit first */
static uint8_t const packet_cfg2_padding_en        = 1 << 6;
static uint8_t const packet_cfg2_alt_crc_seed_0xff = 1 << 5; /* else 0x00 */
static uint8_t const packet_cfg2_codec_3_of_6_en   = 1 << 4;

static uint8_t const packet_length_infinite        = 1 << 6;
static uint8_t const packet_length_value_be        = 1 << 5;
static uint8_t const packet_length_16bits          = 1 << 4; /* else 8 bits */
static uint8_t const packet_length_leave_values    = 1 << 3; /* leave length values of fields */
static uint8_t const packet_length_variable_mask   = 0x07;
static uint8_t const packet_length_variable_shift  = 0;

enum packet_length_variable
{
	PACKET_VARIABLE_DISABLED,
	PACKET_FIELD2_VARIABLE = 2,
	PACKET_FIELD3_VARIABLE,
	PACKET_FIELD4_VARIABLE,
	PACKET_FIELD5_VARIABLE,
	PACKET_VARIABLE_DISABLED2,
	PACKET_VARIABLE_DISABLED3,
};

static uint8_t packet_field_src_mask  = 0x07;
static uint8_t packet_field_src_shift = 0;

enum packet_field_src
{
	PACKET_SRC_FIELD1_DEFAULT, /* same as PACKET_SRC_FIELD1 */
	PACKET_SRC_FIELD1,         /* same as PACKET_SRC_FIELD1_DEFAULT */
	PACKET_SRC_FIELD2,
	PACKET_SRC_FIELD3,
	PACKET_SRC_FIELD4,
};

static uint8_t const packet_field_cfg_4fsk_en       = 1 << 4;
static uint8_t const packet_field_cfg_pn_start      = 1 << 2;
static uint8_t const packet_field_cfg_white_en      = 1 << 1;
static uint8_t const packet_field_cfg_manchester_en = 1 << 0;

static uint8_t const packet_field_crc_start         = 1 << 7;
static uint8_t const packet_field_crc_alt_start     = 1 << 6;
static uint8_t const packet_field_crc_send          = 1 << 5; /* tx only */
static uint8_t const packet_field_crc_alt_send      = 1 << 4; /* tx only */
static uint8_t const packet_field_crc_check         = 1 << 3;
static uint8_t const packet_field_crc_alt_check     = 1 << 2;
static uint8_t const packet_field_crc_en            = 1 << 1;
static uint8_t const packet_field_crc_alt_en        = 1 << 0;

__packed struct packet_field_cfg
{
	uint8_t length_be[2];
	uint8_t cfg;
	uint8_t crc;
};

__packed struct packet_cfg
{
	uint8_t crc;
	uint8_t white_poly_be[2];
	uint8_t white_seed_be[2];
	uint8_t white_cfg;
	uint8_t cfg1;
	uint8_t cfg2;
	uint8_t length;
	uint8_t field_src;
	uint8_t length_adjust;
	uint8_t tx_thresh;
	uint8_t rx_thresh;
	struct  packet_field_cfg field[5];
	struct  packet_field_cfg rx_field[5];
	uint8_t crc_seed_be[4];
};

/* ---- modem configuration property ---------------------------------------- */
static uint8_t const modem_modul_tx_async      = 1 << 7;
static uint8_t const modem_modul_tx_gpio_mask  = 0x03;
static uint8_t const modem_modul_tx_gpio_shift = 5;
static uint8_t const modem_modul_src_mask      = 0x03;
static uint8_t const modem_modul_src_shift     = 3;
static uint8_t const modem_modul_type_mask     = 0x07;
static uint8_t const modem_modul_type_shift    = 0;

enum modem_modul_src
{
	MODUL_SRC_PACKET, /* tx fifo */
	MODUL_SRC_DIRECT, /* gpio */
	MODUL_SRC_PSEUDO, /* random */
};

enum modem_modul_type
{
	MODUL_CW,
	MODUL_OOK,
	MODUL_2FSK,
	MODUL_2GFSK,
	MODUL_4FSK,
	MODUL_4GFSK,
};

static uint8_t const modem_map_ctl_adjust_sync_timeout = 1 << 7;
static uint8_t const modem_map_ctl_invert_rx_bits      = 1 << 6;
static uint8_t const modem_map_ctl_invert_tx_bits      = 1 << 5;
static uint8_t const modem_map_ctl_invert_deviation    = 1 << 4;

static uint8_t const modem_dsm_ctl_xtal_clock          = 1 << 7; /* else pll_clock */
static uint8_t const modem_dsm_ctl_single_loop         = 1 << 6; /* else mash_1_1_1 */
static uint8_t const modem_dsm_ctl_dithering_en        = 1 << 5;
static uint8_t const modem_dsm_ctl_dithering_p1_n1     = 1 << 4; /* else p1_0 */
static uint8_t const modem_dsm_ctl_reset               = 1 << 3; /* else enable */
static uint8_t const modem_dsm_ctl_lsb_forced_high     = 1 << 2;
static uint8_t const modem_dsm_ctl_order_mask          = 0x03;
static uint8_t const modem_dsm_ctl_order_shift         = 0;

static uint8_t const modem_tx_nco_oversample_mask      = 0x03;
static uint8_t const modem_tx_nco_oversample_shift     = 2;
static uint8_t const modem_tx_nco_modulus_25_24_mask   = 0x03;
static uint8_t const modem_tx_nco_modulus_25_24_shift  = 0;

enum modem_tx_nco_oversample
{
	TX_NCO_GAUSS_OVRSMPL10,
	TX_NCO_GAUSS_OVRSMPL40,
	TX_NCO_GAUSS_OVRSMPL20,
};

static uint8_t const modem_tx_ramp_delay_mask          = 0x07;
static uint8_t const modem_tx_ramp_delay_shift         = 0;

static uint8_t const modem_mdm_ctl_input_detector      = 1 << 7; /* else phase_computer */

static uint8_t const modem_if_ctl_zero                 = 1 << 4; /* else non-zero if */
static uint8_t const modem_if_ctl_fixed                = 1 << 3; /* else scaled */
static uint8_t const modem_if_ctl_etsi_mode_mask       = 0x03;
static uint8_t const modem_if_ctl_etsi_mode_shift      = 0;

enum modem_if_ctl_etsi_mode
{
	ETSI_DISABLED,
	ETSI_868MHZ,
	ETSI_169MHZ,
};

static uint8_t const modem_decim_cfg1_ndec2_mask       = 0x03;
static uint8_t const modem_decim_cfg1_ndec2_shift      = 6;
static uint8_t const modem_decim_cfg1_ndec1_mask       = 0x03;
static uint8_t const modem_decim_cfg1_ndec1_shift      = 4;
static uint8_t const modem_decim_cfg1_ndec0_mask       = 0x07;
static uint8_t const modem_decim_cfg1_ndec0_shift      = 1;

enum modem_decim_ndec
{
	DECIMATE_1,
	DECIMATE_2,
	DECIMATE_4,
	DECIMATE_8,
	DECIMATE_16,  /* ndec0 only */
	DECIMATE_32,  /* ndec0 only */
	DECIMATE_64,  /* ndec0 only */
	DECIMATE_128, /* ndec0 only */
};

static uint8_t const modem_decim_cfg0_chfilt_low_pwr           = 1 << 7; /* else chfilt_norm */
static uint8_t const modem_decim_cfg0_droopfilt_bypass         = 1 << 6;
static uint8_t const modem_decim_cfg0_dec3filt_bypass          = 1 << 4;
static uint8_t const modem_decim_cfg0_dec2filt_bypass          = 1 << 3;
static uint8_t const modem_decim_cfg0_rx_dec8filt_gain_x2      = 1 << 2;

static uint8_t const modem_decim_cfg2_ndec3_mask               = 0x03;
static uint8_t const modem_decim_cfg2_ndec3_shift              = 5;
static uint8_t const modem_decim_cfg2_ndec2_gain_mask          = 0x03;
static uint8_t const modem_decim_cfg2_ndec2_gain_shift         = 3;
static uint8_t const modem_decim_cfg2_ndec2_auto_gain          = 1 << 2;

enum modem_decim_ndec2_gain
{
	NDEC2_GAIN_0DB,
	NDEC2_GAIN_12DB,
	NDEC2_GAIN_24DB,
};

static uint8_t const modem_ifpkd_thresh_mask                   = 0x0f;
static uint8_t const modem_ifpkd_thresh1_shift                 = 4;
static uint8_t const modem_ifpkd_thresh0_shift                 = 0;

static uint8_t const modem_rx_bcr_gear_fast_mask               = 0x07;
static uint8_t const modem_rx_bcr_gear_fast_shift              = 3;
static uint8_t const modem_rx_bcr_gear_slow_mask               = 0x07;
static uint8_t const modem_rx_bcr_gear_slow_shift              = 0;

static uint8_t const modem_rx_bcr_misc1_feedback_trloop_bypass = 1 << 7;
static uint8_t const modem_rx_bcr_misc1_feedback_slicer_bypass = 1 << 6;
static uint8_t const modem_rx_bcr_misc1_nco_compen_en          = 1 << 4;
static uint8_t const modem_rx_bcr_misc1_nco_compen_preamble_vaild   = 1 << 3; /* else preamble_end */
static uint8_t const modem_rx_bcr_misc1_loop_gain_x2           = 1 << 2;
static uint8_t const modem_rx_bcr_misc1_nco_mid_phcorr_off     = 1 << 1;
static uint8_t const modem_rx_bcr_misc1_nco_mid_phcorr_esc_ph_error = 1 << 0; /* else esc_1clk */

static uint8_t const modem_rx_bcr_misc0_adc_wtdog_en           = 1 << 7;
static uint8_t const modem_rx_bcr_misc0_adc_reset              = 1 << 6;
static uint8_t const modem_rx_bcr_misc0_dist_toggle            = 1 << 5; /* else dist_normal */
static uint8_t const modem_rx_bcr_misc0_rx_reset_by3_zero      = 1 << 4; /* else reset_by5_zero */

static uint8_t const modem_rx_afc_gear_mask                    = 0x03;
static uint8_t const modem_rx_afc_gear_shift                   = 6;
static uint8_t const modem_rx_afc_gear_gain_mask               = 0x07;
static uint8_t const modem_rx_afc_gear_fast_gain_shift         = 3;
static uint8_t const modem_rx_afc_gear_slow_gain_shift         = 0;

enum modem_rx_afc_gear
{
	AFC_GEAR_PREAMBLE,
	AFC_GEAR_SYNC_WORD,
	AFC_GEAR_MIDPOINT_FREQ_ERROR,
	AFC_GEAR_PREAMBLE2, /* same as AFC_GEAR_PREAMBLE */
};

static uint8_t const modem_rx_afc_wait_mask                    = 0x0f;
static uint8_t const modem_rx_afc_shwait_shift                 = 4;
static uint8_t const modem_rx_afc_lgwait_shift                 = 0;

static uint8_t const modem_rx_afc_ctl_en                       = 1 << 7;
static uint8_t const modem_rx_afc_adaptive_rx_bw_en            = 1 << 6;
static uint8_t const modem_rx_afc_gain_reduced_by_half         = 1 << 5;

static uint8_t const modem_rx_afc_misc_frozen_after_gear_swt   = 1 << 7; /* else arc operate over entire packet */
static uint8_t const modem_rx_afc_misc_feedback_pll_en         = 1 << 6;
static uint8_t const modem_rx_afc_misc_2tb                     = 1 << 5; /* else moving average or min-max */
static uint8_t const modem_rx_afc_misc_frozen_after_preamble   = 1 << 4;
static uint8_t const modem_rx_afc_misc_clk_swt_to_bcr_bclk     = 1 << 3;
static uint8_t const modem_rx_afc_misc_always_en               = 1 << 1;
static uint8_t const modem_rx_afc_misc_expect_large_freq_error = 1 << 0; /* else not expect (normal) */

static uint8_t const modem_adc_ctl_add_12db_ircal              = 1 << 4;
static uint8_t const modem_adc_ctl_direct_reset_en             = 1 << 3;
static uint8_t const modem_adc_ctl_real                        = 1 << 1; /* else complex */

static uint8_t const modem_agc_ctl_over_entire_packet          = 1 << 7; /* else over_preamble */
static uint8_t const modem_agc_ctl_if_gain_loop_always_dec_3db = 1 << 6;
static uint8_t const modem_agc_ctl_rf_gain_loop_always_dec_3db = 1 << 5;
static uint8_t const modem_agc_ctl_gain_inc_signal_reductions  = 1 << 4;
static uint8_t const modem_agc_ctl_speed_div8                  = 1 << 3;
static uint8_t const modem_agc_ctl_adc_gain_via_agc            = 1 << 1; /* else adjustment disabled */
static uint8_t const modem_agc_ctl_peak_det_reset_cycle        = 1 << 0;

static uint8_t const modem_agc_wnd_size_measure_time_mask      = 0x0f;
static uint8_t const modem_agc_wnd_size_measure_time_shift     = 4;
static uint8_t const modem_agc_wnd_size_settle_time_mask       = 0x0f;
static uint8_t const modem_agc_wnd_size_settle_time_shift      = 0;

static uint8_t const modem_4fsk_gain1_isis_off                 = 1 << 7;
static uint8_t const modem_4fsk_gain1_mask                     = 0x7f;
static uint8_t const modem_4fsk_gain1_shift                    = 0;

static uint8_t const modem_4fsk_gain0_phase_compen_2fsk        = 1 << 7;
static uint8_t const modem_4fsk_gain0_mask                     = 0x7f;
static uint8_t const modem_4fsk_gain0_shift                    = 0;

static uint8_t const modem_ook_peak_det_attack_mask            = 0x07;
static uint8_t const modem_ook_peak_det_attack_shift           = 4;
static uint8_t const modem_ook_peak_det_decay_mask             = 0x0f;
static uint8_t const modem_ook_peak_det_decay_shift            = 0;

static uint8_t const modem_ook_ctl1_s2p_map_mask               = 0x03;
static uint8_t const modem_ook_ctl1_s2p_map_shift              = 6;
static uint8_t const modem_ook_ctl1_agc_frozen_after_preamble  = 1 << 5;
static uint8_t const modem_ook_ctl1_freq_div16                 = 1 << 4;
static uint8_t const modem_ook_ctl1_raw_data_sync_bit_clock    = 1 << 3;

static uint8_t const modem_ook_misc_long_filt_wnd              = 1 << 7;
static uint8_t const modem_ook_misc_limit_discharge            = 1 << 6;
static uint8_t const modem_ook_misc_squelch_en                 = 1 << 5;
static uint8_t const modem_ook_misc_discharge_div_mask         = 0x03;
static uint8_t const modem_ook_misc_discharge_div_shift        = 3;
static uint8_t const modem_ook_misc_det_mask                   = 0x03;
static uint8_t const modem_ook_misc_det_shift                  = 0;

enum modem_ook_discharge_div
{
	OOK_DISCHARGE_NO_DIV,
	OOK_DISCHARGE_DIV2,
	OOK_DISCHARGE_DIV4,
	OOK_DISCHARGE_DIV8,
};

enum modem_ook_det
{
	OOK_MOVAVG_PEAK_DET,
	OOK_PEAK_DET,
	OOK_MOVAVG,
	OOK_MINMAX_DET,
};

static uint8_t const modem_raw_ctl_movavg_or_mean              = 1 << 7;
static uint8_t const modem_raw_ctl_consecutive_1_or_0_bypass   = 1 << 6;
static uint8_t const modem_raw_ctl_preamble_pattern_mask       = 0x03;
static uint8_t const modem_raw_ctl_preamble_pattern_shift      = 2;
static uint8_t const modem_raw_ctl_filt_gain_mask              = 0x03;
static uint8_t const modem_raw_ctl_filt_gain_shift             = 0;

enum modem_raw_ctl_preamble_pattern
{
	RAW_PREAMBLE_1010,
	RAW_PREAMBLE_CONSECUTIVE1,
	RAW_PREAMBLE_CONSECUTIVE0,
	RAW_PREAMBLE_RANDOM,
};

enum modem_raw_ctl_filt_gain
{
	RAW_FILT_GAIN8,
	RAW_FILT_GAIN4,
	RAW_FILT_GAIN2,
	RAW_FILT_GAIN1,
};

static uint8_t const modem_ant_div_mode_swt_timer_mask         = 0x03;
static uint8_t const modem_ant_div_mode_swt_timer_shift        = 6;
static uint8_t const modem_ant_div_mode_bias_1_5db             = 1 << 5;
static uint8_t const modem_ant_div_mode_eval_single_ant        = 1 << 4;
static uint8_t const modem_ant_div_mode_11db_thresh            = 1 << 3; /* else 16db */
static uint8_t const modem_ant_div_mode_wait_mask              = 0x07;
static uint8_t const modem_ant_div_mode_wait_shift             = 0;

enum modem_ant_div_swt_timer
{
	ANT_SWT_9BIT_OR_13BITS_8TB,
	ANT_SWT_11BIT_OR_15BITS_8TB,
	ANT_SWT_13BITS_OR_17BIS_8TB,
	ANT_SWT_15BITS_OR_19BITS_8TB,
};

static uint8_t const modem_ant_div_ctl_preamble_thresh_mask    = 0x0f;
static uint8_t const modem_ant_div_ctl_preamble_thresh_shift   = 4;
static uint8_t const modem_ant_div_ctl_movavg_tap_8tb          = 1 << 3;
static uint8_t const modem_ant_div_ctl_mask                    = 0x03;
static uint8_t const modem_ant_div_ctl_shift                   = 1;

enum modem_ant_div_ctl
{
	ANT_DIV_FIXED,
	ANT_DIV_FIXED_INV,
	ANT_DIV_AUTO,
	ANT_DIV_AUTO_INV,
};

static uint8_t const modem_rssi_ctl1_check_thresh_at_latch     = 1 << 5;
static uint8_t const modem_rssi_ctl1_average_mask              = 0x03;
static uint8_t const modem_rssi_ctl1_average_shift             = 3;
static uint8_t const modem_rssi_ctl1_latch_mask                = 0x07;
static uint8_t const modem_rssi_ctl1_latch_shift               = 0;

enum modem_rssi_average
{
	RSSI_AVG4,
	RSSI_AVG2,
	RSSI_BIT1,
	RSSI_SAMPLE1,
};

enum modem_rssi_latch
{
	RSSI_LATCH_DISABLED,
	RSSI_LATCH_PREAMBLE,
	RSSI_LATCH_SYNC,
	RSSI_LATCH_RX_STATE1,
	RSSI_LATCH_RX_STATE2,
	RSSI_LATCH_RX_STATE3,
	RSSI_LATCH_RX_STATE4,
	RSSI_LATCH_RX_STATE5,
};

static uint8_t const modem_rssi_ctl2_jump_down_en              = 1 << 5;
static uint8_t const modem_rssi_ctl2_jump_up_en                = 1 << 4;
static uint8_t const modem_rssi_ctl2_jump_det_en               = 1 << 3;
static uint8_t const modem_rssi_ctl2_measure_4tb               = 1 << 2; /* else 2tb */
static uint8_t const modem_rssi_ctl2_reset_upon_jump_det       = 1 << 1;

static uint8_t const modem_raw_search2_frozen_upon_gear_swt    = 1 << 7;
static uint8_t const modem_raw_search2_raw_filt_en             = 1 << 6;
static uint8_t const modem_raw_search2_period_mask             = 0x07;
static uint8_t const modem_raw_search2_high_period_shift       = 3;
static uint8_t const modem_raw_search2_low_period_shift        = 0;

enum modem_raw_search_period
{
	RAW_SEARCH_2TB,
	RAW_SEARCH_3TB,
	RAW_SEARCH_4TB,
	RAW_SEARCH_5TB,
	RAW_SEARCH_8TB,
	RAW_SEARCH_12TB,
	RAW_SEARCH_14TB,
	RAW_SEARCH_16TB,
};

static uint8_t const modem_clkgen_band_recal_not_forced        = 1 << 4;
static uint8_t const modem_clkgen_band_high_perform            = 1 << 3;
static uint8_t const modem_clkgen_band_mask                    = 0x07;
static uint8_t const modem_clkgen_band_shift                   = 0;

enum modem_clkgen_band
{
	FVCO_DIV4,
	FVCO_DIV6,
	FVCO_DIV8,
	FVCO_DIV12,
	FVCO_DIV16,
	FVCO_DIV24,
	FVCO_DIV24_2,
	FVCO_DIV24_3,
};

static uint8_t const modem_spike_det_en                        = 1 << 7;
static uint8_t const modem_spike_det_thresh_mask               = 0x7f;
static uint8_t const modem_spike_det_thresh_shift              = 0;

static uint8_t const modem_one_shot_afc_en                     = 1 << 7;
static uint8_t const modem_one_shot_afc_bcr_align_en           = 1 << 6;
static uint8_t const modem_one_shot_afc_data_rate_compen_en    = 1 << 5;
static uint8_t const modem_one_shot_afc_movavg_en              = 1 << 4;
static uint8_t const modem_one_shot_afc_wait_cnt_mask          = 0x0f;
static uint8_t const modem_one_shot_afc_wait_cnt_shift         = 0;

static uint8_t const modem_rssi_mute_delay_en                  = 1 << 3;
static uint8_t const modem_rssi_mute_delay_cnt_mask            = 0x07;
static uint8_t const modem_rssi_mute_delay_cnt_shift           = 0;

static uint8_t const modem_dsa_ctl1_qual_src_mask              = 0x03;
static uint8_t const modem_dsa_ctl1_qual_src_shift             = 6;
static uint8_t const modem_dsa_ctl1_en                         = 1 << 5;
static uint8_t const modem_dsa_ctl1_adjust_smpl_error_mask     = 0x0f;
static uint8_t const modem_dsa_ctl1_asjust_smpl_error_shift    = 0;

enum modem_dsa_qual_src
{
	DSA_SPIKE_QUAL,
	DSA_SPIKE_EYE_QUAL,
};

static uint8_t const modem_dsa_ctl2_sync_en                    = 1 << 6;
static uint8_t const modem_dsa_ctl2_bcd_gear_shift_en          = 1 << 5;
static uint8_t const modem_dsa_ctl2_arrival_thresh_mask        = 0x0f;
static uint8_t const modem_dsa_ctl2_arrival_thresh_shift       = 0;

static uint8_t const modem_dsa_qual_movavg_eye_en              = 1 << 7;
static uint8_t const modem_dsa_qual_arr_mask                   = 0x7f;
static uint8_t const modem_dsa_qual_arr_shift                  = 0;

static uint8_t const modem_dsa_rssi_squelch_en                 = 1 << 7;
static uint8_t const modem_dsa_rssi_thresh_mask                = 0x7f;
static uint8_t const modem_dsa_rssi_thresh_shift               = 0;

static uint8_t const modem_dsa_misc_replace_rssi_thresh_irq    = 1 << 7;
static uint8_t const modem_dsa_misc_open_eye_measure_en        = 1 << 6;
static uint8_t const modem_dsa_misc_open_eye_8bit_wnd          = 1 << 5;
static uint8_t const modem_dsa_misc_low_duty_mask              = 0x07;
static uint8_t const modem_dsa_misc_low_duty_shift             = 0;

__packed struct modem_cfg
{
	uint8_t modul;
	uint8_t map_ctl;
	uint8_t dsm_ctl;
	uint8_t data_rate_be[3];
	uint8_t tx_nco_oversample_and_modulus_25_24;
	uint8_t modulus_be[3];
	uint8_t freq_dev_be[3];
	uint8_t freq_ofs_be[2];
	uint8_t tx_coeff[9];
	uint8_t tx_ramp_delay;
	uint8_t mdm_ctl;
	uint8_t if_ctl;
	uint8_t if_freq_be[3];
	uint8_t decim_cfg1;
	uint8_t decim_cfg0;
	uint8_t decim_cfg2;
	uint8_t ifpkd_thresh;
	uint8_t rx_bcr_ovrsmpl_rate_be[2];
	uint8_t rx_bcr_nso_ofs_be[3];
	uint8_t rx_bcr_gain_be[2];
	uint8_t rx_bcr_gear;
	uint8_t rx_bcr_misc1;
	uint8_t rx_bcr_misc0;
	uint8_t rx_afc_gear;
	uint8_t rx_afc_wait;
	uint8_t rx_afc_ctl_and_gain_12_8;
	uint8_t rx_afc_gain_7_0;
	uint8_t rx_afc_lim_be[2];
	uint8_t rx_afc_misc;
	uint8_t rx_afc_fixed_freq_ofs;
	uint8_t adc_ctl;
	uint8_t agc_ctl;
	uint8_t skipped_value[2];  /* 0x2036, 0x2037 */
	uint8_t agc_wnd_size;
	uint8_t agc_rf_peak_det_decay;
	uint8_t agc_if_peak_det_decay;
	uint8_t _4fsk_gain1;
	uint8_t _4fsk_gain0;
	uint8_t _4fsk_thresh_be[2];
	uint8_t _4fsk_map;
	uint8_t ook_peak_det;
	uint8_t ook_slice_ref;
	uint8_t ook_ctl1;
	uint8_t ook_misc;
	uint8_t skipped_value2;    /* 0x2044 */
	uint8_t raw_ctl;
	uint8_t raw_eye_be[2];
	uint8_t ant_div_mode;
	uint8_t ant_div_ctl;
	uint8_t rssi_thresh;
	uint8_t rssi_jump_thresh;
	uint8_t rssi_ctl1;
	uint8_t rssi_ctl2;
	uint8_t rssi_compen;
	uint8_t skipped_value3;    /* 0x204f */
	uint8_t raw_search2;
	uint8_t clkgen_band;
	uint8_t skipped_value4[2]; /* 0x2052, 0x2053 */
	uint8_t spike_det;
	uint8_t one_shot_afc;
	uint8_t rssi_hyst;
	uint8_t rssi_mute;
	uint8_t rssi_fast_delay;
	uint8_t psm_idle_time_be[2];
	uint8_t dsa_ctl1;
	uint8_t dsa_ctl2;
	uint8_t dsa_qual;
	uint8_t dsa_rssi;
	uint8_t dsa_misc;
};

/* ---- modem filter coefficients property ---------------------------------- */
static uint8_t const modem_filt_coeff_9_8_mask                 = 0x03;
static uint8_t const modem_filt_coeff0_9_8_shift               = 2; /* coeff_9_8[3] */
static uint8_t const modem_filt_coeff1_9_8_shift               = 0; /* coeff_9_8[3] */
static uint8_t const modem_filt_coeff2_9_8_shift               = 6; /* coeff_9_8[2] */
static uint8_t const modem_filt_coeff3_9_8_shift               = 4; /* coeff_9_8[2] */
static uint8_t const modem_filt_coeff4_9_8_shift               = 2; /* coeff_9_8[2] */
static uint8_t const modem_filt_coeff5_9_8_shift               = 0; /* coeff_9_8[2] */
static uint8_t const modem_filt_coeff6_9_8_shift               = 6; /* coeff_9_8[1] */
static uint8_t const modem_filt_coeff7_9_8_shift               = 4; /* coeff_9_8[1] */
static uint8_t const modem_filt_coeff8_9_8_shift               = 2; /* coeff_9_8[1] */
static uint8_t const modem_filt_coeff9_9_8_shift               = 0; /* coeff_9_8[1] */
static uint8_t const modem_filt_coeff10_9_8_shift              = 6; /* coeff_9_8[0] */
static uint8_t const modem_filt_coeff11_9_8_shift              = 4; /* coeff_9_8[0] */
static uint8_t const modem_filt_coeff12_9_8_shift              = 2; /* coeff_9_8[0] */
static uint8_t const modem_filt_coeff13_9_8_shift              = 0; /* coeff_9_8[0] */

__packed struct modem_filt_coeff
{
	uint8_t coeff_7_0[14]; /* inverted order */
	uint8_t coeff_9_8[4];
};

__packed struct modem_filt_coeff_full
{
	struct modem_filt_coeff rx1;
	struct modem_filt_coeff rx2;
};

/* ---- power amplifier property -------------------------------------------- */
static uint8_t pa_mode_extern_tx_ramp_en                       = 1 << 7;
static uint8_t pa_mode_digital_sequencing_en                   = 1 << 6;
static uint8_t pa_mode_mask                                    = 0x0f;
static uint8_t pa_mode_shift                                   = 2;
static uint8_t pa_mode_switched_current                        = 1 << 0; /* else switching_amp */

enum pa_mode
{
	PA_HIGH_FINE   = 1,
	PA_HIGH_COARSE = 2,
	PA_LOW         = 6,
	PA_MEDIUM      = 8,
};

static uint8_t pa_clkduty_mask                                 = 0x03;
static uint8_t pa_clkduty_shift                                = 6;
static uint8_t pa_bias_mask                                    = 0x3f;
static uint8_t pa_bias_shift                                   = 0;

enum pa_clkduty
{
	PA_CLKDUTY_DIFF_50,       /* high-power */
	PA_CLKDUTY_SINGLE_25 = 3, /* low-power */
};

static uint8_t pa_time_const_fsk_delay_mask                    = 0x07;
static uint8_t pa_time_const_fsk_delay_shift                   = 5;
static uint8_t pa_time_const_mask                              = 0x1f;
static uint8_t pa_time_const_shift                             = 0;

enum pa_fsk_delay
{
	PA_FSK_DELAY_2US,
	PA_FSK_DELAY_6US,
	PA_FSK_DELAY_10US,
	PA_FSK_DELAY_14US,
	PA_FSK_DELAY_18US,
	PA_FSK_DELAY_22US,
	PA_FSK_DELAY_26US,
	PA_FSK_DELAY_30US,
};

static uint8_t pa_ramp_extern_must_set                         = 1 << 7;
static uint8_t pa_ramp_extern_time_const_mask                  = 0x0f;
static uint8_t pa_ramp_extern_time_const_shift                 = 0;

static uint8_t pa_seq_cfg_double_inc_step                      = 1 << 7; /* else single */
static uint8_t pa_seq_cfg_delay_mask                           = 0x7f;
static uint8_t pa_seq_cfg_delay_shift                          = 0;

struct pa_cfg
{
	uint8_t mode;
	uint8_t level;
	uint8_t bias_clkduty;
	uint8_t time_const;
	uint8_t ramp_extern;
	uint8_t down_delay;
	uint8_t seq_cfg;
};

/* ---- pll synthesizer property -------------------------------------------- */
static uint8_t const synth_ffcp_curr_inc_160ua                 = 1 << 6;
static uint8_t const synth_ffcp_curr_mask                      = 0x3f;
static uint8_t const synth_ffcp_curr_shift                     = 0;

enum synth_ffcp_curr
{
	FFCP_0UA   = 32,
	FFCP_5UA   = 33,
	FFCP_155UA = 63,
	FFCP_160UA = 0,
	FFCP_165UA = 1,
	FFCP_315UA = 31,
};

static uint8_t const synth_ffcp_int_curr_mask                   = 0x0f;
static uint8_t const synth_ffcp_int_curr_shift                  = 0;

enum synth_ffcp_int_curr
{
	FFCP_INT_0UA  = 8,
	FFCP_INT_5UA,
	FFCP_INT_10UA,
	FFCP_INT_15UA,
	FFCP_INT_20UA,
	FFCP_INT_25UA,
	FFCP_INT_30UA,
	FFCP_INT_35UA,
	FFCP_INT_40UA = 0,
	FFCP_INT_45UA,
	FFCP_INT_50UA,
	FFCP_INT_55UA,
	FFCP_INT_60UA,
	FFCP_INT_65UA,
	FFCP_INT_70UA,
	FFCP_INT_75UA,
};

static uint8_t const synth_vco_kv_dir_mask                     = 0x03;
static uint8_t const synth_vco_kv_dir_shift                    = 2;
static uint8_t const synth_vco_kv_int_mask                     = 0x03;
static uint8_t const synth_vco_kv_int_shift                    = 0;

enum synth_vco_kv_dir
{
	VCO_KV_DIR_GND,
	VCO_KV_DIR_HALF,
	VCO_KV_DIR_MAX,
	VCO_KV_DIR_MAX2, /* same as VCO_KV_DIR_MAX */
};

enum synth_vco_kv_int
{
	VCO_KV_INT_GND,
	VCO_KV_INT_33PRC,
	VCO_KV_INT_66PRC,
	VCO_KV_INT_100PRC,
};

static uint8_t const synth_loop_filt_ctl3_r2_mask              = 0x07;
static uint8_t const synth_loop_filt_ctl3_r2_shift             = 0;

enum synth_loop_filt_r2
{
	LOOP_FILT_R2_18KOHM,
	LOOP_FILT_R2_36KOHM,
	LOOP_FILT_R2_54KOHM,
	LOOP_FILT_R2_72KOHM,
	LOOP_FILT_R2_90KOHM,
	LOOP_FILT_R2_108KOHM,
	LOOP_FILT_R2_126KOHM,
	LOOP_FILT_R2_144KOHM,
};

static uint8_t const synth_loop_filt_ctl2_c2_mask              = 0x1f;
static uint8_t const synth_loop_filt_ctl2_c2_shift             = 0;

static uint    const synth_loop_filt_ctl2_c2_base_ff           = 877;
static uint    const synth_loop_filt_ctl2_cr_step_ff           = 335;

static uint8_t const synth_loop_filt_ctl1_c1_mask              = 0x07;
static uint8_t const synth_loop_filt_ctl1_c1_shift             = 4;
static uint8_t const synth_loop_filt_ctl1_c1_code_mask         = 0x03;
static uint8_t const synth_loop_filt_ctl1_c1_code_shift        = 2;
static uint8_t const synth_loop_filt_ctl1_c3_mask              = 0x03;
static uint8_t const synth_loop_filt_ctl1_c3_shift             = 0;

static uint    const synth_loop_filt_ctl1_c1_base_ff           = 4550;
static uint    const synth_loop_filt_ctl1_c1_step_ff           = 380;
static uint    const synth_loop_filt_ctl1_c1_code_base_ff      = 0;
static uint    const synth_loop_filt_ctl1_c1_code_step_ff      = 1000;
static uint    const synth_loop_filt_ctl1_c3_base_ff           = 9000;
static uint    const synth_loop_filt_ctl1_c3_step_ff           = 1000;

static uint8_t const synth_loop_filt_ctl0_bias_mask            = 0x03;
static uint8_t const synth_loop_filt_ctl0_bias_shift           = 0;

enum synth_loop_filt_bias
{
	LOOP_FILT_BIAS_25UA,
	LOOP_FILT_BIAS_34UA,
	LOOP_FILT_BIAS_50UA,
	LOOP_FILT_BIAS_100UA,
};

static uint8_t const synth_vco_kvcal_ladder_en                 = 1 << 4;
static uint8_t const synth_vco_kvcal_mask                      = 0x0f;
static uint8_t const synth_vco_kvcal_shift                     = 0;

struct synth_cfg
{
	uint8_t ffcp_curr;
	uint8_t ffcp_int_curr;
	uint8_t vco_kv;
	uint8_t loop_filt_ctl3;
	uint8_t loop_filt_ctl2;
	uint8_t loop_filt_ctl1;
	uint8_t loop_filt_ctl0;
	uint8_t vco_kvcal;
};

/* ---- match property ------------------------------------------------------ */
static uint8_t const match_ctl_invert                          = 1 << 7;
static uint8_t const match_first_ctl_en                        = 1 << 6;
static uint8_t const match_not_first_logic_or_prev             = 1 << 6; /* else logic_and_prev */
static uint8_t const match_ctl_ofs_mask                        = 0x1f;
static uint8_t const match_ctl_ofs_shift                       = 0;

__packed struct match_byte
{
	uint8_t value;
	uint8_t mask;
	uint8_t ctl;
};

__packed struct match_cfg
{
	struct match_byte byte[4];
};

/* ---- frequency control property ------------------------------------------ */
__packed struct freq_cfg
{
	uint8_t integer;
	uint8_t fract_be[3];
	uint8_t channel_step_size_be[2];
	uint8_t wnd_size;
	uint8_t vco_cnt_rx_adjust;
};

/* ---- rx hop property ----------------------------------------------------- */
static uint8_t const rx_hop_ctl_mode_mask                      = 0x07;
static uint8_t const rx_hop_ctl_mode_shift                     = 4;
static uint8_t const rx_hop_ctl_rssi_timeout_mask              = 0x0f;
static uint8_t const rx_hop_ctl_rssi_timeout_shift             = 0;

enum rx_hop_mode
{
	RX_HOP_DISABLED,
	RX_HOP_PREAMBLE_TIMEOUT,
	RX_HOP_RSSI_PREAMBLE_TIMEOUT,
	RX_HOP_PREAMBLE_SYNC_TIMEOUT,
	RX_HOP_RSSI_PREAMBLE_SYNC_TIMEOUT,
};

__packed struct rx_hop
{
	uint8_t ctl;
	uint8_t table_size;
	uint8_t table[64];
};

} /* namespace si446x */

#endif /* __SI446X_DEFS_HPP__ */
