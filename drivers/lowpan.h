#ifndef _LOWPAN24_H_
#define _LOWPAN24_H_

#include "crc.h"
//#include "usart.h"
#include "lpalg/cbor.h"



//#define LOPAN_JOINED_TO_NET      0x01  // node has network address

//#define LOPAN_FEATURE_NEED_SECRET        0x01  // node need secret
//#define LOPAN_FEATURE_TEA_SECRET         0x02  // node has secret and TEA encription enabled
#define LOPAN_FEATURE_SECRET_REJECTED    0x04  // node has rejected SECRET DUE to control ALGORITHM
#define LOPAN_FEATURE_PIN_CONTROL        0x08  // node need to control PIN code from server
#define LOPAN_FEATURE_PIN_ACCEPTED       0x10  // node accept PIN control
#define LOPAN_FEATURE_PIN_NOTACCEPTED    0x20  // node reject PIN control
#define LOPAN_FEATURE_HAS_RANDOM         0x40  // node has coordinator random number and can control pin and secret
#define LOPAN_FEATURE_CONNECTED          0x80  // node connected


#define LCP_OPT_RENEW_ADDR              0x01

#define LOPAN_HEADER_TYPE_MASK        0xE0
#define LOPAN_HEADER_MORE_DATA        0x10
#define LOPAN_HEADER_WAIT_ACK         0x08
#define LOPAN_HEADER_HOPS_MASK        0x07


#define LOPAN_HEADER_TYPE_FRAGMENT    0x80
#define LOPAN_HEADER_CLEAN_TYPE       0x78 // exclude fragment bit and hops count

#define LOPAN_HEADER_TYPE_LCP         0x00
#define LOPAN_HEADER_TYPE_ITMP        0x20
#define LOPAN_HEADER_TYPE_IP4         0x40
#define LOPAN_HEADER_TYPE_IP6         0x60
#define LOPAN_HEADER_TYPE_FUTHER      0x80
#define LOPAN_HEADER_TYPE_ITMP_FIRST  0xA0
#define LOPAN_HEADER_TYPE_IP4_FIRST   0xC0
#define LOPAN_HEADER_TYPE_IP6_FIRST   0xE0


//#define LOPAN_HEADER_ENCRYPTION       0x10

//#define LOPAN_HEADER_SN               0x08 // SN: Sequence Number
//#define LOPAN_HEADER_NESN             0x04 // NESN: Next Expected Sequence Number.
//#define LOPAN_HEADER_MD               0x02
//#define LOPAN_HEADER_HOPS             0x01

//#define LOPAN_HEADER_SN_SHIFT         3
//#define LOPAN_HEADER_NESN_SHIFT       2






/////////////// LCP ///////////////////


#define LOWPAN_LOCAL_BROADCAST 0xffu
#define LOWPAN_GLOBAL_BROADCAST 0x00

//////////////////

#define LCP_JOIN          0x00

#define LCP_CONNECT       0x01
#define LCP_CONNECT_PAR   0x02
#define LCP_CONNECT_ACK   0x03
#define LCP_CONNECT_REJ   0x04
#define LCP_TERMINATE     0x05

#define LCP_FEATURE_REQ   0x06
#define LCP_FEATURE_RSP   0x07

#define LCP_PING_REQ      0x08
#define LCP_PING_RSP      0x09

#define LCP_SLEEP         0x0A

#define LCP_POLL          0x10



#define LCP_JOIN_ACK      0x0F

//#define LCP_JOIN_NET_ADDR_LEN      0x0F
//#define LCP_JOIN_NET_ADDR_SIGNED   0x80



#define LCP_SCAN_OPT_TEA_SECRET          0x01




#define BROADCAST_ADDRESS 0xFFFFFFFFU
#define UNKNOWN_ADDRESS   0x00000000U

#define LOPAN_JOINED_TO_XTEA     0x02

#define LOWPAN_LOCAL_BROADCAST  0xffu
#define LOWPAN_GLOBAL_BROADCAST 0x00

//#define ENABLE_IPV4_PROCESSING
//#define ENABLE_IPV6_PROCESSING


class itmp_core;

/*

#ifdef USART
#define NO_ADDRESS                0x00
#define SLAVE_ADDRESS             0x04 // in pkt address of slave device looked from slave device (I am slave)
#define MASTER_ADDRESS            0x08 // in pkt address of slave device looked from master device (we are master)
#define TWO_ADDRESSES             0x0C

//#define address_mode SLAVE_ADDRESS
#define address_mode TWO_ADDRESSES



class itmp_link_serial: public itmp_link
  {
  byte* __inbuf;
  int __inbuflen;
  usart* serial;
  int received;
  byte curcrc;
  byte lastchar;
  void write_byte(byte d,byte* crc);
public:
  itmp_link_serial(usart* _serial, byte* buf, int buflen)
    {
    serial=_serial;
    __inbuf=buf;
    __inbuflen=buflen;
    curcrc=0xFF;
    received=0;
    lastchar=0;
    //state = LINK_STATE_CONNECTED;
    }
  virtual void set_addr(byte addr);
  virtual void process();
  virtual void send_frame(uint32_t tp, uint32_t da, const byte* header, int headerlen, const byte* body, int body_len);
  };

#endif
*/
class lowpan_node: public itmp_link
  {
  si4463& rf;

public:
  lowpan_node(si4463& _rf):itmp_link(0,0),rf(_rf) { } // mark first byte as unset
  void begin(gpio_pin* _linkPin, gpio_pin* _actPin);

  int isJoined();
  int sendJoin();

  int sendPoll();

  virtual int readAndProcess(unsigned int timeout);
  virtual void send_frame(uint32_t tp, uint32_t da, const byte* header, int headerlen, const byte* body, int body_len);
  virtual void set_addr(byte addr);

  //void power_up_for_send();
  void power_down();

protected:
//  byte _net_addr[nrf_ADDR_LEN]; // radio network address (full address is local broadcast addtess)
#ifdef ENABLE_IPV4_PROCESSING
  uint32_t myipaddr;
#endif

  unsigned int pastmoment;
  unsigned int TimeOfLastActivity;

  unsigned int rcv_fragmentID; // ID of temporarily stored fragment of fragmented message
  int rcv_fragmentLen; // length of stored part of fragmented message
  int rcv_fragmentType; // type (header and addresses) of temporarily stored fragment of fragmented message

  unsigned int snd_fragmentID; // ID of temporarily stored fragment of fragmented message
  int snd_fragmentLen; // length of stored part of fragmented message
  int snd_fragmentType; // type (header and addresses) of temporarily stored fragment of fragmented message

  /// get route information for given address
  /// return next hop address
  /// return 0 if no routing information for node it meanse that it should be trasmitted to gateway
  /// return 0xFF if no routing information for node it meanse that it should be trasmitted to gateway
  uint8_t getRoute(uint8_t to)
    {
    return 0;
    }

  int read(byte* inbuf, int buflen);
  int int_read(byte* inbuf, int buflen);
  int int_send_frame(const byte* header, int headerlen, const byte* body, int body_len);

#ifdef ENABLE_IPV4_PROCESSING
  void process_ipv4_frame(byte* inbuf, int inbuflen);
#endif
#ifdef ENABLE_IPV6_PROCESSING
  void process_ipv6_frame(byte* inbuf, int inbuflen);
#endif


//  void test_ack();
  };




class CBOR_encoder;
class CBOR_decoder;

typedef int (*itmp3handler) (const char* url, CBOR_decoder& dec, CBOR_encoder& enc);
//typedef int (*rpc_func)(const char* url, const byte* input, int input_len, byte* output);
typedef int (*descr_func)(const char* url, byte* output);


struct topic_descr2
  {
  unsigned short type;
  unsigned short namelen;
  const char* name;
  union {
  const char* descr;
  const descr_func dfunc;
  };
  const itmp3handler handler;
//  const rpc_func handler;
  };




/*


// Connection
#define ITMP_TP_CONNECT 0             // [CONNECT, Realm|uri, Details|dict] 	open connection
#define ITMP_TP_CONNECTED 1           // [CONNECTED, Session|id, Details|dict] 	confirm connection
#define ITMP_TP_ABORT 2               // [ABORT, Code|integer, Reason|string, Details|dict] 	terminate connection
#define ITMP_TP_DISCONNECT 3          // [DISCONNECT, Code|integer, Reason|string, Details|dict] 	clear finish connection
//	Information
#define ITMP_TP_KEEP_ALIVE 4          // [KEEP_ALIVE] keep alive
#define ITMP_TP_ERROR 5               // 	[ERROR, Request|id, Code|integer, Reason|string, Details|dict] 	error notificarion
//	Description
#define ITMP_TP_DESCRIBE 6            // [DESCRIBE, Request|id, Topic|uri, Options|dict] 	get description
#define ITMP_TP_DESCRIPTION 7         // [DESCRIPTION, DESCRIBE.Request|id, description|list, Options|dict] 	description response
//	RPC
#define ITMP_TP_CALL 8                // [CALL, Request|id, Procedure|uri, Arguments, Options|dict] 	call
#define ITMP_TP_RESULT 9              // [RESULT, CALL.Request|id, Result, Details|dict] 	call response
//	RPC Extended
#define ITMP_TP_ARGUMENTS 10          // [ARGUMENTS, CALL.Request|id, Arguments, Options|dict] 	additional arguments for call
#define ITMP_TP_PROGRESS 11           // [PROGRESS, CALL.Request|id, Result, Details|dict] 	call in progress
#define ITMP_TP_CANCEL 12             // [CANCEL, CALL.Request|id, Details|dict] 	call cancel
//	publish
#define ITMP_TP_EVENT 13              // [EVENT, Request|id, Topic|uri, Arguments, Options|dict] 	event
#define ITMP_TP_PUBLISH 14            // [PUBLISH, Request|id, Topic|uri, Arguments, Options|dict] 	event with acknowledge awaiting
#define ITMP_TP_PUBLISHED 15          // [PUBLISHED, Request|id, Publication|id, Options|dict] 	event acknowledged
//	subscribe
#define ITMP_TP_SUBSCRIBE 16          // [SUBSCRIBE, SUBSCRIBE.Request|id, Topic|uri, Options|dict] 	subscribe
#define ITMP_TP_SUBSCRIBED 17         // [SUBSCRIBED, Request|id, SubscriptionId|id, Options|dict] 	subscription confirmed
#define ITMP_TP_UNSUBSCRIBE 18        // [UNSUBSCRIBE, Request|id, Topic|uri, Options|dict]
#define ITMP_TP_UNSUBSCRIBE_BY_ID 19  // [UNSUBSCRIBE_BY_ID, Request|id, SUBSCRIBED.SubscriptionId|id, Options|dict] 	unsibscribe
#define ITMP_TP_UNSUBSCRIBED 20       // [UNSUBSCRIBED, UNSUBSCRIBE.Request|id, Options|dict]
//	anounce
#define ITMP_TP_ANOUNCE 21 // [ANOUNCE, Request|id, Topic|uri, description|list, Options|dict] 	announce interface or event
#define ITMP_TP_ACCEPTED 22 // [ACCEPTED, ANOUNCE.Request|id, Options|dict] 	accept announcement

*/
#endif
