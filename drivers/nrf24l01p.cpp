#include "nrf24l01p.h"
#ifdef LPOS10
#include "LPOS_timers.h"
#else
#include <tim.h>
#endif


struct nrf24l01p_cfg
  {
    unsigned char _CONFIG;
    unsigned char _EN_AA;
    unsigned char _EN_RXADDR;
    unsigned char _SETUP_AW;
    unsigned char _SETUP_RETR;
    unsigned char _RF_CH;
    unsigned char _RF_SETUP;
    unsigned char _STATUS;
    unsigned char _OBSERVE_TX;
    unsigned char _RPD;
    unsigned char _RX_ADDR_P0[5];
    unsigned char _RX_ADDR_P1[5];
    unsigned char _RX_ADDR_P2;
    unsigned char _RX_ADDR_P3;
    unsigned char _RX_ADDR_P4;
    unsigned char _RX_ADDR_P5;
    unsigned char _TX_ADDR[5];
    unsigned char _RX_PW_P0;
    unsigned char _RX_PW_P1;
    unsigned char _RX_PW_P2;
    unsigned char _RX_PW_P3;
    unsigned char _RX_PW_P4;
    unsigned char _RX_PW_P5;
    unsigned char _FIFO_STATUS;


    unsigned char _DYNPD;
    unsigned char _FEATURE;
  };


nrf24l01p_cfg nrf_cfg;

void nrf24l01p::readCFG()
{
    readRegister(0x00,&nrf_cfg._CONFIG,1);
    readRegister(0x01,&nrf_cfg._EN_AA,1);
    readRegister(0x02,&nrf_cfg._EN_RXADDR,1);
    readRegister(0x03,&nrf_cfg._SETUP_AW,1);
    readRegister(0x04,&nrf_cfg._SETUP_RETR,1);
    readRegister(0x05,&nrf_cfg._RF_CH,1);
    readRegister(0x06,&nrf_cfg._RF_SETUP,1);
    readRegister(0x07,&nrf_cfg._STATUS,1);
    readRegister(0x08,&nrf_cfg._OBSERVE_TX,1);
    readRegister(0x09,&nrf_cfg._RPD,1);
    readRegister(0x0A,nrf_cfg._RX_ADDR_P0,5);
    readRegister(0x0B,nrf_cfg._RX_ADDR_P1,5);
    readRegister(0x0C,&nrf_cfg._RX_ADDR_P2,1);
    readRegister(0x0D,&nrf_cfg._RX_ADDR_P3,1);
    readRegister(0x0E,&nrf_cfg._RX_ADDR_P4,1);
    readRegister(0x0F,&nrf_cfg._RX_ADDR_P5,1);
    readRegister(0x10,nrf_cfg._TX_ADDR,5);
    readRegister(0x11,&nrf_cfg._RX_PW_P0,1);
    readRegister(0x12,&nrf_cfg._RX_PW_P1,1);
    readRegister(0x13,&nrf_cfg._RX_PW_P2,1);
    readRegister(0x14,&nrf_cfg._RX_PW_P3,1);
    readRegister(0x15,&nrf_cfg._RX_PW_P4,1);
    readRegister(0x16,&nrf_cfg._RX_PW_P5,1);
    readRegister(0x17,&nrf_cfg._FIFO_STATUS,1);


    readRegister(0x1C,&nrf_cfg._DYNPD,1);
    readRegister(0x1D,&nrf_cfg._FEATURE,1);
  };

//nrf24l01p nrf = nrf24l01p();

void wait_us(int us)
  {
  while (us>0)
    {
    for (int i=0;i<10;i++)
      __no_operation();
    us--;
    }
  }

nrf24l01p::nrf24l01p(){
	//cePin = 8;
	//csnPin = 7;
	//channel = 1;
//	payload = 16;
	_spi = NULL;
  enabled_pipes=0;
  state = NRF_STATE_POWER_DOWN;
}

void nrf24l01p::transferSync(uint8_t *dataout,uint8_t *datain,uint8_t len){
	uint8_t i;
	for(i = 0;i < len;i++){
		datain[i] = _spi->transfer(dataout[i]);
	}
}

void nrf24l01p::transmitSync(const uint8_t *dataout,uint8_t len){
	uint8_t i;
	for(i = 0;i < len;i++){
		_spi->transfer(dataout[i]);
	}
}


#ifdef LPOS10
void nrf24l01p::init(spi_t *__spi)
#else
void nrf24l01p::init(spi *__spi, gpio_pin* _cePin, gpio_pin* _csnPin, gpio_pin* _irqPin )
#endif
// Initializes pins to communicate with the nrf module
// Should be called in the early initializing phase at startup.
{
  _spi=__spi;
  cePin=_cePin;
  csnPin=_csnPin;
  irqPin=_irqPin;
#if 0
#ifdef LPOS10
	cePin.acquire("/dev/gpio/@pe.0 mode=out");
	csnPin.acquire("/dev/gpio/@pa.15 mode=out");
#elif TargetCPU == STM32L051
	cePin.acquire(PORTA | PIN1, GPIO_OUT | GPIO_10MHz);
	csnPin.acquire(PORTA | PIN4, GPIO_OUT | GPIO_10MHz);
	irqPin.acquire(PORTB | PIN0, GPIO_IN | GPIO_FLOATING); // digital output for nRF24L01+
#else
	cePin.acquire(PORTA | PIN1, GPIO_OUT_PUSHPULL_50MHz);
	csnPin.acquire(PORTA | PIN4, GPIO_OUT_PUSHPULL_50MHz);
	irqPin.acquire(PORTB | PIN0, GPIO_IN_FLOATING); // digital output for nRF24L01+
#endif
#endif

    ceLow(); // radio part is off
    csnHi(); // chip is not selected

    // Initialize _spi module
    _spi->begin(1000000); // 1Mhz up to 10Mhz and up to 4Mhz in some states
    state = NRF_STATE_POWER_DOWN;

}

void nrf24l01p::enable_pipe(int pipe_num)
  {
  enabled_pipes|=1<<pipe_num;
  configRegister(EN_RXADDR, enabled_pipes );
  }

void nrf24l01p::disable_pipe(int pipe_num)
  {
  enabled_pipes&=~(1<<pipe_num);
  configRegister(EN_RXADDR, enabled_pipes );
  }





void nrf24l01p::config(int _channel)
// Sets the important registers in the nrf module and powers the module
// in receiving mode
// NB: channel and payload must be set now.
  {
  // Must allow the radio time to settle else configuration bits will not necessarily stick.
  // This is actually only required following power up but some settling time also appears to
  // be required after resets too. For full coverage, we'll always assume the worst.
  // Enabling 16b CRC is by far the most obvious case if the wrong timing is used - or skipped.
  // Technically we require 4.5ms + 14us as a worst case. We'll just call it 5ms for good measure.
  // WARNING: Delay is based on P-variant whereby non-P *may* require different timing.
  wait_us(5000);

#if nrf_ADDR_LEN == 5
  configRegister(SETUP_AW,3);// =1 addr len is 3 bytes, =2 addr len is 4 bytes, =3 addr len is 5 bytes
#elif nrf_ADDR_LEN == 4
  configRegister(SETUP_AW,2);// =1 addr len is 3 bytes, =2 addr len is 4 bytes, =3 addr len is 5 bytes
#elif nrf_ADDR_LEN == 3
  configRegister(SETUP_AW,1);// =1 addr len is 3 bytes, =2 addr len is 4 bytes, =3 addr len is 5 bytes
#else
  #error nrf_ADDR_LEN should be 3,4 or 5
#endif
  // Set RF channel
	configRegister(RF_CH,_channel);
  // Set RF settings
//  configRegister(RF_SETUP, RF_SETUP_2Mbit | RF_SETUP_PWR0dBm );
  configRegister(RF_SETUP, RF_SETUP_250Kbit | RF_SETUP_PWR0dBm );
  configRegister(EN_AA, 0 );
  configRegister(EN_RXADDR, enabled_pipes );

  configRegister(DYNPD, 0x3F); // enable dynamic payload for all data pipes
//  configRegister(FEATURE, EN_DPL | EN_DYN_ACK); // enable dynamic payload and W_TX_PAYLOAD_NOACK command
  configRegister(FEATURE, EN_DPL); // enable dynamic payload and W_TX_PAYLOAD_NOACK command

  configRegister(STATUS,(1<<RX_DR) | (1<<TX_DS) | (1<<MAX_RT) );

  // Start receiver
//  powerUpRx();
  flushRx();
  flushTx();
  }


void nrf24l01p::setRADDR(unsigned int pipe, uint8_t const *adr) // Sets the receiving address
  {
  if (pipe==0) memcpy(pipe0addr,adr,nrf_ADDR_LEN);
  if (pipe==1) memcpy(pipe1addr,adr,nrf_ADDR_LEN);
  if (pipe>5) return;
	ceLow();
  if (pipe<2)
    writeRegister(RX_ADDR_P0+pipe,adr,nrf_ADDR_LEN);
  else
    writeRegister(RX_ADDR_P0+pipe,adr,1);
	ceHi();
  }

/*
void nrf24l01p::setRADDR0(uint8_t * adr)
// Sets the receiving address
  {
	ceLow();
	writeRegister(RX_ADDR_P0,adr,nrf_ADDR_LEN);
	ceHi();
  }

void nrf24l01p::setRADDR1(uint8_t * adr)
// Sets the receiving address
  {
	ceLow();
	writeRegister(RX_ADDR_P1,adr,nrf_ADDR_LEN);
	ceHi();
  }
*/

void nrf24l01p::setTADDR(uint8_t const *adr)
// Sets the transmitting address
  {
	ceLow();
	writeRegister(TX_ADDR,adr,nrf_ADDR_LEN);
	ceHi();
  }

/*
void nrf24l01p::setADDR(uint8_t * adr)
// Sets the transmitting address
{
	ceLow();
	writeRegister(TX_ADDR,adr,nrf_ADDR_LEN);
	ceHi();
	ceLow();
	writeRegister(RX_ADDR_P1,adr,nrf_ADDR_LEN);
	ceHi();
}
*/



void nrf24l01p::configRegister(uint8_t reg, uint8_t value)
// Clocks only one byte into the given nrf register
{
    csnLow();
    _spi->transfer(W_REGISTER | (REGISTER_MASK & reg));
    _spi->transfer(value);
    csnHi();
    wait_us(10);
}

uint8_t nrf24l01p::readRegister(uint8_t reg, uint8_t * value, uint8_t len)
// Reads an array of bytes from the given start position in the nrf registers.
{
    csnLow();
    uint8_t ret=_spi->transfer(R_REGISTER | (REGISTER_MASK & reg));
    transferSync(value,value,len);
    csnHi();
    return ret;
}

uint8_t nrf24l01p::readRegister(uint8_t reg)
// Reads an array of bytes from the given start position in the nrf registers.
{
    csnLow();
    _spi->transfer(R_REGISTER | (REGISTER_MASK & reg));
    uint8_t ret=_spi->transfer(0xFF);
    csnHi();
    return ret;
}

uint8_t nrf24l01p::writeRegister(uint8_t reg, uint8_t const *value, uint8_t len)
// Writes an array of bytes into inte the nrf registers.
{
    csnLow();
    uint8_t ret=_spi->transfer(W_REGISTER | (REGISTER_MASK & reg));
    transmitSync(value,len);
    csnHi();
    return ret;
}

uint8_t nrf24l01p::writeRegister(uint8_t reg, uint8_t value)
// Writes an array of bytes into inte the nrf registers.
{
    csnLow();
    uint8_t ret=_spi->transfer(W_REGISTER | (REGISTER_MASK & reg));
    _spi->transfer(value);
    csnHi();
    return ret;
}

/*
    switch(hal_nrf_get_clear_irq_flags ())
    {
      case (1<<HAL_NRF_MAX_RT):                 // Max retries reached
        hal_nrf_flush_tx();                     // flush tx fifo, avoid fifo jam
        radio_set_status (RF_MAX_RT);
        break;

      case (1<<HAL_NRF_TX_DS):                  // Packet sent
        radio_set_status (RF_TX_DS);
        break;

      case (1<<HAL_NRF_RX_DR):                  // Packet received
        while (!hal_nrf_rx_fifo_empty ())
        {
          hal_nrf_read_rx_pload(pload);
        }
        radio_set_status (RF_RX_DR);
        break;

      case ((1<<HAL_NRF_RX_DR)|(1<<HAL_NRF_TX_DS)): // Ack payload recieved
        while (!hal_nrf_rx_fifo_empty ())
        {
          hal_nrf_read_rx_pload(pload);
        }
        radio_set_status (RF_TX_AP);
        break;

      default:
        break;
    }

*/
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
void nrf24l01p::startListening(void)
{
	writeRegister(CONFIG, nrf_CONFIG | ( (1<<PWR_UP) | (1<<PRIM_RX) ) );
	writeRegister(STATUS,(1 << RX_DR) | (1 << TX_DS) | (1 << MAX_RT));

  writeRegister(RX_ADDR_P0, pipe0addr, nrf_ADDR_LEN);

  // Flush buffers
  flushRx();
  flushTx();

  // Go!
	ceHi();

  // wait for the radio to come up (130us actually only needed)
  wait_us(130);
}

/****************************************************************************/

void nrf24l01p::stopListening(void)
{
  ceLow();
  flushTx();
  flushRx();
}

/*
void nrf24l01p::powerDown(void)
{
  writeRegister(CONFIG, nrf_CONFIG | ( (0<<PWR_UP) | (1<<PRIM_RX) ) );
//  writeRegister(CONFIG,read_register(CONFIG) & ~_BV(PWR_UP));
}
*/

/****************************************************************************/

/*
void nrf24l01p::powerUp(void)
{
  writeRegister(CONFIG, nrf_CONFIG | ( (1<<PWR_UP) | (1<<PRIM_RX) ) );
//  writeRegister(CONFIG,read_register(CONFIG) | _BV(PWR_UP));
}
*/

bool nrf24l01p::sendPkt(unsigned int pipe, const void* buf, uint8_t len ) {




  if (pipe==0) // select pipe address
    {
    csnLow();
    _spi->transfer(W_REGISTER | (REGISTER_MASK & TX_ADDR));
    transmitSync(pipe0addr, nrf_ADDR_LEN);
    csnHi();
    }
  else
    {
    csnLow();
    _spi->transfer(W_REGISTER | (REGISTER_MASK & TX_ADDR));
    _spi->fast_transfer(pipe);
    transmitSync(pipe1addr+1, nrf_ADDR_LEN-1);
    csnHi();
    }

  uint8_t cfg;
  readRegister(CONFIG,&cfg,1);
  cfg &= ~( 1 << PRIM_RX ); // reset PRIM_RX

  // if not powered up then power up and wait for the radio to initialize
  bool powereddown;
  if ((cfg & (1<<PWR_UP))==0){
    powereddown=true;
    writeRegister (CONFIG, (cfg | (1<<PWR_UP) ) & ~( 1 << PRIM_RX ));//      writeRegister( CONFIG, cfg | (1<<PWR_UP) );

    // For nRF24L01+ to go from power down mode to TX or RX mode it must first pass through stand-by mode.
    // There must be a delay of Tpd2stby (see Table 16.) after the nRF24L01+ leaves power down mode before
    // the CEis set high. - Tpd2stby can be up to 5ms per the 1.0 datasheet
    wait_us(1500);
  } else {
    powereddown=false;
    writeRegister (CONFIG, cfg & ~( 1 << PRIM_RX ));
  	ceLow();
    wait_us(25);
  }
  // Send the payload

  csnLow();                    // Pull down chip select
  _spi->transfer( W_TX_PAYLOAD ); // Write cmd to write payload
  transmitSync((const uint8_t*)buf,len);   // Write payload
  csnHi();                    // Pull up chip select

  // Allons!
	ceHi();
  wait_us(25);
  ceLow();


  // ------------
  // At this point we could return from a non-blocking write, and then call
  // the rest after an interrupt

  // Instead, we are going to block here until we get TX_DS (transmission completed and ack'd)
  // or MAX_RT (maximum retries, transmission failed).  Also, we'll timeout in case the radio
  // is flaky and we get neither.

  // IN the end, the send should be blocking.  It comes back in 60ms worst case, or much faster
  // if I tighted up the retry logic.  (Default settings will be 1500us.
  // Monitor the send
  uint8_t observe_tx;
  uint8_t status;
#ifdef LPOS10
  uint32_t sent_at = GetIntTicks();
#else
  uint32_t sent_at = get_us();
  const uint32_t timeout = 1500; //us to wait for timeout
#endif
  do
  {
    status = readRegister(OBSERVE_TX,&observe_tx,1);
    //IF_SERIAL_DEBUG(Serial.print(observe_tx,HEX));
  }
#ifdef LPOS10
  while( ! ( status & ( (1<<TX_DS) | (1<<MAX_RT) ) ) && ( GetIntTicks() - sent_at < MsToTicks(1500) ) );
#else
  while( ! ( status & ( (1<<TX_DS) | (1<<MAX_RT) ) ) && ( get_us() - sent_at < timeout*1000 ) );
#endif

  // The part above is what you could recreate with your own interrupt handler,
  // and then call this when you got an interrupt
  // ------------

  // Call this when you get an interrupt
  // The status tells us three things
  // * The send was successful (TX_DS)
  // * The send failed, too many retries (MAX_RT)
  // * There is an ack packet waiting (RX_DR)
  bool tx_ok, tx_fail,rx_ready;

  // Read the status & reset the status in one easy call
  // Or is that such a good idea?
  status = writeRegister(STATUS,(1<<RX_DR) | (1<<TX_DS) | (1<<MAX_RT) );

  // Report to the user what happened
  tx_ok = status & (1<<TX_DS);
  tx_fail = status & (1<<MAX_RT);
  rx_ready = status & (1<<RX_DR);

  //printf("%u%u%u\r\n",tx_ok,tx_fail,ack_payload_available);

  bool result = tx_ok;
  //IF_SERIAL_DEBUG(Serial.print(result?"...OK.":"...Failed"));

  // Handle the ack packet
  if ( rx_ready )
  {
    //ack_payload_length = getDynamicPayloadSize();
    //IF_SERIAL_DEBUG(Serial.print("[AckPacket]/"));
    //IF_SERIAL_DEBUG(Serial.println(ack_payload_length,DEC));
  }

  // Yay, we are done.

  // Power down
  if (powereddown) {
    powerDown();
  }

  // Flush buffers (Is this a relic of past experimentation, and not needed anymore??)
  //flushTx();

  return result;
}
/****************************************************************************/


/*
uint8_t nrf24l01p::getDynamicPayloadSize(void)
{
  uint8_t result = 0;

  csnLow();
  _spi->transfer( R_RX_PL_WID );
  result = _spi->transfer(0xff);
  csnHi();

  return result;
}
*/

/****************************************************************************/

/*
bool nrf24l01p::available(void)
{
  return available(NULL);
}
*/

/****************************************************************************/

bool nrf24l01p::available(uint8_t* pipe_num)
{
  bool result;
  uint8_t status;
  if (moredata) {
    result=true;
    moredata=false;
  } else {
    csnLow();
    status = _spi->transfer(NOP);
    csnHi();

    result = ( status & (1<<RX_DR) );
  }

  if (result)
  {
    // If the caller wants the pipe number, include that
    if ( pipe_num )
      *pipe_num = ( status >> RX_P_NO ) & 0x07;

    // Clear the status bit

    // ??? Should this REALLY be cleared now?  Or wait until we
    // actually READ the payload?

    writeRegister(STATUS,(1<<RX_DR) );

    // Handle ack payload receipt
    if ( status & (1<<TX_DS) )
    {
      writeRegister(STATUS,(1<<TX_DS));
    }
  }

  return result;
}

/****************************************************************************/

int nrf24l01p::getData( void* buf )
{
    csnLow();                               // Pull down chip select
    _spi->transfer( R_RX_PL_WID );
    unsigned int len = _spi->transfer( 0xFF );
    csnHi();                               // Pull up chip select

/*    if (len<1 || len>32)
      {
      csnLow();                               // Pull down chip select
      _spi->transfer( FLUSH_RX );
      csnHi();                               // Pull up chip select
      configRegister(STATUS,(1<<RX_DR));   // Reset status register
      return -1;
      }*/

  csnLow();                               // Pull down chip select
  _spi->transfer( R_RX_PAYLOAD );            // Send cmd to read rx payload
	for(int i = 0;i < len;i++){
		((char*)buf)[i] = _spi->transfer(0xFF);
	}
  csnHi();                               // Pull up chip select


  // was this the last of the data available?
  //return readRegister(FIFO_STATUS) & (1<<RX_EMPTY);
  moredata = !(readRegister(FIFO_STATUS) & (1<<RX_EMPTY));
  return len;
}














/*
bool nrf24l01p::isSending()
  {
//	uint8_t status;
	if(PTX)
    {
    csnLow();                    // Pull down chip select
    char stat=_spi->transfer(R_REGISTER | (REGISTER_MASK & FIFO_STATUS));
    char fifo_stat=_spi->transfer( 0xFF );     //NOP writing geturn status
    csnHi();                    // Pull up chip select

		if( (stat & ((1 << TX_DS)  | (1 << MAX_RT))) || (fifo_stat&(1<<TX_EMPTY))) //if sending successful (TX_DS) or max retries exceded (MAX_RT).
      {
      ceLow();

      csnLow();
      _spi->transfer(W_REGISTER | (REGISTER_MASK & STATUS));
      _spi->transfer(stat & ((1 << TX_DS)  | (1 << MAX_RT))); // reset TX_DS and MAX_RT bit
      csnHi();

      csnLow();
      _spi->transfer( FLUSH_RX );
      csnHi();

      PTX = 0;
      csnLow();
      _spi->transfer(W_REGISTER | (REGISTER_MASK & CONFIG));
      _spi->transfer(nrf_CONFIG | ( (1<<PWR_UP) | (1<<PRIM_RX)));
      csnHi();

      ceHi();
			return false;
      }
    else
      {
      volatile int x=0;
      }
		return true;
    }
	return false;
  }

int nrf24l01p::dataReady() // Checks if data is available for reading and return number of ready fifo (0,1,2,...5) or 7 if no data
  {
  csnLow();
  //char status=_spi->transfer(R_REGISTER | (REGISTER_MASK & FIFO_STATUS));
  char status2=_spi->transfer(0xFF);
  csnHi();
  //if (status2&(1<<RX_DR)) return 0x07;
  return (status2>>1)&0x07;
  }

int nrf24l01p::getData(uint8_t * data)
// Reads payload bytes into data array
{
    csnLow();                               // Pull down chip select
    _spi->transfer( R_RX_PL_WID );
    unsigned int len = _spi->transfer( 0xFF );
    csnHi();                               // Pull up chip select

    if (len<1 || len>32)
      {
      csnLow();                               // Pull down chip select
      _spi->transfer( FLUSH_RX );
      csnHi();                               // Pull up chip select
      configRegister(STATUS,(1<<RX_DR));   // Reset status register
      return -1;
      }

    csnLow();                               // Pull down chip select
    _spi->transfer( R_RX_PAYLOAD );            // Send cmd to read rx payload
    transferSync(data,data,len); // Read payload
    csnHi();                               // Pull up chip select
    // NVI: per product spec, p 67, note c:
    //  "The RX_DR IRQ is asserted by a new packet arrival event. The procedure
    //  for handling this interrupt should be: 1) read payload through SPI,
    //  2) clear RX_DR IRQ, 3) read FIFO_STATUS to check if there are more
    //  payloads available in RX FIFO, 4) if there are more data in RX FIFO,
    //  repeat from step 1)."
    // So if we're going to clear RX_DR here, we need to check the RX FIFO
    // in the dataReady() function
    configRegister(STATUS,(1<<RX_DR));   // Reset status register
    return len;
}


void nrf24l01p::send(unsigned int pipe, const uint8_t * value, int len)
// Sends a data package to the default address.
  {
  if (PTX==0) // if we was not sending
    {
    PTX = 1;
    configRegister(CONFIG, nrf_CONFIG | ( (1<<PWR_UP) | (0<<PRIM_RX) ) );
    }
  else
    {
    while (PTX) // if we previously write packet wait for sending this packet
      {
      ceHi();

      csnLow();                    // Pull down chip select
      char stat=_spi->transfer(R_REGISTER | (REGISTER_MASK & FIFO_STATUS));
      char fifo_stat=_spi->transfer( 0xFF );     //NOP writing geturn status
      csnHi();                    // Pull up chip select

      if ( (stat & (1<<MAX_RT)))
        {
        csnLow();
        _spi->transfer(W_REGISTER | (REGISTER_MASK & STATUS));
        _spi->transfer(1<<MAX_RT); // reset MAX_RT bit
        csnHi();

        csnLow();
        _spi->transfer(FLUSH_TX);
        csnHi();
        continue;
        }
//      if ( stat & (1<<TX_FULL) )
//        {
//        continue;
//        }
      if ( stat & (1<<TX_DS)  ||  (fifo_stat&(1<<TX_EMPTY))  )
        {
        csnLow();
        _spi->transfer(W_REGISTER | (REGISTER_MASK & STATUS));
        _spi->transfer(1<<TX_DS); // reset MAX_RT bit
        csnHi();
        break;
        }
      }
    }
  ceLow(); // switch off radio tract

  if (pipe==0) // select pipe address
    {
    //writeRegister(TX_ADDR,pipe0addr,nrf_ADDR_LEN);
    csnLow();
    _spi->transfer(W_REGISTER | (REGISTER_MASK & TX_ADDR));
    transmitSync(pipe0addr, nrf_ADDR_LEN);
    csnHi();
    }
  else
    {
    csnLow();
    _spi->transfer(W_REGISTER | (REGISTER_MASK & TX_ADDR));
    _spi->fast_transfer(pipe);
    transmitSync(pipe1addr+1, nrf_ADDR_LEN-1);
    csnHi();
    }

  csnLow();
  _spi->fast_transfer(FLUSH_TX);
  //_spi->transfer(FLUSH_TX);
  csnHi();

  csnLow();                    // Pull down chip select
  _spi->transfer( W_TX_PAYLOAD ); // Write cmd to write payload
  transmitSync(value,len);   // Write payload
  csnHi();                    // Pull up chip select

  ceHi();                     // Start transmission if not started yet
 wait_us(100);
*/
/*
    uint8_t status;
    status = getStatus();

    while (PTX) {
	    status = getStatus();

	    if((status & ((1 << TX_DS)  | (1 << MAX_RT)))){
		    PTX = 0;
		    break;
	    }
    }                  // Wait until last paket is send

    ceLow(); // switch off radio tract

    csnLow();                    // Pull down chip select
    _spi->transfer( STATUS );     //
    _spi->transfer( 0xFF );     //
    csnHi();                    // Pull up chip select

    powerUpTx();       // Set to transmitter mode , Power up

    csnLow();                    // Pull down chip select
    _spi->transfer( FLUSH_TX );     // Write cmd to flush tx fifo
    csnHi();                    // Pull up chip select

    csnLow();                    // Pull down chip select
    _spi->transfer( W_TX_PAYLOAD ); // Write cmd to write payload
    transmitSync(value,len);   // Write payload
    csnHi();                    // Pull up chip select

    ceHi();                     // Start transmission
  */
/*
}
*/

/*
unsigned int nrf24l01p::getStatus(){
	unsigned int rv;
	readRegister(STATUS,(unsigned char*)(&rv),1);
  readRegister(CONFIG,((unsigned char*)(&rv))+1,1);
	return rv;
}
*/

//Power up now. Radio will not power down unless instructed by MCU for config changes etc.
/*
void nrf24l01p::powerUp(void)
{
   uint8_t cfg;
   readRegister(CONFIG,&cfg,1);

   // if not powered up then power up and wait for the radio to initialize
   if ((cfg & (1<<PWR_UP))==0){
      cfg|=(1<<PWR_UP);
      writeRegister (CONFIG, &cfg,1);//      writeRegister( CONFIG, cfg | (1<<PWR_UP) );

      // For nRF24L01+ to go from power down mode to TX or RX mode it must first pass through stand-by mode.
	  // There must be a delay of Tpd2stby (see Table 16.) after the nRF24L01+ leaves power down mode before
	  // the CEis set high. - Tpd2stby can be up to 5ms per the 1.0 datasheet
    //  delay(5);
      for (volatile int i=0;i<100000;i++);

   }
}

void nrf24l01p::powerUpRx(){
	PTX = 0;
	ceLow();
	configRegister(CONFIG, nrf_CONFIG | ( (1<<PWR_UP) | (1<<PRIM_RX) ) );
	ceHi();
	configRegister(STATUS,(1 << TX_DS) | (1 << MAX_RT));
}
void nrf24l01p::powerUpTx(){
	PTX = 1;
	configRegister(CONFIG, nrf_CONFIG | ( (1<<PWR_UP) | (0<<PRIM_RX) ) );
}
*/

void nrf24l01p::flushRx(){
  csnLow();
  _spi->transfer( FLUSH_RX );
  csnHi();
}

void nrf24l01p::flushTx(){
  csnLow();
  _spi->transfer(FLUSH_TX);
  csnHi();
}


void nrf24l01p::ceHi(){
	cePin->set();
}

void nrf24l01p::ceLow(){
	cePin->reset();
}

void nrf24l01p::csnHi(){
	csnPin->set();
}

void nrf24l01p::csnLow(){
	csnPin->reset();
}

void nrf24l01p::powerDown() {
	ceLow();
	configRegister(CONFIG, nrf_CONFIG );
}

void nrf24l01p::powerUpTx() {
  uint8_t cfg;
  readRegister(CONFIG,&cfg,1);
  cfg &= ~( 1 << PRIM_RX ); // reset PRIM_RX

  // if not powered up then power up and wait for the radio to initialize
  if ((cfg & (1<<PWR_UP))==0){
    writeRegister (CONFIG, (cfg | (1<<PWR_UP) ) & ~( 1 << PRIM_RX ));//      writeRegister( CONFIG, cfg | (1<<PWR_UP) );

    // For nRF24L01+ to go from power down mode to TX or RX mode it must first pass through stand-by mode.
    // There must be a delay of Tpd2stby (see Table 16.) after the nRF24L01+ leaves power down mode before
    // the CEis set high. - Tpd2stby can be up to 5ms per the 1.0 datasheet
    wait_us(1500);
  }
}