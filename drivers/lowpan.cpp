#include <stdint.h>
#include <limits.h>

#include <tim.h>
#include "crc.h"
//#include "nrf24l01p.h"
#include "si4463.h"
#include "itmp.h"
#include "lowpan.h"
#include "tea.h"


#define FRAME_DELIMETER			0x7E
#define FRAME_REPLACEMENT		0x7D

#define SERIAL_ADDR_BROADCAST  255
#define SERIAL_ADDR_UNKNOWN    0

#define LOPAN_HEADER_SEQ_MASK         0xFF

class crypter {
  uint32_t iv[2]; // 64 bit initialization vector and temp gamma buffer
  uint32_t cnt;   // counter from 0 to 7 of partially encrypted or signed bytes
public:

  void prepare(const uint32_t* key, const uint32_t* nonce, uint32_t counter){
    iv[0]=counter | nonce[0];
    iv[1]=nonce[1];
    cnt=0;
    tea_encrypt(iv, key);
  }

  void xore(void* a,void* b){
    for (int i=0;i<8;i++) {
      *((char*)a)^= *((char*)b);
      a = ((char*)a) + 1;
      b = ((char*)b) + 1;
    }
  }

  void transform(byte* v, unsigned int len, unsigned int const *k, bool encrypt){
    if ( cnt != 0 ) {
      for (;len>0 && cnt < 8;) {
        ((char*)iv)[cnt] ^= *v;
        if (encrypt) { *v = ((char*)iv)[cnt]; }
        len--;
        v++;
        cnt++;
      }
    }
    cnt&=0x07;

    byte* end=v+(len&0xFFFFFFF8); len-=len&0xFFFFFFF8;

    for (; v<end;) { // for blocks
      tea_encrypt(iv, k);
      xore(iv,v);
      v+=8;
//      iv[0] = iv[0] ^ *((unsigned int*)v);
//      v+=4;
//      iv[1] = iv[1] ^ *((unsigned int*)v);
//      v+=4;
    }
    if (len>0) {
      tea_encrypt(iv, k);
      for (; cnt < len; cnt++,v++) {
        ((char*)iv)[cnt] ^= *v;
        if (encrypt) { *v = ((char*)iv)[cnt]; }
      }
    }
  }

  void writedigest(byte* buf,uint32_t len) {
    memcpy(buf,iv,len);
  }

  bool testdigest(byte* buf,uint32_t len) {
    return memcmp(buf,iv,len)==0;
  }

};

void lowpan_node::set_addr(byte addr)
  {
  _my_addr=addr;
  }

int lowpan_node::isJoined(){
  if ( joined ) // if already has long term key
    return true;
  return false;
}

int lowpan_node::sendJoin(){
  if ( joined ) // if already has long term key
    return true;
  byte i_buf[17];
  int frlen=1+8+8;
  i_buf[0]=LCP_JOIN;
  memcpy(i_buf+1, my_random.bytes, 8); // write random salt
  memcpy(i_buf+9, &core->DeviceID, 8); // write id
  send_frame(LOPAN_HEADER_TYPE_LCP, LOWPAN_GLOBAL_BROADCAST, i_buf, frlen,0,0);
  return false;
}

int lowpan_node::sendPoll(){
  byte i_buf[1];
  int frlen=1;
  i_buf[0]=LCP_POLL;
  send_frame(LOPAN_HEADER_TYPE_LCP, LOWPAN_LOCAL_BROADCAST, i_buf, frlen,0,0);
  return 0;
}

bool itmp_link::isConnected()
  {
  if ( connected_state>0 ) // if already connected return true
    return true;
  return false;
  }

void itmp_link::disconnect()
  {
  connected_state=0;
  }

bool itmp_link::sendConnect(int rssi)
  {
  if ( connected_state>0 ) // if already connected return true
    return true;
  if (linkPin) linkPin->toggle(); // on

  byte i_buf[64];
  int frlen=18;
  i_buf[0]=LCP_CONNECT;
  memcpy(i_buf+1, my_random.bytes, 8); // write random
  memcpy(i_buf+9, &core->DeviceID, 8); // write id
  i_buf[17]=rssi;
  send_frame(LOPAN_HEADER_TYPE_LCP, LOWPAN_LOCAL_BROADCAST, i_buf, frlen);
  return false;
  }

void store_LTK(byte* id);
void store_addr(uint32_t addr);
extern volatile int16_t timecorrection;
int itmp_link::process_frame_LCP(uint8_t sa, byte* buf, int buflen, int rssi) {

  if ( buf[0] == LCP_JOIN_ACK ) { // JOIN_ACK, server ID*8, DevId*8, Long Term Key * 16
    if ( memcmp(&core->DeviceID,buf+9,8) != 0) { // Client ID does not match
      return PACKET_NOT_FOR_US;
    }
    set_LTK(buf+17);
    store_LTK(buf+17);
    sendConnect(rssi);
    return PACKET_ACCEPTED_ANSWER_SENT;

  } else if ( buf[0] == LCP_CONNECT_PAR ) { //[LCP_CONNECT_PAR, COORD_RAND*8, ID*8, opts [1,addr*1 ],COORD_ID*8]
    if ( memcmp(&core->DeviceID,buf+9,8) != 0) { // Client ID does not match
      return PACKET_NOT_FOR_US;
    }
    if ( ( buf[17] & 1 ) && ( buflen >= 19 ) ) {
      set_addr(buf[18]);
      store_addr(buf[18]);
    }
    sendConnect(rssi);
    return PACKET_ACCEPTED_ANSWER_SENT;

  }  else if ( buf[0] == LCP_CONNECT_ACK ) { // [LCP_CONNECT_ACK, COORD_RAND*8, ID*8, opts [1,addr*1 ],COORD_ID*8]
    if ( memcmp(&core->DeviceID,buf+9,8) != 0) { // Client ID does not match
      return PACKET_NOT_FOR_US;
    }
    memcpy(master.nonce.words, buf+1, 8);
    master.nonce.dword ^= my_random.dword;
    connected_state = 1;

    master.err_reconnect++;
    coordinator_addr = sa;
    master.snd_seq = 0; // The sequence number that was last sent by us. Nothing was sent as fact
    master.rcv_seq = 0;  // The sequence number that we expect to receive next.
    master.receive_time = get_us();
    master.state = LOPAN_FEATURE_CONNECTED;
    master.peer_addr = coordinator_addr;
    return PACKET_ACCEPTED;

  } else if ( buf[0] == LCP_PING_REQ ) {
    //lowpan_peer* node=get_node_by_addr(sa);
    //if (node==0)
    //  return;

    buf[0]=LCP_PING_RSP;
    send_frame(LOPAN_HEADER_TYPE_LCP, sa, buf, buflen);
    return PACKET_ACCEPTED_ANSWER_SENT;

  } else if ( buf[0] == LCP_PING_RSP ) {
//    lowpan_peer* node=get_node_by_addr(sa);
//    if (node==0)
//      return;
    return PACKET_ACCEPTED;

  } else if ( buf[0] == LCP_SLEEP ) {
    uint16_t period=buf[1] | (buf[2]<<8);
    int16_t d_period=buf[3] | (buf[4]<<8);
    if (period == 1000 && d_period > -999 && d_period < 999 ) {
      timecorrection=d_period;
    }
    return PACKET_ACCEPTED;

  }
  return PACKET_UNKNOWN_TYPE;
}



/*
void lowpan_gate::process_frame(uint32_t type, byte* buf, int buflen)
  {
  uint8_t da=((type>>8)&0xFF);
  if (_my_addr!=0 && da==_my_addr)
    {
    //processframe_layer3(buf,buflen);
    }
  }

struct lowpan_node_record
  {
  uint64_t id;
  uint64_t last_seen;
  uint8_t state;
  uint8_t features;
  uint8_t random[8];
  };

#define MAX_NODES 8
lowpan_node_record nodes[MAX_NODES];
uint32_t node_records_num=0;

lowpan_node_record* get_node(uint64_t id)
  {
  uint64_t min_time=ULLONG_MAX;
  int old_index=0;
  for (uint32_t i=0; i<node_records_num;i++)
    {
    if (nodes[i].id==id) return &nodes[i];
    if (min_time > nodes[i].last_seen)
      {
      min_time = nodes[i].last_seen;
      old_index=i;
      }
    }
  if (node_records_num < MAX_NODES)
    {
    int s=node_records_num;
    nodes[s].id=id;
    nodes[s].state=0;
    nodes[s].last_seen=0;
    node_records_num++;
    return &nodes[s];
    }
  nodes[old_index].id=id;
  nodes[old_index].state=0;
  nodes[old_index].last_seen=0;
  return &nodes[old_index];
  }

int lowpan_coord::process_LCP_frame(uint32_t type, byte* buf, int buflen)
  {
  byte i_buf[64];
  int frlen=1;
  uint8_t sa=type>>16;
  if ( buf[0] == LCP_CONNECT )
    {
    unsigned long long id=0; read_opt_uncompress(buf+1,buflen,LCP_OPT_DEVID,&id,sizeof(id));
    unsigned int features=0; read_opt_uncompress(buf+1,buflen,LCP_OPT_FEATURES,&features,sizeof(features));
    lowpan_node_record* node=get_node(id);
    if (node==0)
      return -1;

    uint8_t naddr=(node-nodes)+2;

    //unsigned char rnd[8]; read_opt(buf+1,buflen,LCP_OPT_RAND,rnd,sizeof(rnd));

    if (sa==0 || sa!=naddr) // if node has no address or address is busy by other node
      { // PHASE 0 SET ADDRESS FOR NODE
      if (features==0 //&& secure==false
)
        i_buf[0]=LCP_CONNECT_ACK;
      else
        i_buf[0]=LCP_CONNECT_PAR;
      frlen+=write_opt_compress(i_buf+frlen, LCP_OPT_DEVID, &id, 8); // write id
      if ((type>>24)==LOWPAN_GLOBAL_BROADCAST) // if node do not know about radio network group address
        frlen+=write_opt_compress(i_buf+frlen, LCP_OPT_NETADDR_, _net_addr, nrf_ADDR_LEN); // write radio network group address
      frlen+=write_opt_full(i_buf+frlen, LCP_OPT_DEVADDR, &naddr, 1); // write individual device address
      if ((type>>24)==LOWPAN_GLOBAL_BROADCAST) // if node do not know about radio network group address
        int_sendframe(LOPAN_HEADER_TYPE_LCP | (LOWPAN_GLOBAL_BROADCAST<<8), i_buf, frlen);
      else
        int_sendframe(LOPAN_HEADER_TYPE_LCP | (LOWPAN_LOCAL_BROADCAST<<8), i_buf, frlen);
      }
    else
      {
      i_buf[0]=LCP_CONNECT_PAR;
      if ( (//secure ||
(node->features & LOPAN_FEATURE_PIN_CONTROL) != 0) && (node->features & LOPAN_FEATURE_PIN_ACCEPTED) == 0)
        { // PHASE 1 ASK FOR PAIRING
        frlen+=write_opt_compress(i_buf+frlen, LCP_OPT_FEATURES , &id, 4); // write futures
        frlen+=write_opt_compress(i_buf+frlen, LCP_OPT_RAND, &id, 4); // write coordinator random
        frlen+=write_opt_compress(i_buf+frlen, LCP_OPT_DIGEST, &naddr, 1); // write digest
        }
      else if ( (node->features & LOPAN_FEATURE_NEED_SECRET)!=0 && (node->features & LOPAN_FEATURE_TEA_SECRET)==0 && (features & LOPAN_FEATURE_TEA_SECRET)==0 )
        { // PHASE 2 SET NEW LONG TERM KEY
        frlen+=write_opt_compress(i_buf+frlen, LCP_OPT_FEATURES , &id, 4); // write futures
        frlen+=write_opt_compress(i_buf+frlen, LCP_OPT_SECRET, &id, 4); // write coordinator random
        }
      else if ( (node->features & LOPAN_FEATURE_CONNECTED)==0 )
        { // PHASE 3 ACCEPT CONNECTION
        frlen+=write_opt_compress(i_buf+frlen, LCP_OPT_FEATURES , &id, 4); // write futures
        frlen+=write_opt_compress(i_buf+frlen, LCP_OPT_RAND, &id, 4); // write coordinator random
        frlen+=write_opt_compress(i_buf+frlen, LCP_OPT_DIGEST, &naddr, 1); // write digest
        i_buf[0]=LCP_CONNECT_ACK;
        }
      int_sendframe(LOPAN_HEADER_TYPE_LCP | (sa<<8), i_buf, frlen);
      }
    }
  return 0;
  }


int lowpan_coord::process_frame(uint32_t type, byte* buf, int buflen)
  {
  uint8_t da=((type>>8)&0xFF);
  if ( ( _my_addr!=0 && da==_my_addr ) || (type>>24)==LOWPAN_LOCAL_BROADCAST  || (type>>24)==LOWPAN_GLOBAL_BROADCAST ) // if frame for us or from broadcast pipes
    {
    if ( (type & LOPAN_HEADER_TYPE_MASK) == LOPAN_HEADER_TYPE_LCP )
      {
      process_LCP_frame(type,buf,buflen);
      }
    //processframe_layer3(buf,buflen);
    }
  return 0;
  }

*/

void lowpan_node::power_down(){
  rf.powerDown();
}

#ifdef ENABLE_IPV4_PROCESSING


#pragma pack(push,ipaddr,1)
union IPAddr
{
	unsigned char byte[4];
	unsigned long lng;
};
#pragma pack(pop)

IPAddr node_ip;
IPAddr node_mask;

#define TTLC 0x01 // TTL Time To Live field is omitted
#define TOS  0x80 // TTL Time To Live field is omitted

#define PROT_MASK           0x06
#define PROT_UUNCOMPRESSED  0x00 // 00 PROT uncompressed
#define PROT_UDP            0x02 //  01 PROT=17 UDP
#define PROT_ICMP           0x04 //  10 PROT=1 ICMP
#define PROT_TCP            0x06 //  11 PROT=6 TCP

#define SA_TYPE_MASK      0xC0
#define SA_OMMITED        0x00
#define SA_SUFFIX_1       0x40
#define SA_SUFFIX_2       0x80
#define SA_UNCOMPRESSED   0xC0

#define DA_TYPE_MASK      0x30
#define DA_OMMITED        0x00
#define DA_SUFFIX_1       0x10
#define DA_SUFFIX_2       0x20
#define DA_UNCOMPRESSED   0x30

#define LOPAN_HEADER_TYPE_MASK        0xE0


#define P 61616  // 0xF0B0

#define UDP_COMPRESSED_SP 0x80
#define UDP_COMPRESSED_DP 0x40
#define UDP_COMPRESSED_LN 0x20
#define UDP_COMPRESSED_SC 0x10

#define PROT_IP_UDP					0x11

#define PROT_IP_ICMP				0x01
#define ICMP_ECHO_REQUEST		0x08
#define ICMP_ECHO_REPLY			0x00
#define ICMP_ENABLE_REPLY		0x01

#define IP_ICMP_ECHO 0x010000

#define PROT_IP_TCP					0x06

#define ITMP_UDP_PORT 61617

void lowpan_node::process_ipv4_frame(byte* inbuf, int inbuflen){
  if ( ( inbuf[0] & LOPAN_HEADER_TYPE_MASK ) != LOPAN_HEADER_TYPE_IP4 ) {
    return ;
  }
  IPAddr srcipaddr = node_ip;
  srcipaddr.byte[3] = inbuf[1]; // SA
  IPAddr destipaddr = node_ip;
  destipaddr.byte[3] = inbuf[2]; // DA

  uint32_t ttl=6;
  int hdrpos=4;

  if ( (inbuf[3] & TTLC)==0 ) {
    inbuf[hdrpos++];
  }

  if ( ( inbuf[3] & SA_TYPE_MASK ) == SA_SUFFIX_1 ) {
    srcipaddr.byte[3] = inbuf[hdrpos++];
  } else if ( ( inbuf[3] & SA_TYPE_MASK ) == SA_SUFFIX_2 ) {
    srcipaddr.byte[2] = inbuf[hdrpos++];
    srcipaddr.byte[3] = inbuf[hdrpos++];
  } else if ( ( inbuf[3] & SA_TYPE_MASK ) == SA_UNCOMPRESSED ) {
    srcipaddr.byte[0] = inbuf[hdrpos++];
    srcipaddr.byte[1] = inbuf[hdrpos++];
    srcipaddr.byte[2] = inbuf[hdrpos++];
    srcipaddr.byte[3] = inbuf[hdrpos++];
  }

  if ( ( inbuf[3] & DA_TYPE_MASK ) == DA_SUFFIX_1 ) {
    destipaddr.byte[3] = inbuf[hdrpos++];
  } else if ( ( inbuf[3] & DA_TYPE_MASK ) == DA_SUFFIX_2 ) {
    destipaddr.byte[2] = inbuf[hdrpos++];
    destipaddr.byte[3] = inbuf[hdrpos++];
  } else if ( ( inbuf[3] & DA_TYPE_MASK ) == DA_UNCOMPRESSED ) {
    destipaddr.byte[0] = inbuf[hdrpos++];
    destipaddr.byte[1] = inbuf[hdrpos++];
    destipaddr.byte[2] = inbuf[hdrpos++];
    destipaddr.byte[3] = inbuf[hdrpos++];
  }

  uint32_t tos=0;
  if ( (inbuf[3] & TOS)==0 ) {
    tos = inbuf[hdrpos++];
  }

  if (destipaddr.lng != node_ip.lng) { // the packet not for us
    if ( (destipaddr.lng & node_mask.lng) == ( node_ip.lng & node_mask.lng) ) { // we can route the packet
    }
    return; // drop the packet
  }
  // the packet for us test protocol an ports
  uint32_t protocol=0;
  uint16_t udp_srcport,udp_dstport;
  uint8_t icmp_type;
  uint8_t icmp_code;

  if ( (inbuf[3] & PROT_MASK) == PROT_UDP ) {
    protocol = PROT_IP_UDP;
    uint8_t h=inbuf[hdrpos++];

    if ( h & (UDP_COMPRESSED_SP | UDP_COMPRESSED_DP | UDP_COMPRESSED_SC) == (UDP_COMPRESSED_SP | UDP_COMPRESSED_DP | UDP_COMPRESSED_SC) ) {// supercompressed ports
      udp_srcport = udp_dstport = P + (h&0x0f);
    } else if ( h & (UDP_COMPRESSED_SP | UDP_COMPRESSED_DP | UDP_COMPRESSED_SC) == (UDP_COMPRESSED_SP | UDP_COMPRESSED_DP ) ) {// compressed ports (but not supercompressed
      uint8_t l=inbuf[hdrpos++];
      udp_srcport = P + ((h&0x03)<<4) + (l&0x0F);
      udp_dstport = P + ((h&0x0C)<<2) + ((l&0xF0)>>4);
    } else if ( h & (UDP_COMPRESSED_SP | UDP_COMPRESSED_DP | UDP_COMPRESSED_SC) == (UDP_COMPRESSED_SP ) ) {// compressed source port (but not supercompressed)
      uint8_t l=inbuf[hdrpos++];
      udp_srcport = P + ((h&0x0f)<<8) + l;
      udp_dstport = inbuf[hdrpos++]<<8;
      udp_dstport |= inbuf[hdrpos++];
    } else if ( h & (UDP_COMPRESSED_SP | UDP_COMPRESSED_DP | UDP_COMPRESSED_SC) == ( UDP_COMPRESSED_DP ) ) {// compressed destination port (but not supercompressed)
      udp_srcport = inbuf[hdrpos++]<<8;
      udp_srcport |= inbuf[hdrpos++];
      uint8_t l=inbuf[hdrpos++];
      udp_dstport = P + ((h&0x0f)<<8) + l;
    } else if ( h & (UDP_COMPRESSED_SP | UDP_COMPRESSED_DP | UDP_COMPRESSED_SC) == ( 0 ) ) {// not compressed ports
      udp_srcport = inbuf[hdrpos++]<<8;
      udp_srcport |= inbuf[hdrpos++];
      udp_dstport = inbuf[hdrpos++]<<8;
      udp_dstport |= inbuf[hdrpos++];
    }
  } else if ( (inbuf[3] & PROT_MASK) == PROT_ICMP ) {
    protocol = PROT_IP_ICMP;

    uint8_t t=inbuf[hdrpos++];
    icmp_type = t >> 4;
    icmp_code = t & 0x0F;
  } else if ( (inbuf[3] & PROT_MASK) == PROT_TCP ) {
    protocol = PROT_IP_TCP;
    // TODO
  } else { // PROT_UUNCOMPRESSED;
    protocol = inbuf[hdrpos++];
  }

  if (protocol == PROT_IP_UDP) {
    if ( udp_dstport == ITMP_UDP_PORT ) {
      core->process_itmp_frame(this,srcipaddr.lng, (char*)inbuf+hdrpos, inbuflen-hdrpos);

    }
  } else if (protocol == PROT_IP_ICMP) {
    if (icmp_type == ICMP_ECHO_REQUEST && icmp_code==0) {
      //ICMP_ECHO_REPLY
    }
  }
//    if (i_processincome==0 || i_processincome(tr,this,processincome_data)==false)
//      pkt_broker->release_pkt(tr, 0);
}


#endif


byte _inbuf[64];
byte _send_buf[64];
uint32_t _send_buf_tp;
char _send_buf_len=0;
unsigned int const global_key[4]={0x61253461,0xFABC3231,0xF625EFC3,0x95867DC8};

// return -1 if packet was received but with error
// return 0 if time is out and no packet was received
// return 1 if packet was received and no more packets in this session was predicted
// return 2 if packet was received and more packets in this session pending (need one more receive op)
// return 3 if packet was received and more packets in this session pending (need to send one more request)
int lowpan_node::readAndProcess(unsigned int timeout) {
  crypter tea;
  unsigned int pstart=get_us();
  while (get_us()-pstart < timeout) {

    int len=rf.readPkt( 0, _inbuf, sizeof(_inbuf) );
    if (len<=0) continue; // if return 0 - no return packet

    itmp_peer* peer;
    uint32_t pt=_inbuf[0];
    uint32_t da=_inbuf[1];
    uint32_t sa=_inbuf[2];
    uint32_t index=_inbuf[3];

    if ( da == 0 ) { // it should be global broadcast to join
      uint32_t nonce[2]={0x612C3231,0x25E67DC8};
      tea.prepare(global_key, nonce, sa);
      tea.transform(_inbuf, 4, global_key, false);
      tea.transform(_inbuf + 4, len - 4 - 2, global_key, false);
      if (tea.testdigest(&_inbuf[len-2],2)) {
        if ( (pt & LOPAN_HEADER_TYPE_MASK) == LOPAN_HEADER_TYPE_LCP ) {
          int ret = process_frame_LCP(sa,_inbuf+4,len-4-2,rf.getLastRSSI());
          if (ret == PACKET_NOT_FOR_US ||
              ret == PACKET_UNKNOWN_TYPE ||
              ret == PACKET_SIGNATURE_ERROR ||
              ret == PACKET_DUPLICATE
              ) {
            continue; // continue to listen
          }
          return ret==PACKET_ACCEPTED ? ( (pt&LOPAN_HEADER_MORE_DATA) ? 2 : 1 ) : ret;
        }
      }
    } else if ( da == 0xFF || sa == 0 ) {
      uint32_t nonce[2]={0x612C3231,0x25E67DC8};
      tea.prepare(global_key, nonce, sa);
      tea.transform(_inbuf, 4, global_key, false);
      tea.transform(_inbuf + 4, len - 4 - 2, global_key, false);
      if (tea.testdigest(&_inbuf[len-2],2)) {
        if ( ( _my_addr!=0 && da==_my_addr ) || da==LOWPAN_LOCAL_BROADCAST  || da==LOWPAN_GLOBAL_BROADCAST ) {// if frame for us or from broadcast pipes
          if ( (pt & LOPAN_HEADER_TYPE_MASK) == LOPAN_HEADER_TYPE_LCP ) {
            int ret = process_frame_LCP(sa,_inbuf+4,len-4-2,rf.getLastRSSI());
            if (ret == PACKET_NOT_FOR_US ||
                ret == PACKET_UNKNOWN_TYPE ||
                ret == PACKET_SIGNATURE_ERROR ||
                ret == PACKET_DUPLICATE
                ) {
              continue; // continue to listen
            }
            return ret==PACKET_ACCEPTED ? ( (pt&LOPAN_HEADER_MORE_DATA) ? 2 : 1 ) : ret;
          }
        }
      }
    } else { // p2p
      if ( sa == coordinator_addr && coordinator_addr!=0 ) {
        peer = &master;
      } else {
        peer = 0;
      }
    if ( peer ) { //( tp & ( LOPAN_HEADER_TYPE_MASK | LOPAN_HEADER_ENCRYPTION )) != LOPAN_HEADER_TYPE_LCP ) { // test for dublicates for known peer and encrypted LCP
      peer->rssi = rf.getLastRSSI();

    unsigned int seq = (peer->rcv_seq & ~LOPAN_HEADER_SEQ_MASK) | (index & LOPAN_HEADER_SEQ_MASK );
    if ( (index & LOPAN_HEADER_SEQ_MASK ) < ( peer->rcv_seq & LOPAN_HEADER_SEQ_MASK ) ) {
      seq += LOPAN_HEADER_SEQ_MASK+1;
    }
    if ( (unsigned int)(seq - peer->rcv_seq) < ( LOPAN_HEADER_SEQ_MASK>>1 ) ) {
      peer->err_lost+=(unsigned int)(seq - peer->rcv_seq);

      tea.prepare(LTK, peer->nonce.words, seq);
      tea.transform(_inbuf, 4, LTK, false);
      tea.transform(_inbuf + 4, len - 4 - 2, LTK, false);
      if (tea.testdigest(&_inbuf[len-2],2)) {
        peer->rcv_seq = seq + 1;
        peer->receive_time = get_us();
        peer->roundtrip_time = peer->receive_time - peer->send_time;

        if ( (pt & LOPAN_HEADER_TYPE_MASK) == LOPAN_HEADER_TYPE_LCP ) {
          int ret = process_frame_LCP(sa,_inbuf+4,len-4-2,rf.getLastRSSI());
          if (ret == PACKET_NOT_FOR_US ||
              ret == PACKET_UNKNOWN_TYPE ||
              ret == PACKET_SIGNATURE_ERROR ||
              ret == PACKET_DUPLICATE
              ) {
            continue; // continue to listen
          }
          return ret==PACKET_ACCEPTED ? ( (pt&LOPAN_HEADER_MORE_DATA) ? 2 : 1 ) : ret;
        }

      } else {
        peer->err_sign++;

        return PACKET_SIGNATURE_ERROR;
      }
    } else { // it is duplicate
      peer->err_dup++;
      return PACKET_DUPLICATE; // duplicate packet
    }

      if ( ( _my_addr!=0 && da==_my_addr ) || da==LOWPAN_LOCAL_BROADCAST  || da==LOWPAN_GLOBAL_BROADCAST ) {// if frame for us or from broadcast pipes
        if ( (pt & LOPAN_HEADER_TYPE_MASK) == LOPAN_HEADER_TYPE_LCP ) {
          int ret = process_frame_LCP(sa,_inbuf+4,len-4-2,rf.getLastRSSI());
          if (ret == PACKET_NOT_FOR_US ||
              ret == PACKET_UNKNOWN_TYPE ||
              ret == PACKET_SIGNATURE_ERROR ||
              ret == PACKET_DUPLICATE
              ) {
            continue; // continue to listen
          }
          return ret==PACKET_ACCEPTED ? ( (pt&LOPAN_HEADER_MORE_DATA) ? 2 : 1 ) : ret;
        } else {
          int ret = core->process_itmp_frame(this, sa, (char const*)_inbuf+4, len-4-2);
          if (ret == PACKET_NOT_FOR_US ||
              ret == PACKET_UNKNOWN_TYPE ||
              ret == PACKET_SIGNATURE_ERROR ||
              ret == PACKET_DUPLICATE
              ) {
            continue; // continue to listen
          }
          return (pt&LOPAN_HEADER_MORE_DATA) ? 2 : ret ;
        }
      } else {
        return PACKET_NOT_FOR_US;
      }
    }
//    if (ret==0) {// no data was received
/*      if (get_us() - master.receive_time > 5000000) {
        master.receive_time=get_us();
       connected = 0;
        if (linkPin) linkPin->set(); // off
      }
      if ( !connected ) {
        if (get_us() - pastmoment > 1000000) {
          sendConnect();
          pastmoment += 1000000;
        }
      } else {
        pastmoment = get_us();
      }
      return 0;*/;

//    } else if (ret==1) { // data was received and no more data in this time quant
//      return 1;
//    } else if (ret==2) { // data was received and precessed but one more piece awaiting at other end
//      return 2;
//    }
    } // p2p end
  } // end of time loop
  return TIME_OUT;
}

// send frame with type pt, destination address da  with header and body (just two buffer ine following another)
// frame structure
// frame type - 1 byte (3 bit frame type, 1 bit data pending, 1 bit routing header present flag, 3 bits hops passed (initially 0)
// destination address  - 1 byte 0x00 - global broadcast 0xFF local broadcast
// source address  - 1 byte 0x00 - address unset 0xFF - forbidden
// frame counter helper - 1 byte
// payload - N bytes
// digest - 2 bytes

void lowpan_node::send_frame(uint32_t pt, uint32_t da, const byte* header, int headerlen, const byte* body, int body_len) {
  crypter tea;
  uint8_t frame[64];
  frame[0] = pt & LOPAN_HEADER_CLEAN_TYPE;
  frame[1] = da;
  frame[2] = _my_addr;
  frame[3] = 0;  // frame counter, frame index
  if ( headerlen + body_len > 64 - 4 - 2 ) return;

  memcpy(frame + 4, header, headerlen);
  memcpy(frame + 4 + headerlen, body, body_len);

  if ( da == 0 ) { // global broadcast
    uint32_t nonce[2]={0x612C3231,0x25E67DC8};
    tea.prepare(global_key, nonce, _my_addr);
    tea.transform(frame, 4, global_key, false);
    tea.transform(frame + 4, headerlen + body_len, global_key, false);
    tea.writedigest(frame + 4 + headerlen + body_len, 2);
    rf.sendPkt(0, frame,4 + headerlen + body_len + 2);
  } else if (da == 0xFF || _my_addr == 0) { // send broadcast or we have no address
    uint32_t nonce[2]={0x612C3231,0x25E67DC8};
    tea.prepare(global_key, nonce, _my_addr);
    tea.transform(frame, 4, global_key, false);
    tea.transform(frame + 4, headerlen + body_len, global_key, false);
    tea.writedigest(frame + 4 + headerlen + body_len, 2);
    rf.sendPkt(0, frame,4 + headerlen + body_len + 2);
  } else { // send p2p packet
    itmp_peer* peer;
    if ( da == coordinator_addr && coordinator_addr!=0 ) {
      peer = &master;
    } else {
      peer = 0;
    }
    if ( peer ) {
      frame[3] |= peer->snd_seq & LOPAN_HEADER_SEQ_MASK;
      tea.prepare(LTK, peer->nonce.words, peer->snd_seq);
      tea.transform(frame, 4, LTK, false);
      tea.transform(frame+4, headerlen + body_len, LTK, false);
      tea.writedigest(frame + 4 + headerlen + body_len, 2);

      if ( rf.sendPkt(0, frame,4 + headerlen + body_len + 2) ) {
        peer->snd_seq++;
        peer->send_time = get_us();
      }
    }

/*    frame[0]= pt | LOPAN_HEADER_TYPE_FUTHER; // mark first packet
    uint8_t* sign=signature;
    while ( headerlen>0 || body_len>0 || signature_len>0) {
      if ( headerlen + body_len + signature_len < 64-4 )
        frame[0] |= 0x10; // mark last packet
      byte* buf=frame+4;
      while (headerlen>0 && buf-frame < 64) {
        *buf++ = *header++;
        headerlen--;
        }
      while (body_len>0  && buf-frame < 64)
        {
        *buf++ = *body++;
        body_len--;
        }
      while (signature_len>0  && buf-frame < 64)
        {
        *buf++ = *sign++;
        signature_len--;
        }


//      if ( tp & LOPAN_HEADER_ENCRYPTION )
//        tea_encrypt_cfb((char*)frame+6,(buf-frame)-6,my_secret,iv);
      rf.sendPkt(0,frame,buf-frame);
      frame[0]= LOPAN_HEADER_TYPE_FUTHER | ((frame[0]+1) & 0x0F); // mark consequent packets
    }
  }*/
  }
}















