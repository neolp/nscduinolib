
#ifndef _I2CMaster_H_
#define _I2CMaster_H_

#include "sys.h"

class I2CMaster
{
  public:
    virtual int Read(uint16_t dev_addr, uint16_t reg_addr, uint8_t *data, int data_len) = 0;
    virtual int Write(uint16_t dev_addr, uint16_t reg_addr, const uint8_t *data, int data_len) = 0;
};

#endif /* _I2CMaster_H_ */

