#ifndef __SI_4463_H__
#define __SI_4463_H__

#include <stddef.h>
#include <stdint.h>
#ifdef LPOS10
#include "spi-adapter.h"
#include <string.h>
#else
#include <gpio-alloc.h>
#include <spi.h>
#define spi_t spi
#endif

#ifdef LPOS10
#include "LPOS_timers.h"
#else
#include <tim.h>
#endif

#pragma pack(push,1)
struct part_info
{
	uint8_t  chip_rev;
	uint16_t part;
	uint8_t  part_build;
	uint16_t id;
	uint8_t  customer_id;
	uint8_t  rom_id;
};
#pragma pack(pop)

class si4463
{
  int read_cmd(uint8_t const *opt_cmd, void *ptr, size_t size);
  int write_cmd(uint8_t cmd, void const *ptr, size_t size, uint32_t timeout);
  int rw(uint8_t cmd, void const *warg, size_t wsize,void *rarg, size_t rsize, uint32_t timeout);
  int wait_cts(bool stay_selected, uint32_t timeout);
  int set_property(uint16_t prop_addr, void const *ptr, size_t size,uint32_t timeout=0);
  int get_property(uint16_t prop_addr, void *ptr, size_t size,uint32_t timeout=0);
  int get_events_flags();
  void wait_nirq_falling_edge(int timeout);
  bool clearRxFifo();
  int setModeIdle();

public:
	si4463(spi &io, gpio_pin &nsel, gpio_pin &sdn, gpio_pin &nirq);
//	void init ;
	int config(void const *cfg_array, uint32_t timeout = 1000);
	//void config (uint32_t channel);

  void startListening();
//	void powerUpTx();
	void powerDown();
  bool sendPkt(unsigned int pipe, const void* buf, uint8_t len );
  int readPkt(unsigned int pipe, void* buf, uint8_t len );
  int get_modem_rssi();

//  void init(spi_t *__spi, gpio_pin* _cePin, gpio_pin* _csnPin, gpio_pin* _irqPin);
//  void config(int channel);

//  void setRADDR(unsigned int pipe, uint8_t const *addr_array);
//  void setTADDR(uint8_t const *addr_array);


	/* init & configure */

	/* io workflow */
/*	static uint32_t const event_rx     = 0x0001; // packet has been received (after start_rx())
	static uint32_t const event_rx_err = 0x0002; // error has been occurred (after start_rx())
	static uint32_t const event_tx     = 0x8000; // packet has been sent (after start_tx())
*/
  int get_part_info(part_info* info, uint32_t timeout);

	int     start_rx();
	uint32_t read_rx(void *ptr, uint32_t size);
//	uint32_t clear_rx(void);

	uint32_t write_tx(void const *ptr, uint32_t size);
	int     start_tx();

	bool    nirq_is_active() const;

  int getLastRSSI(){
    return _lastRssi;
  }

private:
  spi &io;
	gpio_pin  &nsel;
	gpio_pin  &sdn;
	gpio_pin  &nirq;

  int chip_state;
  int _lastRssi;
};
	static int const event_rx     = 0x0001; /* packet has been received (after start_rx()) */
	static int const event_rx_err = 0x0002; /* error has been occurred (after start_rx()) */
	static int const event_tx     = 0x8000; /* packet has been sent (after start_tx()) */

static inline void sleep_usec(unsigned long long usec)
{
	/* minimize using this sh*t */
	unsigned long long started_at = getlong_us();

	for (;;)
	{
		unsigned long long diff = getlong_us() - started_at;
		if (diff >= usec)
			return;
	}
}


static inline void sleep_msec(uint32_t msec)
{
	sleep_usec(msec * 1000);
}

static inline uint32_t get_ticks()
{
	return getlong_us();
}

static inline uint32_t msec_to_ticks(uint32_t msec)
{
	return (msec) * 1000;
}
// chip states

#define SI_STATE_NOT_CONFIGURED  0

#define SI_STATE_NO_CHENGE  0
#define SI_STATE_SLEP       1
#define SI_STATE_SPI_ACTIVE 2
#define SI_STATE_READY      3
#define SI_STATE_READY_2    4
#define SI_STATE_TUNE_RX    5
#define SI_STATE_TUNE_TX    6
#define SI_STATE_TX         7
#define SI_STATE_RX         8


#endif
