#include "stm32f10x_extend.h"

#include "gpio-alloc.h"
#include "pwm.h"
#include "sys.h"
#include "tim.h"


void pwm::begin(int portpin,int freq, int tickspersec, int initvalue)
  {
    if ( !gpio_pin::isFree(portpin) )
      return;
  if (portpin==(PORTB|PIN8) || portpin==(PORTA|PIN6) )
    {// tim16
    RCC->APB2ENR |= RCC_APB2ENR_TIM16EN;
    if (portpin==(PORTA|PIN6)) // if remap need
      AFIO->MAPR2|=AFIO_MAPR2_TIM16_REMAP;
    else
      AFIO->MAPR2&=~AFIO_MAPR2_TIM16_REMAP;

    TIM=TIM16;
    TIM16->CR1 = TIM_CR1_ARPE | TIM_CR1_CEN;
    TIM16->CR2 = 0;
    TIM16->DIER = 0;
    TIM16->EGR = 0;
    TIM16->CCMR1 = (TIM_CCMR1_OC1M & (TIM_CCMR_OCM_PWM_LOW_MATCH << 4)) | TIM_CCMR1_OC1PE ; // set mode
    TIM16->CCER = TIM_CCER_CC1E;
    TIM16->CNT = 0;
    // bit_freq = Ftimer / (PSC+1) ===>  PSC = (Ftimer / bit_freq) - 1
    // bitfreq = freq * max
    // PSC = (Ftimer / freq * max) - 1
    TIM16->PSC = sys_get_osc() / (tickspersec) - 1;
    TIM16->ARR = tickspersec/freq;
    TIM16->RCR = 0;
    TIM16->CCR1 = initvalue;
    TIM16->BDTR = TIM_BDTR_AOE | TIM_BDTR_MOE;

//    TIM15_CH1.acquire(PORTB | PIN15,GPIO_OUT_ALT_PUSHPULL_50MHz);

    }
  else if (portpin==(PORTB|PIN9) || portpin==(PORTA|PIN7) )
    {// tim17
    RCC->APB2ENR |= RCC_APB2ENR_TIM17EN;
    if (portpin==(PORTA|PIN7)) // if remap need
      AFIO->MAPR2|=AFIO_MAPR2_TIM17_REMAP;
    else
      AFIO->MAPR2&=~AFIO_MAPR2_TIM17_REMAP;

    TIM=TIM17;
    TIM17->CR1 = TIM_CR1_ARPE | TIM_CR1_CEN;
    TIM17->CR2 = 0;
    TIM17->DIER = 0;
    TIM17->EGR = 0;
    TIM17->CCMR1 = (TIM_CCMR1_OC1M & (TIM_CCMR_OCM_PWM_LOW_MATCH << 4)) | TIM_CCMR1_OC1PE ; // set mode
    TIM17->CCER = TIM_CCER_CC1E;
    TIM17->CNT = 0;
    // bit_freq = Ftimer / (PSC+1) ===>  PSC = (Ftimer / bit_freq) - 1
    // bitfreq = freq * max
    // PSC = (Ftimer / freq * max) - 1
    TIM17->PSC = sys_get_osc() / (tickspersec) - 1;
    TIM17->ARR = tickspersec/freq;
    TIM17->RCR = 0;
    TIM17->CCR1 = initvalue;
    TIM17->BDTR = TIM_BDTR_AOE | TIM_BDTR_MOE;
    }
  else
    return;
  pwm_pin.acquire(portpin,GPIO_OUT_ALT_PUSHPULL_50MHz);
  }
void pwm::set(int value)
  {
  TIM->CCR1 = value;
  }

int pwm::get()
  {
  return TIM->CCR1;
  }

bool softpwm_t::isconfigured()
  {
  return values_num>0;
  //return GetIRQHandler(TIM1_UP_TIM16_IRQn)==IRQ_Handler || GetIRQHandler(TIM1_TRG_COM_TIM17_IRQn)==IRQ_Handler;
  }

void softpwm_t::begin(TIM_TypeDef* SELTIM,int freq, int tickspersec)
  {
  if (SELTIM==TIM16)
    {
    RCC->APB2ENR |= RCC_APB2ENR_TIM16EN;
    TIM=TIM16;

    TIM=TIM16;
    TIM16->CR1 = TIM_CR1_ARPE | TIM_CR1_CEN;
    TIM16->CR2 = 0;
    TIM16->DIER = 0;
    TIM16->EGR = 0;
    TIM16->CCMR1 = (TIM_CCMR1_OC1M & (TIM_CCMR_OCM_FROZEN << 4)); // set mode
    TIM16->CCER = 0;
    TIM16->CNT = 0;
    // bit_freq = Ftimer / (PSC+1) ===>  PSC = (Ftimer / bit_freq) - 1
    // bitfreq = freq * max
    // PSC = (Ftimer / freq * max) - 1
    TIM16->PSC = sys_get_osc() / (tickspersec) - 1;
    TIM16->ARR = tickspersec/freq;
    TIM16->RCR = 0;
    TIM16->CCR1 = tickspersec/freq + 1;
    TIM16->BDTR = TIM_BDTR_AOE | TIM_BDTR_MOE;

    TIM16->DIER = TIM_DIER_CC1IE | TIM_DIER_UIE;

    SetIRQHandler(TIM1_UP_TIM16_IRQn, IRQ_Handler, this);
    }
  else if (SELTIM==TIM17)
    {// tim17
    RCC->APB2ENR |= RCC_APB2ENR_TIM17EN;

    TIM=TIM17;
    TIM17->CR1 = TIM_CR1_ARPE | TIM_CR1_CEN;
    TIM17->CR2 = 0;
    TIM17->DIER = 0;
    TIM17->EGR = 0;
    TIM17->CCMR1 = (TIM_CCMR1_OC1M & (TIM_CCMR_OCM_FROZEN << 4)); // set mode
    TIM17->CCER = 0;
    TIM17->CNT = 0;
    // bit_freq = Ftimer / (PSC+1) ===>  PSC = (Ftimer / bit_freq) - 1
    // bitfreq = freq * max
    // PSC = (Ftimer / freq * max) - 1
    TIM17->PSC = sys_get_osc() / (tickspersec) - 1;
    TIM17->ARR = tickspersec/freq;
    TIM17->RCR = 0;
    TIM17->CCR1 = tickspersec/freq + 1;
    TIM17->BDTR = TIM_BDTR_AOE | TIM_BDTR_MOE;

    TIM17->DIER = TIM_DIER_CC1IE | TIM_DIER_UIE;

    SetIRQHandler(TIM1_TRG_COM_TIM17_IRQn, IRQ_Handler, this);
    }
  }


void softpwm_t::addPin(int pin,unsigned short value)
  {
  pins[values_num]=pin;
  values[values_num]=value;
  values_num++;
  }

void softpwm_t::removePin(int pin)
  {
  for (int i=0;i<values_num-1;i++)
    {
    if (pins[i]==pin)
      {
      pins[i]=pins[values_num];
      values[i]=values[values_num];
      values_num--;
      }
    }
  if (pins[values_num-1]==pin)
    values_num--;
  }


void softpwm_t::set(unsigned int PortPin)
  {
  int set_bit=1<<(PortPin&0x0F);
  PortPin>>=4;
  GPIO_TypeDef* GPIOREG= PortPin==0 ? GPIOA : (PortPin==1?GPIOB:GPIOC);
  GPIOREG->BSRR=set_bit;
  }
void softpwm_t::reset(unsigned int PortPin)
  {
  int reset_bit=1<<(PortPin&0x0F);
  PortPin>>=4;
  GPIO_TypeDef* GPIOREG= PortPin==0 ? GPIOA : (PortPin==1?GPIOB:GPIOC);
  GPIOREG->BRR=reset_bit;
  }

bool softpwm_t::set_pwm(unsigned int pin,unsigned int value)
  {
  for (int i=0;i<values_num;i++)
    {
    if (pins[i]==pin)
      {
      values[i]=value; return true;
      }
    }
  return false;
  }


void softpwm_t::IRQ_Handler(void* param)
  {
  softpwm_t* t=(softpwm_t*)param;
  unsigned int tstat=t->TIM->SR;
  if (tstat&TIM_SR_UIF) // update
    {
    t->index=0;
    if (t->values_num>0) // if current value is pwm
      {
      t->set(t->pins[0]);
      t->TIM->CCR1=t->values[t->index];
      }
    }

  if (tstat&TIM_SR_CC1IF) // compare
    {
    if (t->index<t->values_num) // if current value is pwm
      t->reset(t->pins[t->index]);
    t->index++;
    if (t->index<t->values_num) // if current value is pwm
      {
      t->set(t->pins[t->index]);
      t->TIM->CCR1+=t->values[t->index];
      }
    }
  t->TIM->SR=0;
  }
