#ifndef _FAN_HPP_
#define _FAN_HPP_

#include "spi.h"
#include "gpio-alloc.h"

class PWMTimer {
  TIM_TypeDef * TIM;
  volatile uint16_t* CCR;
public:
  PWMTimer():TIM(0),CCR(0){}
  bool isInitialized() { return TIM !=0; }
  void init(  TIM_TypeDef * _TIM, int channel){
    TIM =_TIM;

    if (TIM == TIM2) {
      RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
    } else if (TIM == TIM3) {
      RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
//    } else if (TIM == TIM4) {
//      RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
    } else if (TIM == TIM1) {
      RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
    } else if (TIM == TIM15) {
      RCC->APB2ENR |= RCC_APB2ENR_TIM15EN;
    }
  	TIM->CR1 = TIM_CR1_ARPE | TIM_CR1_CEN;
    TIM->PSC = 2;
    TIM->ARR = 0x100;
    TIM->BDTR = TIM_BDTR_MOE;
    TIM->BDTR = TIM_BDTR_MOE;
    if (channel == 1) {
      TIM->CCMR1 = (TIM->CCMR1 & ~0xFF) | (TIM_CCMR1_OC1M & (TIM_CCMR_OCM_PWM_LOW_MATCH << 4)) | TIM_CCMR1_OC1PE;
      TIM->CCER |= TIM_CCER_CC1E;
      CCR = &(TIM->CCR1);
    } else if (channel == 2) {
      TIM->CCMR1 = (TIM->CCMR1 & ~0xFF00) | (TIM_CCMR1_OC2M & (TIM_CCMR_OCM_PWM_LOW_MATCH << 12)) | TIM_CCMR1_OC2PE;
      TIM->CCER |= TIM_CCER_CC2E;
      CCR = &(TIM->CCR2);
    } else if (channel == 3) {
      TIM->CCMR2 = (TIM->CCMR2 & ~0xFF) | (TIM_CCMR2_OC3M & (TIM_CCMR_OCM_PWM_LOW_MATCH << 4)) | TIM_CCMR2_OC3PE;
      TIM->CCER |= TIM_CCER_CC3E;
      CCR = &(TIM->CCR3);
    } else if (channel == 4) {
      TIM->CCMR2 = (TIM->CCMR2 & ~0xFF00) | (TIM_CCMR2_OC4M & (TIM_CCMR_OCM_PWM_LOW_MATCH << 12)) | TIM_CCMR2_OC4PE;
      TIM->CCER |= TIM_CCER_CC4E;
      CCR = &(TIM->CCR4);
    }
    //TIM->BDTR = TIM_BDTR_AOE | TIM_BDTR_MOE;
  }

  void set(int v) {
    *CCR = v;
  }

};

class Fan
  {
  gpio_pin pwm_pin;
  gpio_pin spd_pin;
  PWMTimer timer;

public:
  unsigned int start;
  unsigned int laststart;
  unsigned int period;
  unsigned int rpm;
  unsigned int value;
  Fan() {
    volatile int x = 0;
    //gpio_pin spics;
    //cs_pin->acquire(PORTA | PIN4,GPIO_OUT_PUSHPULL_50MHz);
  }
  void begin(int _pwm_pin, int _spd_pin, bool analog = false) {
    if (_spd_pin)
      spd_pin.acquire(_spd_pin,GPIO_IN_PULLUP);
    if (analog && _pwm_pin == (PORTB | PIN4) ) {
      AFIO->MAPR = (AFIO->MAPR & ~AFIO_MAPR_TIM3_REMAP) | AFIO_MAPR_TIM3_REMAP_1; //TIM3_REMAP[1:0] = �10� (partial remap)
      timer.init(TIM3,1);
      timer.set(0x00); // #2
      pwm_pin.acquire(_pwm_pin,GPIO_OUT_ALT_PUSHPULL_10MHz);
    } else if (analog && _pwm_pin == (PORTB | PIN5) ) {
      AFIO->MAPR = (AFIO->MAPR & ~AFIO_MAPR_TIM3_REMAP) | AFIO_MAPR_TIM3_REMAP_1; //TIM3_REMAP[1:0] = �10� (partial remap)
      timer.init(TIM3,2);
      timer.set(0x00); // #3
      pwm_pin.acquire(_pwm_pin,GPIO_OUT_ALT_PUSHPULL_10MHz);
    } else if (false && analog && _pwm_pin == (PORTB | PIN6) ) {
      timer.init(TIM4,1);
      timer.set(0x00); // #1
      pwm_pin.acquire(_pwm_pin,GPIO_OUT_ALT_PUSHPULL_10MHz);
    } else if (analog && _pwm_pin == (PORTA | PIN11) ) {
      timer.init(TIM1,4);
      timer.set(0x00); // #4
      pwm_pin.acquire(_pwm_pin,GPIO_OUT_ALT_PUSHPULL_10MHz);
    } else if (analog && _pwm_pin == (PORTB | PIN14) ) {
      AFIO->MAPR2 = AFIO->MAPR2 | AFIO_MAPR2_TIM15_REMAP;
      timer.init(TIM15,1);
      timer.set(0x00);
      pwm_pin.acquire(_pwm_pin,GPIO_OUT_ALT_PUSHPULL_10MHz);
    } else if (analog && _pwm_pin == (PORTB | PIN15) ) {
      AFIO->MAPR2 = AFIO->MAPR2 | AFIO_MAPR2_TIM15_REMAP;
      timer.init(TIM15,2);
      timer.set(0x00);
      pwm_pin.acquire(_pwm_pin,GPIO_OUT_ALT_PUSHPULL_10MHz);
    } else if (analog && _pwm_pin == (PORTB | PIN10) ) {
      AFIO->MAPR = AFIO->MAPR | AFIO_MAPR_TIM2_REMAP_1;
      timer.init(TIM2,3);
      timer.set(0x00);
      pwm_pin.acquire(_pwm_pin,GPIO_OUT_ALT_PUSHPULL_10MHz);
    } else if (analog && _pwm_pin == (PORTB | PIN11) ) {
      AFIO->MAPR = AFIO->MAPR | AFIO_MAPR_TIM2_REMAP_1;
      timer.init(TIM2,4);
      timer.set(0x00);
      pwm_pin.acquire(_pwm_pin,GPIO_OUT_ALT_PUSHPULL_10MHz);
    } else if (analog && _pwm_pin == (PORTA | PIN8) ) {
      timer.init(TIM1,1);
      timer.set(0x00);
      pwm_pin.acquire(_pwm_pin,GPIO_OUT_ALT_PUSHPULL_10MHz);
    } else {
      pwm_pin.acquire(_pwm_pin,GPIO_OUT_PUSHPULL_10MHz);
      pwm_pin.reset();
    }

  }

  void on(int val=0x100){
    if (timer.isInitialized()){
      value = val;
      timer.set(val);
    } else {
      value = 1;
      pwm_pin.set();
    }
  }
  void off(void){
    value = 0;
    if (timer.isInitialized()){
      timer.set(0);
    } else {
      pwm_pin.reset();
    }
  }
  int get(void){
    return value;
  }
  void poll(){
    if (spd_pin.acquired() && spd_pin.read()) { 
      unsigned int now = get_us();
      if ( (now - start) > 520 ) {
        period = start - laststart;
        laststart = start;
        rpm = (60*1000000/2) / period;
      }
      start = now;
    }
  }
  
};

#endif /* _FAN_HPP_ */
