#ifndef _ACCEL_ADXL345_H_
#define _ACCEL_ADXL345_H_

#include "sys.h"
#include <accel.h>

#define ADXL345_DEVID_REG 0x00 //Device ID(R)
#define ADXL345_THRESH_TAP_REG 0x1D //Tap threshold(R/W)
#define ADXL345_OFSX_REG 0x1E //X-axis offset(R/W)
#define ADXL345_OFSY_REG 0x1F //Y-axis offset(R/W)
#define ADXL345_OFSZ_REG 0x20 //Z-axis offset(R/W)
//...
#define ADXL345_BW_RATE_REG 0x2C //Data rate and power mode control(R/W)
#define ADXL345_POWER_CTL_REG 0x2D //Power-saving features control(R/W)
#define ADXL345_INT_ENABLE_REG 0x2E //Interrupt enable controll(R/W)
#define ADXL345_INT_MAP_REG 0x2F //Interrupt mapping control(R/W)
#define ADXL345_INT_SOURCE_REG 0x30 //Source of interrupts(R)
#define ADXL345_DATA_FORMAT_REG 0x31 //Data format control(R/W)
#define ADXL345_DATAX0_REG 0x32 //X-Axis Data 0(R)
#define ADXL345_DATAX1_REG 0x33 //X-Axis Data 1(R)
#define ADXL345_DATAY0_REG 0x34 //Y-Axis Data 0(R)
#define ADXL345_DATAY1_REG 0x35 //Y-Axis Data 1(R)
#define ADXL345_DATAZ0_REG 0x36 //Z-Axis Data 0(R)
#define ADXL345_DATAZ1_REG 0x37 //Z-Axis Data 1(R)
#define ADXL345_FIFO_CTL_REG 0x38 //FIFO control(R/W)
#define ADXL345_FIFO_STATUS_REG 0x39 //FIFO status(R)

class Accelerometer_ADXL345_I2C:public Accelerometer
{
  private:
    I2CMaster *I2C_master;
    uint16_t I2C_addr;
    uint8_t ReceivedValues[6];
    int WriteReg(uint8_t reg,uint8_t value);
    int ReadReg(uint8_t reg, uint8_t &value);
  public:
    Accelerometer_ADXL345_I2C(I2CMaster *_master, uint16_t _i2c_addr)
    {
      I2C_master = _master;
      I2C_addr = _i2c_addr;
    }
    int init(void);
    bool GetValues(Accelerations_t &data);
    uint8_t GetDevID(void); 
    uint8_t GetIntSrcREG(void);
};

#endif /* _ACCEL_ADXL345_H_ */



