#include "stm32f10x.h"
#include "stm32f10x_extend.h"

#include "gpio-alloc.h"
#include "motor823BLDC.h"

int motor823BLDC_driver::begin()
  {
  // if 0: No remap (CH1/PA2, CH2/PA3)
  // AFIO->MAPR2 &= ~AFIO_MAPR2_TIM15_REMAP;
  //else 1: Remap (CH1/PB14, CH2/PB15)
  AFIO->MAPR2 |= AFIO_MAPR2_TIM15_REMAP;
	RCC->APB2ENR |= RCC_APB2ENR_TIM15EN;
	TIM15->CR1 = TIM_CR1_ARPE | TIM_CR1_CEN;
	TIM15->CCMR1 = (TIM_CCMR1_OC1M & (TIM_CCMR_OCM_PWM_LOW_MATCH << 4)) | TIM_CCMR1_OC1PE |
                 (TIM_CCMR1_OC2M & (TIM_CCMR_OCM_PWM_LOW_MATCH << (4+8))) | TIM_CCMR1_OC2PE;
	TIM15->ARR = 0x500;
	TIM15->CCER = TIM_CCER_CC1E | TIM_CCER_CC2E;
	TIM15->BDTR = TIM_BDTR_AOE | TIM_BDTR_MOE;

  EN1.acquire(PORTB | PIN11,GPIO_IN_PULLUP);
  INA1.acquire(PORTA | PIN15,GPIO_OUT_PUSHPULL_50MHz);
  INB1.acquire(PORTA | PIN8, GPIO_OUT_PUSHPULL_50MHz);
  TIM15_CH2.acquire(PORTB | PIN14,GPIO_OUT_ALT_PUSHPULL_50MHz);

  EN2.acquire(PORTB | PIN10,GPIO_IN_PULLUP);
  INA2.acquire(PORTB | PIN13,GPIO_OUT_PUSHPULL_50MHz);
  INB2.acquire(PORTB | PIN12,GPIO_OUT_PUSHPULL_50MHz);
  TIM15_CH1.acquire(PORTB | PIN15,GPIO_OUT_ALT_PUSHPULL_50MHz);

  leftEnc.init();
  rightEnc.init();

  lastmeasuretime=get_us();
  trajectoryreset();

  pos=0;

  return 0;
  }

int motor823BLDC_driver::end()
  {
  return -1;
  }

// U V W - phases - B1 A1 B2 (A2 - unused)
// H1 H2 H3 hall sensors
void motor823BLDC_driver::setphases(int sp) {
  if ( H1 != 0 && H2 == 0 && H3 != 0  ) pos=0;
  if ( H1 != 0 && H2 == 0 && H3 == 0  ) pos=1;
  if ( H1 != 0 && H2 != 0 && H3 == 0  ) pos=2;
  if ( H1 == 0 && H2 != 0 && H3 == 0  ) pos=3;
  if ( H1 == 0 && H2 != 0 && H3 != 0  ) pos=4;
  if ( H1 == 0 && H2 == 0 && H3 != 0  ) pos=5;
  TIM15->CCR2 = sp;
  TIM15->CCR1 = sp;
  switch (pos) {
  case 0: // U- V+ W0
    INB1.reset();
    INA1.reset();
    break;
  case 1: // U- V0 W+
    INB1.set();
    INA1.reset();
    break;
  case 2: // U0 V- W+
    INB1.reset();
    INA1.set();
    break;
  case 3: // U+ V- W0
    INB2.reset();
    INA2.reset();
    break;
  case 4: // U+ V0 W-
    INB2.set();
    INA2.reset();
    break;
  case 5: // U0 V+ W-
    INB2.reset();
    INA2.set();
    break;
  }
}
