#ifndef _AS5048A_HPP_
#define _AS5048A_HPP_

#include "spi.h"
#include "gpio-alloc.h"

class AS5048A
  {
  spi* spi_dev;
  gpio_pin* cs_pin;

  unsigned int angle;
  unsigned int angleErr;
  unsigned int anglePar;
  unsigned int angleDiag;
  int globalangle;

public:
  int getAngle(){return angle;}
  int getGlobalAngle(){return globalangle;}
  void setGlobalAngle(int value){globalangle=value;}
  int getAngleDiag() {return angleDiag;}
  AS5048A() {
    angle = 0;
    angleErr = 0;
    anglePar = 0;
    angleDiag = 0;
    globalangle = 0;
    //gpio_pin spics;
    //cs_pin->acquire(PORTA | PIN4,GPIO_OUT_PUSHPULL_50MHz);
  }
  void begin(spi* _spi_dev, gpio_pin* _cs_pin) {
    cs_pin = _cs_pin;
    spi_dev = _spi_dev;
    spi_dev->begin(10000000, 16); //start with frequency not more than 10 MHz
    spi_dev->setDataMode(SPI_CR1_CPHA );
    //spi_dev->cs = &spics;
  }

    void poll(void){
/*          cs_pin->reset();
          spi_dev->fast_transfer(0xFF);
          spi_dev->fast_transfer(0xFF);
          cs_pin->set();*/
          cs_pin->reset();
//          int ah = spi_dev->fast_transfer(0xFF);
//          int al = spi_dev->fast_transfer(0xFF);
          unsigned int v = spi_dev->fast_transfer(0xFFFF);
          cs_pin->set();
//          unsigned int v=al | (ah<<8);
/*          int par=0;
          for (int i=0;i<16;i++) {
            par ^= (v>>i)&1;
          }
          if (!par){*/
            int newangle = (v)&0x3FFF;
            angleErr = (v>>14)&0x01;

      //int angle = anglesensor.getAngle();
      if ( newangle - (int)angle < -0x2001 ) {
        globalangle += newangle - (int)angle + 0x4000;
      } else if (newangle - (int)angle > 0x2000) {
        globalangle += newangle - (int)angle - 0x4000;
      } else {
        globalangle += newangle - (int)angle;
      }
      angle = newangle;
            
          //}
  }
  void resetGlobalAngle(){
    globalangle=0;
  }
  

  void diag(void){
    cs_pin->reset();
    spi_dev->fast_transfer(0xFFFF);
    //spi_dev->fast_transfer(0xFF);
    //spi_dev->fast_transfer(0xFF);
//          spi_dev->fast_transfer(0x7F);
//          spi_dev->fast_transfer(0xFE);
    cs_pin->set();
    cs_pin->reset();
    unsigned int v = spi_dev->fast_transfer(0x4000|0x3FFD);
    //int ah = spi_dev->fast_transfer(0x7F);
    //int al = spi_dev->fast_transfer(0xFD);
    cs_pin->set();
    cs_pin->reset();
    unsigned int d = spi_dev->fast_transfer(0xFFFF);
    //int dh = spi_dev->fast_transfer(0xFF);
    //int dl = spi_dev->fast_transfer(0xFF);
    cs_pin->set();
    //unsigned int v=al | (ah<<8);
    //unsigned int d=dl | (dh<<8);
    angleDiag = d;
    int newangle = (v)&0x3FFF;
    angleErr = (d>>14)&0x01;
    //anglePar = (ah>>7)&0x01;
    int par=0;
    for (int i=0;i<16;i++) {
      par ^= (v>>i)&1;
    }
    anglePar = par;
    if (angleErr){
      cs_pin->reset();
      //int ah = spi_dev->fast_transfer(0x80);
      //int al = spi_dev->fast_transfer(0x01);
      //spi_dev->fast_transfer(0xFF);
      //spi_dev->fast_transfer(0xFF);
      int a = spi_dev->fast_transfer(0x4001);
      spi_dev->fast_transfer(0xFFFF);
      cs_pin->set();
    }
      if ( newangle - (int)angle < -0x2001 ) {
        globalangle += newangle - (int)angle + 0x4000;
      } else if (newangle - (int)angle > 0x2000) {
        globalangle += newangle - (int)angle - 0x4000;
      } else {
        globalangle += newangle - (int)angle;
      }
      angle = newangle;
  }
};

#endif /* _AS5048A_HPP_ */
