#include "itmp.h"
#include "crc.h"
#include "lib/cbor.h"




void itmp_link::set_LTK(byte* id) {
  memcpy(LTK,id,sizeof(LTK));
  joined=1;
  }



void itmp_core::begin(int features, uint64_t id , const topic_descr* _topics, int _topic_num /*, const char* _devdescr*/) {
  DeviceID=id;
  topics = _topics;
  topics_num = _topic_num;
}

byte itmp_id=0;
int32_t itmp_core::send_event(itmp_link* to, const char* topic, void* data, uint32_t data_len) {
  byte frame[32];
  CBOR_encoder enc((char*)frame,sizeof(frame));
  enc.write_arr(data_len>0 ? 4 : 3 );
  enc.write_int(ITMP_TP_EVENT);
  enc.write_int(itmp_id++);
  enc.write_rawstr(topic);
  if (data_len>0) {
    memcpy(frame+enc.length(),data, data_len);
  }
  if (to->isConnected()) {
    to->send_frame(LOPAN_HEADER_TYPE_ITMP, to->coordinator_addr, frame, enc.length() + data_len );
    return 0;
  }
  return -1;
}
bool itmp_core::isLinkConnected(int linkindex){
  if (linkindex>=0) {
    if ( linkindex<links_num ) return links[linkindex]->isConnected();
    return false;
  } else {
    for (int i=0;i<links_num; i++) {
      if ( links[i]->isConnected() ) {
        return true;
      }
    }
  }
  return false;
}

bool itmp_core::isConnected(){
  for (int i=0;i<links_num; i++) {
    if ( links[i]->connected_state>1 ) {
      return true;
    }
  }
  return false;
}

void itmp_core::disconnect(int linkindex) {
  if (linkindex>=0) {
    if ( linkindex<links_num ) links[linkindex]->disconnect();
  } else {
    for (int i=0;i<links_num; i++) {
      links[i]->disconnect();
    }
  }
}

bool itmp_core::sendConnect(){
  byte frame[32];
  CBOR_encoder enc((char*)frame,sizeof(frame));
  enc.write_arr(3);
  enc.write_int(ITMP_TP_CONNECT);
  enc.write_int(itmp_id++);
  enc.write_rawstr(topics[0].descr);
  for (int i=0;i<links_num; i++) {
    if ( links[i]->isConnected() ) {
      links[i]->send_frame(LOPAN_HEADER_TYPE_ITMP, links[i]->coordinator_addr, frame, enc.length() );
    }
  }
  return 0;
}

int itmp_core::readAndProcess(unsigned int usec, int linkindex) {
  if (linkindex>=0) {
    if ( linkindex<links_num ) return links[linkindex]->readAndProcess(usec);
    return 0;
  } else {
    int ret=0;
    for (int i=0;i<links_num; i++) {
      ret = links[i]->readAndProcess(usec);
      if (ret>0) break;
    }
    return ret;
  }

}


int32_t itmp_core::send_event_int(const char* topic, int val) {
  byte frame[32];
  CBOR_encoder enc((char*)frame,sizeof(frame));
  enc.write_arr(4);
  enc.write_int(ITMP_TP_EVENT);
  enc.write_int(itmp_id++);
  enc.write_rawstr(topic);
  enc.write_arr(1);
  enc.write_int(val);
  for (int i=0;i<links_num; i++) {
    if ( links[i]->isConnected() ) {
      links[i]->send_frame(LOPAN_HEADER_TYPE_ITMP, links[i]->coordinator_addr, frame, enc.length() );
    }
  }
  return 0;
}
int32_t itmp_core::send_event_str(const char* topic, const char* val){
  byte frame[32];
  CBOR_encoder enc((char*)frame,sizeof(frame));
  enc.write_arr(4);
  enc.write_int(ITMP_TP_EVENT);
  enc.write_int(itmp_id++);
  enc.write_rawstr(topic);
  enc.write_arr(1);
  enc.write_rawstr(val);
  for (int i=0;i<links_num; i++) {
    if ( links[i]->isConnected() ) {
      links[i]->send_frame(LOPAN_HEADER_TYPE_ITMP, links[i]->coordinator_addr, frame, enc.length() );
    }
  }
  return 0;
}
int32_t itmp_core::send_event_bstr(const char* topic, const char* val, int vallen){
  byte frame[32];
  CBOR_encoder enc((char*)frame,sizeof(frame));
  enc.write_arr(4);
  enc.write_int(ITMP_TP_EVENT);
  enc.write_int(itmp_id++);
  enc.write_rawstr(topic);
  enc.write_arr(1);
  enc.write_bstr(val,vallen);
  for (int i=0;i<links_num; i++) {
    if ( links[i]->isConnected() ) {
      links[i]->send_frame(LOPAN_HEADER_TYPE_ITMP, links[i]->coordinator_addr, frame, enc.length() );
    }
  }
  return 0;
}



int itmp_core::write_itmp_err(char* buf, int buflen, int tp, int id, int code, const char* str) {
  CBOR_encoder enc(buf,buflen);
  enc.write_arr( 4 ); // array with 4 elements
  enc.write_int( tp ); // first element message type
  enc.write_int( id );  // second element element message id
  enc.write_int( code );  // third element error code
  enc.write_rawstr( str ); // fourth element topic body
  return enc.length();
}

int itmp_core::write_itmp_shortresp(char* buf, int buflen, int tp, int id) {
  CBOR_encoder enc(buf,buflen);
  enc.write_arr( 2 ); // array with 2 elements
  enc.write_int( tp ); // first element message type
  enc.write_int( id );  // second element element message id
  return enc.length();
}

/*
const topic_descr* itmp_core::gettopic(const byte* uri)
  {
  int urilen=uri[0];
  if ( urilen>=0x80 ) return 0; // do not process options
  topic_descr* topics =(topic_descr*)__section_begin("externs");
  for (int i=0;i<__section_size("externs")/sizeof(topic_descr);i++)
    {
    //int topic_name_len=topics[i].name[0];
//    if (topics[i].type==0 && memcmp(uri+1,topics[i].name+1, topic_name_len)==0 && (topic_name_len==urilen || uri[topic_name_len+1]=='/'))
//    if (topics[i].type==0 && memcmp(uri,topics[i].name, topic_name_len)==0)
    if (memcmp(uri,topics[i].name, urilen+1)==0)
      {
      return topics+i;
      }
    }
  return 0;
  }
*/

/*
int writesign(byte* out, const byte* name, int namelen, const byte* sig)
  {
  int i=namelen; out++;
  memcpy(out,name,namelen);
  sig++;
  while(*sig)
    {
    if (*sig=='{')
      {
      sig++;
      while (*sig && *sig!='}')
        sig++;
      if (*sig==0)
        break;
      sig++;
      continue;
      }
    out[i++]=*sig++;
    }
  out--;
  *out=i;
  return i+1;
  }
*/
/*
int write_topic_list(const char* url, const byte* inp, int input_len, byte* out)
  {
  int topics_num=__section_size("externs")/sizeof(topic_descr);
  topic_descr* topics =(topic_descr*)__section_begin("externs");
  int windex=1;
  int urilen=url[0];
  int slash=urilen==0?0:1; // remove delilmiter except first one
  if (urilen>0 && url[urilen]=='/') urilen--;
  int tp=0;
  for (int i=0;i<topics_num;i++)
    {
    if (topics[i].name[0]<=urilen || memcmp(url+1,topics[i].name+1, urilen)!=0)
      continue;
    if (urilen!=0 && topics[i].name[urilen+1]!='/')
      continue;

    for (int j=urilen+1; j<topics[i].name[0]; j++)
      if (topics[i].name[j+1]=='/')
        goto nexttopic;

    {
    windex+=writesign(out+windex, topics[i].name+1+urilen+slash, topics[i].name[0]-urilen-slash, topics[i].descr);
    }
    tp++;
nexttopic:
    ;
    }
  out[0]=tp;
  return windex;
  }
*/


int write_itmp_descr(char* buf, int buflen, uint64_t id, const char* str) {
  CBOR_encoder enc(buf,buflen);
  enc.write_arr(3);
  enc.write_int(ITMP_TP_DESCRIPTION);
  enc.write_int(id);
  enc.write_arr(1);
  enc.write_rawstr(str);
  return enc.length();
}


int itmp_core::write_itmp_root_descr(char* buf, int buflen, uint64_t id) {
  char desc[256];
  CBOR_encoder enc(buf, buflen);
  enc.write_arr(3); // array with 3 elements
  enc.write_int(ITMP_TP_DESCRIPTION); // first element message type
  enc.write_int(id);  // second element element message id

   enc.write_arr(topics_num); // third element array with num+1 element
//  enc.write_rawstr("FireGuard`Fire Alarm and automatic Destiguishing board%1.0.10.435#231268834874553@NSC Communication Siberia`:Node");
  for (int i=0;i<topics_num;i++) {
    int dindex=0;
    const char* src=topics[i].name;
    while (*src) {
      if (dindex>=sizeof(desc)) {
        return (-500); // no memory for root description -- too much of functions should be impossible
      }
      desc[dindex++]=*src++;
    }
    src=topics[i].descr;
    if (*src == '&' ) {
      if (dindex>=sizeof(desc)) {
        enc.seterror(-500);
        goto finish;
      }
      desc[dindex++]='&';
    } else {
      while (*src) {
        if (*src=='`') {
          src++;
          while (*src && *src!='`') src++;
          if (*src=='`') src++;
        } else {
          if (dindex>=sizeof(desc)) {
            enc.seterror(-500);
            goto finish;
          }
          desc[dindex++]=*src++;
        }
      }
    }
    if (dindex>=sizeof(desc)) {
      enc.seterror(-500);
      goto finish;
    }
    desc[dindex]=0;
    enc.write_rawstr(desc);
  }
finish:
  return enc.ok() ? enc.length() : write_itmp_err(buf, buflen, ITMP_TP_ERROR, id, 500, "no memory for root DESCRIBE");
}


int itmp_core::process_describe(itmp_link* from, unsigned int addr, CBOR_decoder& dec, char* resp, int resplen) {
  uint64_t id;
  const char* uri;
  int urilen;

  id=dec.read_int();
  if (dec.error()) {
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, 0, -dec.error(), "DESCRIBE id format error");
  }
  urilen=dec.read_str(&uri);
  if (dec.error()) {
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, -dec.error(), "DESCRIBE uri format error");
  }
  if (urilen == 0) {
    return write_itmp_root_descr(resp, resplen, id);
  } else {
    for (int i=0; i < topics_num; i++) {
      if (urilen==topics[i].namelen && memcmp(uri,topics[i].name, urilen)==0) {
        return write_itmp_descr(resp, resplen, id, topics[i].descr);
      }
    }
  }
  return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, 404, "no such uri for DESCRIBE");
}


int itmp_core::process_event(itmp_link* from, unsigned int addr, CBOR_decoder& dec, char* resp, int resplen) {
  return -1;
}

int itmp_core::process_publish(itmp_link* from, unsigned int addr, CBOR_decoder& dec, char* resp, int resplen) {
  uint64_t id;
  const char* uri;
  //int urilen;

  id=dec.read_int();
  if (dec.error()) {
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, 0, -dec.error(), "DESCRIBE id format error");
  }
  //urilen=
  dec.read_str(&uri);
  if (dec.error()) {
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, -dec.error(), "DESCRIBE uri format error");
  }

  int ret=process_event(from, addr, dec, resp, resplen);
  if (ret>=0) {
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, 404, "no such uri for DESCRIBE");
  }
  return -1;
}

int write_itmp_emptyresp(char* buf, int buflen, int tp, uint64_t id)
  {
  CBOR_encoder enc(buf,buflen);
  enc.write_arr( 2 ); // array with 3 elements
  enc.write_int( tp ); // first element message type
  enc.write_int( id );  // second element element message id
  return enc.length();
  }

int write_itmp_resp(CBOR_encoder& enc, int tp, uint64_t id)
  {
  enc.write_arr( 3 ); // array with 3 elements
  enc.write_int( tp ); // first element message type
  enc.write_int( id );  // second element element message id
  return enc.length();
  }

int itmp_core::process_call(itmp_link* from, unsigned int addr, CBOR_decoder& dec, char* resp, int resplen) {
  uint64_t id;
  const char* uri;
  int urilen;
  char localuri[32];

  id=dec.read_int();
  if (dec.error()) {
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, 0, -dec.error(), "message id format error");
  }
  urilen=dec.read_str(&uri);
  if (dec.error()) {
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, -dec.error(), "CALL uri format error");
  }
/*  if ( dec.restmsg()>0 && dec.is_arr() ) { // if args is present in message
    int argsnum=dec.read_arr();
    if (dec.error()){
      return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, -dec.error(), "CALL parameters format error");
    }
  }*/
  for (int i=0; i < topics_num; i++) {
    if ( ( (urilen==topics[i].namelen && topics[i].type==ITMP_TOPIC_TYPE_EXACT) || (urilen>topics[i].namelen && topics[i].type==ITMP_TOPIC_TYPE_MASK)) && memcmp(uri,topics[i].name, topics[i].namelen)==0) {
      if ( topics[i].handler ) {
        CBOR_encoder enc(resp, resplen);
        int r;
        if (topics[i].type==ITMP_TOPIC_TYPE_MASK ){
          if (topics[i].type==ITMP_TOPIC_TYPE_MASK && urilen>=sizeof(localuri)){
            return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, -dec.error(), "CALL uri format error");
          }
          write_itmp_resp(enc, ITMP_TP_RESULT, id);
          memcpy(localuri,uri,urilen);
          localuri[urilen]=0;
          r = topics[i].handler(uri, dec, enc, topics[i].data);
        } else {
          write_itmp_resp(enc, ITMP_TP_RESULT, id);
          r = topics[i].handler(topics[i].name, dec, enc, topics[i].data);
        }
        if (r==0) {
          return write_itmp_emptyresp(resp, resplen, ITMP_TP_RESULT, id);
        } else if (r>=0) {
          return enc.length();
        } else {
          return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, -r, "");
        }
      } else {
        return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, 501, "not implemented");
      }
    }
  }
/*  if (gencall) {
    int hdr=write_itmp_resp(resp, resplen, ITMP_TP_RESULT, id);
    int r = gencall(uri, dec, resp+hdr, resplen-hdr);
    if (r==0) {
      return write_itmp_emptyresp(resp, resplen, ITMP_TP_RESULT, id);
    } else if (r>=0) {
      return hdr+r;
    } else {
      return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, -r, "");
    }
  }*/
  return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, 404, "no such uri CALL");
}

int itmp_core::process_subscribe(itmp_link* from, unsigned int addr, CBOR_decoder& dec, char* resp, int resplen) {
  uint64_t id;
  const char* uri;
  int urilen;

// IBM/+/Results
// #/Results
// IBM/Software/Results
// IBM/ *ware/Results
  id=dec.read_int();
  if (dec.error()) {
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, 0, -dec.error(), "SUBSCRIBE id format error");
  }
  urilen=dec.read_str(&uri);
  if (dec.error()) {
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, -dec.error(), "SUBSCRIBE uri format error");
  }
  if (from->subs_num >= MAX_SUBS_NUM) {
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, INSUFFICIENT_STORAGE_CODE, "SUBSCRIBE too much");
  } else {
    int err_w=testwildcard(uri,urilen);
    if ( err_w == 0 ) {
      memcpy(from->subs[from->subs_num].wildcard,uri,urilen);
      from->subs[from->subs_num].wildcard[urilen]=0;
      from->subs[from->subs_num].addr = addr;
      from->subs_num++;

/*      for (int i=0; i<registered_uri.size(); i++) {
        if (registered_uri[i].type==1 && registered_uri[i].onsubs_handler && test_uri_for_subscription(registered_uri[i].name, &from->subs[from->subs_num-1]) ) { // test last added subscription for all uri
          registered_uri[i].onsubs_handler(from);
        }
      }*/
      return write_itmp_emptyresp(resp, resplen, ITMP_TP_SUBSCRIBED, id);
    } else { // wrong wildcard
      return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, err_w, "wrong subscription uri for SUBSCRIBE");
    }
  }
  //return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, 404, "no such uri for SUBSCRIBE");
}

int itmp_core::process_unsubscribe(itmp_link* from, unsigned int addr, CBOR_decoder& dec, char* resp, int resplen) {
  uint64_t id;
  const char* uri;
  int urilen;

  id=dec.read_int();
  if (dec.error()) {
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, 0, -dec.error(), "SUBSCRIBE id format error");
  }
  urilen=dec.read_str(&uri);
  if (dec.error()) {
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, -dec.error(), "SUBSCRIBE uri format error");
  }

  for (int i=0;i<from->subs_num;i++) {
    if (from->subs[i].addr==addr && memcmp(from->subs[i].wildcard, uri, urilen)==0 && from->subs[i].wildcard[urilen]==0) {
      if ( i < from->subs_num-1 ) { //if it is not last entry
        from->subs[i] = from->subs[from->subs_num-1];
//        memcpy(&subs[i],&subs[subs_num-1], sizeof(subs[i]));
      }
      from->subs_num--;
      return write_itmp_emptyresp(resp, resplen, ITMP_TP_UNSUBSCRIBED, id);
    }
  }
  return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, 404, "no such uri for UNSUBSCRIBE");
}


/*
int itmp3::process_itmp3(const char* msg, int msglen, char* resp, int resplen) {
  CBOR_decoder dec(msg, msglen);


  int tp;
  uint64_t id;
  const char* uri;
  int urilen;
//  char* opts;
//  int optslen;
  int msgcnt=dec.read_arr();
  if (dec.error()) {
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, 0, -dec.error(), "message format error");
  }

  tp=dec.read_int();
  if (dec.error()) {
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, 0, -dec.error(), "message type format error");
  }
  if (tp==ITMP_TP_CALL) {
    id=dec.read_int();
    if (dec.error()) {
      return write_itmp_err(resp, resplen, ITMP_TP_ERROR, 0, -dec.error(), "message id format error");
    }
    urilen=dec.read_str(&uri);
    if (dec.error()) {
      return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, -dec.error(), "CALL uri format error");
    }
    if ( dec.restmsg()>0 ) { // if args is present in message
      int argsnum=dec.read_arr();
      if (dec.error()){
        return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, -dec.error(), "CALL parameters format error");
      }
    }
    int topics_num=__section_size("externs")/sizeof(topic_descr);
    topic_descr* topics =(topic_descr*)__section_begin("externs");
    for (int i=0; i<topics_num; i++) {
      if (urilen==topics[i].namelen && memcmp(uri,topics[i].name, urilen)==0) {
        if (topics[i].handler) {
          int hdr=write_itmp_resp(resp, resplen, ITMP_TP_RESULT, id);
          CBOR_encoder enc(resp+hdr,resplen-hdr);
          int r = topics[i].handler(uri, dec, enc);
          if (r < 0) {
            return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, -r, "");
          } else if (enc.length() == 0){
            return write_itmp_emptyresp(resp, resplen, ITMP_TP_RESULT, id);
          } else {
            return hdr+enc.length();
          }
        } else {
          return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, 501, "not implemented");
        }
      }
    }
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, 404, "no such uri CALL");
  } else if (tp==ITMP_TP_DESCRIBE) {
    id=dec.read_int();
    if (dec.error()) {
      return write_itmp_err(resp, resplen, ITMP_TP_ERROR, 0, -dec.error(), "DESCRIBE id format error");
    }
    urilen=dec.read_str(&uri);
    if (dec.error()) {
      return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, -dec.error(), "DESCRIBE uri format error");
    }
    if (urilen == 0) {
      return write_itmp_root_descr(resp, resplen, id);
    } else {
    int topics_num=__section_size("externs")/sizeof(topic_descr);
    topic_descr* topics =(topic_descr*)__section_begin("externs");
      for (int i=0;i<topics_num;i++) {
        if (urilen==topics[i].namelen && memcmp(uri,topics[i].name, urilen)==0) {
          return write_itmp_descr(resp, resplen, id, topics[i].descr);
        }
      }
    }
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, 404, "no such uri for DESCRIBE");
  } else if (tp>=5) {
    id=dec.read_int();
    if (dec.error()) {
      return write_itmp_err(resp, resplen, ITMP_TP_ERROR, 0, -dec.error(), "id format error");
    }
    return write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, 501, "not implemented");
    }
  return write_itmp_err(resp, resplen, ITMP_TP_ERROR, 0, 501, "not implemented");
}

*/

char resp[256];
#define resplen sizeof(resp)
int itmp_core::process_itmp_frame(itmp_link* from, unsigned int addr, const char* msg, int msglen) {// process_itmp_frame(itmp_link* from, const char* addr, int addrlen, const char* msg, int msglen, char* resp, int resplen) {
  CBOR_decoder dec(msg, msglen);

  int tp;
  uint64_t id;
//  const char* uri;
//  int urilen;
//  char* opts;
//  int optslen;
  int msgcnt=dec.read_arr(); // it return -TYPE_ERROR_CODE
  if (msgcnt != -TYPE_ERROR_CODE && dec.error()) { // but if bype is wrong it can by just ommitted
    return MESSAGE_ENCODE_ERROR;
  }

  tp=dec.read_int();
  if (dec.error()) {
    return MESSAGE_ENCODE_ERROR;
  }
  if (tp==ITMP_TP_CALL) { // most important method
    int ret=process_call(from, addr, dec, resp, resplen);
    if (ret>0) {
      from->send_frame(LOPAN_HEADER_TYPE_ITMP, addr, (byte*)resp, ret);
      return PACKET_ACCEPTED_ANSWER_SENT;
    }
    return PACKET_ACCEPTED;
  } else if (tp==ITMP_TP_DESCRIBE) { // answer description
    int ret=process_describe(from, addr, dec, resp, resplen);
    if (ret>0) {
      from->send_frame(LOPAN_HEADER_TYPE_ITMP, addr, (byte*)resp, ret);
      return PACKET_ACCEPTED_ANSWER_SENT;
    }
    return PACKET_ACCEPTED;
  } else if ( tp == ITMP_TP_EVENT ) {             // [EVENT, Request|id, Topic|uri, Arguments, Options|dict] 	event
    process_event(from, addr, dec, resp, resplen);
    return PACKET_ACCEPTED;
  } else if ( tp == ITMP_TP_CONNECTED ) {            // [PUBLISH, Request|id, Topic|uri, Arguments, Options|dict] 	event with acknowledge awaiting
    if (from->connected_state == 1) from->connected_state = 2;
    return PACKET_ACCEPTED;
  } else if ( tp == ITMP_TP_PUBLISH ) {            // [PUBLISH, Request|id, Topic|uri, Arguments, Options|dict] 	event with acknowledge awaiting
    int ret=process_publish(from, addr, dec, resp, resplen);
    if (ret>0) {
      from->send_frame(LOPAN_HEADER_TYPE_ITMP, addr, (byte*)resp, ret);
      return PACKET_ACCEPTED_ANSWER_SENT;
    }
    return PACKET_ACCEPTED;
//  } else if ( tp == ITMP_TP_SUBSCRIBE ) {
//    return process_subscribe(from, addr, dec, resp, resplen);
//  } else if ( tp == ITMP_TP_UNSUBSCRIBE ) {
//    return process_unsubscribe(from, addr, dec, resp, resplen);
//  } else if ( tp == ITMP_TP_DESCRIPTION || tp == ITMP_TP_RESULT || tp == ITMP_TP_ERROR || tp == ITMP_TP_PUBLISHED || tp == ITMP_TP_SUBSCRIBED || tp == ITMP_TP_UNSUBSCRIBED || tp == ITMP_TP_PUBLISHED ) {
//    return process_responce(from, addr, tp, dec, resp, resplen);
  } else if (tp>=5) {
    id=dec.read_int();
    if (dec.error()) {
      //write_itmp_err(resp, resplen, ITMP_TP_ERROR, 0, -dec.error(), "id format error");
      return MESSAGE_ENCODE_ERROR;
    }
    write_itmp_err(resp, resplen, ITMP_TP_ERROR, id, 501, "not implemented");
    return PACKET_ACCEPTED_ANSWER_SENT;
  }
  write_itmp_err(resp, resplen, ITMP_TP_ERROR, 0, 501, "not implemented");
  return PACKET_ACCEPTED_ANSWER_SENT;
}


int itmp_core::testwildcard(const char* uri, int urilen) {
  int state=0; // 0 - begin level, 1 - after regular symbol, 2 - after '+' symbol
  if (urilen<1)
    return -4; // too short wildcard
  for (int i=0; i<urilen; i++) {
    if (uri[i] == '#') {
      if (i!=urilen-1) {
        return -1; // '#' - should be last symbol
      }
      //state = 3;
      continue;
    }
    if (uri[i] == '+') {
      if (state!=0) {
        return -2; // '+' shuld be first and the only symbol at given level
      }
      state = 2;
      continue;
    }
    if (uri[i] == '.') {
      state=0;
    } else {
      if (state == 2) {
        return -3; // after '+' should be '.'
      }
      if (uri[i]<32) {
        return -5; // wrong wildcard symbol
      }
      state = 1;
    }
  }
  return 0;
}

bool itmp_core::test_uri_for_subscription(const char* uri, subscription* sub) {
  const char* s=sub->wildcard;
  const char* u=uri;
  while (true) {
    if (*s=='+') { s++; while (*u && *u!='.') u++; }
    else if (*s=='#') { return true; }

    if (*u!=*s) return false;
    if (*u==0) return true;
    u++;
    s++;
  }
  //return false;
}

int itmp_core::emit_event_1(const char* uri, const char* name, int val) {
  char sendbuf[64];
  CBOR_encoder enc(sendbuf, sizeof(sendbuf));

  enc.write_map( 1 );

  enc.write_rawstr( name );
  enc.write_int( val );

  return emit_event(uri, sendbuf, enc.length());
}

int itmp_core::emit_event_1(const char* uri, const char* name, const char* val) {
  char sendbuf[64];
  CBOR_encoder enc(sendbuf, sizeof(sendbuf));

  enc.write_map( 1 );

  enc.write_rawstr( name );
  enc.write_rawstr( val );

  return emit_event(uri, sendbuf, enc.length());
}

int itmp_core::is_someone_subscribed(const char* uri){
  int subsnum=0;
  for (int i=0;i<links_num; i++) {
    if ( links[i]->isConnected() ) {
      for (int j=0; j<links[i]->subs_num; j++) {
        if ( test_uri_for_subscription(uri, &links[i]->subs[j]) ) {
          subsnum++;
        }
      }
    }
  }
  return subsnum;
}

int itmp_core::emit_event(const char* uri, const char* body, int bodylen) {
  byte frame[32];
  CBOR_encoder enc((char*)frame,sizeof(frame));
  enc.write_arr(4);
  enc.write_int(ITMP_TP_EVENT);
  enc.write_int(itmp_id++);
  enc.write_rawstr(uri);
  enc.write_arr(1);
  enc.write_bstr(body, bodylen);
  for (int i=0;i<links_num; i++) {
    if ( links[i]->isConnected() ) {
      for (int j=0; j<links[i]->subs_num; j++) {
        if ( test_uri_for_subscription(uri, &links[i]->subs[j]) ) {
          //sendlen=write_itmp_event(event_buf, event_buf_len, uri, body, bodylen);
          links[i]->send_frame(LOPAN_HEADER_TYPE_ITMP, links[i]->coordinator_addr, frame, enc.length() );
        }
      }
    }
  }
  return 0;
}


/*
int itmp3::process_responce(itmpcon* from, const char* addr, int addrlen, int tp, CBOR_decoder& dec, char* resp, int resplen) {
  int id=dec.read_int();
  if (dec.error()) {
    return 0;//write_itmp_err(resp, resplen, ITMP_TP_ERROR, 0, -dec.error(), "SUBSCRIBE id format error");
  }

  transactions_mutex.k_lockMutex();
  for (int i=0; i<MAX_SIMULTANEOUS_TRANSACTION; i++) {
    if (transactions[i].transaction_id == id && !transactions[i].avialable) {
      transactions[i].transaction_mutex.k_lockMutex();
      transactions_mutex.k_releaseMutex();

      if ( tp == ITMP_TP_ERROR ) {
        transactions[i].result_code=dec.read_int();
        if (dec.error()) {
          transactions[i].result_code = dec.error();
        } else {
          if (transactions[i].result_code>=0) transactions[i].result_code=ERR_INTEGRITY_ERROR;
          if (transactions[i].result_buf) {
            const char* err;
            int errlen;
            errlen=dec.read_str(&err);
            if ( !dec.error() ) {
              if (errlen > transactions[i].result_buf_len-1) errlen = transactions[i].result_buf_len-1;
              memcpy(transactions[i].result_buf, err, errlen );
              transactions[i].result_buf[errlen] = 0; // terminate error message
            }
          }
        }
      } else { //if ( tp == ITMP_TP_DESCRIPTION || tp == ITMP_TP_RESULT ) {
        if ( transactions[i].result_buf_len < dec.restmsg() ) {
          transactions[i].result_code=ERR_TOO_LONG;
        } else {
          if (transactions[i].result_buf) memcpy(transactions[i].result_buf, dec.position(), dec.restmsg() );
          transactions[i].result_code = dec.restmsg();
        }
      }
      transactions[i].transaction_event.k_setEvent(); // flag completed event
      transactions[i].transaction_mutex.k_releaseMutex();
      return 0;
    }
  }
  transactions_mutex.k_releaseMutex();
  return 0;
}

int itmp3::transaction(itmpcon* con, int tp, const char* url, char const* body, int bodylen, char* retbuf, int retbuflen, lp::msec_t iotimeout)
  {
  const char* addr=url;
  int addrlen=0;
  // extract internal connection address
  if (con->addrlen>0) {
    int cnt=con->addrlen;
    while (cnt>0) {
      while (*url && *url != '.') url++;
      if (*url == '.') url++;
      cnt++;
    }
    if (addr!=url && *(url-1)=='.') {
      addrlen=url-addr-1;
    } else {
      addrlen=url-addr;
    }
  }

  char header[128+1];

  con->bus_mutex.k_lockMutex(); // lock bus (only one transaction in time)

  unsigned long cpsr=k_disable_interrupt();
  itmp_sequence_nb++;
  int seq=itmp_sequence_nb;
  k_restore_interrupt(cpsr);


  itmp_transaction* tr=0;
  transactions_mutex.k_lockMutex();
  for (int i=0;i<MAX_SIMULTANEOUS_TRANSACTION; i++) {
    if (transactions[i].avialable) {
      transactions[i].avialable=false;
      tr=&transactions[i];
      tr->transaction_id = seq; // fill right information about transaction
      tr->transaction_event.k_resetEvent(); // reset event of transaction finish

      tr->result_buf = retbuf;
      tr->result_buf_len = retbuflen;
      tr->result_code = ERR_TIME_OUT;
      break;
    }
  }
  transactions_mutex.k_releaseMutex(); // release transactions list

  CBOR_encoder enc(header, sizeof(header));
  enc.write_arr( body && bodylen ? 4 : 3 ); // array with 4 or 3 elements
  enc.write_int( tp );    // first element message type
  enc.write_int( seq );   // second element element message id
  enc.write_rawstr( url );   // third element topic name
  if (enc.freelen() < bodylen) {
    return ERR_NO_MEMORY;
  } else if (body && bodylen) {
    memmove(enc.position(), body, bodylen); // fourth element topic body
  }
  int sendlen = enc.length()+bodylen;

  con->send(addr, addrlen, event_buf, sendlen, con->data);

  kIntTimer tm;
  tm.reset();
  int ret=ERR_TIME_OUT;

  while (tm.getCurrent() < iotimeout)
    {
    int timeout = iotimeout - tm.getCurrent();
//    process(timeout > 0 ? timeout : 0);
    if (tr->transaction_event.k_waitfor(0)>=0) break;
    }

  tr->transaction_mutex.k_lockMutex(); // lock transaction data (only one thread can read and write transaction data)

  if (tr->result_code>=0) // if we have event
    {
    ret=tr->result_code;
    }

  transactions_mutex.k_lockMutex();
  tr->avialable=true;  // free for use
  tr->transaction_mutex.k_releaseMutex(); // unlock nransaction
  transactions_mutex.k_releaseMutex(); // release transactions list

  con->bus_mutex.k_releaseMutex();
  return ret;
  }

int itmp3::transaction(int tp, const char* url, char const* body, int bodylen, char* retbuf, int retbuflen, lp::msec_t iotimeout){
  for (int i = 0; i < cons.size(); i++) {
    const char* cname=cons[i]->conname;
    const char* uname=url;
    while (*cname) {
      if (*cname != *uname) {
        goto nextcon;
      }
      cname++;
      uname++;
    }
    if (*uname!='.' && *uname!=0) {
      goto nextcon;
    }
    return transaction(cons[i], tp, uname+1, body, bodylen, retbuf, retbuflen, iotimeout);

  nextcon: ;
  }
return ERR_FILE_NOT_FOUND;
}
*/
/*
int itmp3::call(const char* url, void const *body, int bodylen, char* retbuf, int retbuflen, lp::msec_t iotimeout){
  return transaction(ITMP_TP_CALL, url, (const char*)body, bodylen, retbuf, retbuflen, iotimeout);
}

int itmp3::getdesc(const char* url, void const *body, int bodylen, char* retbuf, int retbuflen, lp::msec_t iotimeout){
  return transaction(ITMP_TP_DESCRIBE, url, (const char*)body, bodylen, retbuf, retbuflen, iotimeout);
}
*/

//process all itmp packets
/*
int itmp_core::process_frame(itmp_link* from, uint32_t type, byte* buf, int buflen)
  {
  byte outbuf[128];


  int urilen=inbuf->_body[index];
  if ( urilen>=0x80 ) return STOP_PROCESSING; // do not process options

	tp &= (ITMP_HEADER_METHOD_MASK | ITMP_HEADER_T_MASK);
  if (tp==(ITMP_HEADER_METHOD_GET | ITMP_HEADER_CON)) // get method
    {
    const topic_descr* topic = gettopic(inbuf->_body+index);
    if (topic)
      {
      int ret=-1;//topic->handler((char*)inbuf->_body+index,inbuf->_body+index+1+urilen, inbuf->body_len-index-urilen-1, outbuf);

      if (ret>=0)
        {
        from->write_command(ITMP_HEADER_METHOD_GET | ITMP_HEADER_ACK | tpidlen, inbuf->SA, from->my_addr, inbuf->_body+messtart, index-messtart+1+urilen, outbuf, ret);
        return STOP_PROCESSING;//outbuflen;
        }
      else
        {
        int outlen=2;
        outbuf[0]=0x51;
        outbuf[1]=-ret;
        if (-ret>255) { outbuf[2]=(-ret)>>8; outlen=3; }
        outbuf[0]=0x51;
        from->write_command(ITMP_HEADER_METHOD_GET | ITMP_HEADER_ERR | tpidlen, inbuf->SA, from->my_addr, inbuf->_body+messtart, index-messtart+1+urilen, outbuf, outlen);
        return STOP_PROCESSING;//outbuflen;
        }
      }
    else
      {
      outbuf[0]=0x44; // Err Topic Not found
      from->write_command(ITMP_HEADER_METHOD_GET | ITMP_HEADER_ERR | tpidlen, inbuf->SA, from->my_addr, inbuf->_body+messtart, index-messtart+1+urilen, outbuf, 1);
      return STOP_PROCESSING;//outbuflen;
      }
    }
  else if (tp==(ITMP_HEADER_METHOD_DESCR | ITMP_HEADER_CON)) // get description method
    {
      const topic_descr* topic = gettopic(inbuf->_body+index);
      if (topic)
        {
        //from->write_command(ITMP_HEADER_METHOD_DESCR | ITMP_HEADER_ACK | tpidlen, inbuf->SA, from->my_addr, inbuf->_body+messtart, index-messtart+1+urilen,topic->descr,topic->descr[0]+1);
        return STOP_PROCESSING;
        }
      else
        {
        outbuf[0]=0x44; // Err Topic Not found
        from->write_command(ITMP_HEADER_METHOD_DESCR | ITMP_HEADER_ERR | tpidlen, inbuf->SA, from->my_addr, inbuf->_body+messtart, index-messtart+1+urilen, outbuf,1);
        return STOP_PROCESSING;
        }
    }
  return STOP_PROCESSING;
  }
*/




/*  int index=0;
  index+=CBOR_write_arr(buf + index, 3); // array with 3 elements
  index+=CBOR_write_int(buf + index, ITMP_TP_DESCRIPTION); // first element message type
  index+=CBOR_write_int(buf + index, id);  // second element element message id
  index+=CBOR_write_arr(buf + index, 1); // third element array with 1 element
  index+=CBOR_write_str(buf + index, buflen-index, str);  // the only element is message id
  return index;
}*/




int write_itmp_err(char* buf, int buflen, int tp, int id, int code, const char* str) {
  CBOR_encoder enc(buf,buflen);
  enc.write_arr( 4 ); // array with 4 elements
  enc.write_int( tp ); // first element message type
  enc.write_int( id );  // second element element message id
  enc.write_int( code );  // third element error code
  enc.write_rawstr( str ); // fourth element topic body
  return enc.length();
}
#define ERR_NO_MEMORY -1
/*
int itmp3::write_itmp_event(char* buf, int buflen, const char* topic, const char* body, int bodylen) {
  itmp_sequence_nb++;
  CBOR_encoder enc(buf,buflen);
  enc.write_arr( body && bodylen ? 4 : 3 ); // array with 4 or 3 elements
  enc.write_int( ITMP_TP_EVENT ); // first element message type
  enc.write_int( itmp_sequence_nb );  // second element element message id
  enc.write_rawstr( topic ); // third element topic name
  if (enc.freelen() < bodylen) {
    return ERR_NO_MEMORY;
  } else if (body && bodylen) {
    memmove(enc.position(), body, bodylen); // fourth element topic body
  }
  return enc.length()+bodylen;
}
*/

/*
int itmp3::adduri(const char* uri, const char* desc, itmp3handler handler) {
  iface i;
  i.name=uri;
  i.namelen=strlen(uri);
  i.desc=desc;
  i.handler=handler;
  ifaces.push_back(i);
  return ifaces.size();
}
*/

/*
int itmp3::add_conection(itmpsendevent func, void* data) {
  itmpcon c;
  c.send=func;
  c.data=data;
  cons.push_back(c);
  return cons.size();
}

int itmp3::del_conection(itmpsendevent func, void* data) {
  for (int i=0;i<cons.size();i++) {
    if (cons[i].send == func && cons[i].data == data ) {
      cons.erase(&cons[i]);
      return cons.size();
    }
  }
  return -1;
}

int itmp3::emit_event(const char* uri, const char* body, int bodylen) {
  if (event_buf_len<bodylen+32) {
    if (event_buf) delete[] event_buf;
    event_buf = new char[bodylen+32];
    if (event_buf) event_buf_len=bodylen+32;
    else event_buf_len=0;
  }
  if (event_buf == 0 ) {
    return ERR_NO_MEMORY;
  }

  int sendlen=write_itmp_event(event_buf, event_buf_len, uri, body, bodylen);
  for (int i=0;i<cons.size();i++) {
    cons[i].send(event_buf,sendlen, cons[i].data);
  }
  return cons.size();
}

*/
