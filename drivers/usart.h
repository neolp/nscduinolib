
#ifndef _USART_H_
#define _USART_H_

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "blockfifo.h"
#include "gpio-alloc.h"

class usart
  {
  USART_TypeDef * USART;
  public:
  gpio_pin* en;
  usart(USART_TypeDef * _USART)
    {
    USART=_USART;
    en=0;
    }
  block_fifo<128> usart_tx_fifo;
  block_fifo<16> usart_rx_fifo;

//#define USART_COUNTERS
#ifdef USART_COUNTERS
struct usart_state volatile usart_state =
{
	.rx_pack_common_pdu_err_cnt = 0,
	.rx_pack_frame_err_cnt = 0,
	.rx_pack_crc_err_cnt = 0,
	.rx_err_cnt = 0,
	.rx_ovr_cnt = 0,
	.rx_noise_cnt = 0,
};
#endif

  /* 115200, parity none, 1 stop bit */
  void usart_init(int baud, bool remap=false);


  void write(const char *data, size_t ln);
  void write(char d);
  void writeHEX(int data, int ln=8);
  void writeDEC(int data, int ln=1);

  int available(void)
    {
    return usart_rx_fifo.datalen();
    }
  int read(uint8_t *data, size_t max_ln)
    {
    return usart_rx_fifo.read(data,max_ln);
    }
  int read()
    {
    while (usart_rx_fifo.datalen()==0)
      ;
    return usart_rx_fifo.read();
    }

  void IRQHandler(void);
  };

class pollusart // interruptless usart
  {
  USART_TypeDef * USART;
  public:
  gpio_pin* en;
  pollusart(USART_TypeDef * _USART)
    {
    USART=_USART;
    en=0;
    }

  /* 115200, parity none, 1 stop bit */
  void usart_init(int baud);


  void write(const char *data, size_t ln);
  void write(char d);

  int available(void)
    {
    return USART->SR & USART_SR_RXNE;
    }

  int read()
    {
    while (!(USART->SR & USART_SR_RXNE))
      ;
    return USART->DR;
    }
  };


#endif /* _USART_H_ */