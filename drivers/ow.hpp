
/*
//Usage example:
// timer7 for 1 us ticks
void ow_init(void)
  {
	bool ext_osc = (RCC->CFGR & RCC_CFGR_SW) == RCC_CFGR_SW_HSE;
	int div = sys_get_osc() / 1000;
	
	TIM7->CR1 = TIM_CR1_CEN; // TIM_CR1_ARPE | TIM_CR1_CEN;
	TIM7->PSC = (sys_get_osc() / 1000000) -1;
	TIM7->ARR = div;
	TIM7->EGR |= TIM_EGR_UG;
	TIM7->DIER |= TIM_DIER_UIE;
	NVIC_EnableIRQ(TIM7_IRQn);
  }
gpio_pin ow_pin(PORTA | PIN2, GPIO_IN_FLOATING);
ow owport(&ow_pin,&TIM7->ARR);
extern "C" { void TIM7_IRQHandler(void); }
void TIM7_IRQHandler(void){
  owport.timer_irq();
  //		GPIOC->ODR ^= GPIOC_LEDB;
  TIM7->SR &= ~TIM_SR_UIF;
  NVIC_ClearPendingIRQ(TIM7_IRQn);
}
*/

#ifndef _OW_HPP_
#define _OW_HPP_

#include "tim.h"
#include "stm32f10x.h"
#include "stm32f10x_extend.h"
#include "sys.h"

enum owstate
  {
  reset,
  reset_release,
  reset_sample,
  
  write_bit,
  write_zero_release,
  write_one_release,

  read_bit,
  read_release,
  read_sample,

  search_bit,
  search_release,
  search_sample,
  search_write,
  search_write_one_release, 
  search_write_zero_release,
  
  done,
  pause, // wait before next reset impulse
  };
#define READ_ROM 0x33
#define SEARCH_ROM 0xF0
#define MATCH_ROM 0x55
#define SKIP_ROM 0xCC
#define ALARM_SEARCH 0xEC

#define CONVERT_T 0x44 // ds1822 specific command
#define WRITE_SCRATCHPAD 0x4E
#define READ_SCRATCHPAD 0xBE
#define COPY_SCRATCHPAD 0x48
#define RECALL_E2 0xB8
#define READ_POWER_SUPPLY 0xB4

class ow {
  volatile uint16_t* m_TIM_ARR;
  gpio_pin * m_ow_pin;

  owstate ow_next_state;
  int bit;
  int knownbit;
  int lastselected0;
  int readbit;
  unsigned char writebuf[10]; int writebuf_len;
  unsigned char readbuf[9]; int readbuf_len;

  inline void next_step(owstate st, uint16_t tm) { ow_next_state = (st); *m_TIM_ARR = (tm)-1; }
  inline void driver_on() { m_ow_pin->set(); }
  inline void driver_off() { m_ow_pin->reset(); }
  inline bool line_state() { return m_ow_pin->read(); }
public:
  ow(gpio_pin * ow_pin, volatile uint16_t* TIM_ARR): m_ow_pin(ow_pin),m_TIM_ARR(TIM_ARR),ow_next_state(done) {}
  owstate state() { return ow_next_state; }





/*
bool OWFirst(OWsearchstate* searchstate) {
  for (int byte=0;byte<8;byte++) {
    searchstate->rom[byte]=0;
  }
  searchstate->knownbit=-2;
  return OWNext(searchstate);
  }

bool OWNext(OWsearchstate* searchstate) {
  if (searchstate->knownbit==-1) return false;
  
  command = SEARCH_ROM;
  bit = 0;
  driver_on(); next_step(reset_release,480); // enable interrupt

  unsigned char r,w,crc8 = 0;
  signed char bit,knownbit,lastselected0;


  knownbit=searchstate->knownbit;
  if ( !OWreset() ) return false;
  OWreadwritebyte(0xF0); // search rom command
	
	lastselected0=-1;
	//DEBUGSTR(2,"search ");
	for (bit=0;bit<8*8;bit++)
		{
		if ((bit&0x07)==0) w=searchstate->rom[bit>>3];
		r=0;
		if (OWreadwritebit(true)) r|=0x80;
		if (OWreadwritebit(true)) r|=0x40;
		
		if (r==0x00) //conflict
			{
			if (bit == knownbit)  { r=0x80; } // select 1
			else if ( bit > knownbit ) { lastselected0=bit; r=0x00;} // select 0
			else if ( bit < knownbit ) { r=w&0x01?0x80:0x00; if (r==0) lastselected0=bit; } // select the same
			}
		else if (r==0xC0) //error no devices
			{ //DEBUGSTR(2,"no bit "); DEBUGHEX(2,bit); DEBUGSTR(2," exit \n\r");return false;
			}
		else // normal code
			{ r&=0x80; }
		w>>=1; w|=r;
		OWreadwritebit(r);
		if ((bit&0x07)==0x07)
			{
			//DEBUGHEX(2,w); DEBUGSTR(2," ");
			searchstate->rom[bit>>3]=w;
			crc8=docrc8(crc8,w); // accumulate the CRC
			}

		}
	//DEBUGSTR(2,"\n\r last bit "); DEBUGHEX(2,lastselected0); DEBUGSTR(2," crc "); DEBUGHEX(2,crc8); DEBUGSTR(2,"\n\r");
	searchstate->knownbit=lastselected0;
	return crc8==0;
	}


void search(){
  

}
*/
// reset, match rom, rom, convert T, wait for time or for release line    reset,match rom, rom, read scrach, read 8 bytes, read crc

  void start(){
    driver_on(); next_step(reset_release,480);
  }
  void OWFirst() {
    writebuf[0] = SEARCH_ROM;
    writebuf_len = 1;
    for (int byte=0; byte<8; byte++) {
      readbuf[byte]=0;
    }
    readbuf_len = 8;
    
    knownbit=-2;
    
    start();
    }

  void OWNext() {
    if (knownbit==-1) return ;
    start();
  }

  void StartConversion(unsigned char* addr) {
    writebuf[0] = MATCH_ROM;
    for (int byte=0; byte<8; byte++) {
      writebuf[byte+1]=*addr++;
    }
    writebuf[9] = CONVERT_T;
    writebuf_len = 10;
    for (int byte=0; byte<9; byte++) {
      readbuf[byte]=0;
    }
    readbuf_len = 9;
    
    knownbit=-2;
    
    start();
    }

  void readConversion(unsigned char* addr) {
    writebuf[0] = MATCH_ROM;
    for (int byte=0; byte<8; byte++) {
      writebuf[byte+1]=*addr++;
    }
    writebuf[9] = READ_SCRATCHPAD;
    writebuf_len = 10;
    for (int byte=0; byte<9; byte++) {
      readbuf[byte]=0;
    }
    readbuf_len = 9;
    
    knownbit=-2;
    
    start();
    }


  // reset, match rom, rom, convert T, wait for time or for release line    reset,match rom, rom, read scrach, read 8 bytes, read crc

  void timer_irq(void)
    {
    switch (ow_next_state)
      {
      //case reset: driver_on(); next_step(reset_release,480); break;
      case reset_release: 
        driver_off();
        next_step(reset_sample,72);
        break;
      case reset_sample: 
        if (line_state()) { next_step(write_bit,300); bit=0; /*byte=READ_ROM;*/}
        else { next_step(reset,32000); bit = -1; }
        break;
      case write_bit:
        driver_on(); // pull down the line  
        if ( bit >= writebuf_len ) { // if we write all bits
            bit=0; 
            if ( writebuf[0] == SEARCH_ROM ) {
              next_step(search_bit,9);
            } else {
              next_step(read_sample,9); 
            }
        } else {
          if ((writebuf[bit>>3]>>(bit&0x07))&0x01) { next_step(write_one_release,9); }
          else { next_step(write_zero_release,60); }
          bit++;
        }
        break;
      case write_one_release: 
        driver_off();
        next_step(write_bit,71);
        break;
      case write_zero_release:
        driver_off();
        next_step(write_bit,20);
        break;
      case read_bit:
        if (bit >= readbuf_len)
          { bit=0; next_step( pause,32000); }
        else
          { driver_on(); next_step(read_release,9); }
        break;
      case read_release:
        driver_off(); next_step(read_sample,9); break;
      case read_sample:
        if (line_state())
          readbuf[bit>>3]|=1<<(bit&0x07);
        bit++;
        next_step(read_bit,64); 
        break;


      case search_bit:
        if (bit >= 2*8*8)
          { bit=0; next_step( pause,32000); }
        else
          { driver_on(); next_step(search_release,9); }
        break;
      case search_release:
        driver_off(); next_step(search_sample,9); break;
      case search_sample:
        if ( (bit & 0x01) == 0) { // first bit of pair
          readbit = line_state() ? 1 : 0;
          next_step( search_bit, 64 );
        } else { // second bit of pair
          if (line_state()) readbit |= 0x02;
          if ( readbit == 0x00 ) { // conflict
            if (bit == knownbit)  { readbit = 0x01; } // select 1
            else if ( bit > knownbit ) { lastselected0=bit; readbit=0x00;} // select 0
            else if ( bit < knownbit ) { readbit = readbuf[bit>>4]>>((bit>>1)&0x07)&0x01 ? 0x01 : 0x00; if (readbit==0) lastselected0=bit; } // select the same
          } else if ( readbit == 0x03 ) { // error no devices
          } else { // normal code - one device
            readbit &= 0x01;
          }
          readbuf[bit>>4] |= readbit << ((bit>>1)&0x07);
          next_step( search_write, 64 );
        }
        bit++;
        break;
      case search_write:
        driver_on(); // pull down the line  
        if ( bit >= readbuf_len ) { // if we write all bits
            bit=0; next_step(done,9); 
        } else {
          if ((readbuf[bit>>4]>>((bit>>1)&0x07))&0x01) { next_step(search_write_one_release,9); }
          else { next_step(search_write_zero_release,60); }
          bit++;
        }
        break;
      case search_write_one_release: 
        driver_off();
        next_step(search_bit,71);
        break;
      case search_write_zero_release:
        driver_off();
        next_step(search_bit,20);
        break;


      case pause:
      default:
        next_step(pause,32000); 
        break;
      }
    }

  void ow_readstart()
    {
    next_step(reset,2);
    }

  bool ow_ready()
    {
    return ow_next_state==pause;
    }
};
#endif
