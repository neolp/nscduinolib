
#include <gpio-alloc.h>
#include <I2CMaster.h>
#include <accel_adxl345.h>

int Accelerometer_ADXL345_I2C::WriteReg(uint8_t reg,uint8_t value)
{
  return I2C_master->Write(I2C_addr, reg, &value, sizeof(value));  
}

int Accelerometer_ADXL345_I2C::ReadReg(uint8_t reg, uint8_t &value)
{
  return  I2C_master->Read(I2C_addr, reg, &value, sizeof(value)); 
}

int Accelerometer_ADXL345_I2C::init(void)
{

  int ret = 0;
  ret = WriteReg(ADXL345_POWER_CTL_REG,0x00);//reinitialize stendby-mode
  if (ret<0) return ret;
//  ret = WriteReg(ADXL345_POWER_CTL_REG,0x10);//set sleep-mode
//  if (ret<0) return ret;
  ret = WriteReg(ADXL345_POWER_CTL_REG,0x08);//start measure 
  if (ret<0) return ret;
  ret = WriteReg(ADXL345_DATA_FORMAT_REG,0x0B);//set data format 
  if (ret<0) return ret;
  ret = WriteReg(ADXL345_INT_MAP_REG,0x00);//map interrupts on INT1_pin
  if (ret<0) return ret;
  ret = WriteReg(ADXL345_INT_ENABLE_REG,0x80);//enable "Data_ready" interrupt
  if (ret<0) return ret;
  return GetDevID();
}

uint8_t Accelerometer_ADXL345_I2C::GetIntSrcREG(void)
{
  uint8_t ISreg = 0;
  int ret = ReadReg(/*ADXL345_INT_SOURCE_REG*/ADXL345_INT_ENABLE_REG, ISreg);
  return ISreg;
}


uint8_t Accelerometer_ADXL345_I2C::GetDevID(void)
{
  uint8_t DevID = 0;
  int ret = I2C_master->Read(I2C_addr, ADXL345_DEVID_REG, &DevID, 1);
  return DevID;
}

inline int CombineAccData(int8_t hi,uint8_t low)
{
  int ret = hi;
  ret <<= 8;
  ret |= low;
  return ret;
}
bool Accelerometer_ADXL345_I2C::GetValues(Accelerations_t &data)
{
  static const int bytes_count = 6;
  uint8_t received_bytes[bytes_count];
  int ret = I2C_master->Read(I2C_addr, ADXL345_DATAX0_REG, received_bytes, bytes_count);
  if (ret<0) return false;
  data.x = CombineAccData(received_bytes[1], received_bytes[0]);
  data.y = CombineAccData(received_bytes[3], received_bytes[2]);
  data.z = CombineAccData(received_bytes[5], received_bytes[4]);
  return true;
}