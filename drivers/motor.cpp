#include "stm32f10x.h"

#include "gpio-alloc.h"
#include "motor.h"

int motor_driver::begin()
  {
	bool ext_osc = (RCC->CFGR & RCC_CFGR_SW) == RCC_CFGR_SW_HSE;
	uint16_t div = 24000;//sys_get_osc() / 1000;

	TIM1->CR1 = TIM_CR1_ARPE | TIM_CR1_CEN;
//	TIM1->CR2 = 0;

	TIM1->CCMR1 = TIM_CCMR1_OC1PE | /*TIM_CCMR1_OC1M_0 | */ TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_2 ;
	TIM1->CCMR2 = TIM_CCMR2_OC4PE | /*TIM_CCMR2_OC4M_0 | */ TIM_CCMR2_OC4M_1 | TIM_CCMR2_OC4M_2 ;
	TIM1->ARR = 0x500;
	TIM1->CCER = TIM_CCER_CC1E | TIM_CCER_CC4E ;
	TIM1->BDTR = TIM_BDTR_AOE | TIM_BDTR_MOE;

	TIM1->PSC = 10;
//	TIM1->ARR = div;
//	TIM1->EGR |= TIM_EGR_UG;
//	TIM1->DIER = 0;
//	NVIC_EnableIRQ(TIM1_IRQn);

  EN1.acquire(PORTB | PIN13,GPIO_OUT_PUSHPULL_50MHz); // EN 1
  TIM1_CH1.acquire(PORTA | PIN8,GPIO_OUT_ALT_PUSHPULL_50MHz);// TIM1_CH1
  IN2.acquire(PORTB | PIN12,GPIO_OUT_PUSHPULL_50MHz); // IN 2
  IN4.acquire(PORTB | PIN14,GPIO_OUT_PUSHPULL_50MHz); // IN 4
  TIM1_CH4.acquire(PORTA | PIN11,GPIO_OUT_ALT_PUSHPULL_50MHz);// TIM1_CH4
  EN2.acquire(PORTB | PIN15,GPIO_OUT_PUSHPULL_50MHz); // EN 2
  return 0;
  }

int motor_driver::end()
  {
  return -1;
  }
void motor_driver::set(int alpha,int beta)
  {
  if (alpha==0)
    EN1.reset();
  else if (alpha>0)
    {
    EN1.set();
    IN2.set();
    TIM1->CCR1 = TIM1->ARR-alpha;
    }
  else
    {
    EN1.set();
    IN2.reset();
    TIM1->CCR1 = -alpha;
    }

  if (beta==0)
    EN2.reset();
  else if (beta>0)
    {
    EN2.set();
    IN4.set();
    TIM1->CCR4 = TIM1->ARR-beta;
    }
  else
    {
    EN2.set();
    IN4.reset();
    TIM1->CCR4 = -beta;
    }
  }
