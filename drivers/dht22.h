
//Humidity and temperature mesurement with DHT22  (AOSONG AM2302 unit)
//Usage example:
/*
extern "C" { void EXTI2_IRQHandler(void); }
gpio_pin am2302_pin(PORTA | PIN2,GPIO_IN_FLOATING);
dht22_sensor am2302(&am2302_pin);

void EXTI2_IRQHandler(void) {
  unsigned int pending=EXTI->PR;
  EXTI->PR = pending;
  NVIC_ClearPendingIRQ(EXTI2_IRQn);
  am2302.IRQ();
};
*/
// 

#ifndef _DHT22_H_
#define _DHT22_H_

#include "tim.h"
#include "gpio-alloc.h"

#define MIN_UPDATE_PERIOD_US 2000000
#define OUTDATE_TIME_US 10000000
#define START_SIGNAL_LENGTH_US 1000
#define LOGIC_LEVEL_US 50
#define DEBUG

class dht22_sensor
  {
  unsigned int last_updated_us, pulse_start_us, last_succesfull_reading_us;
  unsigned short int temperature_value, humidity_value;
  unsigned char temperature_h, temperature_l, humidity_h, humidity_l, parity, bit_number;
  gpio_pin* dht22_pin;
  bool dht22_pinstate;
#ifdef DEBUG
  unsigned int pulse_length_max_us, pulse_length_min_us;    
#endif /* DEBUG */      

public:
  unsigned int errs_counter;
  
  dht22_sensor(gpio_pin * _dht22_pin): dht22_pin(_dht22_pin), temperature_value(0x7FFF), 
      humidity_value(0x7FFF), errs_counter(0)
    {
      last_updated_us = get_us();
#ifdef DEBUG
      pulse_length_max_us = LOGIC_LEVEL_US;
      pulse_length_min_us = LOGIC_LEVEL_US;    
#endif /* DEBUG */     
    }

  void IRQ(void){
    unsigned short int bit_value;
    dht22_pinstate = !dht22_pinstate;
    if (dht22_pinstate) {
      pulse_start_us = get_us();
      bit_number --;
    }else{
      unsigned int pulse_length = get_us()-pulse_start_us;
      bit_value = (pulse_length < LOGIC_LEVEL_US) ? (0) : (1);
#ifdef DEBUG
      if (bit_number<40)
      {
        if (pulse_length > pulse_length_max_us) {
          pulse_length_max_us = pulse_length;
        }else if(pulse_length < pulse_length_min_us) {
          pulse_length_min_us = pulse_length;
        }
      }
      
#endif /* DEBUG */      
      if (bit_number<8)
      {//parity
        parity = (parity << 1) + bit_value;
        if (bit_number==0)
        {//parity check
          unsigned char calculated_parity = humidity_h + humidity_l + temperature_h + temperature_l; 
          if (calculated_parity == parity){
            temperature_value = temperature_h * 0x100 + temperature_l;
            humidity_value = humidity_h * 0x100 + humidity_l;
            last_succesfull_reading_us = get_us();
          } else {errs_counter++;}
        }
      }else if (bit_number<16)
      {//temperature_low
        temperature_l = (temperature_l << 1) + bit_value;
      }else if (bit_number<24)
      {//temperature_high
        temperature_h = (temperature_h << 1) + bit_value;
      }else if (bit_number<32)
      {//humiditlast_updated_usy_low
        humidity_l = (humidity_l << 1) + bit_value;
      }else //if (bit_number<40)
      {//humidity_high
        humidity_h = (humidity_h << 1) + bit_value;
      }
    }
  }
 
  int update(void) {
    if ((get_us() - last_updated_us) <= MIN_UPDATE_PERIOD_US) {
      return -1; //early to start, need 2s timeout betwen measurement
    }
    dht22_pin -> cfg(GPIO_OUT_OPENDRAIN_10MHz);
    dht22_pin -> reset();
    pulse_start_us = get_us();
    while ((get_us()-pulse_start_us) < START_SIGNAL_LENGTH_US);//wait 1ms 
    dht22_pin -> cfg(GPIO_IN_FLOATING);
    bit_number = 42;
    dht22_pinstate = true;
    last_updated_us = get_us();
    return 0;
  }
  
  int get_temperature(void) {
    if ((get_us() - last_succesfull_reading_us) >= OUTDATE_TIME_US) {    
      return 0x7FFF;
    }
    if (temperature_value&0x8000) return (-(temperature_value&0x7FFF));
    return temperature_value;
  }
  
  int get_humidity(void) {
    if ((get_us() - last_succesfull_reading_us) >= OUTDATE_TIME_US) {    
      return 0x7FFF;
    }
    return humidity_value;
  }
  
};

#endif /* _DHT22_H_ */
