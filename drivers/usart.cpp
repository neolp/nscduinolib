#include "stm32f10x.h"

#include "usart.h"
#include "sys.h"

inline uint32_t usart_get_baud_rate(unsigned int f_osc, unsigned int baud_rate)
    {
    return (f_osc + (baud_rate>>1)) / baud_rate;
    }

void usart::usart_init(int baud, bool remap)
    {
    if (USART==USART1)
      {
        if (remap) {
      gpio_pin::configure(GPIOB,6,GPIO_OUT_ALT_PUSHPULL_2MHz);// USART1 TX remap
      gpio_pin::configure(GPIOB,7,GPIO_IN_FLOATING);// USART1 RX remap
      AFIO->MAPR = (AFIO->MAPR & 0x1FFFF) | AFIO_MAPR_USART1_REMAP | AFIO_MAPR_SWJ_CFG_JTAGDISABLE;
        } else {
      gpio_pin::configure(GPIOA,9,GPIO_OUT_ALT_PUSHPULL_2MHz);// USART1 TX
      gpio_pin::configure(GPIOA,10,GPIO_IN_FLOATING);// USART1 RX
      AFIO->MAPR = ((AFIO->MAPR & 0x1FFFF) & ~AFIO_MAPR_USART1_REMAP) | AFIO_MAPR_SWJ_CFG_JTAGDISABLE;
        }
      RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
      }
    else if (USART==USART2)
      {
      RCC->APB1ENR |= RCC_APB1ENR_USART2EN;
        if (remap) {
      gpio_pin::configure(GPIOD,5,GPIO_OUT_ALT_PUSHPULL_2MHz);// USART1 TX remap
      gpio_pin::configure(GPIOD,6,GPIO_IN_FLOATING);// USART1 RX remap
      AFIO->MAPR = (AFIO->MAPR & 0x1FFFF) | AFIO_MAPR_USART2_REMAP | AFIO_MAPR_SWJ_CFG_JTAGDISABLE;
        } else {
      gpio_pin::configure(GPIOA,2,GPIO_OUT_ALT_PUSHPULL_2MHz);// USART2 TX
      gpio_pin::configure(GPIOA,3,GPIO_IN_FLOATING);// USART2 RX

      AFIO->MAPR = ((AFIO->MAPR & 0x1FFFF) & ~AFIO_MAPR_USART2_REMAP) | AFIO_MAPR_SWJ_CFG_JTAGDISABLE;
        }
      }

    bool ext_osc = (RCC->CFGR & RCC_CFGR_SW) == RCC_CFGR_SW_HSE;
    //double f_osc = (double)sys_get_osc();
    unsigned int f_osc = sys_get_osc();

    usart_tx_fifo.init();
    usart_rx_fifo.init();
    USART->BRR = usart_get_baud_rate(f_osc, baud);//115200);
    USART->CR1 = USART_CR1_UE | USART_CR1_RXNEIE | USART_CR1_TE | USART_CR1_RE;
    USART->CR2 = 0;
    USART->CR3 = 0;

    if (USART==USART1) NVIC_EnableIRQ(USART1_IRQn);
    else if (USART==USART2) NVIC_EnableIRQ(USART2_IRQn);
    }


char hex[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
void usart::writeHEX(int data, int ln){
  for (int d=ln-1; d>=0; d--) {
    write(hex[(data>>(d*4))&0x0F]);
  }
}

void usart::writeDEC(int data, int ln){
  if (data<0) {
    write('-');
    data=-data;
    ln--;
  }
  int div=1;
  if ( data >= 1000000000 )      { div = 1000000000; if ( ln < 10) ln = 10; }
  else if ( data >= 100000000  ) { div = 100000000;  if ( ln < 9 ) ln = 9;  }
  else if ( data >= 10000000   ) { div = 10000000;   if ( ln < 8 ) ln = 8;  }
  else if ( data >= 1000000    ) { div = 1000000;    if ( ln < 7 ) ln = 7;  }
  else if ( data >= 100000     ) { div = 100000;     if ( ln < 6 ) ln = 6;  }
  else if ( data >= 10000      ) { div = 10000;      if ( ln < 5 ) ln = 5;  }
  else if ( data >= 1000       ) { div = 1000;       if ( ln < 4 ) ln = 4;  }
  else if ( data >= 100        ) { div = 100;        if ( ln < 3 ) ln = 3;  }
  else if ( data >= 10         ) { div = 10;         if ( ln < 2 ) ln = 2;  }
  else                           { div = 1;          if ( ln < 1 ) ln = 1;  }

  for (int d=ln-1; d>=0; d--) {
    write((data / div) +'0');
    data %= div;
    div /= 10;
  }
}

void usart::write(const char *data, size_t ln)
  {
	int_fast8_t err;

  if (en) en->set(); //	GPIOC->ODR |= GPIOC_RS485DE;
	while (ln--)
	{
		do
		{
    err=usart_tx_fifo.write(*data);
		} while (err==0);
		data++;
		if (!(USART->CR1 & USART_CR1_TCIE))
		{
			int dt=usart_tx_fifo.read();

			if (dt>=0)
			{
				USART->DR = dt;
   			USART->SR = ~USART_SR_TC;
				USART->CR1 |= USART_CR1_TCIE;
			}
		}
	}
}

void usart::write(char d)
  {
	int_fast8_t err;

  if (en) en->set(); //	GPIOC->ODR |= GPIOC_RS485DE;
  do
		{
    err=usart_tx_fifo.write(d);
		} while (err==0);
	if (!(USART->CR1 & USART_CR1_TCIE))
		{
		int dt=usart_tx_fifo.read();

		if (dt>=0)
			{
			USART->DR = dt;
 			USART->SR = ~USART_SR_TC;
			USART->CR1 |= USART_CR1_TCIE;
			}
		}
	}



void usart::IRQHandler(void)
  {
	if (USART->SR & USART_SR_RXNE)
    {
		uint8_t data = USART->DR;
#ifdef USART_COUNTERS
		int err=usart_rx_fifo.write(data);
		if (err==0)
			usart_state.rx_ovr_cnt++;
#else
  usart_rx_fifo.write(data);
#endif
	}

	if (USART->SR & USART_SR_NE)
    {
		USART->SR &= ~USART_SR_NE;
#ifdef USART_COUNTERS
		usart_state.rx_noise_cnt++;
#endif
    }

	if ((USART->SR & USART_SR_FE) || (USART->SR & USART_SR_PE))
    {
		USART->SR &= ~(USART_SR_FE | USART_SR_PE);
#ifdef USART_COUNTERS
		usart_state.rx_err_cnt++;
#endif
    }

	if (USART->SR & USART_SR_ORE)
    {
    volatile char dummy = USART->DR;
		USART->SR &= ~USART_SR_ORE;
#ifdef USART_COUNTERS
		usart_state.rx_ovr_cnt++;
#endif
    }

	if ( (USART->SR & USART_SR_TC) && (USART->CR1 & USART_CR1_TCIE))
    {
		//int_fast8_t err;
		int data;

    data=usart_tx_fifo.read();
//		cycl_buf_fet(err, usart_tx_buf, uint8_t, data,USART_TX_ARRAY_MASK);
		if (data<0)
      {
			USART->SR = ~USART_SR_TC;
			USART->CR1 &= ~USART_CR1_TCIE;
      if (en) en->reset(); //			GPIOC->ODR &= ~GPIOC_RS485DE;
      }
		else
			USART->DR = data;
    }

  }


void pollusart::usart_init(int baud)
    {
    if (USART==USART1)
      {
      gpio_pin::configure(GPIOA,9,GPIO_OUT_ALT_PUSHPULL_2MHz);// USART1 TX
      gpio_pin::configure(GPIOA,10,GPIO_IN_FLOATING);// USART1 RX
      RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
      }
    else if (USART==USART2)
      {
      RCC->APB1ENR |= RCC_APB1ENR_USART2EN;
      gpio_pin::configure(GPIOA,2,GPIO_OUT_ALT_PUSHPULL_2MHz);// USART2 TX
      gpio_pin::configure(GPIOA,3,GPIO_IN_FLOATING);// USART2 RX
      }

    bool ext_osc = (RCC->CFGR & RCC_CFGR_SW) == RCC_CFGR_SW_HSE;
    //double f_osc = (double)sys_get_osc();
    unsigned int f_osc = sys_get_osc();

    USART->BRR = usart_get_baud_rate(f_osc, baud);//115200);
    USART->CR1 = USART_CR1_UE | USART_CR1_TE | USART_CR1_RE;
    USART->CR2 = 0;
    USART->CR3 = 0;
    }

void pollusart::write(const char *data, size_t ln)
  {
  if (en) en->set();
	while (ln--)
    {
    while (!(USART->SR & USART_SR_TXE)) // wait for empty data register
      ;
    USART->DR = *data++;
    }

  while (!(USART->SR & USART_SR_TC))
    ;
  if (en) en->reset();
  }

void pollusart::write(char d)
  {
  if (en) en->set();
  while (!(USART->SR & USART_SR_TXE)) // wait for empty data register
    ;
  USART->DR = d;

  while (!(USART->SR & USART_SR_TC))
    ;
  if (en) en->reset();
  }

