
#include <assert.h>
#include <ctassert.h>
#include <string.h>
#include <drivers/si446x.hpp>
#include <lib/serializer.hpp>

namespace si446x {

template <typename T, size_t N>
uint8_t (&countof_helper(T (&)[N]))[N];                /* return ref to N elements octet array */
#define countof(array) (sizeof(countof_helper(array))) /* wow! */

/* ---- rf_module -------------------------------------------------------------- */
rf_module::rf_module(__spi_t &io, __gpio_t &nsel): io_(io), nsel_(nsel)
	{ }

int rf_module::init(msec_t timeout)
{
	nsel_.set();
	return 0;
}

int rf_module::wait_cts_cycle(msec_t timeout)
{
	ssize_t ret = io_.write_octet(0x44, timeout);
	if (ret < 0)
		return -EIO;

	uint8_t cts;
	ret = io_.read_octet(&cts, timeout);
	if (ret < 0)
		return -EIO;

	if (cts != 0xff)
		return -EBUSY;
	return 0;
}

void rf_module::nsel_minimal_pause(void)
{
	sleep_usec(20);
}

int rf_module::wait_cts(bool stay_selected, msec_t timeout)
{
	tick_t const started_at = get_ticks();

	scoped_inode_lock lk(io_.get_dev(), timeout);
	ssize_t ret = lk.get_errcode();
	if (ret)
		return ret;

	for (;;)
	{
		nsel_.clr();
		int ret = wait_cts_cycle(timeout);
		if (!ret)
		{
			if (!stay_selected)
				nsel_.set();
			return 0;
		}
		nsel_.set();

		tick_t const now  = get_ticks();
		tick_t const diff = now - started_at;
		tick_t const tout = msec_to_ticks(timeout);
		if (diff > tout)
			return -ETIMEDOUT;

		nsel_minimal_pause();
	}
}

int rf_module::write(uint8_t cmd, void const *ptr, size_t size, msec_t timeout)
{
	scoped_inode_lock lk(io_.get_dev(), timeout);
	ssize_t ret = lk.get_errcode();
	if (ret)
		return ret;

	gpio_scoped_level level(nsel_, false);

	ret = io_.write_octet(cmd, timeout);
	if (ret < 0)
		return -EIO;

	if (size > 0)
	{
		ret = io_.force_write(ptr, size, timeout);
		if (ret != size)
			return -EIO;
	}
	else
	{
		/* dummy: rf_module a1 revision bug workaround */
		ret = io_.write_octet(cmd, timeout);
		if (ret < 0)
			return -EIO;
	}

	return 0;
}

int rf_module::read(uint8_t const *opt_cmd, void *ptr, size_t size, msec_t timeout)
{
	scoped_inode_lock lk(io_.get_dev(), timeout);
	ssize_t ret = lk.get_errcode();
	if (ret)
		return ret;

	gpio_scoped_level level(nsel_, false);

	if (opt_cmd != NULL)
	{
		ret = io_.write_octet(*opt_cmd, timeout);
		if (ret < 0)
			return -EIO;
	}

	ret = io_.force_read(ptr, size, timeout);
	if (ret != size)
		return -EIO;

	return 0;
}

int rf_module::rw_once(uint8_t cmd, void const *warg, size_t wsize,
		void *rarg, size_t rsize, msec_t timeout)
{
	scoped_inode_lock lk(io_.get_dev(), timeout);
	ssize_t ret = lk.get_errcode();
	if (ret)
		return ret;

	ret = wait_cts(false, timeout);
	if (ret)
		return ret;

	ret = write(cmd, warg, wsize, timeout);
	if (ret)
		return ret;

	ret = wait_cts(true, timeout);
	if (ret)
	{
		nsel_.set();
		return ret;
	}

	return read(NULL, rarg, rsize, timeout);
}

int rf_module::rw(uint8_t cmd, void const *warg, size_t wsize,
		void *rarg, size_t rsize, msec_t timeout)
{
	int ret = -ETIMEDOUT;

	for (uint i = 0; i < 3; ++i)
	{
		ret = rw_once(cmd, warg, wsize, rarg, rsize, timeout);
		if (!ret)
			return 0;
	}
	return ret;
}

int rf_module::power_on(uint32_t xtal_freq, bool txco, msec_t timeout)
{
	uint8_t const boot_opt = 0x01; /* boot up as transceiver */
	uint8_t const xtal_opt = txco ? 0x01 : 0x00;

	uint8_t wbuf[sizeof(boot_opt) + sizeof(xtal_opt) + sizeof(xtal_freq)];
	void *wptr = wbuf;
	wptr = put_be(wptr, boot_opt);
	wptr = put_be(wptr, xtal_opt);
	wptr = put_be(wptr, xtal_freq);

	return rw(0x02, wbuf, sizeof(wbuf), NULL, 0, timeout);
}

int rf_module::nop(msec_t timeout)
{
	return rw(0x00, NULL, 0, NULL, 0, timeout);
}

int rf_module::get_part_info(struct part_info *info, msec_t timeout)
{
	uint8_t rbuf[sizeof(info->chip_rev) + sizeof(info->part_build)
			+ sizeof(info->part) + sizeof(info->id)
			+ sizeof(info->customer_id) + sizeof(info->rom_id)];
	int ret = rw(0x01, NULL, 0, rbuf, sizeof(rbuf), timeout);
	if (ret)
		return ret;

	void const *rptr = rbuf;
	rptr = get_be(rptr, &info->chip_rev);
	rptr = get_be(rptr, &info->part);
	rptr = get_be(rptr, &info->part_build);
	rptr = get_be(rptr, &info->id);
	rptr = get_be(rptr, &info->customer_id);
	rptr = get_be(rptr, &info->rom_id);
	return 0;
}

int rf_module::get_func_info(struct func_info *info, msec_t timeout)
{
	uint8_t rbuf[sizeof(info->rev_ext) + sizeof(info->rev_branch)
			+ sizeof(info->rev_int) + sizeof(info->func)
			+ sizeof(info->patch)];
	int ret = rw(0x10, NULL, 0, rbuf, sizeof(rbuf), timeout);
	if (ret)
		return ret;

	void const *rptr = rbuf;
	rptr = get_be(rptr, &info->rev_ext);
	rptr = get_be(rptr, &info->rev_branch);
	rptr = get_be(rptr, &info->rev_int);
	rptr = get_be(rptr, &info->patch);
	rptr = get_be(rptr, &info->func);
	return 0;
}

int rf_module::gpio_cfg(struct gpio_cfg const &wcfg, struct gpio_cfg *rcfg,
		msec_t timeout)
{
	uint8_t wbuf[sizeof(wcfg.gpio) + sizeof(wcfg.nirq) + sizeof(wcfg.sdo)
			+ sizeof(wcfg.general)];
	void *wptr = wbuf;
	for (size_t i = 0; i < countof(wcfg.gpio); ++i)
		wptr = put_be(wptr, wcfg.gpio[i]);
	wptr = put_be(wptr, wcfg.nirq);
	wptr = put_be(wptr, wcfg.sdo);
	wptr = put_be(wptr, wcfg.general);

	uint8_t rbuf[sizeof(wbuf)];
	int ret = rw(0x13, wbuf, sizeof(wbuf), rbuf, sizeof(rbuf), timeout);
	if (ret)
		return ret;

	void const *rptr = rbuf;
	for (size_t i = 0; i < countof(rcfg->gpio); ++i)
		rptr = get_be(rptr, &rcfg->gpio[i]);
	rptr = get_be(rptr, &rcfg->nirq);
	rptr = get_be(rptr, &rcfg->sdo);
	rptr = get_be(rptr, &rcfg->general);

	return 0;
}

int rf_module::read_adc(uint8_t adc_cfg, struct adc_info *info, msec_t timeout)
{
	uint8_t rbuf[sizeof(info->gpio) + sizeof(info->batt) + sizeof(info->temp)
			+ sizeof(info->temp_slope) + sizeof(info->temp_intercept)];
	int ret = rw(0x14, &adc_cfg, sizeof(adc_cfg), rbuf, sizeof(rbuf), timeout);
	if (ret)
		return ret;

	void const *rptr = rbuf;
	rptr = get_be(rptr, &info->gpio);
	rptr = get_be(rptr, &info->batt);
	rptr = get_be(rptr, &info->temp);
	rptr = get_be(rptr, &info->temp_slope);
	rptr = get_be(rptr, &info->temp_intercept);

	return 0;
}

int rf_module::fifo_ctl(struct fifo_info *info, uint8_t flags, msec_t timeout)
{
	uint8_t rbuf[sizeof(info->rx_count) + sizeof(info->tx_space)];
	int ret = rw(0x15, &flags, sizeof(flags), rbuf, sizeof(rbuf), timeout);
	if (ret)
		return ret;

	void const *rptr = rbuf;
	rptr = get_be(rptr, &info->rx_count);
	rptr = get_be(rptr, &info->tx_space);

	return 0;
}

ssize_t rf_module::get_packet_size(msec_t timeout)
{
	uint8_t const req[] = { 0, 0, 0, 0, 0 };
	uint16_t      size;

	int ret = rw(0x16, req, sizeof(req), &size, sizeof(size), timeout);
	if (ret)
		return ret;
  size = (size >> 8) | ((size&0xff)<<8);
	//get_be(&size, &size);
	return size;
}

int rf_module::ircal(struct ircal_cfg const &cfg, msec_t timeout)
{
	ctassert(sizeof(cfg) == sizeof(cfg.src_step_size) + sizeof(cfg.src_rssi_avg)
			+ sizeof(cfg.rx_chain_setting1) + sizeof(cfg.rx_chain_setting2),
			si446x_ircal_cfg_size);

	return rw(0x17, &cfg, sizeof(cfg), NULL, 0, timeout);
}

int rf_module::set_proto(bool ieee802_15_4g, msec_t timeout)
{
	uint8_t const proto = ieee802_15_4g;
	return rw(0x18, &proto, sizeof(proto), NULL, 0, timeout);
}

int rf_module::get_irq_stat(struct irq_stat_req const &req,
		struct irq_stat_resp *resp, msec_t timeout)
{
	uint8_t wbuf[sizeof(req.ph_clr_pend) + sizeof(req.modem_clr_pend)
			+ sizeof(req.chip_clr_pend)];
	void *wptr = wbuf;
	wptr = put_be(wptr, req.ph_clr_pend);
	wptr = put_be(wptr, req.modem_clr_pend);
	wptr = put_be(wptr, req.chip_clr_pend);

	ctassert(sizeof(*resp) == sizeof(resp->global_pend) + sizeof(resp->global_stat)
			+ sizeof(resp->ph_pend) + sizeof(resp->ph_stat)
			+ sizeof(resp->modem_pend) + sizeof(resp->modem_stat)
			+ sizeof(resp->chip_pend) + sizeof(resp->chip_stat),
			si446x_irq_stat_resp_size);
	return rw(0x20, wbuf, sizeof(wbuf), resp, sizeof(*resp), timeout);
}

int rf_module::get_ph_stat(struct irq_stat_ph_resp *resp, msec_t timeout)
{
	ctassert(sizeof(*resp) == sizeof(resp->pend) + sizeof(resp->stat),
			si446x_irq_stat_ph_resp_size);

	return rw(0x21, NULL, 0, resp, sizeof(*resp), timeout);
}

int rf_module::get_modem_stat(struct irq_stat_modem_resp *resp, msec_t timeout)
{
	uint8_t rbuf[sizeof(resp->pend) + sizeof(resp->stat)
			+ sizeof(resp->rssi_curr) + sizeof(resp->rssi_latch)
			+ sizeof(resp->rssi_ant1) + + sizeof(resp->rssi_ant2)];
	int ret = rw(0x22, NULL, 0, rbuf, sizeof(rbuf), timeout);
	if (ret)
		return ret;

	void const *rptr = rbuf;
	rptr = get_be(rptr, &resp->pend);
	rptr = get_be(rptr, &resp->stat);
	rptr = get_be(rptr, &resp->rssi_curr);
	rptr = get_be(rptr, &resp->rssi_latch);
	rptr = get_be(rptr, &resp->rssi_ant1);
	rptr = get_be(rptr, &resp->rssi_ant2);
	return 0;
}

int rf_module::get_chip_stat(struct irq_stat_chip_resp *resp, msec_t timeout)
{
	uint8_t rbuf[sizeof(resp->pend) + sizeof(resp->stat) + sizeof(resp->error)];
	int ret = rw(0x23, NULL, 0, rbuf, sizeof(rbuf), timeout);
	if (ret)
		return ret;

	void const *rptr = rbuf;
	rptr = get_be(rptr, &resp->pend);
	rptr = get_be(rptr, &resp->stat);
	rptr = get_be(rptr, &resp->error);
	return 0;
}

int rf_module::get_device_state(struct device_state *state, msec_t timeout)
{
	ctassert(sizeof(*state) == sizeof(state->main_state) + sizeof(state->channel),
			si446x_device_state_size);
	return rw(0x33, NULL, 0, state, sizeof(*state), timeout);
}

int rf_module::start_tx(struct start_tx_req const &req, msec_t timeout)
{
	uint8_t wbuf[sizeof(req.channel) + sizeof(req.condition) + sizeof(req.size)];
	void *wptr = wbuf;
	wptr = put_be(wptr, req.channel);
	wptr = put_be(wptr, req.condition);
	wptr = put_be(wptr, req.size);

	return rw(0x31, wbuf, sizeof(wbuf), NULL, 0, timeout);
}

int rf_module::start_rx(struct start_rx_req const &req, msec_t timeout)
{
	uint8_t wbuf[sizeof(req.channel) + sizeof(req.condition) + sizeof(req.size)
			+ sizeof(req.timeout_state) + sizeof(req.vaild_state)
			+ sizeof(req.invalid_state)];
	void *wptr = wbuf;
	wptr = put_be(wptr, req.channel);
	wptr = put_be(wptr, req.condition);
	wptr = put_be(wptr, req.size);
	wptr = put_be(wptr, req.timeout_state);
	wptr = put_be(wptr, req.vaild_state);
	wptr = put_be(wptr, req.invalid_state);

	return rw(0x32, wbuf, sizeof(wbuf), NULL, 0, timeout);
}

int rf_module::write_tx(void const *ptr, size_t size, msec_t timeout)
{
	/* cts not works for this command */
	return write(0x66, ptr, size, timeout);
}

int rf_module::read_rx(void *ptr, size_t size, msec_t timeout)
{
	/* cts not works for this command */
	uint8_t const cmd = 0x77;
	return read(&cmd, ptr, size, timeout);
}

int rf_module::set_property_raw(uint16_t prop_addr, void const *ptr, size_t size,
		msec_t timeout)
{
	uint8_t const group     = prop_addr >> 8;
	uint8_t const ofs_props = prop_addr;
	uint8_t const num_props = size;

	if (size > set_prop_max_size)
		return -EINVAL;

	uint8_t wbuf[sizeof(group) + sizeof(num_props) + sizeof(ofs_props)
			+ set_prop_max_size];
	void *wptr = wbuf;
	wptr = put_be(wptr, group);
	wptr = put_be(wptr, num_props);
	wptr = put_be(wptr, ofs_props);
	for (size_t i = 0; i < size; ++i)
		wptr = put_be(wptr, (static_cast<uint8_t const *>(ptr))[i]);

	return rw(0x11, wbuf, sizeof(group) + sizeof(num_props) + sizeof(ofs_props)
			+ size, NULL, 0, timeout);
}

int rf_module::get_property_raw(uint16_t prop_addr, void *ptr, size_t size,
		msec_t timeout)
{
	uint8_t const group     = prop_addr >> 8;
	uint8_t const ofs_props = prop_addr;
	uint8_t const num_props = size;

	if (size > get_prop_max_size)
		return -EINVAL;

	uint8_t wbuf[sizeof(group) + sizeof(num_props) + sizeof(ofs_props)];
	void *wptr = wbuf;
	wptr = put_be(wptr, group);
	wptr = put_be(wptr, num_props);
	wptr = put_be(wptr, ofs_props);

	return rw(0x12, wbuf, sizeof(wbuf), ptr, size, timeout);
}

int rf_module::set_property(uint16_t prop_addr, void const *void_ptr, size_t size,
		msec_t timeout)
{
	uint8_t const *ptr = static_cast<uint8_t const *>(void_ptr);

	for (;;)
	{
		size_t const part_size = size > set_prop_max_size
				? set_prop_max_size : size;
		if (!part_size)
			return 0;

		int ret = set_property_raw(prop_addr, ptr, part_size, timeout);
		if (ret)
			return ret;

		prop_addr += part_size;
		ptr       += part_size;
		size      -= part_size;
	}
}


int rf_module::set_verify_property(uint16_t prop_addr, void const *void_ptr, size_t size,
		msec_t timeout)
{
	uint8_t const *ptr = static_cast<uint8_t const *>(void_ptr);

	for (;;)
	{
		size_t const part_size = size > set_prop_max_size
				? set_prop_max_size : size;
		if (!part_size)
			return 0;

		int ret = set_property_raw(prop_addr, ptr, part_size, timeout);
		if (ret)
			return ret;

		ctassert(set_prop_max_size <= get_prop_max_size, si446x_prop_size);

		uint8_t prop_rbuf[set_prop_max_size];
		ret = get_property_raw(prop_addr, prop_rbuf, part_size, timeout);
		if (ret)
			return ret;

		if (memcmp(ptr, prop_rbuf, part_size))
			return -EINVAL;

		prop_addr += part_size;
		ptr       += part_size;
		size      -= part_size;
	}
}

int rf_module::get_property(uint16_t prop_addr, void *void_ptr, size_t size,
		msec_t timeout)
{
	uint8_t *ptr = static_cast<uint8_t *>(void_ptr);

	for (;;)
	{
		size_t const part_size = size > get_prop_max_size
				? get_prop_max_size : size;
		if (!part_size)
			return 0;

		int ret = get_property_raw(prop_addr, ptr, part_size, timeout);
		if (ret)
			return ret;

		prop_addr += part_size;
		ptr       += part_size;
		size      -= part_size;
	}
}

int rf_module::apply_cfg_array(void const *cfg_array, msec_t timeout)
{
	uint8_t const *ptr = static_cast<uint8_t const *>(cfg_array);

	while (*ptr != 0x00)
	{
		uint8_t const  op_size = *ptr++ - 1;
		uint8_t const  op_cmd  = *ptr++;
		uint8_t const *op_data = ptr;
		ptr += op_size;

		int ret = rw(op_cmd, op_data, op_size, NULL, 0, timeout);
		if (ret)
			return ret;

#if 1
		static struct irq_stat_req const irq_req =
		{
			/* clear all pending interrupts */
			.ph_clr_pend    = 0x00,
			.modem_clr_pend = 0x00,
			.chip_clr_pend  = 0x00,
		};

		struct irq_stat_resp irq_resp;
		ret = get_irq_stat(irq_req, &irq_resp);
		if (ret)
			return ret;
#endif
	}
	return 0;
}

#if 0
/* ---- rf_module_cts ------------------------------------------------------- */
rf_module_cts::rf_module_cts(spi_t &io, gpio_t &nsel, gpio_t &cts):
		rf_module(io, nsel), cts_(cts), isready_(0)
	{ }

int rf_module_cts::init(msec_t timeout)
{
	/* init callback */
	return rf_module::init(timeout);
}

int rf_module_cts::wait_cts(bool stay_selected, msec_t timeout)
{
	/* todo: wait cts pin */
	return -1;
}
#endif

/* ---- rf_channel ---------------------------------------------------------- */
rf_channel::rf_channel(__spi_t &io, __gpio_t &nsel, __gpio_t &sdn, __gpio_t &nirq):
		rf_(io, nsel), sdn_(sdn), nirq_(nirq)
{
	power_down();
}

void rf_channel::power_down()
{
	sdn_.set(true);
}

int rf_channel::power_up(void const *cfg_array, msec_t timeout)
{
	sdn_.set(true);
	sleep_msec(25);
	sdn_.set(false);

	int ret = rf_.init(timeout);
	if (ret)
		return ret;

	sleep_msec(20);

	return rf_.apply_cfg_array(cfg_array, timeout);
}

uint rf_channel::get_events_flags()
{
	if (nirq_.read())
		return 0;

	struct irq_stat_resp irq_resp;
	struct irq_stat_req const irq_clear_req =
	{
		.ph_clr_pend    = 0x00,
		.modem_clr_pend = 0x00,
		.chip_clr_pend  = 0x00,
	};

	int ret = rf_.get_irq_stat(irq_clear_req, &irq_resp);
	if (ret)
	{
		assert(false);
		return 0;
	}

	uint flags = 0;
	if (irq_resp.ph_pend & (1 << 3))
		flags |= event_rx_err;
	if (irq_resp.ph_pend & (1 << 4))
		flags |= event_rx;
	if (irq_resp.ph_pend & (1 << 5))
		flags |= event_tx;
	return flags;
}

int rf_channel::start_rx()
{
	struct device_state state;
	int ret = rf_.get_device_state(&state);
	if (ret >= 0 && state.main_state == STATE_RX)
		return 0;

	struct start_rx_req const rx_req =
	{
		.channel       = 0,
		.condition     = 0,
		.size          = 0,
		.timeout_state = 0,
		.vaild_state   = 3,
		.invalid_state = 3,
	};

	return rf_.start_rx(rx_req);
}

int rf_channel::start_tx()
{
	struct start_tx_req const tx_req =
	{
		.channel    = 0,
		.condition  = 0x30,
		.size       = 0,
	};

	return rf_.start_tx(tx_req);
}

ssize_t rf_channel::clear_rx(void)
{
	struct fifo_info fifo;
	int ret = rf_.fifo_ctl(&fifo);
	if (ret)
		return ret;

	for (size_t i = 0; i < fifo.rx_count; ++i)
	{
		uint8_t octet;
		ret = rf_.read_rx(&octet, sizeof(octet));
		if (ret < 0)
			return ret;
	}
	return fifo.rx_count;
}

ssize_t rf_channel::read_rx(void *ptr, size_t size)
{
/*	struct fifo_info fifo;
	int ret = rf_.fifo_ctl(&fifo);
	if (ret)
		return ret;

	if (fifo.rx_count > size)
	{
		clear_rx();
		return -E2BIG;
	}*/
  int ret = rf_.get_packet_size();

  if (ret>0) {


//	ret = rf_.read_rx(ptr, fifo.rx_count);
	int rdret = rf_.read_rx(ptr, ret);
  }
	return ret;//fifo.rx_count;
}

ssize_t rf_channel::write_tx(void const *ptr, size_t size)
{
	int ret = rf_.write_tx(ptr, size);
	if (ret < 0)
		return ret;

	return size;
}

bool rf_channel::nirq_is_active() const
{
	return !nirq_.read();
}

/* ---- rf_packet_handler --------------------------------------------------- */
rf_packet_handler::rf_packet_handler(rf_channel &rf): rf_(rf)
	{ }

__packed struct header
{
	uint8_t payload_size;
	uint8_t lowpan;
	uint8_t dst_addr;
	uint8_t src_addr;
};

ssize_t rf_packet_handler::read_pkt(struct endpoint *ep, void *ptr, size_t size)
{
	ctassert(sizeof(struct header) < rf_packet_handler::payload_fixed_size,
		si446x_packet_handler_size_check);

	uint8_t total_packet[payload_fixed_size];

	ssize_t ret = rf_.read_rx(total_packet, sizeof(total_packet));
//	if (ret != payload_fixed_size)
//		return -EIO;

	struct header const &hdr = *reinterpret_cast<struct header const *>(total_packet);
	if (ep != NULL)
	{
		ep->lowpan   = hdr.lowpan;
		ep->dst_addr = hdr.dst_addr;
		ep->src_addr = hdr.src_addr;
	}

	if (hdr.payload_size > payload_fixed_size - sizeof(hdr))
		return -EIO;

	if (hdr.payload_size > size)
		return -E2BIG;

	memcpy(ptr, total_packet + sizeof(hdr), hdr.payload_size);
	return hdr.payload_size;
}

ssize_t rf_packet_handler::write_pkt(struct endpoint const &ep, void const *ptr, size_t size)
{
	struct header hdr =
	{
		.payload_size = size+sizeof(hdr)-1,
		.lowpan       = ep.lowpan,
		.dst_addr     = ep.dst_addr,
		.src_addr     = ep.src_addr,
	};

//	if (size + sizeof(hdr) > payload_fixed_size)
//		return -E2BIG;

  uint8_t l[] = { (uint8_t)(size+sizeof(hdr)-1)};
  //rf_.set_properties(RH_RF24_PROPERTY_PKT_FIELD_2_LENGTH_7_0, l, sizeof(l));
  /*bool RH_RF24::set_properties(uint16_t firstProperty, const uint8_t* values, uint8_t count)
{
    uint8_t buf[15];

    buf[0] = firstProperty >> 8;   // GROUP
    buf[1] = count;                // NUM_PROPS
    buf[2] = firstProperty & 0xff; // START_PROP
    uint8_t i;
    for (i = 0; i < 12 && i < count; i++)
	buf[3 + i] = values[i]; // DATAn
    return command(RH_RF24_CMD_SET_PROPERTY, buf, count + 3);
} */
  #define RH_RF24_PROPERTY_PKT_FIELD_2_LENGTH_7_0           0x1212
  rf_.rf_.set_property(RH_RF24_PROPERTY_PKT_FIELD_2_LENGTH_7_0,l,sizeof(l));

	ssize_t ret = rf_.write_tx(&hdr, sizeof(hdr));
	if (ret < 0)
		return ret;

	ret = rf_.write_tx(ptr, size);
	if (ret < 0)
		return ret;

/*
	size_t padding_size = payload_fixed_size - sizeof(hdr) - size;
	for (size_t i = 0; i < padding_size; ++i)
	{
		uint8_t padding_octet = 0xa5;
		ret = rf_.write_tx(&padding_octet, sizeof(padding_octet));
		if (ret < 0)
			return ret;
	}*/

	return size;
}

rf_channel &rf_packet_handler::channel() const
{
	return rf_;
}

/* ---- rf_pipe ------------------------------------------------------------- */
rf_pipe::rf_pipe(rf_packet_handler &rf): rf_(rf)
#ifdef LPOS10
		, event_(0)
#endif
	{ }

void rf_pipe::wait_nirq_falling_edge(msec_t timeout)
{
#ifdef LPOS10
	event_.k_resetEvent();
	if (rf_.channel().nirq_is_active())
		return;

	msec_t max_poll_timeout = 5;
	int ret = event_.k_decSemaphore(1, timeout > max_poll_timeout ? max_poll_timeout : timeout);
	int volatile bkpt = ret;
#endif
}

#ifdef LPOS10
void rf_pipe::nirq_falling_edge()
{
	event_.k_setEvent();
}
#endif

ssize_t rf_pipe::recv(struct rf_packet_handler::endpoint *ep,
		void *inbuf, size_t insize, msec_t timeout)
{
	int ret = rf_.channel().start_rx();
	if (ret)
		return ret;

	tick_t const recv_started_at = get_ticks();
	for (;;)
	{
		uint flags = rf_.channel().get_events_flags();

		if (flags & rf_channel::event_rx_err)
		{
			rf_.channel().clear_rx();
			return -EIO;
		}
		else if (flags & rf_channel::event_rx)
			return rf_.read_pkt(ep, inbuf, insize);

		tick_t diff = get_ticks() - recv_started_at;
		if (diff >= msec_to_ticks(timeout))
			return -ETIMEDOUT;

		wait_nirq_falling_edge(timeout);
	}
}

ssize_t rf_pipe::send(struct rf_packet_handler::endpoint const &ep,
		void const *buf, size_t size, msec_t timeout)
{
	rf_.channel().get_events_flags();

	int ret = rf_.write_pkt(ep, buf, size);
	if (ret < 0)
		return ret;

	ret = rf_.channel().start_tx();
	if (ret)
		return ret;

	tick_t const send_started_at = get_ticks();
	for (;;)
	{
		if (rf_.channel().get_events_flags() & rf_channel::event_tx)
			return size;

		tick_t diff = get_ticks() - send_started_at;
		if (diff >= msec_to_ticks(timeout))
			return -ETIMEDOUT;

		wait_nirq_falling_edge(timeout);
	}
}

bool rf_pipe::is_answer_endpoint(struct rf_packet_handler::endpoint const &ep,
		struct rf_packet_handler::endpoint const &rep, uint8_t broadcast)
{
	return rep.lowpan == ep.lowpan && rep.dst_addr == ep.src_addr
			&& (rep.src_addr == ep.dst_addr || ep.dst_addr == broadcast);
}

#if 0
ssize_t rf_pipe::send_and_recv(struct rf_packet_handler::endpoint const &ep,
		void const *outbuf, size_t outsize,
		void *inbuf, size_t insize, msec_t timeout)
{
	ssize_t ret = send(ep, outbuf, outsize, timeout);
	if (ret < 0)
		return ret;

	struct rf_packet_handler::endpoint rep;
	ret = recv(&rep, inbuf, insize, timeout);
	if (ret < 0 || !is_answer_endpoint(ep, rep))
		return ret;

	return ret;
}

ssize_t rf_pipe::send_and_check_echo(struct rf_packet_handler::endpoint const &ep,
		void const *outbuf, size_t outsize, msec_t timeout)
{
	uint8_t buf[64];
	if (outsize > sizeof(buf))
		return -E2BIG;

	ssize_t ret = send_and_recv(ep, outbuf, outsize, buf, sizeof(buf), timeout);
	if (ret < 0)
		return ret;

	if (ret == outsize && !memcmp(outbuf, buf, ret))
		return 0;
	return -EIO;
}
#endif

bool rf_pipe::is_proper_endpoint(struct rf_packet_handler::endpoint const &ep,
		uint8_t lowpan, uint8_t self_addr, uint8_t broadcast)
{
	return ep.lowpan == lowpan && (ep.dst_addr == self_addr || ep.dst_addr == broadcast);
}

void rf_pipe::make_answer_endpoint(struct rf_packet_handler::endpoint *ep,
		struct rf_packet_handler::endpoint const &rep,
		uint8_t self_addr)
{
	ep->lowpan   = rep.lowpan;
	ep->dst_addr = rep.src_addr;
	ep->src_addr = self_addr;
}

void rf_pipe::make_answer_endpoint(struct rf_packet_handler::endpoint *ep, uint8_t self_addr)
{
	ep->dst_addr = ep->src_addr;
	ep->src_addr = self_addr;
}

} /* namespace si446x */
