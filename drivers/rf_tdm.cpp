
#include <assert.h>
#include <string.h>
#include <drivers/rf_tdm.hpp>

namespace rf_tdm {

/* ---- trace --------------------------------------------------------------- */
void trace::add(implementation &impl, char event, uint8_t addr,
		uint16_t ofst_msec, uint16_t slpt_msec)
{
	tick_t const now  = impl.get_ticks();
	tick_t const diff = now - tstamp_;
	msec_t const msec = impl.ticks_to_msec(diff);

	assert(array_ != NULL && count_ > 0 && idx_ < count_);

	array_[idx_].event     = event;
	array_[idx_].addr      = addr;
	array_[idx_].ofst_msec = ofst_msec;
	array_[idx_].slpt_msec = slpt_msec;
	array_[idx_].diff_msec = msec < UINT16_MAX ? msec : UINT16_MAX;

	if (++idx_ >= count_)
		idx_ = 0;

	tstamp_ = now;
}

/* ---- coordinator --------------------------------------------------------- */
coordinator::coordinator(implementation &impl, trace *trc):
		impl_(impl), trace_(trc)
	{ }


void coordinator::tracev(char event, uint8_t addr, msec_t slpt_msec)
{
	if (trace_ != NULL)
	{
		tick_t const now    = impl_.get_ticks();
		tick_t const period = impl_.msec_to_ticks(calc_period());
		msec_t const ofst   = period ? impl_.ticks_to_msec(now % period) : 0;
		trace_->add(impl_, event, addr, ofst < UINT16_MAX ? ofst : UINT16_MAX,
				slpt_msec < UINT16_MAX ? slpt_msec : UINT16_MAX);
	}
}

ssize_t coordinator::seek(uint8_t addr) const
{
	for (size_t i = 0; i < devs_.size(); ++i)
	{
		if (devs_[i] == addr)
			return i;
	}
	return -100500;
}

ssize_t coordinator::add(uint8_t addr)
{
	ssize_t idx = seek(addr);
	if (idx >= 0)
		return idx;

	size_t size = devs_.size();
	devs_.push_back(addr);
	if (devs_.size() != size + 1)
		return -ENOMEM;

	return devs_.size() - 1;
}

msec_t coordinator::calc_period() const
{
	msec_t const period = impl_.silence_time() + impl_.slot_time() * devs_.size();
	msec_t const    min = impl_.min_period();
	return period > min ? period : min;
}

msec_t coordinator::calc_device_delay(size_t idx) const
{
	return impl_.silence_time() + impl_.slot_time() * idx;
}


msec_t coordinator::calc_remaining_time_in_period() const
{
	tick_t const now    = impl_.get_ticks();
	tick_t const period = impl_.msec_to_ticks(calc_period());
	if (period == 0)
		/* just now */
		return 0;

	return impl_.ticks_to_msec(period - now % period);
}

msec_t coordinator::calc_device_sleep_time(size_t idx) const
{
	msec_t const rem = calc_remaining_time_in_period();
	msec_t const dly = calc_device_delay(idx);
	return rem + dly;
}

msec_t coordinator::calc_silence_sleep_time() const
{
	msec_t const rem = calc_remaining_time_in_period();
	return rem;
}


ssize_t coordinator::listen()
{
	struct endpoint ep;
	uint8_t request[32];
	ssize_t ret = impl_.recv(&ep, request, sizeof(request), impl_.slot_time());
	if (ret < 0)
		return ret;
	if (ret < 1 || ep.lowpan != impl_.lowpan() || ep.dst_addr != impl_.coordinator_addr())
	{
		tracev('e', 0, 0);
		return -EIO;
	}

	uint8_t response[32];
	if (request[0] == 0x10)
		ret = process_connect(request + 1, ret - 1, response, sizeof(response));
	else if (request[0] == 0xd0)
		ret = process_data(ep.src_addr, request + 1, ret - 1, response, sizeof(response));
	else
		ret = -EIO;

	if (ret <= 0)
	{
		tracev('E', 0, 0);
		return ret;
	}

	impl_.switch_pause();
	ep.dst_addr = ep.src_addr;
	ep.src_addr = impl_.coordinator_addr();
	return impl_.send(ep, response, ret);
}

ssize_t coordinator::process_connect(uint8_t const *req, size_t reqsize,
		uint8_t *resp, size_t respsize)
{
	if (respsize < 4 + reqsize)
		return -E2BIG;

	int addr = registry(req, reqsize);
	if (addr < 0)
		return addr;

	ssize_t idx = add(addr);
	if (idx < 0)
		return idx;

	msec_t const slp = calc_device_sleep_time(idx);
	tracev('c', addr, slp);

	resp[0] = 0x11;
	resp[1] = addr;
	resp[2] = slp;
	resp[3] = slp >> 8;
	memcpy(resp + 4, req, reqsize);
	return 4 + reqsize;
}

ssize_t coordinator::process_data(uint8_t addr, uint8_t const *req, size_t reqsize,
		uint8_t *resp, size_t respsize)
{
	if (respsize < 3)
		return -E2BIG;

	ssize_t idx = seek(addr);
	if (idx < 0)
	{
		resp[0] = 0xcc;
		resp[1] = 0x00;
		resp[2] = 0x00;
		tracev('r', addr, 0);
	}
	else
	{
		msec_t const slp = calc_device_sleep_time(idx);
		tracev('d', addr, slp);

		resp[0] = 0xd1;
		resp[1] = slp;
		resp[2] = slp >> 8;
	}

	ssize_t ret = on_recv(req, reqsize, resp + 3, respsize - 3);
	return ret < 0 ? 3 : 3 + ret;
}

/* ---- device -------------------------------------------------------------- */
device::device(implementation &impl, trace *trc):
		impl_(impl), addr_(impl.default_addr()), trace_(trc)
	{ }


void device::tracev(char event, uint8_t addr, msec_t slpt_msec)
{
	if (trace_ != NULL)
		trace_->add(impl_, event, addr, 0, slpt_msec < UINT16_MAX ? slpt_msec : UINT16_MAX);
}

void device::restore(uint8_t addr)
{
	addr_ = addr;
}

bool device::connected() const
{
	return addr_ != impl_.default_addr();
}

uint8_t device::get_addr() const
{
	return addr_;
}


ssize_t device::recv(void *buf, size_t size, msec_t timeout) const
{
	struct endpoint ep;
	ssize_t ret = impl_.recv(&ep, buf, size, timeout);
	if (ret < 0)
		return ret;

	if (ep.lowpan != impl_.lowpan() || ep.src_addr != impl_.coordinator_addr()
			|| ep.dst_addr != addr_)
		return -EIO;

	return ret;
}

template <typename F>
ssize_t device::recv_cyclic(void *buf, size_t size, F &f, msec_t timeout) const
{
	tick_t const started_at = impl_.get_ticks();

	for (;;)
	{
		ssize_t ret = recv(buf, size, timeout);
		if (ret >= 0)
		{
			if (f(buf, ret) >= 0)
				return ret;
		}

		tick_t const now  = impl_.get_ticks();
		tick_t const diff = now - started_at;
		if (diff >= impl_.msec_to_ticks(timeout))
			return -ETIMEDOUT;
	}
}

ssize_t device::send(void const *buf, size_t size) const
{
	struct endpoint ep =
	{
		.lowpan   = impl_.lowpan(),
		.dst_addr = impl_.coordinator_addr(),
		.src_addr = addr_,
	};

	return impl_.send(ep, buf, size);
}


class connect_response_functor
{
public:
	connect_response_functor(uint8_t const *serial, size_t serial_size):
			serial_(serial), serial_size_(serial_size),
			addr_(0), msec_(0)
		{ }

	int operator()(void const *buf, ssize_t size)
	{
		/* response: [0x11][addr(1)][msec.le(2)][serial(n)] */
		uint8_t const *ptr = static_cast<uint8_t const *>(buf);

		if (size < serial_size_ + 4)
			return -EIO;

		if (ptr[0] != 0x11 || memcmp(ptr + 4, serial_, serial_size_))
			return -EIO;

		addr_ = ptr[1];
		msec_ = ptr[2] | (ptr[3] << 8);
		return 0;
	}

	uint8_t get_recv_addr() const
	{
		return addr_;
	}

	msec_t get_recv_msec() const
	{
		return msec_;
	}

private:
	uint8_t const *serial_;
	size_t         serial_size_;
	uint8_t        addr_;
	msec_t         msec_;
};

int device::connect(msec_t *required_sleep_period, msec_t timeout)
{
	if (connected())
		return 0;

	/* request: [0x10][serial(n)] */
	uint8_t request[32] = { 0x10 };
	uint8_t     *serial = request + 1;
	ssize_t serial_size = get_serial(serial, sizeof(request) - 1);
	if (serial_size < 0)
	{
		tracev('E', 0, 0);
		return serial_size;
	}

	ssize_t ret = send(request, serial_size + 1);
	if (ret < 0)
	{
		tracev('E', 0, 0);
		return ret;
	}

	tracev('c', 0, 0);

	/* response */
	uint8_t response[32];
	connect_response_functor f(serial, serial_size);
	ret = recv_cyclic(response, sizeof(response), f, timeout);
	if (ret < 0)
	{
		tracev('Z', 0, 0);
		return ret;
	}

	addr_ = f.get_recv_addr();
	if (required_sleep_period != NULL)
		*required_sleep_period = f.get_recv_msec();

	tracev('C', addr_, f.get_recv_msec());
	return 0;
}


class data_response_functor
{
public:
	data_response_functor(): clear_req_(false), msec_(0), ofs_(0)
		{ }

	int operator()(void const *buf, ssize_t size)
	{
		/* response: [0xd1 or 0xcc][msec.le(2)][data(n)] */
		uint8_t const *ptr = static_cast<uint8_t const *>(buf);

		if (size < 3)
			return -EIO;

		if (ptr[0] != 0xcc && ptr[0] != 0xd1)
			return -EIO;

		clear_req_ = ptr[0] == 0xcc;
		msec_      = ptr[1] | (ptr[2] << 8);
		ofs_       = 3;
		return 0;
	}

	bool clear_request() const
	{
		return clear_req_;
	}

	msec_t get_recv_msec() const
	{
		return msec_;
	}

	ssize_t offset() const
	{
		return ofs_;
	}

private:
	bool   clear_req_;
	msec_t msec_;
	size_t ofs_;
};


ssize_t device::send_and_recv(void const *outbuf, size_t outsize,
		void *inbuf, size_t insize,
		msec_t *required_sleep_period, msec_t timeout)
{
	if (!connected())
		return -EINVAL;

	/* request: [0xd0][data(n)] */
	uint8_t request[32] = { 0xd0 };
	size_t  request_size = outsize + 1;
	if (request_size + 1 > sizeof(request))
	{
		tracev('E', addr_, 0);
		return -E2BIG;
	}

	memcpy(request + 1, outbuf, outsize);
	ssize_t ret = send(request, request_size);
	if (ret < 0)
	{
		tracev('E', addr_, 0);
		return ret;
	}

	tracev('d', addr_, 0);

	/* response */
	uint8_t response[32];
	data_response_functor f;
	ret = recv_cyclic(response, sizeof(response), f, timeout);
	if (ret < 0)
	{
		tracev('Z', addr_, 0);
		return ret;
	}

	if (f.offset() > ret || insize < ret - f.offset())
	{
		tracev('E', addr_, 0);
		return -EINVAL;
	}

	if (f.clear_request())
		addr_ = impl_.default_addr();
	if (required_sleep_period != NULL)
		*required_sleep_period = f.get_recv_msec();
	memcpy(inbuf, response + f.offset(), ret - f.offset());

	tracev('D', addr_, f.get_recv_msec());
	return ret - f.offset();
}

} /* namespace rf_tdm */
