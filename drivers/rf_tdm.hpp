
#ifndef __RF_TDM_HPP__
#define __RF_TDM_HPP__

/*
 * Time division multiplex network between devices and coordinator.
 *
 * Implements simple network, transport and session layer.
 * Physical and channel layers shoud be implemented via rf driver.
 *
 * Coordinator planning:
 * ([silence period][slot0][slot1]...[slotN])([silence period][slot0]...)...
 */

#include <stddef.h>
#include <stdint.h>
#include <vector>

#ifdef LPOS10
#include <lplib/lpdefs.h>
#include <LPOS_errors.h>
#endif

namespace rf_tdm {

/* reqired typedefs */
#ifdef LPOS10

typedef ::lp::ssize_t      ssize_t;
typedef ::lp::msec_t       msec_t;
typedef unsigned long long tick_t;

#else /* !LPOS10 */

typedef int                ssize_t;
typedef unsigned int       msec_t;
typedef unsigned long long tick_t;

#endif


/* required error codes */
#ifdef LPOS10

static int const EIO       = -ERR_IO_ERROR;
static int const EINVAL    = -ERR_WRONG_VALUE;
static int const E2BIG     = -ERR_TOO_LONG;
static int const EBUSY     = -ERR_BUSY;
static int const ETIMEDOUT = -ERR_TIME_OUT;
static int const ENOMEM    = -ERR_NO_MEMORY;

#else /* LPOS10 */

static int const EIO       = 1;
static int const EINVAL    = 2;
static int const E2BIG     = 3;
static int const EBUSY     = 4;
static int const ETIMEDOUT = 5;
static int const ENOMEM    = 6;

#endif


struct endpoint
{
	uint8_t lowpan;
	uint8_t dst_addr;
	uint8_t src_addr;
};

class implementation
{
public:
	/* addressing */
	virtual uint8_t coordinator_addr() const = 0;
	virtual uint8_t default_addr() const = 0;
	virtual uint8_t lowpan() const = 0;

	/* timing */
	virtual msec_t  min_period() const = 0;   /* for coordinator only */
	virtual msec_t  silence_time() const = 0; /* for coordinator only */
	virtual msec_t  slot_time() const = 0;    /* for coordinator only */
	virtual void    switch_pause() const { }  /* for coordinator only */

	virtual tick_t  get_ticks() const = 0;
	virtual tick_t  msec_to_ticks(msec_t msec) const = 0;
	virtual tick_t  ticks_to_msec(tick_t ticks) const = 0; /* for coordinator only */

	/* io */
	virtual ssize_t recv(struct endpoint *ep, void *buf, size_t size, msec_t timeout) = 0;
	virtual ssize_t send(struct endpoint const &ep, void const *buf, size_t size) = 0;
};


class trace
{
public:
	struct point
	{
		char     event;     /* event token */
		uint8_t  addr;
		uint16_t ofst_msec; /* time offset of current period */
		uint16_t slpt_msec; /* sleep time sent to device */
		uint16_t diff_msec; /* time after previous event */
	};

	template <size_t N>
	trace(struct point (&array)[N]): array_(array), idx_(0), count_(N),
			tstamp_(0)
		{ }

	void add(implementation &impl, char event, uint8_t addr,
			uint16_t ofst_msec, uint16_t slpt_msec);

private:
	struct point *array_;
	size_t        idx_;
	size_t        count_;
	tick_t        tstamp_;
};

class coordinator
{
public:
	coordinator(implementation &impl, trace *trc = NULL);

	/* get address for an device with @serial */
	virtual int  registry(void const *serial, size_t size) { return -1; }

	/*
	 * received packet callback:
	 * 1. parse (@inbuf, @insize) packet
	 * 2. build response into (@outbuf, @outsize) and return message size
	 */
	virtual ssize_t on_recv(void const *inbuf, size_t insize,
			void *outbuf, size_t outsize)          { return 0; }

	/* main processing method */
	ssize_t listen();

private:
	void    tracev(char event, uint8_t addr, msec_t slpt_msec);

	ssize_t seek(uint8_t addr) const;
	ssize_t add(uint8_t addr);
	msec_t  calc_period() const;
	msec_t  calc_device_delay(size_t idx) const;

	msec_t  calc_remaining_time_in_period() const;
	msec_t  calc_device_sleep_time(size_t idx) const;
	msec_t  calc_silence_sleep_time() const;

	ssize_t process_connect(uint8_t const *req, size_t reqsize,
			uint8_t *resp, size_t respsize);
	ssize_t process_data(uint8_t addr, uint8_t const *req, size_t reqsize,
			uint8_t *resp, size_t respsize);

	implementation  &impl_;
	vector<uint8_t> devs_; /* sorry, O(n) */
	trace          *trace_;
};

class device
{
public:
	device(implementation &impl, trace *trc = NULL);

	/* serial number for connect() conflict prevention */
	virtual ssize_t get_serial(void *serial, size_t size) const { return 0; }

	/* @addr export/import to/from backup memory */
	void    restore(uint8_t addr);
	uint8_t get_addr() const;

	/* connection control */
	bool    connected() const;
	int     connect(msec_t *required_sleep_period, msec_t timeout);

	/* io */
	ssize_t send_and_recv(void const *outbuf, size_t outsize,
			void *inbuf, size_t insize,
			msec_t *required_sleep_period, msec_t timeout);

private:
	void    tracev(char event, uint8_t addr, msec_t slpt_msec);

	ssize_t recv(void *buf, size_t size, msec_t timeout) const;
	ssize_t send(void const *buf, size_t size) const;

	template <typename F>
	ssize_t recv_cyclic(void *buf, size_t size, F &f, msec_t timeout) const;

	implementation &impl_;
	uint8_t         addr_;
	trace          *trace_;
};

} /* namespace rf_tdm */

#endif /* __RF_TDM_HPP__ */
