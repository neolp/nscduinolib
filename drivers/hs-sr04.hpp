#ifndef __HS_SR04_H__
#define __HS_SR04_H__

// The HC-SR04 ultrasonic sensor driver
// It is designed for single pin use
// connect trig pin of HC-SR04 to selected pin and solder 2.2K resistor between trig and echo ping of HC-SR04

#define MEASURE_PHASE_STOP 0
#define MEASURE_PHASE_START 1
#define MEASURE_PHASE_WAIT_FOR_ECHO 2
#define MEASURE_PHASE_WAIT_FOR_ECHO_STOP 3
#define MEASURE_PHASE_PAUSE 4
#define MEASURE_PHASE_DONE 5
#define MEASURE_SINGLE 0
#define MEASURE_CYCLIC 1
#define MEASURE_STOP 2

class hssr04 {
  gpio_pin sonar_trig;
  gpio_pin sonar_echo;
  unsigned int starttime;
  uint8_t measurephase;
  uint8_t measuretype;
  unsigned short _distance;
public:
  
  bool acquired(){
    return sonar_trig.acquired() && sonar_echo.acquired();
  }
  void begin(int trigpin, int echopin){
    sonar_trig.acquire(trigpin,GPIO_OUT_PUSHPULL_50MHz);
    sonar_echo.acquire(echopin,GPIO_IN_PULLDOWN);
    measurephase=0;
    _distance=0;
  }
  void start(int _measuretype){
    measuretype = _measuretype;
    measurephase = MEASURE_PHASE_START;
  }
  void stop(){
    measurephase = MEASURE_PHASE_STOP;
  }
  bool isdone() { // use only for single measurement mode
    return measurephase == MEASURE_PHASE_DONE;
  }
  bool ismeasured() { // use only for single measurement mode
    return measurephase == MEASURE_PHASE_DONE || measurephase == MEASURE_PHASE_PAUSE;
  }
  int distance() {
    return _distance;
  }
  void poll() {
    __istate_t cpsr;
    if (!acquired())
      return;
    switch (measurephase) {
    case MEASURE_PHASE_START:
      cpsr=__get_interrupt_state();
      __disable_interrupt();
      sonar_trig.set();
      for (volatile int i=0;i<25;i++);
      sonar_trig.reset();
      __set_interrupt_state(cpsr);
      starttime=get_us();
      measurephase = MEASURE_PHASE_WAIT_FOR_ECHO;
      break;
    case MEASURE_PHASE_WAIT_FOR_ECHO: //wait for up to start time measurement
      if (sonar_echo.read()) { // echo detection
        starttime=get_us();
        measurephase = MEASURE_PHASE_WAIT_FOR_ECHO_STOP;
      }
      if ( get_us()-starttime > 48000){ // no start of echo detection
        _distance=8001;
        measurephase = MEASURE_PHASE_PAUSE;
      }
      break;
    case MEASURE_PHASE_WAIT_FOR_ECHO_STOP:
      if (sonar_echo.read()==0) { // echo finish detected
        _distance=10*(get_us()-starttime)/58;
        measurephase = MEASURE_PHASE_PAUSE;
      }
      if (get_us()-starttime>49000){ // NO echo detected
        _distance=8000;
        measurephase = MEASURE_PHASE_PAUSE;
      }
      break;
    case MEASURE_PHASE_PAUSE:
      if (get_us()-starttime>50000) {
        if (measuretype == MEASURE_CYCLIC) {
          measurephase = MEASURE_PHASE_START;
        } else if (measuretype == MEASURE_SINGLE) {
          measurephase = MEASURE_PHASE_DONE;
        }
      }
      break;
    }
  }
};


template<int channels> 
class hssr04_array {
  hssr04 sonars[channels];
  uint8_t currentindex;
  uint8_t measuretype;
public:
  hssr04_array():measuretype(MEASURE_STOP){}
  bool isConfigured(){
    for (int i=0;i<channels; i++) {
      if (!sonars[i].acquired()) return false;
    }
    return true;
  }
  void begin( const uint8_t (&sonarpins)[channels*2] ) {
    currentindex = 0;
    for (int i=0;i<channels; i++) {
      sonars[i].begin(sonarpins[i*2],sonarpins[i*2+1]);
    }
  }
  bool isStarted() {
    return measuretype != MEASURE_STOP;
  }
  void start(int _measuretype){
    measuretype = _measuretype;
    currentindex = 0;
    sonars[currentindex].start(MEASURE_SINGLE);
  }
  void stop() {
    currentindex = 0;
    sonars[currentindex].stop();
    measuretype = MEASURE_STOP;
  }
  int distance(unsigned int index) {
    if (index<channels) {
      return sonars[index].distance();
    }
    return 10000; // ??
  }
  void poll(){
    if (measuretype == MEASURE_STOP)
      return;
    if ( sonars[currentindex].isdone() ) {
      currentindex++;
      if (currentindex>=channels)
        currentindex=0;
      if (measuretype == MEASURE_CYCLIC) {
        sonars[currentindex].start(MEASURE_SINGLE);
      } else if (measuretype == MEASURE_SINGLE) {
        ; // do nothing
      }
    }
    sonars[currentindex].poll();
      
  }

};

#endif /* __HS_SR04_H__ */
