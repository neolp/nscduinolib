
#ifndef __TEA_H__
#define __TEA_H__

extern void tea_encrypt(unsigned int *v, unsigned int const *k);
extern void tea_decrypt(unsigned int *v, unsigned int const *k);

extern void tea_encrypt_ofb(char* v, unsigned int size, unsigned int const *k, unsigned int const *ofbIV );
#define tea_decrypt_ofb(v, size, k, ofbIV ) tea_encrypt_ofb(v, size, k, ofbIV )

extern unsigned int tea_encrypt_pcbc(unsigned int *v, unsigned int size, unsigned int const *k);
extern unsigned int tea_decrypt_pcbc(unsigned int *v, unsigned int size, unsigned int const *k);

void tea_encrypt_cfb(char* v, unsigned int size, unsigned int const *k, unsigned int* iv);
void tea_decrypt_cfb(char *v, unsigned int size, unsigned int const *k, unsigned int* iv);

#endif /* __TEA_H__ */