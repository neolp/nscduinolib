/*
 * MedianFilter.hpp
 *
 *  Created on: Jan 19, 2017
 *      Author: tsigulin
 */

#ifndef SMALL_FILTER_HPP_
#define SMALL_FILTER_HPP_

#include <string.h>
#include <stdlib.h>
namespace LPAlg
{
	template <typename T, int Depth>
	class SmallMedianFilter
	{
	private:
		SmallMedianFilter(const SmallMedianFilter<T,Depth> &other);
		unsigned int m_Index;
		unsigned int m_Collected;
		T m_data[Depth];
	public:
		SmallMedianFilter():
			m_Index(0),
			m_Collected(0)
		{
			for (unsigned int i=0; i < Depth; i++)
			{
				m_data[i] = 0;
			}

		}
		void Init()
		{
			m_Index = 0;
			m_Collected = 0;
		}
		~SmallMedianFilter()
		{
		}
		void Push(T value)
		{
			m_data[m_Index] = value;
			++m_Index;
			if (m_Index>=Depth)
			{
				m_Index = 0;
			}
			if (m_Collected < Depth)
			{
				++m_Collected;
			}
		}
		struct t_compare
		{
			static int compare(const void *a,const void *b)
			{
				T v1 = *(const T *)a;
				T v2 = *(const T *)b;
				return v1-v2;
			}

		};
		T GetFilterValue()
		{
		T m_sorted[Depth];
			memcpy(m_sorted,m_data,m_Collected*sizeof(T));
			qsort(m_sorted,m_Collected,sizeof(T),t_compare::compare);
			return m_sorted[m_Collected/2];
		}
	};


}


#endif /* MEDIANFILTER_SLOW_HPP_ */
