
#ifndef __SERIALIZER_HPP__
#define __SERIALIZER_HPP__

#include <stdint.h>

#if defined(LITTLE_ENDIAN)
#define __le
#elif defined(BIG_ENDIAN)
#define __be
#else
#error "unknown endianess"
#endif

namespace serializer_internals {

template <typename T>
inline uint be_shift(uint shift_at_end)
{
	return (sizeof(T) - 1) * 8 - shift_at_end;
}


static inline void put_u8(void **ptr, uint8_t value)
{
	uint8_t * const u8_ptr = static_cast<uint8_t *>(*ptr);
	*u8_ptr = value;
	*ptr    = u8_ptr + 1;
}

template <typename T>
static inline void put_u8_le(void **ptr, T value, uint shift)
{
	put_u8(ptr, value >> shift);
}

template <typename T>
static inline void put_u8_be(void **ptr, T value, uint shift)
{
	put_u8(ptr, value >> be_shift<T>(shift));
}

template <typename T>
inline void put(void **ptr, T value)
{
	T * const t_ptr = static_cast<T *>(*ptr);
	*t_ptr = value;
	*ptr   = static_cast<uint8_t *>(*ptr) + sizeof(T);
}


static inline uint8_t get_u8(void const **ptr)
{
	uint8_t const * const u8_ptr = static_cast<uint8_t const *>(*ptr);
	uint8_t const value = *u8_ptr;
	*ptr = u8_ptr + 1;
	return value;
}

template <typename T>
static inline T get_u8_le(void const **ptr, uint shift)
{
	return static_cast<T>(get_u8(ptr)) << shift;
}

template <typename T>
static inline T get_u8_be(void const **ptr, uint shift)
{
	return static_cast<T>(get_u8(ptr)) << be_shift<T>(shift);
}

template <typename T>
inline T get(void const **ptr)
{
	T const * const t_ptr = static_cast<T const *>(*ptr);
	T const value = *t_ptr;
	*ptr = static_cast<uint8_t const *>(*ptr) + sizeof(T);
	return value;
}

} /* namespace serializer_serializer_internals */


/* ---- put little endian integer ------------------------------------------- */
template <typename T>
inline void *put_le(void *ptr, T value)
{
#ifdef __le
	serializer_internals::put(&ptr, value);
	return ptr;
#else
	serializer_internals::put_u8_le(&ptr, value, 0);
	if (sizeof(T) > 1)
		serializer_internals::put_u8_le(&ptr, value, 8);
	if (sizeof(T) > 2)
	{
		serializer_internals::put_u8_le(&ptr, value, 16);
		serializer_internals::put_u8_le(&ptr, value, 24);
	}
	if (sizeof(T) > 4)
	{
		serializer_internals::put_u8_le(&ptr, value, 32);
		serializer_internals::put_u8_le(&ptr, value, 40);
		serializer_internals::put_u8_le(&ptr, value, 48);
		serializer_internals::put_u8_le(&ptr, value, 56);
	}
	return ptr;
#endif
}

/* ---- put big endian integer ---------------------------------------------- */
template <typename T>
inline void *put_be(void *ptr, T value)
{
#ifdef __be
	serializer_internals::put(&ptr, value);
	return ptr;
#else
	serializer_internals::put_u8_be(&ptr, value, 0);
	if (sizeof(T) > 1)
		serializer_internals::put_u8_be(&ptr, value, 8);
	if (sizeof(T) > 2)
	{
		serializer_internals::put_u8_be(&ptr, value, 16);
		serializer_internals::put_u8_be(&ptr, value, 24);
	}
	if (sizeof(T) > 4)
	{
		serializer_internals::put_u8_be(&ptr, value, 32);
		serializer_internals::put_u8_be(&ptr, value, 40);
		serializer_internals::put_u8_be(&ptr, value, 48);
		serializer_internals::put_u8_be(&ptr, value, 56);
	}
	return ptr;
#endif
}

/* ---- get little endian integer ------------------------------------------- */
template <typename T>
inline void const *get_le(void const *ptr, T *value)
{
#ifdef __le
	*value = serializer_internals::get<T>(&ptr);
	return ptr;
#else
	*value = serializer_internals::get_u8_le<T>(&ptr, 0);
	if (sizeof(T) > 1)
		*value |= serializer_internals::get_u8_le<T>(&ptr, 8);
	if (sizeof(T) > 2)
	{
		*value |= serializer_internals::get_u8_le<T>(&ptr, 16);
		*value |= serializer_internals::get_u8_le<T>(&ptr, 24);
	}
	if (sizeof(T) > 4)
	{
		*value |= serializer_internals::get_u8_le<T>(&ptr, 32);
		*value |= serializer_internals::get_u8_le<T>(&ptr, 40);
		*value |= serializer_internals::get_u8_le<T>(&ptr, 48);
		*value |= serializer_internals::get_u8_le<T>(&ptr, 56);
	}
	return ptr;
#endif
}

template <typename T>
inline T get_le(void const **ptr)
{
	T value;
	*ptr = get_le(*ptr, &value);
	return value;
}

template <typename T>
inline T get_le(void const *ptr)
{
	return get_le<T>(&ptr);
}

/* ---- get big endian integer ---------------------------------------------- */
template <typename T>
inline void const *get_be(void const *ptr, T *value)
{
#ifdef __be
	*value = serializer_internals::get<T>(&ptr);
	return ptr;
#else
	*value = serializer_internals::get_u8_be<T>(&ptr, 0);
	if (sizeof(T) > 1)
		*value |= serializer_internals::get_u8_be<T>(&ptr, 8);
	if (sizeof(T) > 2)
	{
		*value |= serializer_internals::get_u8_be<T>(&ptr, 16);
		*value |= serializer_internals::get_u8_be<T>(&ptr, 24);
	}
	if (sizeof(T) > 4)
	{
		*value |= serializer_internals::get_u8_be<T>(&ptr, 32);
		*value |= serializer_internals::get_u8_be<T>(&ptr, 40);
		*value |= serializer_internals::get_u8_be<T>(&ptr, 48);
		*value |= serializer_internals::get_u8_be<T>(&ptr, 56);
	}
	return ptr;
#endif
}

template <typename T>
inline T get_be(void const **ptr)
{
	T value;
	*ptr = get_be(ptr, &value);
	return value;
}

template <typename T>
inline T get_be(void const *ptr)
{
	return get_be<T>(&ptr);
}

#endif /* __SERIALIZER_HPP__ */
