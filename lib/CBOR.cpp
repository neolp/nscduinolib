#include "CBOR.h"

#if defined(ARM) || defined(LPOS10)
#include <intrinsics.h>

#define htons(n) (__REV16(n))
#define ntohs(n) htons(n)

#define htonl(n) (__REV(n))
#define ntohl(n) htonl(n)

#define htonll(n) (htonl(n>>32) | (unsigned long long)(htonl(n & 0xFFFFFFFF))<<32)
#define ntohll(n) htonll(n)

#else

#define htons(n) (((( (n) & 0xffu) ) << 8) | (((n) & 0xff00u) >> 8))
#define ntohs(n) htons(n)

#define htonl(n) (((( (n) & 0xffu) ) << 24) | ((( (n) & 0xff00u) ) << 8) | (((n) & 0xff000000u) >> 24) | (((n) & 0xff0000u) >> 8))
#define ntohl(n) htonl(n)

#ifndef htonll
	#define htonll(n) (htonl(n>>32) | (unsigned long long)(htonl(n & 0xFFFFFFFF))<<32)
	#define ntohll(n) htonll(n)
#endif
#endif

#include <string.h>
///////////////////////////// CBOR /////////////////////////////////////////////
///////////////////////////// CBOR /////////////////////////////////////////////
///////////////////////////// CBOR /////////////////////////////////////////////

int CBOR_encoder::write_elem(uint32_t tag, uint32_t len) {
  if (len<=0x17) {
    if (_err || index+1 > buflen) {
      _err=ERR_NO_MEMORY;
      return 0;
    }
    _buf[index++]=tag | len;
    return 1;
  } else if (len<=0xFF) {
    if (_err || index+2 > buflen) {
      _err=ERR_NO_MEMORY;
      return 0;
    }
    _buf[index++]=tag | CBOR_LEN_TAG_1;
    _buf[index++]=len;
    return 2;
  } else if (len<=0xFFFF) {
    if (_err || index+3 > buflen) {
      _err=ERR_NO_MEMORY;
      return 0;
    }
    _buf[index++]=tag | CBOR_LEN_TAG_2;
    _buf[index++]=len>>8;
    _buf[index++]=len&0xFF;
    return 3;
  } else if (len==0xFFFFFFFF) {
    if (_err || index+1 > buflen) {
      _err=ERR_NO_MEMORY;
      return 0;
    }
    _buf[index++]=tag | CBOR_LEN_TAG_BREAK;
    return 1;
  } else {
    if (_err || index+5 > buflen) {
      _err=ERR_NO_MEMORY;
      return 0;
    }
    _buf[index++]=tag | CBOR_LEN_TAG_4;
    _buf[index++]=len>>24;
    _buf[index++]=len>>16;
    _buf[index++]=len>>8;
    _buf[index++]=len;
    return 5;
  }
}

int CBOR_encoder::modify_elem(int stored_index, uint32_t len) {
  // process initial bytes
  uint32_t ib,tag, ai;
  ib = _buf[stored_index];
  tag = ib & 0xe0;
  ai = ib & 0x1f;

  if (ai<24) {// length inline
    _buf[stored_index]=tag | len;
  } else if (ai==24) {
    _buf[stored_index+1]=len;
  } else if (ai==25) {
    _buf[stored_index+1]=len>>8;
    _buf[stored_index+2]=len;
  } else if (ai==26) {
    _buf[stored_index+1]=len>>24;
    _buf[stored_index+2]=len>>16;
    _buf[stored_index+3]=len>>8;
    _buf[stored_index+4]=len;
//  } else if (ai==27) {
  }
  return 0;
}

int CBOR_encoder::write_map(uint32_t elementsnum) {
  return write_elem(CBOR_MAP_TAG, elementsnum);
}

int CBOR_encoder::write_arr(uint32_t elementsnum) {
  return write_elem(CBOR_ARR_TAG, elementsnum);
}

int CBOR_encoder::write_buf(const void* buf, int buflen) { // raw write previously encoded buffer
  if (_err || freelen() < buflen) {
    _err=ERR_NO_MEMORY;
    return 0;
  }
  memmove(_buf+index, buf, buflen);
  index+=buflen;
  return buflen;
}

int CBOR_encoder::write_bstr(const void* str, int len) {
  int prefix = write_elem(CBOR_BSTR_TAG, len);
  if (_err || freelen()<len) {
    _err=ERR_NO_MEMORY;
    return 0;
  }
  memcpy(_buf+index, str, len);
  index+=len;
  return prefix+len;
}


int CBOR_encoder::write_str(const char* str, int ilen) {
  int len=w1251_to_utf8_length(str, ilen);
  int prefix = write_elem(CBOR_STR_TAG, len);
  if (_err || freelen()<len) {
    _err=ERR_NO_MEMORY;
    return 0;
  }
  w1251_to_utf8_encode(_buf+index, len, str);
  index+=len;
  return prefix+len;
}

int CBOR_encoder::write_str(const char* str) {
  int len=w1251_to_utf8_length(str);
  int prefix = write_elem(CBOR_STR_TAG, len);
  if (_err || freelen()<len) {
    _err=ERR_NO_MEMORY;
    return 0;
  }
  w1251_to_utf8_encode(_buf+index, len, str);
  index+=len;
  return prefix+len;
}

int CBOR_encoder::write_rawstr(const char* str) { //CBOR_write_str(char* buf, int buflen, const char* str, int len) {
  int len=strlen(str);
  int prefix = write_elem(CBOR_STR_TAG, len);
  if (_err || freelen()<len) {
    _err=ERR_NO_MEMORY;
    return 0;
  }
  memcpy (_buf+index, str, len);
  index+=len;
  return prefix+len;
}

/*
int CBOR_encoder::write_rawstr(const char* str, int len) {
  int prefix = write_elem(CBOR_STR_TAG, len);
  memcpy (buf+prefix, str, len);
  index+=len;
  return prefix+len;
}
*/

int CBOR_encoder::write_int(int32_t n) {
  int32_t ui = n >> 31;    // extend sign to whole length
  uint32_t mt = ui & CBOR_NINT_TAG; // 0x20  extract major type
  ui ^= n;                 // complement negatives

  return write_uint(ui, mt);
}

int CBOR_encoder::write_uint(uint32_t n, uint8_t tag) {
  /*
   * q: what about index range check?
   * a: if hardfault will be occurred, buffer is overflow!
   */
  if (n < 24) {
    if (_err || index+1 > buflen) {
      _err=ERR_NO_MEMORY;
      return 0;
    }
    _buf[index++] = tag | n;
    return 1;
  } else if (n < 256) {
    if (_err || index+2 > buflen) {
      _err=ERR_NO_MEMORY;
      return 0;
    }
    _buf[index++] = tag | 24;
    _buf[index++] = n;
    return 2;
  } else if (n < 65536) {
    if (_err || index+3 > buflen) {
      _err=ERR_NO_MEMORY;
      return 0;
    }
    _buf[index++] = tag | 25;
    _buf[index++] = n >> 8;
    _buf[index++] = n & 0xFF;
    return 3;
  } else {
    if (_err || index+5 > buflen) {
      _err=ERR_NO_MEMORY;
      return 0;
    }
    _buf[index++] = tag | 26;
    _buf[index++] = n >> 24;
    _buf[index++] = n >> 16;
    _buf[index++] = n >> 8;
    _buf[index++] = n;
    return 5;
  }
}

int CBOR_decoder::is_arr() {
  const char* b=buf;
  // process initial bytes
  uint32_t ib,mt,ai;

  ib = *b++;
  mt = ib >> 5;
  ai = ib & 0x1f;
  if (mt!=CBOR_ARR_MT) {
    return -TYPE_ERROR_CODE;
  }
  if (ai<24) {// length inline
    return ai;
  } else if (ai==24) {
    ai=*b++;
    return ai;
  } else if (ai==25) {
#if __CORE__ == 7 // only for cortex m3
    ai=ntohs(*((unsigned short*)b));
#else
    ai=(b[0]<<8) | b[1];
#endif
    return ai;
  } else if (ai==26) {
#if __CORE__ == 7 // only for cortex m3
    ai=ntohl(*((unsigned int*)b));
#else
    ai=(b[0]<<24) | (b[1]<<16) | (b[2]<<8) | b[3];
#endif
    return ai;
//  } else if (ai==27) {
//    ai=htonll(*((unsigned long long*)b));
//    return ai;
  } else {
    return -FORMAT_ERROR_CODE;
  }
}

int CBOR_decoder::read_arr() {
  const char* b=buf;
  // process initial bytes
  uint32_t ib,mt,ai;

  ib = *b++;
  mt = ib >> 5;
  ai = ib & 0x1f;
  if (mt!=CBOR_ARR_MT) {
    _err=-TYPE_ERROR_CODE; return -TYPE_ERROR_CODE;
  }
  if (ai<24) {// length inline
    buf=b;
    return ai;
  } else if (ai==24) {
    ai=*b++;
    buf=b;
    return ai;
  } else if (ai==25) {
#if __CORE__ == 7 // only for cortex m3
    ai=ntohs(*((unsigned short*)b));
#else
    ai=(b[0]<<8) | b[1];
#endif
    b+=2;
    buf=b;
    return ai;
  } else if (ai==26) {
#if __CORE__ == 7 // only for cortex m3
    ai=ntohl(*((unsigned int*)b));
#else
    ai=(b[0]<<24) | (b[1]<<16) | (b[2]<<8) | b[3];
#endif
    b+=4;
    buf=b;
    return ai;
//  } else if (ai==27) {
//    ai=htonll(*((unsigned long long*)b));
//    b+=8;
//    *buf=b;
//    return ai;
  } else {
    _err=-FORMAT_ERROR_CODE; return -FORMAT_ERROR_CODE;
  }
}















int CBOR_decoder::is_map() {
  const char* b=buf;
  // process initial bytes
  uint32_t ib,mt,ai;

  ib = *b++;
  mt = ib >> 5;
  ai = ib & 0x1f;
  if (mt!=CBOR_MAP_MT) {
    return -TYPE_ERROR_CODE;
  }
  if (ai<24) {// length inline
    return ai;
  } else if (ai==24) {
    ai=*b++;
    return ai;
  } else if (ai==25) {
#if __CORE__ == 7 // only for cortex m3
    ai=ntohs(*((unsigned short*)b));
#else
    ai=(b[0]<<8) | b[1];
#endif
    return ai;
  } else if (ai==26) {
#if __CORE__ == 7 // only for cortex m3
    ai=ntohl(*((unsigned int*)b));
#else
    ai=(b[0]<<24) | (b[1]<<16) | (b[2]<<8) | b[3];
#endif
    return ai;
//  } else if (ai==27) {
//    ai=htonll(*((unsigned long long*)b));
//    return ai;
  } else {
    return -FORMAT_ERROR_CODE;
  }
}

int CBOR_decoder::read_map() {
  const char* b=buf;
  // process initial bytes
  uint32_t ib,mt,ai;

  ib = *b++;
  mt = ib >> 5;
  ai = ib & 0x1f;
  if (mt!=CBOR_MAP_MT) {
    _err=-TYPE_ERROR_CODE; return 0;
  }
  if (ai<24) {// length inline
    buf=b;
    return ai;
  } else if (ai==24) {
    ai=*b++;
    buf=b;
    return ai;
  } else if (ai==25) {
#if __CORE__ == 7 // only for cortex m3
    ai=ntohs(*((unsigned short*)b));
#else
    ai=(b[0]<<8) | b[1];
#endif
    b+=2;
    buf=b;
    return ai;
  } else if (ai==26) {
#if __CORE__ == 7 // only for cortex m3
    ai=ntohl(*((unsigned int*)b));
#else
    ai=(b[0]<<24) | (b[1]<<16) | (b[2]<<8) | b[3];
#endif
    b+=4;
    buf=b;
    return ai;
//  } else if (ai==27) {
//    ai=htonll(*((unsigned long long*)b));
//    b+=8;
//    *buf=b;
//    return ai;
  } else {
    _err=-FORMAT_ERROR_CODE; return 0;
  }
}


uint64_t CBOR_decoder::read_int() {
  const char* b=buf;
  // process initial bytes
  uint32_t ib,mt,ai;
  uint64_t val;

  ib = *b++;
  mt = ib >> 5;
  val = ai = ib & 0x1f;
  switch (ai) {
    case 24: val = *b++; break;
    case 25: val = (b[0]<<8) | b[1]; b+=2; break;
    case 26: val = (b[0]<<24) | (b[1]<<16) | (b[2]<<8) | b[3]; b+=4; break;
    case 27: val = ((long long)b[0]<<(32+24)) | ((long long)b[1]<<(16+32)) | ((long long)b[2]<<(8+32)) | ((long long)b[3]<<32) | (b[4]<<24) | (b[5]<<16) | (b[6]<<8) | b[7]; b+=4; b+=8; break;
    case 28: case 29: case 30: case 31: _err=-1; return 0;
  }
  if (mt==0) { buf=b; return val; }
  if (mt==1) { buf=b; return ~val; }
  _err=-TYPE_ERROR_CODE; return 0;
}

bool CBOR_decoder::is_bool(){
  const char* b=buf;
  uint32_t mt;

  mt = (*b);
  if (mt==CBOR_FALSE_TAG) { return true; }
  if (mt==CBOR_TRUE_TAG) { return true; }
  return false;
}
bool CBOR_decoder::read_bool(){
  const char* b=buf;
  uint32_t ib = *b++;
  if (ib==CBOR_FALSE_TAG) { buf=b; return false; }
  if (ib==CBOR_TRUE_TAG) { buf=b; return true; }
  _err=-TYPE_ERROR_CODE; return false;
}

bool CBOR_decoder::is_int() {
  const char* b=buf;
  uint32_t mt;

  mt = (*b) >> 5;
  if (mt==0) { return true; }
  if (mt==1) { return true; }
  return false;
}

int CBOR_decoder::is_str() {
  const char* b=buf;
  // process initial bytes
  uint32_t ib,mt,ai;

  ib = *b++;
  mt = ib >> 5;
  ai = ib & 0x1f;
  if (mt!=CBOR_STR_MT) {
    return -TYPE_ERROR_CODE;
  }
  if (ai<24) {// length inline
    return ai;
  } else if (ai==24) {
    ai=*b++;
    return ai;
  } else if (ai==25) {
#if __CORE__ == 7 // only for cortex m3
    ai=ntohs(*((unsigned short*)b));
#else
    ai=(b[0]<<8) | b[1];
#endif
    return ai;
  } else if (ai==26) {
#if __CORE__ == 7 // only for cortex m3
    ai=ntohl(*((unsigned int*)b));
#else
    ai=(b[0]<<24) | (b[1]<<16) | (b[2]<<8) | b[3];
#endif
    return ai;
//  } else if (ai==27) {
//    ai=htonll(*((unsigned long long*)b));
//    return ai;
  } else {
    return -FORMAT_ERROR_CODE;
  }
}


// read string from CBOR stream, move buf pointer and store string start if success, return string length
int CBOR_decoder::read_str(cpchar* str) {
  const char* b=buf;
  // process initial bytes
  uint32_t ib,mt,ai;

  ib = *b++;
  mt = ib >> 5;
  ai = ib & 0x1f;
  if (mt!=CBOR_STR_MT) {
    _err=-TYPE_ERROR_CODE; return 0;
  }
  if (ai<24) {// length inline
    *str=b;
    buf=b+ai;
    return ai;
  } else if (ai==24) {
    ai=*b++;
    *str=b;
    buf=b+ai;
    return ai;
  } else if (ai==25) {
#if __CORE__ == 7 // only for cortex m3
    ai=ntohs(*((unsigned short*)b));
#else
    ai=(b[0]<<8) | b[1];
#endif
    b+=2;
    *str=b;
    buf=b+ai;
    return ai;
  } else if (ai==26) {
#if __CORE__ == 7 // only for cortex m3
    ai=ntohl(*((unsigned int*)b));
#else
    ai=(b[0]<<24) | (b[1]<<16) | (b[2]<<8) | b[3];
#endif
    b+=4;
    *str=b;
    buf=b+ai;
    return ai;
//  } else if (ai==27) {
//    ai=htonll(*((unsigned long long*)b));
//    b+=8;
//    *str=b;
//    *buf=b+ai;
//    return ai;
  } else {
    _err=-FORMAT_ERROR_CODE; return 0;
  }
}

int CBOR_decoder::is_bstr() {
  const char* b=buf;
  // process initial bytes
  uint32_t ib,mt,ai;

  ib = *b++;
  mt = ib >> 5;
  ai = ib & 0x1f;
  if (mt!=CBOR_BSTR_MT) {
    return -TYPE_ERROR_CODE;
  }
  if (ai<24) {// length inline
    return ai;
  } else if (ai==24) {
    ai=*b++;
    return ai;
  } else if (ai==25) {
#if __CORE__ == 7 // only for cortex m3
    ai=ntohs(*((unsigned short*)b));
#else
    ai=(b[0]<<8) | b[1];
#endif
    return ai;
  } else if (ai==26) {
#if __CORE__ == 7 // only for cortex m3
    ai=ntohl(*((unsigned int*)b));
#else
    ai=(b[0]<<24) | (b[1]<<16) | (b[2]<<8) | b[3];
#endif
    return ai;
//  } else if (ai==27) {
//    ai=htonll(*((unsigned long long*)b));
//    return ai;
  } else {
    return -FORMAT_ERROR_CODE;
  }
}


// read string from CBOR stream, move buf pointer and store string start if success, return string length
int CBOR_decoder::read_bstr(cpchar* str) {
  const char* b=buf;
  // process initial bytes
  uint32_t ib,mt,ai;

  ib = *b++;
  mt = ib >> 5;
  ai = ib & 0x1f;
  if (mt!=CBOR_BSTR_MT) {
    _err=-TYPE_ERROR_CODE; return -TYPE_ERROR_CODE;
  }
  if (ai<24) {// length inline
    *str=b;
    buf=b+ai;
    return ai;
  } else if (ai==24) {
    ai=*b++;
    *str=b;
    buf=b+ai;
    return ai;
  } else if (ai==25) {
#if __CORE__ == 7 // only for cortex m3
    ai=ntohs(*((unsigned short*)b));
#else
    ai=(b[0]<<8) | b[1];
#endif
    b+=2;
    *str=b;
    buf=b+ai;
    return ai;
  } else if (ai==26) {
#if __CORE__ == 7 // only for cortex m3
    ai=ntohl(*((unsigned int*)b));
#else
    ai=(b[0]<<24) | (b[1]<<16) | (b[2]<<8) | b[3];
#endif
    b+=4;
    *str=b;
    buf=b+ai;
    return ai;
//  } else if (ai==27) {
//    ai=htonll(*((unsigned long long*)b));
//    b+=8;
//    *str=b;
//    *buf=b+ai;
//    return ai;
  } else {
    _err=-FORMAT_ERROR_CODE; return -FORMAT_ERROR_CODE;
  }
}
