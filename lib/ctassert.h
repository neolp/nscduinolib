
#ifndef __COMPILE_TIME_ASSERT_H__
#define __COMPILE_TIME_ASSERT_H__

#define __ctassert_paste__(a, b, c) a##_##b##_##c
#define __ctassert_paste(a, b, c)   __ctassert_paste__(a, b, c)

#define ctassert(cond, module_token) \
	typedef int __ctassert_paste(__ctassert, module_token, __LINE__)[cond ? 1 : -1]

#endif /* __COMPILE_TIME_ASSERT_H__ */
