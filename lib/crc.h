
#ifndef __CRC_H__
#define __CRC_H__

#include <stdint.h>
#include <stddef.h>

typedef unsigned char byte;

unsigned char crc8_get(void const *data, int ln);
extern unsigned char const crc8_table[256];

#define docrc8(oldcrc, b) (crc8_table[(byte)((byte)(oldcrc))^((byte)(b))])
byte docrc8buf(const byte* data, int ln, byte crc);
byte docrc8buf(byte const *data, int ln);


extern unsigned char const crc8_table[256];
#define docrc8(oldcrc, b) (crc8_table[(byte)((byte)(oldcrc))^((byte)(b))])
byte docrc8buf(const byte* data, int ln, byte crc);
/*{
	for (size_t i = 0; i < ln; i++)
		crc = docrc8(crc, data[i]);
	return crc;
}*/



//static byte docrc8(byte crc, byte data);
byte docrc8buf(byte const *data, int ln);

uint16_t crc16_ccitt_get(uint8_t const *data, size_t ln);

#endif /* __CRC_H__ */
