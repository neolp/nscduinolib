#ifndef __CBOR_H__
#define __CBOR_H__

#include <stddef.h>
#include <stdint.h>

#define CBOR_TAG_MASK 0xE0

#define CBOR_PINT_TAG 0x00
#define CBOR_NINT_TAG 0x20
#define CBOR_BSTR_TAG 0x40
#define CBOR_STR_TAG  0x60
#define CBOR_ARR_TAG  0x80
#define CBOR_MAP_TAG  0xA0
#define CBOR_TAG_TAG  0xC0
#define CBOR_SMPL_TAG 0xE0

#define CBOR_PINT_MT 0
#define CBOR_NINT_MT 1
#define CBOR_BSTR_MT 2
#define CBOR_STR_MT  3
#define CBOR_ARR_MT  4
#define CBOR_MAP_MT  5
#define CBOR_TAG_MT  6
#define CBOR_SMPL_MT 7

#define CBOR_FALSE_TAG 0xF4
#define CBOR_TRUE_TAG 0xF5
#define CBOR_NULL_TAG 0xF6
#define CBOR_UNDEF_TAG 0xF7

#define CBOR_FLOAT16_TAG 0xF9
#define CBOR_FLOAT32_TAG 0xFA
#define CBOR_FLOAT64_TAG 0xFB
#define CBOR_BREAK_TAG 0xFF

#define CBOR_LEN_TAG_1  0x18
#define CBOR_LEN_TAG_2  0x19
#define CBOR_LEN_TAG_4  0x1A
#define CBOR_LEN_TAG_BREAK  0x1F

#ifdef LPOS
	#include <errors.h>
	#include <lposopt.h>
#endif
#ifdef LPOS10
  #include <LPOS_errors.h>
#endif
#ifdef NSCDUINO
  #define ERR_NO_MEMORY -1
#endif

#ifndef ERR_NO_MEMORY
  #define ERR_NO_MEMORY -1
#endif

#define FORMAT_ERROR_CODE 419
#define TYPE_ERROR_CODE 420

class CBOR_encoder;
class CBOR_decoder;

class CBOR_encoder {
  char* _buf;
  int buflen;
  int index;
  int _err;

  int write_elem(uint32_t tag, uint32_t len);
public:
  CBOR_encoder(char* b,int blen) {
    _buf=b; buflen=blen; index=0; _err=0;
  }
  int write_buf(const void* buf, int buflen); // raw write previously encoded buffer
  int write_map(uint32_t elementsnum);
  int write_arr(uint32_t elementsnum);
  int write_rawstr(const char* str);
  int write_str(const char* str);
  int write_str(const char* str, int len);
  int write_bstr(const void* str, int len);
  int write_int(int32_t n);
  int write_uint(uint32_t n, uint8_t tag = CBOR_PINT_TAG);
  int modify_elem(int stored_index, uint32_t elementsnum);
  int length() {
    return index;
  }
  int freelen() {
    return buflen-index;
  }
  char* position() {
    return _buf + index;
  }
  int err() {
    return _err;
  }
  char* buf() {
    return _buf;
  }
  bool ok() {
    return _err==0;
  }
  void seterror(int err) {
    _err = err;
  }
  int error() {
    return _err;
  }

};

class CBOR_decoder {
  int _err;
  const char* buf;
  const char* bufend;
public:
  CBOR_decoder(const void* b, int blen) { _err=0; buf=(const char*)b; bufend=(const char*)b+blen; }

  bool ok() {
    return _err==0;
  }
  int error() {
    return _err;
  }
  void reset_error() {
    _err=0;
  }
  typedef const char* cpchar;
  int is_arr(); // >=0 if nest element is array and retirn their length   <0 if next element is not string
  int read_arr();
  int is_map(); // >=0 if nest element is array and retirn their length   <0 if next element is not string
  int read_map();
  bool is_int();
  uint64_t read_int();
  bool is_bool();
  bool read_bool();
  int read_str(cpchar* str);
  int is_str(); // >=0 if nest element is string and retirn their length   <0 if next element is not string
  int read_bstr(cpchar* str);
  int is_bstr(); // >=0 if nest element is byte string and retirn their length   <0 if next element is not byte string

  int restmsg() {
    return bufend-buf;
  }
  const char* position() {
    return buf;
  }
};

int w1251_to_utf8_length(const char* source);
int w1251_to_utf8_length(const char* source, int sourcelen);
int w1251_to_utf8_encode(char* buf, int buflen, const char* source);
int utf8_to_w1251_decode(const char* utf8, char* windows1251, int n);

#endif
