
#ifndef __DEFS_HPP__
#define __DEFS_HPP__

#include <stddef.h>
#include <stdint.h>

typedef          int ssize_t;
typedef unsigned int uint;

#endif /* __DEFS_HPP__ */
