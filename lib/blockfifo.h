#ifndef LPOS_BLOCK_FOFO_H
#define LPOS_BLOCK_FOFO_H
#include <string.h>

template<int size>
class block_fifo
	{
	volatile unsigned int wr;
	volatile unsigned int rd;
	char data[size];
public:
	void init() { wr=rd=0; }
  void clear() { wr=rd=0; }
	int write(char d)
		{
		unsigned int wrt = wr;
		unsigned int rdt = rd;
		unsigned int freelen = wrt>=rdt ? size-wrt+rdt-1 : rdt-wrt-1;
		if (freelen<1) return 0;

    data[wrt]=d;
		wrt++; while (wrt>=size) wrt-=size;
		wr=wrt;
		return 1;
		}
	int write(const void* buf, unsigned int len)
		{
		unsigned int wrt = wr;
		unsigned int rdt = rd;
		unsigned int freelen = wrt>=rdt ? size-wrt+rdt-1 : rdt-wrt-1;
		if (len>freelen) len=freelen;
		if (len==0) return 0;

		unsigned int slen=size-wrt; // trailer size
		if (len<=slen) memcpy(&data[wrt],buf,len);
		else
			{
			memcpy(&data[wrt],buf,slen);
			memcpy(&data[0],(char*)buf+slen,len-slen);
			}
		wrt+=len; while (wrt>=size) wrt-=size;
		wr=wrt;
		return len;
		}

	int getwritecontbuf(void** buf, unsigned int len)
		{
		unsigned int wrt = wr;
		unsigned int rdt = rd;
		unsigned int freelen = wrt>=rdt ? size-wrt+rdt-1 : rdt-wrt-1;
		if (len>freelen) len=freelen;
		if (len==0) return 0;

		unsigned int slen=size-wrt; // trailer size
		if (len<=slen) 
      {
      *buf=&data[wrt];
      return len;
      }
		else
			{
      *buf=&data[wrt];
			return slen;
			}
		}

	int writecontbuffinish(unsigned int len)
		{
		unsigned int wrt = wr;
		unsigned int rdt = rd;
		unsigned int freelen = wrt>=rdt ? size-wrt+rdt-1 : rdt-wrt-1;
		if (len>freelen) len=freelen;
		if (len==0) return 0;

		wrt+=len; while (wrt>=size) wrt-=size;
		wr=wrt;
		return len;
		}

	int read()
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
    if (rdt==wrt) return -1;
    int ret=data[rdt];
		rdt++; while (rdt>=size) rdt-=size;
		rd=rdt;
		return ret;
		}

	int get2(void* buf,unsigned int offset, unsigned int len)
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
		unsigned int datalen = wrt>=rdt ? wrt-rdt : size-rdt+wrt;

		if (offset>datalen) return 0;
		if (offset+len>datalen) len=datalen-offset;
		if (len==0) return 0;

		rdt += offset; while (rdt>=size) rdt-=size;

		unsigned int slen=size-rdt;
		if (len<=slen) memcpy(buf,&data[rdt],len);
		else
			{
			memcpy(buf,&data[rdt],slen);
			memcpy((char*)buf+slen,&data[0],len-slen);
			}
		return len;
		}

 	int getreadcontbuf(void** buf,unsigned int offset, unsigned int len)
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
		unsigned int datalen = wrt>=rdt ? wrt-rdt : size-rdt+wrt;

		if (offset>datalen) return 0;
		if (offset+len>datalen) len=datalen-offset;
		if (len==0) return 0;

		rdt += offset; while (rdt>=size) rdt-=size;

		unsigned int slen=size-rdt;
		if (len<=slen) 
      { *buf = &data[rdt]; return len; }
		else
			{ *buf = &data[rdt]; return slen; }
		}


	char peek(unsigned int offset)
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
		unsigned int datalen = wrt>=rdt ? wrt-rdt : size-rdt+wrt;

		if (offset>=datalen) return 0;
		rdt += offset; while (rdt>=size) rdt-=size;
    return  data[rdt];
		}

	char peek()
		{
    return data[rd];
		}

	bool isempty()
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
    return rdt==wrt;
		}
  
	int read(void* buf, unsigned int len)
		{
		len=get2(buf,0,len);
		unsigned int rdt = rd;
		rdt+=len; while (rdt>=size) rdt-=size;
		rd=rdt;
		return len;
		}

	int skip(unsigned int len)
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
		unsigned int datalen = wrt>=rdt ? wrt-rdt : size-rdt+wrt;
		if (len>datalen) len=datalen;
		rdt+=len; while (rdt>=size) rdt-=size;
		rd=rdt;
		return len;
		}

	int skipone()
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
		if (rdt!=wrt)
      {
      rdt+=1; while (rdt>=size) rdt-=size;
      rd=rdt;
      return 1;
      }
    return 0;
		}
		
	int datalen()
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
		unsigned int datalen = wrt>=rdt ? wrt-rdt : size-rdt+wrt;
		return datalen;
		}

	int freelen()
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
		unsigned int freelen = wrt>=rdt ? size-wrt+rdt-1 : rdt-wrt-1;
		return freelen;
		}
	};

class sized_block_fifo
	{
  int size;
	volatile unsigned int wr;
	volatile unsigned int rd;
	char* data;
  bool dynamic;
public:
  sized_block_fifo()
    { data = 0; size=0; dynamic=false; wr=rd=0; }
  sized_block_fifo(int _size)
    {
    if (_size>0) { data = new char[_size]; } else { data = 0; }
    if (data) { dynamic=true; } else { dynamic=false;} 
    size=_size; wr=rd=0; 
    }
  sized_block_fifo(char* buf, int _size)
    { data = buf; size=_size; dynamic=false; wr=rd=0; }
  bool valid() { return data!=0; }
  ~sized_block_fifo()
    { if (dynamic) delete data; }
	void init(int _size) 
    {
    if (dynamic) delete data;
    if (_size>0) { data = new char[_size]; } else { data = 0; }
    if (data) { dynamic=true; } else { dynamic=false;} 
    size=_size;
    wr=rd=0; 
    }
	void init(char* buf, int _size) 
    {
    if (dynamic) delete data; 
    data = buf; size=_size; dynamic=false; wr=rd=0; 
    }
	int write(const void* buf, unsigned int len)
		{
		unsigned int wrt = wr;
		unsigned int rdt = rd;
		unsigned int freelen = wrt>=rdt ? size-wrt+rdt-1 : rdt-wrt-1;
		if (len>freelen) len=freelen;
		if (len==0) return 0;

		unsigned int slen=size-wrt; // trailer size
		if (len<=slen) memcpy(&data[wrt],buf,len);
		else
			{
			memcpy(&data[wrt],buf,slen);
			memcpy(&data[0],(char*)buf+slen,len-slen);
			}
		wrt+=len; while (wrt>=size) wrt-=size;
		wr=wrt;
		return len;
		}

	int get2(void* buf,unsigned int offset, unsigned int len)
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
		unsigned int datalen = wrt>=rdt ? wrt-rdt : size-rdt+wrt;

		if (offset>datalen) return 0;
		if (offset+len>datalen) len=datalen-offset;
		if (len==0) return 0;

		rdt += offset; while (rdt>=size) rdt-=size;

		unsigned int slen=size-rdt;
		if (len<=slen) memcpy(buf,&data[rdt],len);
		else
			{
			memcpy(buf,&data[rdt],slen);
			memcpy((char*)buf+slen,&data[0],len-slen);
			}
		return len;
		}

	int gettofifo(sized_block_fifo* to, unsigned int offset, unsigned int len)
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
		unsigned int datalen = wrt>=rdt ? wrt-rdt : size-rdt+wrt;

		if (offset>datalen) return 0;
		if (offset+len>datalen) len=datalen-offset;
		if (len==0) return 0;

		rdt += offset; while (rdt>=size) rdt-=size;

		unsigned int slen=size-rdt;
		if (len<=slen) to->write(&data[rdt],len);
		else
			{
			to->write(&data[rdt],slen);
			to->write(&data[0],len-slen);
			}
		return len;
    }

	char peek(unsigned int offset)
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
		unsigned int datalen = wrt>=rdt ? wrt-rdt : size-rdt+wrt;

		if (offset>=datalen) return 0;
		rdt += offset; while (rdt>=size) rdt-=size;
    return  data[rdt];
		}

	char peek()
		{
    return data[rd];
		}

	bool isempty()
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
    return rdt==wrt;
		}
  
	int read(void* buf, unsigned int len)
		{
		len=get2(buf,0,len);
		unsigned int rdt = rd;
		rdt+=len; while (rdt>=size) rdt-=size;
		rd=rdt;
		return len;
		}

	int skip(unsigned int len)
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
		unsigned int datalen = wrt>=rdt ? wrt-rdt : size-rdt+wrt;
		if (len>datalen) len=datalen;
		rdt+=len; while (rdt>=size) rdt-=size;
		rd=rdt;
		return len;
		}

	int skipone()
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
		if (rdt!=wrt)
      {
      rdt+=1; while (rdt>=size) rdt-=size;
      rd=rdt;
      return 1;
      }
    return 0;
		}
		
	int capacity()
		{
		return size>1?size-1:0;
		}

	int datalen()
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
		unsigned int datalen = wrt>=rdt ? wrt-rdt : size-rdt+wrt;
		return datalen;
		}

	int freelen()
		{
		unsigned int rdt = rd;
		unsigned int wrt = wr;
		unsigned int freelen = wrt>=rdt ? size-wrt+rdt-1 : rdt-wrt-1;
		return freelen;
		}
	};





//TODO: fix adding last element. (fails)
template<class T, int size>
class fifo
{
	volatile T* wr;
	volatile T* rd;
	T data[size];
public:
	void init() { wr=data; rd=data; }
	int write(T dt)
	{
		volatile T* wrt=wr;
		wrt++;
		if (wrt>=&data[size])
			wrt-=size;
		if (wrt!=rd)
		{
			*wr=dt;
			wr=wrt;
			return 1;
		}
		return 0;
	}

	int get(T* pdt, int offset)
		{
		volatile T* rdt = rd;
		volatile T* wrt = wr;
		unsigned int datalen = wrt>=rdt ? wrt-rdt : wrt+size-rdt; //size-rdt+wrt
		if (offset>=datalen) return 0;
		rdt += offset; if (rdt>=&data[size]) rdt-=size;
		*pdt=*rdt;
		return 1;
		}

	int read(T* pdt)
		{
		volatile T* rdt=rd;
		if (rdt!=wr)
			{
			*pdt=*rdt; 
			rdt++; if (rdt>=&data[size]) rdt-=size;
			rd=rdt;
			return 1;
			}
		return 0;
		}
	T readv()
		{
		volatile T* rdt=rd;
		T ret;
		if (rdt!=wr)
			{
			ret=*rdt; 
			rdt++; if (rdt>=&data[size]) rdt-=size;
			rd=rdt;
			return ret;
			}
		return ret;
		}

	int skip(unsigned int len)
		{
		volatile T* rdt = rd;
		volatile T* wrt = wr;
		unsigned int datalen = wrt>=rdt ? wrt-rdt : size-rdt+wrt;
		if (len>datalen) len=datalen;
		rdt+=len; if (rdt>=&data[size]) rdt-=size;
		rd=rdt;
		return len;
		}
		
	int datalen()
		{
		volatile T* rdt = rd;
		volatile T* wrt = wr;
		unsigned int datalen = wrt>=rdt ? wrt-rdt : wrt+size-rdt; //size-rdt+wrt;
		return datalen;
		}

	int freelen()
	{
		volatile T* rdt = rd;
		volatile T* wrt = wr;
		unsigned int freelen;
		if (wrt>=rdt)
		{
			freelen=size+rdt-wrt-1;
		} else
		{
			freelen=rdt-wrt-1;
		}
		return freelen;
	}
};

	
#endif

