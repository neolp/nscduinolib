
#include "tea.h"

void tea_encrypt(unsigned int *v, unsigned int const *k)
{
	unsigned int v0 = v[0];
	unsigned int v1 = v[1];
	unsigned int sum = 0;
	unsigned int const delta = 0x9E3779B9;
	unsigned int k0 = k[0];
	unsigned int k1 = k[1];
	unsigned int k2 = k[2];
	unsigned int k3 = k[3];

	for (unsigned char i = 0; i < 32; i++)
	{
		sum += delta;
		v0 += ((v1 << 4) + k0) ^ (v1 + sum) ^ ((v1 >> 5) + k1);
		v1 += ((v0 << 4) + k2) ^ (v0 + sum) ^ ((v0 >> 5) + k3);
	}
	v[0] = v0;
	v[1] = v1;
}

void tea_decrypt(unsigned int *v, unsigned int const *k)
{
	unsigned int v0 = v[0];
	unsigned int v1 = v[1];
	unsigned int sum = 0xC6EF3720;
	unsigned int const delta = 0x9E3779B9;
	unsigned int k0 = k[0];
	unsigned int k1 = k[1];
	unsigned int k2 = k[2];
	unsigned int k3 = k[3];

	for (unsigned char i = 0; i < 32; i++)
	{
		v1 -= ((v0 << 4) + k2) ^ (v0 + sum) ^ ((v0 >> 5) + k3);
		v0 -= ((v1 << 4) + k0) ^ (v1 + sum) ^ ((v1 >> 5) + k1);
		sum -= delta;
	}
	v[0] = v0;
	v[1] = v1;
}

static unsigned int tea_cbc_salt[2] = { 0x1F03A564, 0xE0F75A9B };

unsigned int tea_encrypt_pcbc(unsigned int *v, unsigned int size, unsigned int const *k)
{
	unsigned int salt[2] = { tea_cbc_salt[0], tea_cbc_salt[1] };
	unsigned int i;

	for (i = 0; i < size / sizeof(unsigned long long); i++)
	{
		v[i << 1] ^= salt[0];
		v[(i << 1) + 1] ^= salt[1];

		tea_encrypt(v + (i << 1), k);

		salt[0] ^= v[i << 1];
		salt[1] ^= v[(i << 1) + 1];
	}
	return i * sizeof(unsigned long long);
}

unsigned int tea_decrypt_pcbc(unsigned int *v, unsigned int size, unsigned int const *k)
{
	unsigned int salt[2] = { tea_cbc_salt[0], tea_cbc_salt[1] };
	unsigned int newsalt[2];
	unsigned int i;

	for (i = 0; i < size / sizeof(unsigned long long); i++)
	{
		newsalt[0] = v[i << 1];
		newsalt[1] = v[(i << 1) + 1];

		tea_decrypt(v + (i << 1), k);

		v[i << 1] ^= salt[0];
		v[(i << 1) + 1] ^= salt[1];

		salt[0]	^= newsalt[0];
		salt[1] ^= newsalt[1];
	}
	return i * sizeof(unsigned long long);
}

//static unsigned int tea_ofb_IV[2] = { 0x1F03A564, 0xE0F75A9B };

void tea_encrypt_ofb(char* v, unsigned int size, unsigned int const *k, unsigned int const *ofbIV )
  {
  char* end=v+(size&0xFFFFFFF8); size-=size&0xFFFFFFF8;
  union {
    unsigned int iIV[2];
    char bIV[8];
  } IV;
  IV.iIV[0] = ofbIV[0];
  IV.iIV[1] = ofbIV[1];

	for (; v<end;) // for blocks
    {
		tea_encrypt(IV.iIV, k);
    *((unsigned int*)v) = IV.iIV[0] ^ *((unsigned int*)v);
    v+=4;
    *((unsigned int*)v) = IV.iIV[1] ^ *((unsigned int*)v);
    v+=4;
    }
  tea_encrypt(IV.iIV, k);
  for (int j = 0; j < size; j++,v++)
    *v ^= IV.bIV[j];
  }

/*void tea_decrypt_ofb(char *v, unsigned int size, unsigned int const *k, unsigned int const *ofbIV)
  {
  tea_encrypt_ofb(v, size, k, ofbIV);
  }
*/

//static unsigned int tea_cfb_IV[2] = { 0x1F03A564, 0xE0F75A9B };

void tea_encrypt_cfb(char* v, unsigned int size, unsigned int const *k, unsigned int* iv)
  {
  char* end=v+(size&0xFFFFFFF8); size-=size&0xFFFFFFF8;

	for (; v<end;) // for blocks
    {
		tea_encrypt(iv, k);
    *((unsigned int*)v)=iv[0]=iv[0] ^ *((unsigned int*)v);
    v+=4;
    iv[1] = *((unsigned int*)v) = iv[1] ^ *((unsigned int*)v);
    v+=4;
    }
  tea_encrypt(iv, k);
  for (int j = 0; j < size; j++,v++)
    *v ^= ((char*)iv)[j];
  }

void tea_decrypt_cfb(char *v, unsigned int size, unsigned int const *k, unsigned int* iv)
  {
  char* end=v+(size&0xFFFFFFF8); size-=size&0xFFFFFFF8;

	for (; v<end;) // for blocks
    {
		tea_encrypt(iv, k);

    register unsigned int t;
    t=*((unsigned int*)v);
    *((unsigned int*)v)=iv[0] ^ t;
    iv[0]=t;
    v+=4;
    t=*((unsigned int*)v);
    *((unsigned int*)v) = iv[1] ^ t;
    iv[1]=t;
    v+=4;
    }
  tea_encrypt(iv, k);
  for (int j = 0; j < size; j++,v++)
    *v ^= ((char*)iv)[j];
  }
