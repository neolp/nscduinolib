
#ifndef __STM32L051XX_EXTENSIONS_H__
#define __STM32L051XX_EXTENSIONS_H__

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include "stm32l051xx.h"

/* all values not shifted */

#define __regn_diff(n, base, diff) \
	((uintptr_t)(base) + (uintptr_t)(n) * (uintptr_t)(diff))
#define __regn(n, first, second) \
	__regn_diff(n, first, (uintptr_t)(second) - (uintptr_t)(first))

/* ---- power --------------------------------------------------------------- */
/* PWR->CR VOS values */
#define PWR_CR_VOS_FORBIDDEN            0x00
#define PWR_CR_VOS_1V8                  0x01
#define PWR_CR_VOS_1V5                  0x02
#define PWR_CR_VOS_1V2                  0x03

/* ---- rcc csr rtcsel values ----------------------------------------------- */
#define RCC_CSR_RTCSEL_BASE_NOCLK       0x00
#define RCC_CSR_RTCSEL_BASE_LSE         0x01
#define RCC_CSR_RTCSEL_BASE_LSI         0x02
#define RCC_CSR_RTCSEL_BASE_HSE         0x03

/* ---- gpio ---------------------------------------------------------------- */
#define GPIO(n) \
	((GPIO_TypeDef *)__regn(n, GPIOA_BASE, GPIOB_BASE))

#define GPIO_BSR(n) \
	(*((uint16_t volatile *)&GPIO(n)->BSRR + 0))

#define GPIO_BRR(n) \
	(*((uint16_t volatile *)&GPIO(n)->BSRR + 1))

/* GPIO->MODER mode values */
#define GPIO_MODE_INPUT                 0x00
#define GPIO_MODE_OUTPUT                0x01
#define GPIO_MODE_ALT                   0x02
#define GPIO_MODE_ANALOG                0x03 /* default */

/* GPIO->OSPEEDR mode values */
#define GPIO_OSPEED_VERY_LOW            0x00
#define GPIO_OSPEED_LOW                 0x01
#define GPIO_OSPEED_MEDIUM              0x02
#define GPIO_OSPEED_HIGH                0x03

/* GPIO->PUPDR pull mode values */
#define GPIO_PUPD_NONE                  0x00
#define GPIO_PUPD_PULLUP                0x01
#define GPIO_PUPD_PULLDOWN              0x02
#define GPIO_PUPD_RESERVED              0x03 /* don't use this */

/* GPIO->AFR base mask value */
#define GPIO_AFR_AFSEL_BASE             0x0f

/* ---- adc ----------------------------------------------------------------- */
enum adc_smp
{
	ADC_SMPR_1S5,
	ADC_SMPR_7S5,
	ADC_SMPR_13S5,
	ADC_SMPR_28S5,
	ADC_SMPR_41S5,
	ADC_SMPR_55S5,
	ADC_SMPR_71S5,
	ADC_SMPR_239S5,
};

enum adc_ckmode
{
	ADC_CKMODE_ADCCLK,
	ADC_CKMODE_PCLK2, /* div 2 */
	ADC_CKMODE_PCLK4, /* div 4 */
	ADC_CKMODE_PCLK,
};

/* ---- dma ----------------------------------------------------------------- */
enum dma_pl
{
	DMA_PL_LOW,
	DMA_PL_MEDIUM,
	DMA_PL_HIGH,
	DMA_PL_VERY_HIGH,
};

enum dma_msize
{
	DMA_MSIZE_8,
	DMA_MSIZE_16,
	DMA_MSIZE_32,
};

#endif /* __STM32L051XX_EXTENSIONS_H__ */
