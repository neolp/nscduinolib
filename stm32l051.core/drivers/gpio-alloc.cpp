
#include <assert.h>
#include <ctype.h>
#include <gpio-alloc.h>
#include <intrinsics.h>


static unsigned short volatile port_using_mask[4]={0,0,0,0}; // reads and writes should be atomic

void gpio_pin::configure(GPIO_TypeDef* GPIOREG, int pin, int cfg)
  {

  if (pin<0 || pin > 15) return;
  __istate_t cpsr=__get_interrupt_state();
  __disable_interrupt();
#if TargetCPU == STM32L051
  switch ((int)GPIOREG)
    {
    case (int)GPIOA: RCC->IOPENR|=RCC_IOPENR_GPIOAEN; break;
    case (int)GPIOB: RCC->IOPENR|=RCC_IOPENR_GPIOBEN; break;
    case (int)GPIOC: RCC->IOPENR|=RCC_IOPENR_GPIOCEN; break;
    case (int)GPIOD: RCC->IOPENR|=RCC_IOPENR_GPIODEN; break;
    case (int)GPIOH: RCC->IOPENR|=RCC_IOPENR_GPIOHEN; break;
//    case (int)GPIOE: RCC->APB2ENR|=RCC_APB2ENR_IOPEEN; break;
//    case (int)GPIOF: RCC->APB2ENR|=RCC_APB2ENR_IOPFEN; break;
//    case (int)GPIOG: RCC->APB2ENR|=RCC_APB2ENR_IOPGEN; break;
    default: __set_interrupt_state(cpsr); return;
    }
  __set_interrupt_state(cpsr);
  GPIOREG->MODER   =(GPIOREG->MODER   & ~(0x03 << ((pin)<<1))) | ((((cfg)>>6)&0x03) << ((pin)<<1));
  GPIOREG->OTYPER  =(GPIOREG->OTYPER  & ~(0x01 << ((pin)<<0))) | ((((cfg)>>2)&0x01) << ((pin)<<0));
  GPIOREG->OSPEEDR =(GPIOREG->OSPEEDR & ~(0x03 << ((pin)<<1))) | ((((cfg)>>0)&0x03) << ((pin)<<1));
  GPIOREG->PUPDR   =(GPIOREG->PUPDR   & ~(0x03 << ((pin)<<1))) | ((((cfg)>>4)&0x03) << ((pin)<<1));
  if (pin>7) GPIOREG->AFR[1]=(GPIOREG->AFR[1] & ~(0x0F << ((pin-8)<<2))) | (((cfg&0x0F00)>>8) << ((pin-8)<<2));
  else       GPIOREG->AFR[0]=(GPIOREG->AFR[0] & ~(0x0F << ((pin-0)<<2))) | (((cfg&0x0F00)>>8) << ((pin-0)<<2));
#else
  RCC->APB2ENR|=RCC_APB2ENR_AFIOEN;
  switch ((int)GPIOREG)
    {
    case (int)GPIOA: RCC->APB2ENR|=RCC_APB2ENR_IOPAEN; break;
    case (int)GPIOB: RCC->APB2ENR|=RCC_APB2ENR_IOPBEN; break;
    case (int)GPIOC: RCC->APB2ENR|=RCC_APB2ENR_IOPCEN; break;
    case (int)GPIOD: RCC->APB2ENR|=RCC_APB2ENR_IOPDEN; break;
//    case (int)GPIOE: RCC->APB2ENR|=RCC_APB2ENR_IOPEEN; break;
//    case (int)GPIOF: RCC->APB2ENR|=RCC_APB2ENR_IOPFEN; break;
//    case (int)GPIOG: RCC->APB2ENR|=RCC_APB2ENR_IOPGEN; break;
    default: __set_interrupt_state(cpsr); return;
    }
  __set_interrupt_state(cpsr);
  if (pin>7) GPIOREG->CRH=(GPIOREG->CRH & ~(0x0F << ((pin-8)<<2))) | ((cfg&0x0F) << ((pin-8)<<2));
  else       GPIOREG->CRL=(GPIOREG->CRL & ~(0x0F << ((pin-0)<<2))) | ((cfg&0x0F) << ((pin-0)<<2));
  if (cfg&0x10)   GPIOREG->BSRR=1<<pin; // set poll up
  else            GPIOREG->BRR =1<<pin;  // reset poll up -> set poll down
#endif
  }

void gpio_pin::set(GPIO_TypeDef* GPIOREG, int pin_id, int val)
  {
  if (val)
    GPIOREG->BSRR=1<<pin_id; //set
  else
    GPIOREG->BRR=1<<pin_id; // reset
  }
int gpio_pin::read(GPIO_TypeDef* GPIOREG, int pin_id)
  {
  return (GPIOREG->IDR>>pin_id)&0x01;
  }

int gpio_pin::acquire(int port, int cfg)
  {
  port_id=port>>4;
  pin_id=port&0x0F;
  if (port_id>3 || pin_id > 15) return -1;

  switch (port_id)
    {
    case 0: GPIOREG=GPIOA; break;
    case 1: GPIOREG=GPIOB; break;
    case 2: GPIOREG=GPIOC; break;
    case 3: GPIOREG=GPIOD; break;
//    case 4: GPIOREG=GPIOE; break;
//    case 5: GPIOREG=GPIOF; break;
//    case 6: GPIOREG=GPIOG; break;
    }
  __istate_t cpsr=__get_interrupt_state();
  __disable_interrupt();

  if ((port_using_mask[port_id]&(1<<pin_id))==0)
    {
    port_using_mask[port_id]|=(1<<pin_id);
    }
  else
    {
    GPIOREG=0; __set_interrupt_state(cpsr); return -2;
    }
  __set_interrupt_state(cpsr);
  configure(GPIOREG, pin_id, cfg);
  return 0;
  }

void gpio_pin::deconfigure(GPIO_TypeDef* GPIOREG, int pin) // return to reset state
  {
  __istate_t cpsr=__get_interrupt_state();
  __disable_interrupt();
#if TargetCPU == STM32L051
  GPIOREG->MODER   =(GPIOREG->MODER   & ~(0x03 << ((pin)<<1))) | ((0x03) << ((pin)<<1));
  GPIOREG->OTYPER  =(GPIOREG->OTYPER  & ~(0x01 << ((pin)<<0))) | ((0) << ((pin)<<0));
  GPIOREG->OSPEEDR =(GPIOREG->OSPEEDR & ~(0x03 << ((pin)<<1))) | ((0) << ((pin)<<1));
  GPIOREG->PUPDR   =(GPIOREG->PUPDR   & ~(0x03 << ((pin)<<1))) | ((0) << ((pin)<<1));
#else
  if (pin>7) GPIOREG->CRH=(GPIOREG->CRH & ~(0x0F << ((pin-8)<<2))) | ((0x04) << ((pin-8)<<2));
  else       GPIOREG->CRL=(GPIOREG->CRL & ~(0x0F << ((pin-0)<<2))) | ((0x04) << ((pin-0)<<2));
#endif
  GPIOREG->BRR=1<<pin;  // reset poll up
  __set_interrupt_state(cpsr);
  }

int gpio_pin::release() // return to reset state and power off port if unused
  {
  if (GPIOREG==0) return 0;
  __istate_t cpsr=__get_interrupt_state();
  __disable_interrupt();
  deconfigure(GPIOREG, pin_id);
//  if (pin_id>7) GPIOREG->CRH=(GPIOREG->CRH & (0x0F << ((pin_id-8)<<2))) | ((0x04) << ((pin_id-8)<<2));
//  else          GPIOREG->CRL=(GPIOREG->CRL & (0x0F << ((pin_id-0)<<2))) | ((0x04) << ((pin_id-0)<<2));
//  GPIOREG->BRR=1<<pin_id;  // reset poll up
  port_using_mask[port_id]&=~(1<<pin_id);
  if (port_using_mask[port_id]==0) // switch off the clock
    {
#if TargetCPU == STM32L051
  switch ((int)GPIOREG)
    {
    case (int)GPIOA: RCC->IOPENR&=~RCC_IOPENR_GPIOAEN; break;// RCC->IOPENR|=RCC_IOPENR_IOPAEN; break;
    case (int)GPIOB: RCC->IOPENR&=~RCC_IOPENR_GPIOBEN; break;// RCC->IOPENR|=RCC_APB2ENR_IOPBEN; break;
    case (int)GPIOC: RCC->IOPENR&=~RCC_IOPENR_GPIOCEN; break;// RCC->IOPENR|=RCC_APB2ENR_IOPCEN; break;
    case (int)GPIOD: RCC->IOPENR&=~RCC_IOPENR_GPIODEN; break;// RCC->IOPENR|=RCC_APB2ENR_IOPDEN; break;
    case (int)GPIOH: RCC->IOPENR&=~RCC_IOPENR_GPIOHEN;// RCC->IOPENR|=RCC_APB2ENR_IOPDEN; break;
//    case (int)GPIOE: RCC->APB2ENR|=RCC_APB2ENR_IOPEEN; break;
//    case (int)GPIOF: RCC->APB2ENR|=RCC_APB2ENR_IOPFEN; break;
//    case (int)GPIOG: RCC->APB2ENR|=RCC_APB2ENR_IOPGEN; break;
    }
#else
    switch (port_id)
      {
      case 0: RCC->APB2ENR&=~RCC_APB2ENR_IOPAEN; break;
      case 1: RCC->APB2ENR&=~RCC_APB2ENR_IOPBEN; break;
      case 2: RCC->APB2ENR&=~RCC_APB2ENR_IOPCEN; break;
      case 3: RCC->APB2ENR&=~RCC_APB2ENR_IOPDEN; break;
  //    case 4: RCC->APB2ENR&=~RCC_APB2ENR_IOPEEN; break;
  //    case 5: RCC->APB2ENR&=~RCC_APB2ENR_IOPFEN; break;
  //    case 6: RCC->APB2ENR&=~RCC_APB2ENR_IOPGEN; break;
      }
#endif
    }
  GPIOREG=0;
  __set_interrupt_state(cpsr);
  return 0;
  }









/*

lp::ssize_t dgpio_pin::ioctl(int id, void *buf, lp::size_t ln, lp::msec_t time_out)
{
	lp::ssize_t ret = -1;

	// just one user...
	switch (id)
	{
	case GET_GPIO_IN_BOOL:
		if (ln < sizeof(bool))
			goto exit;
		*(bool *)buf = read();
		ret = sizeof(bool);
		break;
	case GET_GPIO_OUT_BOOL:
		if (ln < sizeof(bool))
			goto exit;
		*(bool *)buf = get();
		ret = sizeof(bool);
		break;
	case SET_GPIO_OUT_BOOL:
		if (ln != sizeof(bool))
			goto exit;
		set(*(bool *)buf);
		ret = sizeof(bool);
		break;
	case TOGGLE_GPIO_OUT:
		if (ln)
			goto exit;
		toggle();
		ret = 0;
		break;

	case GET_GPIO_MODE:
		// unable to get pin mode; todo...
		goto exit;
	case SET_GPIO_MODE:
	{
		enum ioctl_gpio_mode mode = *(enum ioctl_gpio_mode *)buf;
		if (ln != sizeof(mode))
			goto exit;

		dgpio_set_mode(dgpio_get_port(pin), dgpio_get_pin(pin),
				dgpio_mode_convert(mode));
		ret = sizeof(mode);
		break;
	}
	case GET_GPIO_SPEED_MODE:
		// unable to get pin mode; todo...
		goto exit;
	case SET_GPIO_SPEED_MODE:
		// todo...
		goto exit;

	case GET_MMAP_PIPE_OUTVEC:
		// do not allowable; todo...
		goto exit;
	case GET_MMAP_PIPE_SETVEC:
	{
		struct ioctl_mmap_pipe_iovec *iovec =
				(struct ioctl_mmap_pipe_iovec *)buf;

		if (ln < sizeof(*iovec))
			goto exit;
		iovec->ptr = (void *)sreg;
		iovec->size = sizeof(*sreg);
		ret = sizeof(*iovec);
		break;
	}
	case GET_MMAP_PIPE_CLRVEC:
	{
		struct ioctl_mmap_pipe_iovec *iovec =
				(struct ioctl_mmap_pipe_iovec *)buf;

		if (ln < sizeof(*iovec))
			goto exit;
		iovec->ptr = (void *)rsreg;
		iovec->size = sizeof(*sreg);
		ret = sizeof(*iovec);
		break;
	}
	case GET_MMAP_PIPE_INVEC:
	{
		struct ioctl_mmap_pipe_iovec *iovec =
				(struct ioctl_mmap_pipe_iovec *)buf;

		if (ln < sizeof(*iovec))
			goto exit;
		iovec->ptr = (void *)inreg;
		iovec->size = sizeof(*inreg);
		ret = sizeof(*iovec);
		break;
	}
	case SET_MMAP_PIPE_OUTVEC:
	case SET_MMAP_PIPE_SETVEC:
	case SET_MMAP_PIPE_CLRVEC:
	case SET_MMAP_PIPE_INVEC:
		// unable to change mmap gpio addresses!.. todo: proper error code
		goto exit;
	case GET_MMAP_PIPE_IOCNT_U32:
	case CLR_MMAP_PIPE_IOCNT_U32:
		// do not allowable
		goto exit;
	}
exit:
	return ret;
}

static struct vfsname_arg_descr const dgpio_descr[] =
{
	{
		.parser = vfsname_parse_enum32,
		.prefix = "mode=",
		.flags = 0,
		.suffix = "in|inpup|inpdown|out|outod",
	},
	{
		.parser = vfsname_parse_portcfg,
		.prefix = NULL,
		.flags = 0,
		.suffix = NULL,
	},
};

i_node *dgpio_alloc::create(const char *name, int mode, void *param, lp::msec_t time_out)
{
	int32_t mode_num = -1;
	uint16_t pmode = DGPIO_SPEED_HIGH;
	struct vfsname_portcfg pcfg = { .port = -1, .pin = -1 };

	if (name == NULL || *name == '\0')
		return this;

	// try to parse pin number
	if (vfsname_parse_arg(name, NULL, dgpio_descr,
			sizeof(dgpio_descr) / sizeof(dgpio_descr[0]),
			&mode_num, &pcfg) < 0)
		return NULL;
	if (pcfg.port < 0 || pcfg.pin < 0)
		return NULL;

	// check port mode
	if (mode_num < 0 || mode_num > 4)
		mode_num = 3;
	if (mode_num <= 2)
	{
		// inputs
		pmode |= DGPIO_MODE_INPUT;
		if (mode_num == 1)
			pmode |= DGPIO_PULL_UP;
		else if (mode_num == 2)
			pmode |= DGPIO_PULL_DOWN;
	}
	else
	{
		// outputs
		pmode |= DGPIO_MODE_OUTPUT;
		if (mode_num == 4)
			pmode |= DGPIO_OPEN_DRAIN;
	}

	// try to capture a pin
	dgpio_t pin = dgpio_create(pcfg.port, pcfg.pin);
	if (dgpio_captcfg(&pin, pmode))
		return NULL;

	// try to get free inode
	dgpio_pin *inode = new dgpio_pin();
	if (inode == NULL)
	{
		dgpio_release(&pin);
		return NULL;
	}

	// configure new inode
	inode->sreg = &GPIO(pcfg.port)->BSRRL;
	inode->rsreg = &GPIO(pcfg.port)->BSRRH;
	inode->inreg = (uint16_t volatile *)&GPIO(pcfg.port)->IDR;
	inode->mask = 1 << pcfg.pin;
	inode->pin = pin;

	return inode;
}

lp::ssize_t dgpio_alloc::ioctl(int id, void *buf, lp::size_t ln, lp::msec_t time_out)
{
	lp::ssize_t ret = -1;

	switch (id)
	{
	case CAPTURE_GPIO:
	{
		struct ioctl_gpio_capt_req const *req =
				(struct ioctl_gpio_capt_req const *)buf;
		uint16_t mode;
		dgpio_t pin;

		if (ln != sizeof(*req))
			goto exit;
		if (req->node == NULL)
			goto exit;

		// check pin and try to capture
		mode = dgpio_mode_convert(req->mode)
				| dgpio_speed_mode_convert(req->spmode);
		if (req->port > PORTI || req->pin > 15)
			goto exit;
		pin = dgpio_create(req->port, req->pin);
		if (dgpio_captcfg(&pin, mode))
			goto exit;

		// try to get free inode
		dgpio_pin *inode = new dgpio_pin();
		if (inode == NULL)
		{
			dgpio_release(&pin);
			goto exit;
		}

		// configure new inode
		inode->sreg = &GPIO(req->port)->BSRRL;
		inode->rsreg = &GPIO(req->port)->BSRRH;
		inode->inreg = (uint16_t volatile *)&GPIO(req->port)->IDR;
		inode->mask = 1 << req->pin;
		inode->pin = pin;

		*req->node = inode;
		ret = sizeof(*req);
		break;
	}
	}

exit:
	return ret;
}

void dgpio_alloc_init(void)
{
	k_addnsentry("/dev/gpio", &dgpio_alloc::instance());
}
*/