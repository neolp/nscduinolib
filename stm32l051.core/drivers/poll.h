
#ifndef __POLL_H__
#define __POLL_H__

#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#define poll_while(cond, max_times_u32, timeout_lval_bool) \
do { \
	uint32_t const __max_times = (uint32_t)(max_times_u32); \
	uint32_t __poll; \
	 \
	for (__poll = __max_times; __poll > 0; __poll--) \
	{ \
		if (!(cond)) \
			break; \
	} \
	*((bool *)&(timeout_lval_bool)) = !__poll; \
} while (0)

static inline bool poll(uint32_t volatile *reg, uint32_t mask, uint32_t value,
                        uint32_t poll_times)
{
	for (; poll_times > 0; --poll_times)
	{
		uint32_t const rd_value = *reg & mask;
		if (rd_value == value)
			return true;
	}
	return false;
}

static inline void poll_assert(uint32_t volatile *reg, uint32_t mask, uint32_t value,
                               uint32_t poll_times)
{
	bool result = poll(reg, mask, value, poll_times);
	if (!result)
		assert(false);
}

#endif /* __POLL_H__ */
