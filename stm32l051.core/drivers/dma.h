
#ifndef __DMA_H__
#define __DMA_H__

#include "types.h"

extern uint32_t dma_conv_cnt;

extern void __dma_init(void);

#endif /* __DMA_H__ */
