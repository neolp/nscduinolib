
#ifndef __ADC_H__
#define __ADC_H__

#include <assert.h>
#include "types.h"

/* ---- channels, configuration --------------------------------------------- */
/* NOTE: channels MUST to be sorted! */
enum adc_id
{
	ADC_T2,     /* 1 */
	ADC_T1,     /* 2 */
	ADC_EXT,   /* 4 */
	ADC_T4,     /* 8 */
	ADC_T3,     /* 9 */
	ADC_VREFINT, /* 17 */
	ADC_TEMP,    /* 18 */
	__ADC_NUM,
};

extern uint8_t const adc_ch[__ADC_NUM];
extern uint16_t volatile adc_val[__ADC_NUM];

extern void __adc_init(void);

void adc_single_dma(void* ADC_array, int NUMBER_OF_ADC_CHANNEL );

/* ---- convertion ----------------------------------------------------------- */
static uint16_t const adc_conv_max = 0xfff;
static uint16_t const adc_vrefext_mv = 3325;
static uint16_t const adc_vrefint_mv = 1224;
static uint16_t const volatile * const adc_vrefint_cal = (uint16_t const volatile *)0x1ff80078;
static uint16_t const volatile * const adc_ts_cal1 = (uint16_t const volatile *)0x1ff8007a;
static uint16_t const volatile * const adc_ts_cal2 = (uint16_t const volatile *)0x1ff8007e;
static uint16_t const adc_avg_slope_mul = 1000;
static uint16_t const adc_avg_slope_div = 1610;

int32_t ComputeTemperature(uint32_t measure, uint32_t vdda);

static inline uint32_t __adc_get_mv_vref(uint16_t conv, uint16_t vref_mv)
{
	uint32_t prod = (uint32_t)conv * (uint32_t)vref_mv;
	return prod / adc_conv_max;
}

static inline uint32_t __adc_get_mv(uint16_t conv)
{
	return __adc_get_mv_vref(conv, adc_vrefext_mv);
}

/* accuracy unknown (check RP3 tolerance) */
static inline uint32_t adc_get_vsupp(uint16_t conv)
{
	return __adc_get_mv(conv) * 2;
}

/* accuracy 1% */
static inline uint32_t adc_get_vrefext(uint16_t conv)
{
	return (adc_vrefint_mv * adc_conv_max) / conv;
}

/* linearity �2 �C */
static inline int32_t adc_get_temp(uint16_t conv)
{
	int32_t mv = __adc_get_mv(conv);
	int32_t ts30_mv = __adc_get_mv_vref(*adc_ts_cal1, 3000);
	int32_t diff = mv - ts30_mv;
	int32_t const mul = 1024;
	int32_t temp = (((diff * adc_avg_slope_mul * mul) / adc_avg_slope_div)
			+ (30 * mul)) / mul;

//	assert(temp >= -40 && temp <= 200);
	return temp;
}

#endif /* __ADC_H__ */
