
#include <assert.h>
#include "eeprom.h"
#include "stm32l051xx.h"

#define FLASH_PEKEY1  0x89ABCDEF
#define FLASH_PEKEY2  0x02030405

#define FLASH_PRGKEY1 0x8C9DAEBF
#define FLASH_PRGKEY2 0x13141516

#define FLASH_OPTKEY1 0xFBEAD9C8
#define FLASH_OPTKEY2 0x24252627

/* all simply copypasted from reference manual */

void eeprom::lock(void)
{
	// A.3.2 Locking data EEPROM and FLASH_PECR register code example
	// (1) Wait till no operation is on going
	// (2) Locks the NVM by setting PELOCK in PECR
	while ((FLASH->SR & FLASH_SR_BSY) != 0) // (1)
	{
		// For robust implementation, add here time-out management
	}
	FLASH->PECR |= FLASH_PECR_PELOCK; // (2)
}

void eeprom::unlock(void)
{
	// A.3 NVM Operation code example
	// A.3.1 Unlocking the data EEPROM and FLASH_PECR register code example
	// (1) Wait till no operation is on going
	// (2) Check if the PELOCK is unlocked
	// (3) Perform unlock sequence
	while ((FLASH->SR & FLASH_SR_BSY) != 0) // (1)
	{
		// For robust implementation, add here time-out management
	}
	if ((FLASH->PECR & FLASH_PECR_PELOCK) != 0) // (2)
	{
		FLASH->PEKEYR = FLASH_PEKEY1; // (3)
		FLASH->PEKEYR = FLASH_PEKEY2;
	}
}

void eeprom::write(void const *buf, size_t size, uintptr_t start_addr)
{
	uint8_t *fptr       = reinterpret_cast<uint8_t *>(start_addr);
	uint8_t const *dptr = static_cast<uint8_t const *>(buf);

	for (size_t i = 0; i < size; ++i)
		fptr[i] = dptr[i];
}

void eeprom::read(void *buf, size_t size, uintptr_t start_addr)
{
	uint8_t const *fptr = reinterpret_cast<uint8_t const *>(start_addr);
	uint8_t	*dptr       = static_cast<uint8_t *>(buf);
	
	for (size_t i = 0; i < size; ++i)
		dptr[i] = fptr[i];
}

void eeprom::erase(uintptr_t start_addr)
{
	FLASH->PECR |= FLASH_PECR_ERASE | FLASH_PECR_DATA; /* (1) */
	*(__IO uint32_t *)start_addr = (uint32_t)0; /* (2) */

	while ((FLASH->SR & FLASH_SR_BSY) != 0)
		/* long, long, cycles of polling instead __WFI(); */;

	FLASH->PECR &= ~(FLASH_PECR_ERASE | FLASH_PECR_DATA); /* (4) */
}
