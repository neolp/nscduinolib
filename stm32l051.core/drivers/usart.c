
#include <assert.h>
#include "usart.h"
#include "stm32l051xx_core.h"
#include "sys.h"
#include "irq-prior.h"

static uint16_t __usart_get_brr_ovrsmp16(uint32_t bps, uint32_t clk)
{
	if (!bps)
		bps = 1;
	return clk / bps;
}

/* 115200 bps, 8 data bits, no parity, 1 stop bit */
void __usart_init(void)
{
	/* switch off */
	USART1->CR1 = 0;
	USART1->ICR = 0xffffffff;
	
	/* reset unsused registers */
	USART1->GTPR = 0;
	USART1->RTOR = 0;
	USART1->RQR = 0;
	
	/* switch on */
	USART1->BRR = __usart_get_brr_ovrsmp16(115200, sys_get_clk());
	USART1->CR3 = USART_CR3_EIE;
	USART1->CR2 = 0;
	USART1->CR1 = USART_CR1_UE | USART_CR1_RE | USART_CR1_TE
			| USART_CR1_RXNEIE | USART_CR1_TCIE | USART_CR1_TXEIE
			| USART_CR1_PEIE;
	
	/* enable interrupt */
	//NVIC_SetPriority(USART1_IRQn, IPR_USART);
	//NVIC_EnableIRQ(USART1_IRQn);
}

struct usart_cbuf
{
	uint8_t *ptr;
	size_t volatile front;
	size_t volatile rear;
};

static int __usart_put(struct usart_cbuf *cbuf, uint8_t symb)
{
	size_t front = cbuf->front;
	size_t rear = cbuf->rear;
}

static int __usart_get()
{
	
}

extern void USART1_IRQHandler(void)
{
	uint32_t sr = USART1->ISR;
	USART1->ICR = sr & (USART_ICR_WUCF | USART_ICR_CMCF | USART_ICR_EOBCF
			| USART_ICR_RTOCF | USART_ICR_CTSCF | USART_ICR_LBDCF
			| USART_ICR_TCCF | USART_ICR_IDLECF | USART_ICR_ORECF
			| USART_ICR_NCF | USART_ICR_FECF | USART_ICR_PECF);
	
	/* ---- tx ---------------------------------------------------------- */
	if (sr & USART_ISR_TXE)
	{
		/* transmit data register empty */
		int volatile tmp = 1;
		USART1->TDR = 'Q';
	}
	if (sr & USART_ISR_TC)
	{
		/* transmission complete */
	}
	
	/* ---- rx ---------------------------------------------------------- */
	if (sr & USART_ISR_RXNE)
	{
		/* read data register not empty */
	}
	if (sr & USART_ISR_ORE)
	{
		/* overrun */
	}
	if (sr & USART_ISR_NE)
	{
		/* noise detected */
	}
	if (sr & USART_ISR_FE)
	{
		/* framing error */
	}
	if (sr & USART_ISR_PE)
	{
		/* parity error */
	}
}
