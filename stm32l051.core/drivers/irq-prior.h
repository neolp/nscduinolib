
#ifndef __INTERRUPT_PRIORITY_TABLE_H__
#define __INTERRUPT_PRIORITY_TABLE_H__

#include "types.h"

#define IPR_DMA                         0
#define IPR_USART                       1

#endif /* __INTERRUPT_PRIORITY_TABLE_H__ */
