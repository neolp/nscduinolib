
#ifndef __GPIO_ALLOCATOR_H__
#define __GPIO_ALLOCATOR_H__

#include <stdbool.h>
#include <stdint.h>
#if TargetCPU == STM32L051
#include "stm32l051xx.h"
//#include "ST/iostm32l051xx.h"
#else
#include "stm32f10x.h"
#endif


/* simple dynamic gpio manager for drivers internal using */
class gpio_pin
  {
  GPIO_TypeDef* GPIOREG;
  unsigned char port_id;
  unsigned char pin_id;
public:
  gpio_pin():GPIOREG(0) {}
  gpio_pin(int port,int cfg):GPIOREG(0) { acquire(port, cfg); }
  ~gpio_pin() { release(); }
  int getport(){
    return (port_id<<4)| pin_id;
  }
  int acquire(int port,int cfg);
  static void configure(GPIO_TypeDef* GPIOREG, int pin, int cfg);
  int release();
  static void deconfigure(GPIO_TypeDef* GPIOREG, int pin);
  static void set(GPIO_TypeDef* GPIOREG, int pin_id, int val);
  static int read(GPIO_TypeDef* GPIOREG, int pin_id);
  void cfg(int cfg)
    {
#if TargetCPU == STM32L051
    GPIOREG->MODER   =(GPIOREG->MODER   & ~(0x03 << ((pin_id)<<1))) | ((((cfg)>>6)&0x03) << ((pin_id)<<1));
    GPIOREG->OTYPER  =(GPIOREG->OTYPER  & ~(0x01 << ((pin_id)<<0))) | ((((cfg)>>2)&0x01) << ((pin_id)<<0));
    GPIOREG->OSPEEDR =(GPIOREG->OSPEEDR & ~(0x03 << ((pin_id)<<1))) | ((((cfg)>>0)&0x03) << ((pin_id)<<1));
    GPIOREG->PUPDR   =(GPIOREG->PUPDR   & ~(0x03 << ((pin_id)<<1))) | ((((cfg)>>4)&0x03) << ((pin_id)<<1));
    if (pin_id>7) GPIOREG->AFR[1]=(GPIOREG->AFR[1] & ~(0x0F << ((pin_id-8)<<2))) | (((cfg&0x0F00)>>8) << ((pin_id-8)<<2));
    else       GPIOREG->AFR[0]=(GPIOREG->AFR[0] & ~(0x0F << ((pin_id-0)<<2))) | (((cfg&0x0F00)>>8) << ((pin_id-0)<<2));
#else
    if (pin_id>7) GPIOREG->CRH=(GPIOREG->CRH & ~(0x0F << ((pin_id-8)<<2))) | ((cfg&0x0F) << ((pin_id-8)<<2));
    else          GPIOREG->CRL=(GPIOREG->CRL & ~(0x0F << ((pin_id-0)<<2))) | ((cfg&0x0F) << ((pin_id-0)<<2));
#endif
    }

  void set()
    {
    GPIOREG->BSRR=1<<pin_id; // set
    }

  void clr()
  {
    GPIOREG->BRR=1<<pin_id; // clr
  }

  bool acquired() { return GPIOREG!=0; }

  void pull_up() // enable pull up mode for pull up/down input pin
    {
    GPIOREG->BSRR=1<<pin_id; // set
    }

  void reset()
    {
    GPIOREG->BRR=1<<pin_id; // reset
    }

  void pull_down() // enable pull down mode for pull up/down input pin
    {
    GPIOREG->BRR=1<<pin_id; // reset
    }

  int read()
    {
    return (GPIOREG->IDR>>pin_id)&0x01;
    }

  bool toggle(void)
    {
    int ret=(GPIOREG->ODR>>pin_id)&0x01;
    if (ret)
      GPIOREG->BRR=1<<pin_id; // reset
    else
      GPIOREG->BSRR=1<<pin_id; //set
    return ret;
    }

  void set(int val)
    {
    if (val)
      GPIOREG->BSRR=1<<pin_id; //set
    else
      GPIOREG->BRR=1<<pin_id; // reset
    }

  };

#if TargetCPU == STM32L051

#define GPIO_AF0                        (0x0000)
#define GPIO_AF1                        (0x0100)
#define GPIO_AF2                        (0x0200)
#define GPIO_AF3                        (0x0300)
#define GPIO_AF4                        (0x0400)
#define GPIO_AF5                        (0x0500)
#define GPIO_AF6                        (0x0600)
#define GPIO_AF7                        (0x0700)
#define GPIO_AF8                        (0x0800)
#define GPIO_AF9                        (0x0900)
#define GPIO_AF10                       (0x0A00)
#define GPIO_AF11                       (0x0B00)
#define GPIO_AF12                       (0x0C00)
#define GPIO_AF13                       (0x0D00)
#define GPIO_AF14                       (0x0E00)
#define GPIO_AF15                       (0x0F00)

#define GPIO_IN                         (0x00)
#define GPIO_OUT                        (0x40)
#define GPIO_ALT                        (0x80)
#define GPIO_ANALOG                     (0xC0)

#define GPIO_OUT_OPENDRAIN              (0x04)

#define GPIO_FLOATING                   (0x00)
#define GPIO_PULL_UP                    (0x10)
#define GPIO_PULL_DOWN                  (0x20)


#define GPIO_400kHz                     (0x00)
#define GPIO_2MHz                       (0x01)
#define GPIO_10MHz                      (0x02)
#define GPIO_40MHz                      (0x03)

#else

//speed low (input)
#define GPIO_IN_ANALOG                  (0x00|0x00)
#define GPIO_IN_FLOATING                (0x04|0x00)
#define GPIO_IN_PULLDOWN                (0x08|0x00)
#define GPIO_IN_PULLUP                  (0x08|0x10|0x00)
#define GPIO_IN_reserv                  (0x0C|0x00)
//speed medium
#define GPIO_OUT_PUSHPULL_10MHz         (0x00|0x01)
#define GPIO_OUT_OPENDRAIN_10MHz        (0x04|0x01)
#define GPIO_OUT_ALT_PUSHPULL_10MHz     (0x08|0x01)
#define GPIO_OUT_ALT_OPENDRAIN_10MHz    (0x0C|0x01)
//speed high
#define GPIO_OUT_PUSHPULL_2MHz          (0x00|0x02)
#define GPIO_OUT_OPENDRAIN_2MHz         (0x04|0x02)
#define GPIO_OUT_ALT_PUSHPULL_2MHz      (0x08|0x02)
#define GPIO_OUT_ALT_OPENDRAIN_2MHz     (0x0C|0x02)
//speed very high
#define GPIO_OUT_PUSHPULL_50MHz         (0x00|0x03)
#define GPIO_OUT_OPENDRAIN_50MHz        (0x04|0x03)
#define GPIO_OUT_ALT_PUSHPULL_50MHz     (0x08|0x03)
#define GPIO_OUT_ALT_OPENDRAIN_50MHz    (0x0C|0x03)

#endif

#define PORTA   0x0000
#define PORTB   0x0010
#define PORTC   0x0020
#define PORTD   0x0030
#define PORTE   0x0040
#define PORTF   0x0050
#define PORTG   0x0060


#define PIN0  0
#define PIN1  1
#define PIN2  2
#define PIN3  3
#define PIN4  4
#define PIN5  5
#define PIN6  6
#define PIN7  7
#define PIN8  8
#define PIN9  9
#define PIN10  10
#define PIN11  11
#define PIN12  12
#define PIN13  13
#define PIN14  14
#define PIN15  15

#endif /* __GPIO_ALLOCATOR_H__ */
