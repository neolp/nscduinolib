
#ifndef __TIM_H__
#define __TIM_H__

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include "sys.h"
#include "gpio-alloc.h"

//#include "include/limb_driver.h"

//extern unsigned int get_ms();
//extern unsigned long long getlong_ms();
extern unsigned int get_us();
extern unsigned long long getlong_us();

/*
inline unsigned int msToTicks(unsigned int ms)
  {
  return ms*24;
  }
inline unsigned int usToTicks(unsigned int us)
  {
  return us*24;
  }
*/

enum pwm_ch
{
	PWM_CH1 = 0,
	PWM_CH2,
};

extern void tim_init(void);
extern void tim2_init(void);
extern void tim2_stop(void);

extern void pwm_init(void);

extern void pwm_set(enum pwm_ch ch, uint16_t val);

//class TIM

class T2PWM {
public:
  void init (int freq, int maxval){
    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
    TIM2->CR1  = TIM_CR1_CMS_1;
    TIM2->CR2  = 0;
    TIM2->PSC  = (sys_get_clk()/(freq * maxval)) - 1;
    TIM2->ARR  = maxval;
    TIM2->CNT  = 0;
    TIM2->SR   = 0;
    TIM2->DIER = 0;
    TIM2->CR1  = TIM_CR1_CMS_1 | TIM_CR1_CEN;
    TIM2->CCMR1  = 0;
  }

  void prepare (gpio_pin& P, int val){
    int port = P.getport();
    if (port == (PORTA | PIN0)) {
      P.cfg(GPIO_ALT | GPIO_AF2);
      TIM2->CCMR1 = TIM_CCMR1_OC1CE | (6<<4);
      TIM2->CCER  |= TIM_CCER_CC1E;
      TIM2->CCR1   = val;
    } else if (port == (PORTA | PIN1)) {
      P.cfg(GPIO_ALT | GPIO_AF2);
      TIM2->CCMR1 = TIM_CCMR1_OC2CE | (6<<12);
      TIM2->CCER  |= TIM_CCER_CC2E;
      TIM2->CCR2   = val;
    } else if (port == (PORTA | PIN2)) {
      P.cfg(GPIO_ALT | GPIO_AF2);
      TIM2->CCMR2 = TIM_CCMR2_OC3CE | (6<<4);
      TIM2->CCER  |= TIM_CCER_CC3E;
      TIM2->CCR3   = val;
    } else if (port == (PORTA | PIN3)) {
      P.cfg(GPIO_ALT | GPIO_AF2);
      TIM2->CCMR2 = TIM_CCMR2_OC4CE | (6<<12);
      TIM2->CCER  |= TIM_CCER_CC4E;
      TIM2->CCR4   = val;
    } else if (port == (PORTA | PIN5)) {
      P.cfg(GPIO_ALT | GPIO_AF5);
      TIM2->CCMR1 = TIM_CCMR1_OC1CE | (6<<4);
      TIM2->CCER  |= TIM_CCER_CC1E;
      TIM2->CCR1   = val;
    } else if (port == (PORTA | PIN15)) {
      P.cfg(GPIO_ALT | GPIO_AF5);
      TIM2->CCMR1 = TIM_CCMR1_OC1CE | (6<<4);
      TIM2->CCER  |= TIM_CCER_CC1E;
      TIM2->CCR1   = val;
    } else if (port == (PORTB | PIN3)) {
      P.cfg(GPIO_ALT | GPIO_AF5);
      TIM2->CCMR1 = TIM_CCMR1_OC2CE | (6<<12);
      TIM2->CCER  |= TIM_CCER_CC2E;
      TIM2->CCR2   = val;
    } else if (port == (PORTB | PIN10)) {
      P.cfg(GPIO_ALT | GPIO_AF2);
      TIM2->CCMR2 = TIM_CCMR2_OC3CE | (6<<4);
      TIM2->CCER  |= TIM_CCER_CC3E;
      TIM2->CCR3   = val;
    } else if (port == (PORTB | PIN11)) {
      P.cfg(GPIO_ALT | GPIO_AF2);
      TIM2->CCMR2 = TIM_CCMR2_OC4CE | (6<<12);
      TIM2->CCER  |= TIM_CCER_CC4E;
      TIM2->CCR4   = val;
    } else {
      assert(false); // unknown pin
    }
  }
  void on (gpio_pin& P, int val){
    int port = P.getport();
    if (port == (PORTA | PIN0)) {
      TIM2->CCMR1 = TIM_CCMR1_OC1CE | (6<<4);
      TIM2->CCER  |= TIM_CCER_CC1E;
      TIM2->CCR1   = val;
    } else if (port == (PORTA | PIN1)) {
      TIM2->CCMR1 = TIM_CCMR1_OC2CE | (6<<12);
      TIM2->CCER  |= TIM_CCER_CC2E;
      TIM2->CCR2   = val;
    } else if (port == (PORTA | PIN2)) {
      TIM2->CCMR2 = TIM_CCMR2_OC3CE | (6<<4);
      TIM2->CCER  |= TIM_CCER_CC3E;
      TIM2->CCR3   = val;
    } else if (port == (PORTA | PIN3)) {
      TIM2->CCMR1 = TIM_CCMR2_OC4CE | (6<<12);
      TIM2->CCER  |= TIM_CCER_CC4E;
      TIM2->CCR4   = val;
    } else if (port == (PORTA | PIN5)) {
      TIM2->CCMR1 = TIM_CCMR1_OC1CE | (6<<4);
      TIM2->CCER  |= TIM_CCER_CC1E;
      TIM2->CCR1   = val;
    } else if (port == (PORTA | PIN15)) {
      TIM2->CCMR1 = TIM_CCMR1_OC1CE | (6<<4);
      TIM2->CCER  |= TIM_CCER_CC1E;
      TIM2->CCR1   = val;
    } else if (port == (PORTB | PIN3)) {
      TIM2->CCMR1 = TIM_CCMR1_OC2CE | (6<<12);
      TIM2->CCER  |= TIM_CCER_CC2E;
      TIM2->CCR2   = val;
    } else if (port == (PORTB | PIN10)) {
      TIM2->CCMR2 = TIM_CCMR2_OC3CE | (6<<4);
      TIM2->CCER  |= TIM_CCER_CC3E;
      TIM2->CCR3   = val;
    } else if (port == (PORTB | PIN11)) {
      TIM2->CCMR1 = TIM_CCMR2_OC4CE | (6<<12);
      TIM2->CCER  |= TIM_CCER_CC4E;
      TIM2->CCR4   = val;
    } else {
      assert(false); // unknown pin
    }
  }
  void off (gpio_pin& P){
    int port = P.getport();
    if (port == (PORTA | PIN0)) {
      TIM2->CCER  &= ~TIM_CCER_CC1E;
    } else if (port == (PORTA | PIN1)) {
      TIM2->CCER  &= ~TIM_CCER_CC2E;
    } else if (port == (PORTA | PIN2)) {
      TIM2->CCER  &= ~TIM_CCER_CC3E;
    } else if (port == (PORTA | PIN3)) {
      TIM2->CCER  &= ~TIM_CCER_CC4E;
    } else if (port == (PORTA | PIN5)) {
      TIM2->CCER  &= ~TIM_CCER_CC1E;
    } else if (port == (PORTA | PIN15)) {
      TIM2->CCER  &= ~TIM_CCER_CC1E;
    } else if (port == (PORTB | PIN3)) {
      TIM2->CCER  &= ~TIM_CCER_CC2E;
    } else if (port == (PORTB | PIN10)) {
      TIM2->CCER  &= ~TIM_CCER_CC3E;
    } else if (port == (PORTB | PIN11)) {
      TIM2->CCER  &= ~TIM_CCER_CC4E;
    } else {
      assert(false); // unknown pin
    }
  }
  void off (){
//    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
  }
};



class T22PWM {
public:
  void init (int freq, int maxval){
    RCC->APB2ENR |= RCC_APB2ENR_TIM22EN;
    TIM22->CR1  = TIM_CR1_CMS_1;
    TIM22->CR2  = 0;
    unsigned int div=sys_get_clk()/(freq * maxval);

    TIM22->PSC  = div? div - 1:0;
    TIM22->ARR  = maxval;
    TIM22->CNT  = 0;
    TIM22->SR   = 0;
    TIM22->DIER = 0;
    TIM22->CR1  = TIM_CR1_CMS_1 | TIM_CR1_CEN;
    TIM22->CCMR1  = 0;
  }

  void prepare (gpio_pin& P, int val){
    int port = P.getport();
    if (port == (PORTB | PIN4)) {
      P.cfg(GPIO_ALT | GPIO_AF4);
      TIM22->CCMR1 = TIM_CCMR1_OC1CE | (6<<4);
      TIM22->CCER  |= TIM_CCER_CC1E;
      TIM22->CCR1   = val;
    } else if (port == (PORTB | PIN5)) {
      P.cfg(GPIO_ALT | GPIO_AF4);
      TIM22->CCMR1 = TIM_CCMR1_OC2CE | (6<<12);
      TIM22->CCER  |= TIM_CCER_CC2E;
      TIM22->CCR2   = val;
    } else if (port == (PORTA | PIN6)) {
      P.cfg(GPIO_ALT | GPIO_AF5);
      TIM22->CCMR1 = TIM_CCMR1_OC2CE | (6<<12);
      TIM22->CCER  |= TIM_CCER_CC2E;
      TIM22->CCR1   = val;
    } else if (port == (PORTA | PIN7)) {
      P.cfg(GPIO_ALT | GPIO_AF5);
      TIM22->CCMR1 = TIM_CCMR1_OC2CE | (6<<12);
      TIM22->CCER  |= TIM_CCER_CC2E;
      TIM22->CCR2   = val;
    } else if (port == (PORTC | PIN6)) {
      P.cfg(GPIO_ALT | GPIO_AF0);
      TIM22->CCMR2 = TIM_CCMR2_OC3CE | (6<<4);
      TIM22->CCER  |= TIM_CCER_CC3E;
      TIM22->CCR3   = val;
    } else if (port == (PORTC | PIN7)) {
      P.cfg(GPIO_ALT | GPIO_AF0);
      TIM22->CCMR2 = TIM_CCMR2_OC4CE | (6<<12);
      TIM22->CCER  |= TIM_CCER_CC4E;
      TIM22->CCR4   = val;


    } else if (port == (PORTA | PIN5)) {
      P.cfg(GPIO_ALT | GPIO_AF5);
      TIM22->CCMR1 = TIM_CCMR1_OC1CE | (6<<4);
      TIM22->CCER  |= TIM_CCER_CC1E;
      TIM22->CCR1   = val;
    } else if (port == (PORTA | PIN15)) {
      P.cfg(GPIO_ALT | GPIO_AF5);
      TIM22->CCMR1 = TIM_CCMR1_OC1CE | (6<<4);
      TIM22->CCER  |= TIM_CCER_CC1E;
      TIM22->CCR1   = val;
    } else if (port == (PORTB | PIN3)) {
      P.cfg(GPIO_ALT | GPIO_AF5);
      TIM22->CCMR1 = TIM_CCMR1_OC2CE | (6<<12);
      TIM22->CCER  |= TIM_CCER_CC2E;
      TIM22->CCR2   = val;
    } else if (port == (PORTB | PIN10)) {
      P.cfg(GPIO_ALT | GPIO_AF2);
      TIM22->CCMR2 = TIM_CCMR2_OC3CE | (6<<4);
      TIM22->CCER  |= TIM_CCER_CC3E;
      TIM22->CCR3   = val;
    } else if (port == (PORTB | PIN11)) {
      P.cfg(GPIO_ALT | GPIO_AF2);
      TIM22->CCMR2 = TIM_CCMR2_OC4CE | (6<<12);
      TIM22->CCER  |= TIM_CCER_CC4E;
      TIM22->CCR4   = val;
    } else {
      assert(false); // unknown pin
    }
  }

  int get (){
    return TIM22->CCR1;
  }
  void set (int val){
    if (val<0) val=0;
    if (val>TIM22->ARR) val=TIM22->ARR;
    TIM22->CCR1 = val;
  }

  void on (gpio_pin& P, int val){
    int port = P.getport();
    if (port == (PORTA | PIN0)) {
      TIM22->CCMR1 = TIM_CCMR1_OC1CE | (6<<4);
      TIM22->CCER  |= TIM_CCER_CC1E;
      TIM22->CCR1   = val;
    } else if (port == (PORTA | PIN1)) {
      TIM22->CCMR1 = TIM_CCMR1_OC2CE | (6<<12);
      TIM22->CCER  |= TIM_CCER_CC2E;
      TIM22->CCR2   = val;
    } else if (port == (PORTA | PIN2)) {
      TIM22->CCMR2 = TIM_CCMR2_OC3CE | (6<<4);
      TIM22->CCER  |= TIM_CCER_CC3E;
      TIM22->CCR3   = val;
    } else if (port == (PORTA | PIN3)) {
      TIM22->CCMR1 = TIM_CCMR2_OC4CE | (6<<12);
      TIM22->CCER  |= TIM_CCER_CC4E;
      TIM22->CCR4   = val;
    } else if (port == (PORTA | PIN5)) {
      TIM22->CCMR1 = TIM_CCMR1_OC1CE | (6<<4);
      TIM22->CCER  |= TIM_CCER_CC1E;
      TIM22->CCR1   = val;
    } else if (port == (PORTA | PIN15)) {
      TIM22->CCMR1 = TIM_CCMR1_OC1CE | (6<<4);
      TIM22->CCER  |= TIM_CCER_CC1E;
      TIM22->CCR1   = val;
    } else if (port == (PORTB | PIN3)) {
      TIM22->CCMR1 = TIM_CCMR1_OC2CE | (6<<12);
      TIM22->CCER  |= TIM_CCER_CC2E;
      TIM22->CCR2   = val;
    } else if (port == (PORTB | PIN10)) {
      TIM22->CCMR2 = TIM_CCMR2_OC3CE | (6<<4);
      TIM22->CCER  |= TIM_CCER_CC3E;
      TIM22->CCR3   = val;
    } else if (port == (PORTB | PIN11)) {
      TIM22->CCMR1 = TIM_CCMR2_OC4CE | (6<<12);
      TIM22->CCER  |= TIM_CCER_CC4E;
      TIM22->CCR4   = val;
    } else {
      assert(false); // unknown pin
    }
  }
  void off (gpio_pin& P){
    int port = P.getport();
    if (port == (PORTA | PIN0)) {
      TIM22->CCER  &= ~TIM_CCER_CC1E;
    } else if (port == (PORTA | PIN1)) {
      TIM22->CCER  &= ~TIM_CCER_CC2E;
    } else if (port == (PORTA | PIN2)) {
      TIM22->CCER  &= ~TIM_CCER_CC3E;
    } else if (port == (PORTA | PIN3)) {
      TIM22->CCER  &= ~TIM_CCER_CC4E;
    } else if (port == (PORTA | PIN5)) {
      TIM22->CCER  &= ~TIM_CCER_CC1E;
    } else if (port == (PORTA | PIN15)) {
      TIM22->CCER  &= ~TIM_CCER_CC1E;
    } else if (port == (PORTB | PIN3)) {
      TIM22->CCER  &= ~TIM_CCER_CC2E;
    } else if (port == (PORTB | PIN10)) {
      TIM22->CCER  &= ~TIM_CCER_CC3E;
    } else if (port == (PORTB | PIN11)) {
      TIM22->CCER  &= ~TIM_CCER_CC4E;
    } else {
      assert(false); // unknown pin
    }
  }
  void off (){
//    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
  }
};



class T2Encoder
  {
  gpio_pin A;
  gpio_pin B;
//  unsigned short lastenc;
//  int lastpos;
  public:
  void init();

  unsigned short get()
    {
    return TIM2->CNT;
    }
/*  int pos()
    {
    unsigned short enc=TIM2->CNT;
    lastpos+=(signed short)(lastenc-enc);
    lastenc=enc;
    return lastpos;
    }*/
  };
/*class T3Encoder
  {
  gpio_pin A;
  gpio_pin B;
//  unsigned short lastenc;
//  int lastpos;
  public:
  void init();
  unsigned short get()
    {
    return TIM3->CNT;
    }*/
/*  int pos()
    {
    unsigned short enc=TIM3->CNT;
    lastpos+=(signed short)(lastenc-enc);
    lastenc=enc;
    return lastpos;
    }*/
//  };

typedef void (*IRQHandler)(void * param);

int SetIRQHandler(IRQn_Type irq, IRQHandler handler, void* param);
int ResetIRQHandler(IRQn_Type irq, IRQHandler handler);
IRQHandler GetIRQHandler(IRQn_Type irq);
void* GetIRQHandlerParam(IRQn_Type irq);


#endif /* __TIM_H__ */