
//#include "include/core.h"
#include <tim.h>

#define __CHECK_DEVICE_DEFINES
#if TargetCPU == STM32L051
//#include "stm32l0xx.h"
//#include "ST/iostm32l051xx.h"
#include "core_cm0plus.h"
#include "stm32l051xx.h"
#else
#include "stm32f10x.h"
#include "stm32f10x_extend.h"
#endif


/* timer6 for 1 ms ticks */
unsigned long long ticks=0;
uint32_t volatile ms = 0;



#include "gpio-alloc.h"

/*#define TIM_CR1_ARPE (1<<7)
#define TIM_CR1_OPM (1<<3)
#define TIM_CR1_CEN (1<<0)
#define TIM_DIER_UIE (1<<0)
#define TIM_EGR_UG (1<<0)
#define TIM_SR_UIF (1<<0)*/

void tim_init(void)
{
  RCC->APB1ENR &= ~RCC_APB1ENR_TIM6EN;
  ticks = 0;
  RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;

  //load psc
  TIM6->CR1  = 0;
  TIM6->PSC  = (sys_get_clk()/1000000) - 1;// 15;//23;
  TIM6->ARR  = 1;
  TIM6->CNT  = 0;
  TIM6->SR   = 0;
  TIM6->DIER = TIM_DIER_UIE;
  TIM6->CR1  = TIM_CR1_OPM | TIM_CR1_CEN;

  //wait ev
  while (!(TIM6->SR & TIM_SR_UIF));
  TIM6->SR = 0;

  //start
  TIM6->ARR  = 65535;
  TIM6->CNT  = 0;
  TIM6->SR   = 0;
  TIM6->CR1  = TIM_CR1_CEN;

  NVIC_ClearPendingIRQ(TIM6_IRQn);
  NVIC_EnableIRQ(TIM6_IRQn);
}

void tim2_init(void) {
    RCC->APB1ENR &= ~RCC_APB1ENR_TIM2EN;
    TIM2->CR1  = TIM_CR1_CMS_1;
    TIM2->CR2  = 0;
    TIM2->PSC  = 200*((sys_get_clk()/1000000)) - 1;// 15;//23;
    TIM2->ARR  = 1;
    TIM2->CNT  = 0;
    TIM2->SR   = 0;
    TIM2->DIER = 0;
    TIM2->CR1  = TIM_CR1_CMS_1 | TIM_CR1_CEN;
    TIM2->CCMR1  = 0;
#define TIM_CCMR2_OC3CE                     ((uint32_t)0x00000080)            /*!<Output Compare 3 Clear Enable */
//    TIM2->CCMR2  = TIM_CCMR2_OC3CE | (6<<4);
    TIM2->CCMR2  = TIM_CCMR2_OC4CE | (6<<12);
    TIM2->CCER  = TIM_CCER_CC4P;

    //start
    TIM2->ARR  = 32768;
    TIM2->CNT  = 0;
    TIM2->SR   = 0;
    TIM2->CCR4   = 100;
//    TIM2->CR1  = TIM_CR1_CEN;
}
void tim2_stop(void) {
  TIM2->CR1  = 0;
}

extern "C" { void TIM6_IRQHandler(void); }

unsigned int get_ms()
  {
  __disable_interrupt();
  unsigned long long now=ticks;
  unsigned int t1=TIM6->CNT;
  unsigned int ov1=TIM6->SR;
  unsigned int t2=TIM6->CNT;
  unsigned int ov2=TIM6->SR;
  if (t1==t2) now+=t1+ov1*65536;
  else now+=t2+ov2*65536;
  __enable_interrupt();
  unsigned int ms = (now)/1000;
  return ms;
  }

unsigned long long getlong_ms()
  {
  __disable_interrupt();
  unsigned long long now=ticks;
  unsigned int t1=TIM6->CNT;
  unsigned int ov1=TIM6->SR;
  unsigned int t2=TIM6->CNT;
  unsigned int ov2=TIM6->SR;
  if (t1==t2) now+=t1+ov1*65536;
  else now+=t2+ov2*65536;
  __enable_interrupt();
  return (now)/1000;
  }

unsigned int get_us()
  {
  __disable_interrupt();
  unsigned long long now=ticks;
  unsigned int t1=TIM6->CNT;
  unsigned int ov1=TIM6->SR;
  unsigned int t2=TIM6->CNT;
  unsigned int ov2=TIM6->SR;
  if (t1==t2) now+=t1+ov1*65536;
  else now+=t2+ov2*65536;
  __enable_interrupt();
  return (now);
  }

unsigned long long getlong_us()
  {
  __disable_interrupt();
  unsigned long long now=ticks;
  unsigned int t1=TIM6->CNT;
  unsigned int ov1=TIM6->SR;
  unsigned int t2=TIM6->CNT;
  unsigned int ov2=TIM6->SR;
  if (t1==t2) now+=t1+ov1*65536;
  else now+=t2+ov2*65536;
  __enable_interrupt();
  return (now);
  }


void TIM6_IRQHandler(void)
{
  ticks+=65536;
  ms++;
/*	static uint16_t cnt = 0;

	if (++cnt == 1000)
	{
		cnt = 0;
		sec++;
//		GPIOC->ODR ^= GPIOC_LEDB;
	}*/

  TIM6->SR = 0;

  //NVIC_ClearPendingIRQ(TIM6_IRQn);
}
