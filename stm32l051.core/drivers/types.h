
#ifndef __TYPES_H__
#define __TYPES_H__

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

typedef unsigned int uint;
typedef unsigned long ulong;

typedef int32_t ssize_t;

#endif /* __TYPES_H__ */
