
#ifndef __SYS_H__
#define __SYS_H__

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

static void const * const uid = reinterpret_cast<uint32_t const *>(0x1ff80050);
static size_t const  uid_size = 12;

extern "C" { void __sys_pre_init(void); void __sys_init(void); }

extern __noreturn void reboot(void);
extern uint32_t sys_get_clk(void);

class power
{

public:
	/* init rtc via lsi to use wakeup event */
	static void init();
	static bool lse_alive(void);

	/* enter to standby mode */
	static void stop_for_X_ms(uint32_t ms);

	/* backup registers rw */
	static void store(size_t idx, uint16_t value);
	static int  load(size_t idx);


	static bool init_lse(void);
	static void init_lsi(void);

	static void unlock_rtc(void);
	static void init_rtc_minimally();
	static void reconf_rtc(uint16_t wakeup_cnt);
	static void lock_rtc(void);

	static uint32_t volatile *get_bkp(size_t idx);
};

#endif /* __SYS_H__ */
