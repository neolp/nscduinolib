
#ifndef __EEPROM_H__
#define __EEPROM_H__

#include <stddef.h>
#include <stdint.h>

namespace eeprom {

extern void lock(void);
extern void unlock(void);

/*
 * - why? i can 'cast' eeprom pointer to my structure directly?..
 * - because if your structure is packed then unaligned reading / writing can occur!
 *   modify your stucture, recalculate checksum and do io like atomic operation
 * - ok
 */
extern void write(void const *buf, size_t size, uintptr_t start_addr);
extern void read(void *buf, size_t size, uintptr_t start_addr);

extern void erase(uintptr_t start_addr);

} /* namespace eeprom */

#endif /* __EEPROM_H__ */
