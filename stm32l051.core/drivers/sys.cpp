
#include <assert.h>
#include "stm32l051xx_core.h"
#include "sys.h"
#include "poll.h"

extern "C" { void __sys_pre_init(void); void __sys_init(void); }

extern "C" { void RTC_IRQHandler(void); }

__noreturn void reboot(void)
{
	SCB->AIRCR = SCB_AIRCR_VECTKEY_Msk & (0x5fa << SCB_AIRCR_VECTKEY_Pos)
			| SCB_AIRCR_SYSRESETREQ_Msk;
	for (;;)
		/* impossible case hardcoded impl */
		(reinterpret_cast<void (*)(void)>(4))();
}

extern void abort(void)
{
#ifndef NDEBUG
	__ASM volatile ("bkpt #11");
#else
	reboot();
#endif
}

extern void HardFault_Handler(void)
{
	abort();
}

void vref_off(void) {
  SYSCFG->CFGR3 = 0;
}

void vref_on(void) {
	bool timeout;

	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;

	// perform two dummy read operaions
	uint32_t volatile dummy;
	dummy = PWR->CR;
	dummy = SYSCFG->CFGR3;

	// switch on vrefint & temperature buffers
	SYSCFG->CFGR3 = SYSCFG_CFGR3_EN_VREFINT | SYSCFG_CFGR3_ENBUF_VREFINT_ADC | SYSCFG_CFGR3_ENBUF_SENSOR_ADC;


	// poll until vrefint become ready (power register)
	poll_while(!(PWR->CSR & PWR_CSR_VREFINTRDYF), 0x100000, timeout);
	if (timeout)
		assert(false);
	poll_while(!(SYSCFG->CFGR3 & SYSCFG_CFGR3_VREFINT_RDYF), 0x100000, timeout);
	if (timeout)
		assert(false);
}

/*
 * NOTE: this function is called before __iar_program_start()!
 * Clocks: MSI (2.1 MHz, default), HSI16 (16 MHz, optional div4).
 * Usage: switch on HSI16 and multiply it by 2 via PLL.
 */
void __sys_pre_init(void)
{
	bool timeout;
	uint32_t volatile dummy;

	/* ---- config power ------------------------------------------------ */
	/* enable power peripheral and perform two dummy read operaions to
	   guarantee normal periphery work (see errata) */
	RCC->APB1ENR = RCC_APB1ENR_PWREN;
	dummy = PWR->CR;
	dummy = PWR->CR;

	/* set power to 1.8v range (allow to use pll 32 MHz clock) */
  while(PWR->CSR & PWR_CSR_VOSF);
	PWR->CR = PWR_CR_VOS & (PWR_CR_VOS_1V8 << 11);
  while(PWR->CSR & PWR_CSR_VOSF);

	/* wait until voltage regulator will change */
	poll_while(PWR->CSR & PWR_CSR_VOSF, 0x100000, timeout);
	if (timeout)
		/* strange behaviour: regulator not works properly */
		assert(false);

	/* ---- config clocks ----------------------------------------------- */
	/* switch on HSI (MSI must be enabled) and switch off PLL */
	RCC->CR = RCC_CR_HSION | RCC_CR_MSION;
/*	poll_while(RCC->CR & RCC_CR_PLLRDY, 0x1000000, timeout);
	if (timeout)
		/ strange behaviour: PLL didn't stopped!
		assert(false);
  */

	/* check default settings */
	uint32_t const sw = RCC->CFGR & RCC_CFGR_SW;
	uint32_t const swst = RCC->CFGR & RCC_CFGR_SWS;
  if (sw != RCC_CFGR_SW_MSI || swst != RCC_CFGR_SWS_MSI)
		// strange behaviour: MSI must be default clock source
  	assert(false);

	RCC->CFGR = RCC_CFGR_SW_MSI | RCC_CFGR_SWS_MSI; // | RCC_CFGR_PLLSRC_HSI; /* defaults 0 */
	//		| RCC_CFGR_PLLMUL3 | RCC_CFGR_PLLDIV4; //  | RCC_CFGR_PLLDIV2

	// wait until the clocks will become stable
	poll_while(!(RCC->CR & RCC_CR_HSIRDY), 0x1000000, timeout);
	if (timeout)
		// strange behaviour: HSI didn't become stable!
		assert(false);

//	/* okay, switch pll and wait until it will become stable
//	RCC->CR = RCC_CR_HSION | RCC_CR_MSION;// | RCC_CR_PLLON;
//	poll_while(!(RCC->CR & RCC_CR_PLLRDY), 0x1000000, timeout);
//	if (timeout)
		// strange behaviour: PLL didn't become stable!
//		assert(false);

	// switch mcu clock to PLL
//	RCC->CFGR = (RCC->CFGR & ~(RCC_CFGR_SW)) | RCC_CFGR_SW_PLL;
//	poll_while((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL, 0x1000000, timeout);
//	if (timeout)
//		/* strange behaviour: system clock didn't change */
//		assert(false);

  // switch mcu clock to HSI 16MHz
  RCC->CFGR = (RCC->CFGR & ~(RCC_CFGR_SW)) | RCC_CFGR_SW_HSI;
	poll_while((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI, 0x1000000, timeout);
	if (timeout)
		// strange behaviour: system clock didn't change
		assert(false);
  RCC->ICSCR = (RCC->ICSCR & ~RCC_ICSCR_MSIRANGE) | RCC_ICSCR_MSIRANGE_0;
}

uint32_t sys_get_clk(void)
{
	return 16000000;
}

void __sys_init(void)
{




	/* ---- clocks ------------------------------------------------------ */
	/* switch on gpio */
	RCC->IOPENR = RCC_IOPENR_GPIOAEN | RCC_IOPENR_GPIOBEN | RCC_IOPENR_GPIOCEN
			| RCC_IOPENR_GPIODEN | RCC_IOPENR_GPIOHEN;

	/* switch on AHB clocks */
	RCC->AHBENR = RCC_AHBENR_CRCEN | RCC_AHBENR_MIFEN | RCC_AHBENR_DMA1EN;

	/* switch on APB clocks */
	RCC->APB2ENR = RCC_APB2ENR_DBGMCUEN | RCC_APB2ENR_USART1EN
			| RCC_APB2ENR_SPI1EN | RCC_APB2ENR_ADC1EN
			| RCC_APB2ENR_SYSCFGEN;
	RCC->APB1ENR = RCC_APB1ENR_PWREN;

  vref_on();

  /* (1) Enable the peripheral clock of GPIOA */
  /* (2) Select input mode (00) on GPIOA pin 0 */
  /* (3) Select Port A for pin 0 extended interrupt by writing 0000 in EXTI0 (reset value) */
  /* (4) Configure the corresponding mask bit in the EXTI_IMR register */
  /* (5) Configure the Trigger Selection bits of the Interrupt line on rising edge */
  /* (6) Configure the Trigger Selection bits of the Interrupt line on falling edge */
  GPIOA->MODER = (GPIOA->MODER & ~(GPIO_MODER_MODE0)); /* (2) */
  //SYSCFG->EXTICR[0] &= (uint16_t)~SYSCFG_EXTICR1_EXTI0_PA; /* (3) */
  EXTI->IMR |= 0x0001; /* (4) */
  EXTI->RTSR |= 0x0001; /* (5) */
  //EXTI->FTSR |= 0x0001; /* (6) */
  /* Configure NVIC for Extended Interrupt */
  /* (7) Enable Interrupt on EXTI0_1 */
  /* (8) Set priority for EXTI0_1 */
  NVIC_EnableIRQ(EXTI0_1_IRQn); /* (7) */
  NVIC_SetPriority(EXTI0_1_IRQn,0); /* (8) */

  EXTI->IMR |= 1<<20;
  EXTI->RTSR |= 1<<20;
}

/* ---- low power mode logic ------------------------------------------------ */
#pragma optimize = none
void power::init()
{
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;
	PWR->CSR     |= PWR_CSR_EWUP1;
	PWR->CR      |= PWR_CR_CSBF | PWR_CR_CWUF;

//#ifdef USE_LSE
	unlock_rtc();
	bool ok = init_lse();
	if (!ok)
	  init_lsi();
	lock_rtc();
//#else
//	init_lsi();
//#endif

	unlock_rtc();
	init_rtc_minimally();
	lock_rtc();
}


void power::stop_for_X_ms(uint32_t ms)
{
	unlock_rtc();
	reconf_rtc(ms*2048/1000);
	RTC->ISR &= ~RTC_ISR_WUTF;
	lock_rtc();

  int tmpreg = PWR->CR;
  tmpreg&= ~(PWR_CR_PDDS | PWR_CR_LPSDSR);
  tmpreg |= PWR_CR_LPSDSR;
  tmpreg |= PWR_CR_ULP;
  tmpreg |= PWR_CR_CWUF;
//  tmpreg |= PWR_CR_VOS_0 | PWR_CR_VOS_1; //11: 1.2 V (range 3)
//  tmpreg |= PWR_CR_FWU; // fast wake up
  PWR->CR = tmpreg;

  //PWR->CSR &= ~ PWR_CSR_WUF;
  while( (PWR->CSR & PWR_CSR_WUF) == PWR_CSR_WUF );
//  RCC->CFGR |= RCC_CFGR_STOPWUCK ; // 16M
//  RCC->CFGR &=~RCC_CFGR_STOPWUCK ; // 64K
/*  uint32_t RCC_IOPENR_ = RCC->RCC_IOPENR;
  RCC->RCC_IOPENR = 0;
  uint32_t RCC_AHBENR_ = RCC->RCC_AHBENR;
  RCC->RCC_AHBENR = 0 ;
  uint32_t RCC_APB2ENR_ = RCC->RCC_APB2ENR;
  RCC->RCC_APB2ENR = 0;
  uint32_t RCC_APB1ENR_ =  RCC->RCC_APB1ENR;
  RCC->RCC_APB1ENR = 0;
  uint32_t RCC_IOPSMENR_ = RCC->RCC_IOPSMENR;*/
  RCC->IOPSMENR = 0;
  RCC->AHBSMENR = 0;
  RCC->APB2SMENR = 0;

  //////////////////////

// Take care of PA4, set to Analog input
// PA4 is set as input by default, this cause current leakage(~4uA)
//GPIOA->MODER |= (0x03 << 4*2);

// Disable GPIO clock for stop mode
//RCC->IOPENR &= (uint32_t)~(RCC_IOPENR_GPIOAEN | RCC_IOPENR_GPIOBEN | RCC_IOPENR_GPIOCEN | RCC_IOPENR_GPIODEN | RCC_IOPENR_GPIOHEN);

  FLASH->ACR |= FLASH_ACR_RUN_PD;
  FLASH->ACR |= FLASH_ACR_SLEEP_PD;
  FLASH->ACR |= FLASH_ACR_LATENCY; // set 1 WS for flash


	//PWR->CR  &= ~(PWR_CR_PDDS | PWR_CR_LPSDSR);

	SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;

  RCC->APB1ENR &= ~RCC_APB1ENR_PWREN;

  vref_off();

  bool timeout;
  // switch mcu clock to MSI 64KHz
  RCC->CFGR = (RCC->CFGR & ~(RCC_CFGR_SW)) | RCC_CFGR_SW_MSI;
	poll_while((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_MSI, 0x1000000, timeout);
	if (timeout)
		// strange behaviour: system clock didn't change
		assert(false);



  #define RCC_CR_HSI16OUTEN                      ((uint32_t)0x00000020)
  RCC->CR &= ~(RCC_CR_HSION | RCC_CR_HSIKERON | RCC_CR_HSIDIVEN | RCC_CR_HSI16OUTEN |
               RCC_CR_HSEON | RCC_CR_HSEBYP | RCC_CR_PLLON);

  while(PWR->CSR & PWR_CSR_VOSF);
  PWR->CR |= PWR_CR_VOS_0 | PWR_CR_VOS_1; //11: 1.2 V (range 3)
  while(PWR->CSR & PWR_CSR_VOSF);

	__WFI();

  RCC->APB1ENR |= RCC_APB1ENR_PWREN;
  while(PWR->CSR & PWR_CSR_VOSF);
//  PWR->CR = (PWR->CR & ~(PWR_CR_VOS)) | PWR_CR_VOS_0; //01: 1.8 V (range 1)
  PWR->CR = (PWR->CR & ~(PWR_CR_VOS)) | PWR_CR_VOS_1; //10: 1.5 V (range 2)
  while(PWR->CSR & PWR_CSR_VOSF);

  // switch HSI clock ON
  RCC->CR |= RCC_CR_HSION;
  // switch mcu clock to HSI 16MHz
  RCC->CFGR = (RCC->CFGR & ~(RCC_CFGR_SW)) | RCC_CFGR_SW_HSI;
	poll_while((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI, 0x1000000, timeout);
	if (timeout)
		// strange behaviour: system clock didn't change
		assert(false);

  FLASH->ACR &= ~FLASH_ACR_LATENCY; //set 0 ws for flash

  //__sys_pre_init();

  vref_on();

}

void power::init_lsi(void)
{
	if ((RCC->CSR & RCC_CSR_RTCSEL) != RCC_CSR_RTCSEL_LSI)
	{
		RCC->CSR |= RCC_CSR_RTCRST;
		RCC->CSR &= ~(RCC_CSR_RTCEN | RCC_CSR_RTCRST);
	}

	RCC->CSR |= RCC_CSR_LSION;
	poll_assert(&RCC->CSR, RCC_CSR_LSIRDY, RCC_CSR_LSIRDY, 0x400000);
}

bool power::init_lse(void)
{
	uint32_t const csr = RCC->CSR;
	if (!(csr & RCC_CSR_LSEON) || !(csr & RCC_CSR_LSERDY) || (csr & RCC_CSR_RTCSEL) != RCC_CSR_RTCSEL_LSE)
	{
		RCC->CSR |= RCC_CSR_RTCRST;
		RCC->CSR &= ~(RCC_CSR_RTCEN | RCC_CSR_RTCRST);
	}

	RCC->CSR  = (RCC->CSR & ~(RCC_CSR_LSEDRV | RCC_CSR_LSEBYP)) | (RCC_CSR_LSEDRV & (3 << 11)); /* highest */
	RCC->CSR |= RCC_CSR_LSEON;
	return poll(&RCC->CSR, RCC_CSR_LSERDY, RCC_CSR_LSERDY, 0x400000);
}

bool power::lse_alive(void)
{
	return RCC->CSR & RCC_CSR_LSERDY;
}

void power::unlock_rtc(void)
{
	PWR->CR |= PWR_CR_DBP;
	RTC->WPR = 0xca;
	RTC->WPR = 0x53;
}

uint32_t volatile *power::get_bkp(size_t idx)
{
	switch (idx)
	{
	case 0:
		return &RTC->BKP0R;
	case 1:
		return &RTC->BKP1R;
	case 2:
		return &RTC->BKP2R;
	case 3:
		return &RTC->BKP3R;
	default:
		return NULL;
	}
}

void power::store(size_t idx, uint16_t value)
{
	uint32_t volatile *reg = get_bkp(idx);
	if (reg != NULL)
	{
    unlock_rtc();
		*reg = value | (~value << 16);
		lock_rtc();
	}
}

int power::load(size_t idx)
{
	uint32_t volatile *reg = get_bkp(idx);
	if (reg == NULL)
		return -1;

	uint32_t value = *reg;
	uint16_t top = ~value >> 16, bottom = value;
	if (top != bottom)
		return -1;

	return bottom;
}

void power::init_rtc_minimally()
{
	/* lse = 1, lsi = 2 */
	bool const lse_okay  = RCC->CSR & RCC_CSR_LSERDY;
	uint8_t const rtcsel = lse_okay ? RCC_CSR_RTCSEL_BASE_LSE : RCC_CSR_RTCSEL_BASE_LSI;

	RCC->CSR      = (RCC->CSR & ~RCC_CSR_RTCSEL) | ((rtcsel << 16) & RCC_CSR_RTCSEL) | RCC_CSR_RTCEN;
	RTC->ISR      = RTC_ISR_INIT;
	RTC->CR       = 0;

	poll_assert(&RTC->ISR, RTC_ISR_INITF | RTC_ISR_WUTWF,
	                       RTC_ISR_INITF | RTC_ISR_WUTWF, 0xffffff);

	RTC->CR       = RTC_CR_WUCKSEL & (0 << 0);
	RTC->TR       = 0;
	RTC->DR       = 0;
	RTC->SSR      = 0;
	RTC->SHIFTR   = 0;
	RTC->CALR     = 0;
	RTC->TAMPCR   = 0;
	RTC->OR       = 0;

  if (lse_okay) {
    RTC->PRER     = 0x007F00FF;
  } else {
    RTC->PRER     = 0x007F00FF;
  }
	RTC->WUTR     = 65535;
	RTC->ALRMAR   = 0;
	RTC->ALRMBR   = 0;
	RTC->ALRMASSR = 0;
	RTC->ALRMBSSR = 0;

	RTC->ISR      = RTC_ISR_WUTF;
	//RTC->CR      |= RTC_CR_WUTE | RTC_CR_WUTIE;
  NVIC_EnableIRQ(RTC_IRQn);
}



void RTC_IRQHandler(void) {
	power::unlock_rtc();
	RTC->ISR &= ~RTC_ISR_WUTF;
	power::lock_rtc();
  EXTI->PR=1<<20;
volatile int xxx=0;
NVIC_ClearPendingIRQ(RTC_IRQn);
}

extern "C" { void EXTI0_1_IRQHandler(void); }

void EXTI0_1_IRQHandler(void) {
  int pending=EXTI->PR;
  if (pending&1) {
    EXTI->PR = 1;
    }


volatile int xxx=0;
NVIC_ClearPendingIRQ(EXTI0_1_IRQn);
}


void power::reconf_rtc(uint16_t wakeup_cnt)
{
	RTC->CR      &= ~RTC_CR_WUTE;
	poll_assert(&RTC->ISR, RTC_ISR_WUTWF, RTC_ISR_WUTWF, 0xffffff);

	RTC->WUTR     = wakeup_cnt ? wakeup_cnt - 1 : 0;
	RTC->CR      |= RTC_CR_WUTE | RTC_CR_WUTIE;
}

void power::lock_rtc(void)
{
	RTC->WPR = 0x00;
	PWR->CR &= ~PWR_CR_DBP;
}
