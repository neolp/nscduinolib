
#include <assert.h>
#include "adc.h"
#include "stm32l051xx_core.h"
#include "poll.h"

// NOTE: channels MUST to be sorted! synchronize it with @enum adc_id
static uint8_t const adc_ch[__ADC_NUM] =
{
	[ADC_T2]      = 1,
	[ADC_T1]      = 2,
	[ADC_EXT]    = 4,
	[ADC_T4]      = 8,
	[ADC_T3]      = 9,
	[ADC_VREFINT] = 17,
	[ADC_TEMP]    = 18,
};
uint16_t volatile adc_val[__ADC_NUM];

// Real sampling time Tconv = (12.5 + smpr) * adc clock cycles;
// Adc clock reference = apb_clk / 2;
// NOTE: temp sensor sampling time must to be >= 2.2 us

static uint8_t const adc_smpr = ADC_SMPR_239S5;

void __adc_init(void)
{
	bool timeout;

	// debug
	for (ssize_t i = 0; i < __ADC_NUM - 1; i++)
	{
		if (adc_ch[i] >= adc_ch[i + 1])
			assert(false);
	}

	// reset adc, calibrate
	ADC1->IER = 0;
	ADC1->CR = 0;
	ADC1->CR = ADC_CR_ADCAL;

	// wait while calibration worked
	poll_while(ADC1->CR & ADC_CR_ADCAL, 0x100000, timeout);
	if (timeout)
		assert(false);
	poll_while(!(ADC1->ISR & ADC_ISR_EOCAL), 0x100000, timeout);
	if (timeout)
		assert(false);
	assert(ADC1->CR & ADC_CR_ADVREGEN);

	// configure mode
	assert(!(ADC1->CR & (ADC_CR_ADSTART | ADC_CR_ADEN)));
	ADC->CCR = ADC_CCR_TSEN | ADC_CCR_VREFEN;
	ADC1->CFGR1 = /* ADC_CFGR1_CONT | ADC_CFGR1_OVRMOD | */ ADC_CFGR1_DMACFG | ADC_CFGR1_DMAEN;
	ADC1->CFGR2 = ADC_CFGR2_CKMODE & (ADC_CKMODE_PCLK2 << 14);
	ADC1->SMPR  = adc_smpr;

	// configure channels
	uint32_t chsel = 0;
	for (size_t i = 0; i < __ADC_NUM; i++)
		chsel |= 1 << adc_ch[i];
	ADC1->CHSELR = chsel;

	// switch on
	ADC1->CR = ADC_CR_ADVREGEN | ADC_CR_ADEN;

	// wait until adc become ready
	poll_while(ADC1->ISR & ADC_ISR_ADRDY, 0x100000, timeout);
	if (timeout)
		assert(false);

	// start
//	ADC1->CR = ADC_CR_ADEN | ADC_CR_ADVREGEN | ADC_CR_ADSTART;
	ADC1->CR = ADC_CR_ADEN | ADC_CR_ADVREGEN;
}
















// A.8.1 Calibration code example
// (1) Ensure that ADEN = 0
// (2) Clear ADEN
// (3) Set ADCAL=1
// (4) Wait until EOCAL=1
// (5) Clear EOCAL
void adc_calibration() {
  if ((ADC1->CR & ADC_CR_ADEN) != 0) // (1)
  {
  ADC1->CR &= (uint32_t)(~ADC_CR_ADEN); // (2)
  }
  ADC1->CR |= ADC_CR_ADCAL; // (3)
  while ((ADC1->ISR & ADC_ISR_EOCAL) == 0) // (4)
  {
  // For robust implementation, add here time-out management
  }
  ADC1->ISR |= ADC_ISR_EOCAL; // (5)
}

// A.8.2 ADC enable sequence code example
// (1) Clear the ADRDY bit
// (2) Enable the ADC
// (3) Wait until ADC ready
void adc_enable() {
ADC1->ISR |= ADC_ISR_ADRDY; // (1)
ADC1->CR |= ADC_CR_ADEN; // (2)
if ((ADC1->CFGR1 & ADC_CFGR1_AUTOFF) == 0)
{
while ((ADC1->ISR & ADC_ISR_ADRDY) == 0) // (3)
{
// For robust implementation, add here time-out management
}
}
}
// A.8.3 ADC disable sequence code example
// (1) Ensure that no conversion on going
// (2) Stop any ongoing conversion
// (3) Wait until ADSTP is reset by hardware i.e. conversion is stopped
// (4) Disable the ADC
// (5) Wait until the ADC is fully disabled
void adc_disable() {
if ((ADC1->CR & ADC_CR_ADSTART) != 0) // (1)
{
ADC1->CR |= ADC_CR_ADSTP; // (2)
}
while ((ADC1->CR & ADC_CR_ADSTP) != 0) // (3)
{
// For robust implementation, add here time-out management
}
ADC1->CR |= ADC_CR_ADDIS; // (4)
while ((ADC1->CR & ADC_CR_ADEN) != 0) // (5)
{
// For robust implementation, add here time-out management
}
}

//A.8.4 ADC clock selection code example
// (1) Select PCLK by writing 11 in CKMODE
//ADC1->CFGR2 |= ADC_CFGR2_CKMODE; // (1)

// A.8.5 Single conversion sequence code example - Software trigger
// (1) Select HSI16 by writing 00 in CKMODE (reset value)
// (2) Select the auto off mode
// (3) Select CHSEL17 for VRefInt
// (4) Select a sampling mode of 111 i.e. 239.5 ADC clk to be greater than 17.1us
// (5) Wake-up the VREFINT (only for Temp sensor and VRefInt)
void adc_single() {
//ADC1->CFGR2 &= ~ADC_CFGR2_CKMODE; // (1)
ADC1->CFGR1 |= ADC_CFGR1_AUTOFF; // (2)
ADC1->CHSELR = ADC_CHSELR_CHSEL17; // (3)
ADC1->SMPR |= ADC_SMPR_SMP_0 | ADC_SMPR_SMP_1 | ADC_SMPR_SMP_2; // (4)
ADC->CCR |= ADC_CCR_VREFEN; // (5)
// Performs the AD conversion
ADC1->CR |= ADC_CR_ADSTART; // start the ADC conversion
while ((ADC1->ISR & ADC_ISR_EOC) == 0) // wait end of conversion
{
// For robust implementation, add here time-out management
}
}

// A.8.6 Continuous conversion sequence code example - Software trigger
// (1) Select HSI16 by writing 00 in CKMODE (reset value)
// (2) Select the continuous mode and scanning direction
// (3) Select CHSEL4, CHSEL9 and CHSEL17
// (4) Select a sampling mode of 111 i.e. 239.5 ADC clk to be greater than 5 us
// (5) Enable interrupts on EOC, EOSEQ and overrrun
// (6) Wake-up the VREFINT (only for Temp sensor and VRefInt)
void adc_continuous() {
//ADC1->CFGR2 &= ~ADC_CFGR2_CKMODE; // (1)
ADC1->CFGR1 |= ADC_CFGR1_WAIT |ADC_CFGR1_CONT | ADC_CFGR1_SCANDIR;// (2)
ADC1->CHSELR = ADC_CHSELR_CHSEL4 | ADC_CHSELR_CHSEL9 \
| ADC_CHSELR_CHSEL17; // (3)
ADC1->SMPR |= ADC_SMPR_SMP_0 | ADC_SMPR_SMP_1 | ADC_SMPR_SMP_2; // (4)
ADC1->IER = ADC_IER_EOCIE | ADC_IER_EOSEQIE | ADC_IER_OVRIE; // (5)
ADC->CCR |= ADC_CCR_VREFEN; // (6)
// Configure NVIC for ADC
// (1) Enable Interrupt on ADC
// (2) Set priority for ADC
NVIC_EnableIRQ(ADC1_COMP_IRQn); // (1)
NVIC_SetPriority(ADC1_COMP_IRQn,0); // (2)
}

//A.8.7 Single conversion sequence code example - Hardware trigger
// (1) Select HSI16 by writing 00 in CKMODE (reset value)
// (2) Select the external trigger on falling edge and external trigger on TIM22_TRGO by selecting TRG4 (EXTSEL = 100)
// (3) Select CHSEL17 for VRefInt
// (4) Select a sampling mode of 111 i.e. 239.5 ADC clk to be greater than 5us
// (5) Wake-up the VREFINT (only for Temp sensor and VRefInt)
void adc_single_hard() {
//ADC1->CFGR2 &= ~ADC_CFGR2_CKMODE; // (1)
ADC1->CFGR1 |= ADC_CFGR1_EXTEN_0 | ADC_CFGR1_EXTSEL_2 ; // (2)
ADC1->CHSELR = ADC_CHSELR_CHSEL17; // (3)
ADC1->SMPR |= ADC_SMPR_SMP_0 | ADC_SMPR_SMP_1 | ADC_SMPR_SMP_2; // (4)
ADC->CCR |= ADC_CCR_VREFEN; // (5)
}


// A.8.8 Continuous conversion sequence code example - Hardware trigger
// (1) Select HSI16 by writing 00 in CKMODE (reset value)
// (2) Select the external trigger on TIM22_TRGO (TRG4 i.e. EXTSEL = 100 and rising edge, the continuous mode and scanning direction
// (3) Select CHSEL4, CHSEL9 and CHSEL17
// (4) Select a sampling mode of 111 i.e. 239.5 ADC clk to be greater than 5us
// (5) Enable interrupts on EOC, EOSEQ and overrrun
// (6) Wake-up the VREFINT (only for Temp sensor and VRefInt)
void adc_continuous_hard() {
//ADC1->CFGR2 &= ~ADC_CFGR2_CKMODE; // (1)
ADC1->CFGR1 |= ADC_CFGR1_EXTEN_0 | ADC_CFGR1_EXTSEL_2 | ADC_CFGR1_CONT \
| ADC_CFGR1_SCANDIR; // (2)
ADC1->CHSELR = ADC_CHSELR_CHSEL4 | ADC_CHSELR_CHSEL9 \
| ADC_CHSELR_CHSEL17; // (3
ADC1->SMPR |= ADC_SMPR_SMP_0 | ADC_SMPR_SMP_1 | ADC_SMPR_SMP_2; // (4)
ADC1->IER = ADC_IER_EOCIE | ADC_IER_EOSEQIE | ADC_IER_OVRIE; // (5)
ADC->CCR |= ADC_CCR_VREFEN; // (6)
// Configure NVIC for ADC
// (1) Enable Interrupt on ADC
// (2) Set priority for ADC
NVIC_EnableIRQ(ADC1_COMP_IRQn); // (1)
NVIC_SetPriority(ADC1_COMP_IRQn,0); // (2)
}

// A.8.9 DMA one shot mode sequence code example
// (1) Enable the peripheral clock on DMA
// (2) Enable DMA transfer on ADC - DMACFG is kept at 0 for one shot mode
// (3) Configure the peripheral data register address
// (4) Configure the memory address
// (5) Configure the number of DMA transfer to be performs on DMA channel 1
// (6) Configure increment, size and interrupts
// (7) Enable DMA Channel 1

void adc_single_dma(void* ADC_array, int NUMBER_OF_ADC_CHANNEL ) {


RCC->AHBENR |= RCC_AHBENR_DMA1EN; // (1)
ADC1->CFGR1 |= ADC_CFGR1_DMAEN; // (2)

DMA1_Channel1->CCR &= ~DMA_CCR_EN; // (7)

DMA1_Channel1->CPAR = (uint32_t) (&(ADC1->DR)); // (3)
DMA1_Channel1->CMAR = (uint32_t)(ADC_array); // (4)
DMA1_Channel1->CNDTR = NUMBER_OF_ADC_CHANNEL; // (5)
DMA1_Channel1->CCR |= DMA_CCR_MINC | DMA_CCR_MSIZE_0 | DMA_CCR_PSIZE_0 | DMA_CCR_TEIE | DMA_CCR_TCIE /*| DMA_CCR_CIRC*/; // (6)
DMA1_Channel1->CCR |= DMA_CCR_EN; // (7)



//    ADC1->CFGR1 |= ADC_CFGR1_AUTOFF; // (2)
//    ADC->CCR |= ADC_CCR_VREFEN; // (5)
    // Performs the AD conversion
    ADC1->CR |= ADC_CR_ADSTART; // start the ADC conversion

    //and wait for comlete
}



//A.8.10 DMA circular mode sequence code example
// (1) Enable the peripheral clock on DMA
// (2) Enable DMA transfer on ADC and circular mode
// (3) Configure the peripheral data register address
// (4) Configure the memory address
// (5) Configure the number of DMA tranfer to be performs on DMA channel 1
// (6) Configure increment, size, interrupts and circular mode
// (7) Enable DMA Channel 1
void adc_circular_dma(void* ADC_array, int NUMBER_OF_ADC_CHANNEL) {
  RCC->AHBENR |= RCC_AHBENR_DMA1EN; // (1)
ADC1->CFGR1 |= ADC_CFGR1_DMAEN | ADC_CFGR1_DMACFG; // (2)
DMA1_Channel1->CPAR = (uint32_t) (&(ADC1->DR)); // (3)
DMA1_Channel1->CMAR = (uint32_t)(ADC_array); // (4)
DMA1_Channel1->CNDTR = NUMBER_OF_ADC_CHANNEL; // (5)
DMA1_Channel1->CCR |= DMA_CCR_MINC | DMA_CCR_MSIZE_0 | DMA_CCR_PSIZE_0 | DMA_CCR_TEIE | DMA_CCR_CIRC; // (6)
DMA1_Channel1->CCR |= DMA_CCR_EN; // (7)
}
// A.8.11 Wait mode sequence code example
// (1) Select PCLK by writing 11 in CKMODE
// (2) Select the continuous mode and the wait mode
// (3) Select CHSEL4, CHSEL9 and CHSEL17
// (4) Select a sampling mode of 111 i.e. 239.5 ADC clk to be greater than 17.1us
// (5) Enable interrupts on overrrun
// (6) Wake-up the VREFINT (only for Temp sensor and VRefInt)
void adc_wait_mode() {

ADC1->CFGR2 |= ADC_CFGR2_CKMODE; // (1)
ADC1->CFGR1 |= ADC_CFGR1_CONT | ADC_CFGR1_WAIT; // (2)
ADC1->CHSELR = ADC_CHSELR_CHSEL4 | ADC_CHSELR_CHSEL9 \
| ADC_CHSELR_CHSEL17; // (3
ADC1->SMPR |= ADC_SMPR_SMP_0 | ADC_SMPR_SMP_1 | ADC_SMPR_SMP_2; // (4)
ADC1->IER = ADC_IER_OVRIE; // (5)
ADC->CCR |= ADC_CCR_VREFEN; // (6)
}
//A.8.12 Auto off and no wait mode sequence code example
// (1) Select HSI16 by writing 00 in CKMODE (reset value)
// (2) Select the external trigger on TIM22_TRGO and falling edge,the continuous mode, scanning direction and auto off
// (3) Select CHSEL4, CHSEL9 and CHSEL17
// (4) Select a sampling mode of 111 i.e. 239.5 ADC clk to be greater than 5us
// (5) Enable interrupts on EOC, EOSEQ and overrrun
// (6) Wake-up the VREFINT (only for Temp sensor and VRefInt)
void adc_auto_off() {
//ADC1->CFGR2 &= ~ADC_CFGR2_CKMODE; // (1)
ADC1->CFGR1 |= ADC_CFGR1_EXTEN_0 | ADC_CFGR1_EXTSEL_2 \
| ADC_CFGR1_SCANDIR | ADC_CFGR1_AUTOFF; // (2)
ADC1->CHSELR = ADC_CHSELR_CHSEL4 | ADC_CHSELR_CHSEL9 \
| ADC_CHSELR_CHSEL17; // (3)
ADC1->SMPR |= ADC_SMPR_SMP_0 | ADC_SMPR_SMP_1 | ADC_SMPR_SMP_2; // (4)
ADC1->IER = ADC_IER_EOCIE | ADC_IER_EOSEQIE | ADC_IER_OVRIE; // (5)
ADC->CCR |= ADC_CCR_VREFEN; // (6)
}

//A.8.13 Auto off and wait mode sequence code example
// (1) Select HSI16 by writing 00 in CKMODE (reset value)
// (2) Select the continuous mode, the wait mode and the Auto off
// (3) Select CHSEL4, CHSEL9 and CHSEL17
// (4) Select a sampling mode of 111 i.e. 239.5 ADC clk to be greater than 5us
// (5) Enable interrupt on overrrun
// (6) Wake-up the VREFINT (only for Temp sensor and VRefInt)
void adc_auto_off_and_wait() {
//ADC1->CFGR2 &= ~ADC_CFGR2_CKMODE; // (1)
ADC1->CFGR1 |= ADC_CFGR1_CONT | ADC_CFGR1_WAIT | ADC_CFGR1_AUTOFF;// (2)
ADC1->CHSELR = ADC_CHSELR_CHSEL4 | ADC_CHSELR_CHSEL9 \
| ADC_CHSELR_CHSEL17; // (3)
ADC1->SMPR |= ADC_SMPR_SMP_0 | ADC_SMPR_SMP_1 | ADC_SMPR_SMP_2; // (4)
ADC1->IER = ADC_IER_OVRIE; // (5)
ADC->CCR |= ADC_CCR_VREFEN; // (6)
}

//A.8.14 Analog watchdog code example
// Define the upper limit 15% above the factory value
// the value is adapted according to the application power supply
// versus the factory calibration power supply
/*
void adc_analog_watchdog() {

uint16_t vrefint_high = (*VREFINT_CAL_ADDR)* VDD_CALIB / VDD_APPLI * 115 / 100;
// Define the lower limit 15% below the factory value
// the value is adapted according to the application power supply
// versus the factory calibration power supply
uint16_t vrefint_low = (*VREFINT_CAL_ADDR) * VDD_CALIB / VDD_APPLI * 85 / 100;
// (1) Select HSI16 by writing 00 in CKMODE (reset value)
// (2) Select the continuous mode and configure the Analog watchdog to monitor only CH17
// (3) Define analog watchdog range : 16b-MSW is the high limit and 16b-LSW is the low limit
// (4) Select CHSEL4, CHSEL9 and CHSEL17
// (5) Select a sampling mode of 111 i.e. 239.5 ADC clk to be greater than 5us
// (6) Enable interrupts on EOC, EOSEQ and Analog Watchdog
// (7) Wake-up the VREFINT (only for VBAT, Temp sensor and VRefInt)
//ADC1->CFGR2 &= ~ADC_CFGR2_CKMODE; // (1)
ADC1->CFGR1 |= ADC_CFGR1_CONT \
| (17<<26) | ADC_CFGR1_AWDEN | ADC_CFGR1_AWDSGL; // (2)
ADC1->TR = (vrefint_high << 16) + vrefint_low; // (3
ADC1->CHSELR = ADC_CHSELR_CHSEL4 | ADC_CHSELR_CHSEL9
| ADC_CHSELR_CHSEL17; // (4)
ADC1->SMPR |= ADC_SMPR_SMP_0 | ADC_SMPR_SMP_1 | ADC_SMPR_SMP_2; // (5)
ADC1->IER = ADC_IER_EOCIE | ADC_IER_EOSEQIE | ADC_IER_AWDIE; // (6)
ADC->CCR |= ADC_CCR_VREFEN; // (7)
}
*/

//A.8.15 Oversampling code example
// (1) Select HSI16 by writing 00 in CKMODE (reset value)
// Enable oversampling with ratio 16 and shifted by 1, without trigger
//ADC1->CFGR2 = (ADC1->CFGR2 & (~ADC_CFGR2_CKMODE))
//| (ADC_CFGR2_OVSE | ADC_CFGR2_OVSR_1 | ADC_CFGR2_OVSR_0
//| ADC_CFGR2_OVSS_0); // (1)


// A.8.16 Temperature configuration code example
// (1) Select HSI16 by writing 00 in CKMODE (reset value)
// (2) Select continuous mode
// (3) Select CHSEL18 for temperature sensor
// (4) Select a sampling mode of 111 i.e. 239.5 ADC clk to be greater than 2.2us
// (5) Wake-up the Temperature sensor (only for Temp sensor and VRefInt)

void adc_t() {

//ADC1->CFGR2 &= ~ADC_CFGR2_CKMODE; // (1)
ADC1->CFGR1 |= ADC_CFGR1_CONT; // (2)
ADC1->CHSELR = ADC_CHSELR_CHSEL18; // (3)
ADC1->SMPR |= ADC_SMPR_SMP; // (4)
ADC->CCR |= ADC_CCR_TSEN; // (5)
}

//A.8.17 Temperature computation code example
// Temperature sensor calibration value address
#define TEMP130_CAL_ADDR ((uint16_t*) ((uint32_t) 0x1FF8007E))
#define TEMP30_CAL_ADDR ((uint16_t*) ((uint32_t) 0x1FF8007A))
#define VDD_CALIB (3000)
//#define VDD_APPLI ((uint16_t) (330))
int32_t ComputeTemperature(uint32_t measure, uint32_t vdda)
{
int32_t temperature;
temperature = ((measure * vdda / VDD_CALIB) - (int32_t) *TEMP30_CAL_ADDR );
temperature = temperature * (int32_t)(130 - 30);
temperature = temperature / (int32_t)(*TEMP130_CAL_ADDR - *TEMP30_CAL_ADDR);
temperature = temperature + 30;
return(temperature);
}