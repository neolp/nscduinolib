
#ifndef __BIT_OPERATIONS_H__
#define __BIT_OPERATIONS_H__

#include "types.h"

/* ---- shifting ------------------------------------------------------------ */
#define lshift(type, v, mask, sh)	((type)(((type)(v) & (type)(mask)) << (sh)))
#define rshift(type, v, mask, sh)	((type)(((type)(v) >> (sh)) & (type)(mask)))

#define lshift_u(v, mask, sh)		lshift(uint, v, mask, sh)
#define rshift_u(v, mask, sh)		rshift(uint, v, mask, sh)

//#define lshift_u8(v, sh)		lshift(v, 0xff, sh)
//#define rshift_u8(v, sh)		rshift(v, 0xff, sh)

/* ---- bits changing ------------------------------------------------------- */
#define change_bit(lval, mask, cond) \
do { \
	if (cond) \
		(lval) |= (mask); \
	else \
		(lval) &= ~(mask); \
} while (0)

#define change_val(lval, clrmask, setmask) \
do { \
	(lval) &= ~(clrmask); \
	(lval) |= (setmask); \
} while (0)

#endif /* __BIT_OPERATIONS_H__ */
