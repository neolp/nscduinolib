
#include "dma.h"
#include "stm32l051xx_core.h"
#include "adc.h"
#include "irq-prior.h"

void __dma_init(void)
{
	/* reset channel */
	DMA1_CSELR->CSELR = 0;
	DMA1_Channel1->CCR = 0;

	/* configure channel */
	DMA1_Channel1->CNDTR = __ADC_NUM;
	DMA1_Channel1->CPAR = (uint32_t)&ADC1->DR;
	DMA1_Channel1->CMAR = (uint32_t)adc_val;
	DMA1_Channel1->CCR = (DMA_CCR_PL & (DMA_PL_VERY_HIGH << 12))
			| (DMA_CCR_MSIZE & (DMA_MSIZE_16 << 10))
			| (DMA_CCR_PSIZE & (DMA_MSIZE_16 << 8))
			| DMA_CCR_MINC | DMA_CCR_CIRC
			| ((DMA_CCR_TEIE | DMA_CCR_TCIE) * 1)
			| DMA_CCR_EN;
	
	/* enable irq handler */
	NVIC_SetPriority(DMA1_Channel1_IRQn, IPR_DMA);
	NVIC_EnableIRQ(DMA1_Channel1_IRQn);
}

uint32_t dma_conv_cnt = 0;

static inline void dma_irq(void)
{
	uint32_t flags = DMA1->ISR;
	DMA1->IFCR = flags;
	
	dma_conv_cnt += 1;
}

extern void DMA1_Channel1_IRQHandler(void)
{
	dma_irq();
}
